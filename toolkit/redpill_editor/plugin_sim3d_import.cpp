#include "plugin_sim3d_import.h"

#include "app_mediator.h"
#include "file_commands.h"
#include "item_commands.h"
#include "transformation_commands.h"

#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QMessageBox>
#include <QDir>

#include "plugin_set_lightmap.h"

#if SIM3D_PLUGINS==1
RPPluginSim3dImport rp_plugin_sim3d_import_instance;
#endif

RPPluginSim3dImportWindow::RPPluginSim3dImportWindow(QWidget *parent):
    QDialog(parent)
{
    QVBoxLayout * layout = new QVBoxLayout(this);
    setLayout(layout);

    layout->addWidget(new QLabel("Import multiple models"));
    _folderPicker = new RPUIFilePicker();
    _folderPicker->setLabel("File Location");
    _folderPicker->setType(RPUIFilePicker::kTypeFolder);
    layout->addWidget(_folderPicker);

    _prefixText = new RPUITextInput();
    _prefixText->setLabel("Name prefix");
    layout->addWidget(_prefixText);

    _suffixText = new RPUITextInput();
    _suffixText->setLabel("Name suffix");
    layout->addWidget(_suffixText);

    _swapUVs = new RPUIBooleanPicker();
    _swapUVs->setLabel("Swap UVs");
    _swapUVs->setValue(true);
    layout->addWidget(_swapUVs);

    _assignLightmaps = new RPUIBooleanPicker();
    _assignLightmaps->setLabel("Assign Lightmaps");
    _assignLightmaps->setValue(true);
    layout->addWidget(_assignLightmaps);

    _centerObjects = new RPUIBooleanPicker();
    _centerObjects->setLabel("Center Objects");
    _centerObjects->setValue(true);
    layout->addWidget(_centerObjects);

    _alignObjects = new RPUIBooleanPicker();
	_alignObjects->setLabel("Align Objects");
    _alignObjects->setValue(true);
    layout->addWidget(_alignObjects);

    _importButton = new QPushButton();
    _importButton->setText("Import");
    connect(_importButton,SIGNAL(clicked()),this,SLOT(import()));

    _cancelButton = new QPushButton();
    _cancelButton->setText("Cancel");
    connect(_cancelButton,SIGNAL(clicked()),this,SLOT(close()));

    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    layout->addWidget(buttonBox);
    buttonBox->addButton(_importButton,QDialogButtonBox::ActionRole);
    buttonBox->addButton(_cancelButton,QDialogButtonBox::ActionRole);
}

void RPPluginSim3dImportWindow::show() {
    QDialog::show();
}

void RPPluginSim3dImportWindow::closeWindow()
{
    close();
}

void RPPluginSim3dImportWindow::import()
{
    QString path = _folderPicker->getPath();
    if (path.isEmpty()) {
        QMessageBox::information(this,"Select a path","You must select a folder containing the 3D Collada models",QMessageBox::Ok);
    }
    else {
        QStringList nameFilter("*.dae");
        QDir directory(path);
        QStringList daeFiles = directory.entryList(nameFilter);
        if (daeFiles.size()==0) {
            QMessageBox::information(this,"Collada files not found","No Collada DAE files found at path " + path,QMessageBox::Ok);
        }
        else {
            QStringList::iterator it;
            QGLContext * ctx = RPAppMediator::get()->mainViewContext();
            DrawableList drwList;
            for (it=daeFiles.begin(); it!=daeFiles.end();++it) {
                QString fullPath = path + QString("/") + *it;
                RPImportCommand * cmd = new RPImportCommand(ctx,fullPath.toStdString());
                RPAppMediator::get()->executeCommand(cmd);
                applyActions(cmd->getDrawable(),cmd->getTransform());
                drwList.push_back(cmd->getDrawable());
            }

            if (_assignLightmaps->getValue()) {
                RPCommand * cmd = new RPPluginSetLightmapCommand(ctx,drwList,"_lm","jpg",path.toStdString());
                RPAppMediator::get()->executeCommand(cmd);
            }

            DrawableList::iterator drwIt;
            for (drwIt=drwList.begin(); drwIt!=drwList.end(); ++drwIt) {
                vwgl::Drawable * drw = (*drwIt).getPtr();
                QString prefix = _prefixText->getValue();
                QString suffix = _suffixText->getValue();
                std::string name = drw->getName();
                name = vwgl::System::get()->removeExtension(name);
                name = prefix.toStdString() + name + suffix.toStdString();
                drw->setName(name);
            }
            RPAppMediator::get()->sceneController()->notifyChange();
            close();
        }
    }
}

void RPPluginSim3dImportWindow::applyActions(vwgl::Drawable * drw,vwgl::TransformNode * trx)
{
    if (drw && trx) {
        RPCommand * cmd = nullptr;
        QGLContext * ctx = RPAppMediator::get()->mainViewContext();
        if (_swapUVs->getValue()) {
            cmd = new RPSwapUVsCommand(ctx,drw->getSolidList(),0,1);
            RPAppMediator::get()->executeCommand(cmd);
        }
		vwgl::Vector3 center;
        if (_centerObjects->getValue()) {
			vwgl::ptr<vwgl::BoundingBox> bbox = new vwgl::BoundingBox(drw);
			center = bbox->center();
			vwgl::Vector3 translate(center.x() * -1.0f, center.y() * -1.0f, center.z() * -1.0f);
            cmd = new RPTransformCommand(ctx, trx, translate,vwgl::Vector3(),vwgl::Vector3(1.0));
            RPAppMediator::get()->executeCommand(cmd);
            freezeTransforms(drw,trx);

            bbox->setVertexData(drw);
            translate.set(0.0f, bbox->min().y() * -1.0f, 0.0f);
            RPCommand * cmd = new RPTransformCommand(ctx, trx,translate,vwgl::Vector3(),vwgl::Vector3(1.0));
            RPAppMediator::get()->executeCommand(cmd);
            freezeTransforms(drw,trx);


        }
        if (_alignObjects->getValue()) {
			center.y(0.0f);
			cmd = new RPTransformCommand(ctx, trx, center, vwgl::Vector3(), vwgl::Vector3(1.0));
			RPAppMediator::get()->executeCommand(cmd);
        }
    }
}

void RPPluginSim3dImportWindow::freezeTransforms(vwgl::Drawable * drw, vwgl::TransformNode * trx)
{
    if (drw && trx) {
        RPCommand * cmd = new RPFreezeTransformsCommand(RPAppMediator::get()->mainViewContext(), trx, drw);
        RPAppMediator::get()->executeCommand(cmd);
    }
}

RPPluginSim3dImport::RPPluginSim3dImport()
    :RPEditorPlugin("com.vitaminew.redpill_editor","Sim3dImportPlugin"), _importWindow(nullptr)
{
    _menuName = "Sim 3D";
    _submenuName = "Import DAE";
}

void RPPluginSim3dImport::executeAction()
{
    if (!_importWindow) {
        _importWindow = new RPPluginSim3dImportWindow();
        _importWindow->setModal(true);
    }
    _importWindow->show();
}
