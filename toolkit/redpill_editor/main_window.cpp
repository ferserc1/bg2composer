#include "main_window.h"
#include "ui_rpmainwindow.h"

#include <QStandardPaths>
#include <QCloseEvent>

#include "app_mediator.h"
#include "file_commands.h"
#include "edit_commands.h"
#include "light_commands.h"
#include "gizmo_tools.h"
#include "drawable_info_view.h"
#include "transform_view.h"
#include "material_widget.h"
#include "shadow_projector_widget.h"
#include "scene_graph_widget.h"
#include "light_widget.h"
#include "camera_widget.h"
#include "joint_view.h"
#include "drawable_commands.h"
#include "scene_graph_commands.h"
#include "item_widget.h"
#include "transformation_commands.h"
#include "library_widget.h"
#include "dock_node_info_view.h"
#include "item_commands.h"

#include "editor_plugin_register.h"

#include <vwgl/drawable_node.hpp>
#include <vwgl/boundingbox.hpp>
#include <vwgl/transform_node.hpp>

void RPExportDrawableThread::run() {
    RPSelectionManager * selectMgr = RPAppMediator::get()->state()->selectionManager();
    DrawableList & drwList = selectMgr->selectedDrawables();
    DrawableList::iterator it;
    int fileIndex = 1;
    for (it=drwList.begin(); it!=drwList.end(); ++it) {
        vwgl::Drawable * drw = (*it).getPtr();
        QString name(drw->getName().c_str());
        if (name.isEmpty()) {
            name = "object_" + QString::number(fileIndex);
            ++fileIndex;
        }

        QFileInfo fileInfo(name);
        if (fileInfo.suffix()!="vwglb") {
            name += ".vwglb";
        }
        QString dstPath = QString::fromUtf8(_path.c_str()) + "/" + fileInfo.baseName();
        QDir().mkpath(dstPath);
        std::string fullName = dstPath.toStdString() + "/" + name.toStdString();
        RPAppMediator::get()->executeCommand(new RPSaveDrawableCommand(fullName, (*it).getPtr()));
    }
    emit saveDone(true);
}


RPMainWindow::RPMainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::RPMainWindow),
	_aboutDialog(nullptr)
{
    setWindowIcon(QIcon(":/appicon/icon16.png"));

	ui->setupUi(this);

#ifndef Q_WS_MACX
    ui->menuFile->addSeparator();
    QAction * action = ui->menuFile->addAction("Quit");
    connect(action,SIGNAL(triggered()),this,SLOT(appQuit()));
#endif

	// Object properties view
	QVBoxLayout * layout = new QVBoxLayout();
	ui->scrollAreaWidgetContents->setLayout(layout);
	buildObjectPropertiesView(layout);

	// Material properties view
	layout = new QVBoxLayout();
	ui->materialEditorScrollArea->setLayout(layout);
	buildMaterialPropertiesView(layout);

	// Piece properties view
	layout = new QVBoxLayout();
	ui->pieceScrollArea->setLayout(layout);
	buildPiecePropertiesView(layout);

	// Scene graph view
	layout = new QVBoxLayout();
	layout->setMargin(0);
	ui->sceneGraphScrollArea->setLayout(layout);
	buildSceneGraphView(layout);

	// Status bar
	_statusLabel = new QLabel("");
	statusBar()->addWidget(_statusLabel);

	_showGridCheck = new QCheckBox("grid");
	_showGridCheck->setStyleSheet("background-color: transparent");
	connect(_showGridCheck, SIGNAL(clicked()),this,SLOT(on_actionShow_Hide_grid_triggered()));
	statusBar()->addWidget(_showGridCheck);

	_showAxisCheck = new QCheckBox("axis");
	_showAxisCheck->setStyleSheet("background-color: transparent");
	connect(_showAxisCheck, SIGNAL(clicked()),this,SLOT(on_actionToggleCoordinate_axis_triggered()));
	statusBar()->addWidget(_showAxisCheck);

	_showGizmosBtn = new QPushButton("Show Gizmos");
	connect(_showGizmosBtn, SIGNAL(clicked()),this,SLOT(on_actionToggleAll_gizmos_triggered()));
	statusBar()->addWidget(_showGizmosBtn);

	_hideGizmosBtn = new QPushButton("Hide Gizmos");
	connect(_hideGizmosBtn, SIGNAL(clicked()),this,SLOT(on_actionToggleHide_All_triggered()));
	statusBar()->addWidget(_hideGizmosBtn);

	QVBoxLayout * assetLo = new QVBoxLayout();
	ui->assetsDockWidgetContent->setLayout(assetLo);
	RPLibraryWidget * assetLibraryWidget = new RPLibraryWidget(RPAppMediator::get()->assetLibrary());
	assetLibraryWidget->setType(RPLibraryWidget::kTypeAssets);
	assetLo->addWidget(assetLibraryWidget);

	_polygonCountLabel = new QLabel("");
	statusBar()->addWidget(_polygonCountLabel);

	QVBoxLayout * matLo = new QVBoxLayout();

	RPLibraryWidget * materialLibraryWidget = new RPLibraryWidget(RPAppMediator::get()->materialLibrary());
	materialLibraryWidget->setType(RPLibraryWidget::kTypeMaterials);
	matLo->addWidget(materialLibraryWidget);
	ui->matlibDockWidgetContent->setLayout(matLo);
	restoreWindowSettings();

	_trVisitor = new vwgl::TransformVisitor();
	RPAppMediator::get()->sceneController()->registerView(this);

	sceneChanged();

	connect(qApp,SIGNAL(aboutToQuit()),this,SLOT(prepareToQuit()));

	reloadPlugins();

    _exportThread = new RPExportDrawableThread();
    connect(_exportThread,SIGNAL(saveDone(bool)),this,SLOT(exportThreadDone(bool)));
}

RPMainWindow::~RPMainWindow()
{
	delete ui;
}

void RPMainWindow::on_actionProperties_triggered()
{
	if (ui->propertiesDockWidget->isVisible()) {
		ui->propertiesDockWidget->hide();
	}
	else {
		ui->propertiesDockWidget->show();
	}
}

void RPMainWindow::on_actionImport_triggered()
{
	QGLContext * ctx = ui->mainGLView->context();
    QString dir = RPAppMediator::get()->settings()->getLastOpenDirectory();
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                     dir,
													 tr("Supported files (*.vwglb *.vwglt *.dae)"));
	if (!fileName.isEmpty()) {
        RPAppMediator::get()->settings()->setLastOpenDirectory(dir);
		RPAppMediator::get()->executeCommand(new RPImportCommand(ctx,fileName.toStdString()));
	}
}

void RPMainWindow::on_actionTranslate_triggered()
{
	RPAppMediator::get()->state()->setTool(new RPTranslateTool());
}

void RPMainWindow::on_actionRotate_triggered()
{
	RPAppMediator::get()->state()->setTool(new RPRotateTool());
}

void RPMainWindow::on_actionScale_triggered()
{
	RPAppMediator::get()->state()->setTool(new RPScaleTool());
}

void RPMainWindow::on_actionSelect_triggered()
{
	RPAppMediator::get()->state()->setTool(new RPSelectTool());
}

void RPMainWindow::on_actionUndo_triggered()
{
	RPAppMediator::get()->undo();
}

void RPMainWindow::on_actionRedo_triggered()
{
	RPAppMediator::get()->redo();
}

void RPMainWindow::on_actionExport_Selected_triggered()
{
	RPSelectionManager * selectMgr = RPAppMediator::get()->state()->selectionManager();
    QString defaultDir = RPAppMediator::get()->settings()->getLastSaveDirectory();
	if (selectMgr->selectedDrawables().size()==1) {
        QString fileName = QFileDialog::getSaveFileName(this, tr("Export selection"),
                                                        defaultDir,
                                                        tr("3D Object file (*.vwglb)"));
		if (!fileName.isEmpty()) {
            RPAppMediator::get()->settings()->setLastSaveDirectory(fileName);
			RPAppMediator::get()->executeCommand(new RPSaveDrawableCommand(fileName.toStdString(),selectMgr->selectedDrawable()));
		}
	}
	else if (selectMgr->selectedDrawables().size()>1) {
        QString directory = QFileDialog::getExistingDirectory(this,
                                                              tr("Select directory",
                                                              defaultDir.toStdString().c_str(),
                                                              QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
		if (!directory.isEmpty()) {
            RPAppMediator::get()->settings()->setLastSaveDirectory(directory);
            RPAppMediator::get()->showProcessWindow("Exporting selection");
            _exportThread->configure(directory.toStdString(),nullptr);
            _exportThread->start();
		}
	}
}

void RPMainWindow::on_actionMaterial_Editor_triggered()
{
	if (ui->materialEditorWidget->isVisible()) {
		ui->materialEditorWidget->hide();
	}
	else {
		ui->materialEditorWidget->show();
	}
}

void RPMainWindow::on_actionPieceProperties_triggered()
{
	if (ui->solidPropertiesDockWidget->isVisible()) {
		ui->solidPropertiesDockWidget->hide();
	}
	else {
		ui->solidPropertiesDockWidget->show();
	}
}

void RPMainWindow::on_actionScene_View_triggered()
{
	if (ui->sceneGraphDockWidget->isVisible()) {
		ui->sceneGraphDockWidget->hide();
	}
	else {
		ui->sceneGraphDockWidget->show();
	}
}


void RPMainWindow::buildObjectPropertiesView(QVBoxLayout * lo)
{
	lo->addWidget(new RPUIDrawableInfoView());
	lo->addWidget(new RPTransformView());
    lo->addWidget(new RPDockNodeInfoView());
	lo->addWidget(new RPJointView());
	lo->addWidget(new RPUIShadowProjectorWidget());
	lo->addWidget(new RPLightWidget());
	lo->addWidget(new RPCameraWidget());

	lo->addSpacerItem(new QSpacerItem(40,40,QSizePolicy::Minimum, QSizePolicy::Expanding));
}

void RPMainWindow::on_actionMaterial_Library_triggered()
{
	if (ui->matlibDockWidget->isVisible()) {
		ui->matlibDockWidget->hide();
	}
	else {
		ui->matlibDockWidget->show();
	}
}


void RPMainWindow::on_actionAsset_Library_triggered()
{
	if (ui->assetsDockWidget->isVisible()) {
		ui->assetsDockWidget->hide();
	}
	else {
		ui->assetsDockWidget->show();
	}
}


void RPMainWindow::buildPiecePropertiesView(QVBoxLayout * lo)
{
	RPItemWidget * itemWidget = new RPItemWidget();
	lo->addWidget(itemWidget);

	RPUIShadowProjectorWidget * itemShadow = new RPUIShadowProjectorWidget();
	itemShadow->setTargetSolid();
	lo->addWidget(itemShadow);

	lo->addSpacerItem(new QSpacerItem(40,40,QSizePolicy::Minimum, QSizePolicy::Expanding));
}

void RPMainWindow::buildMaterialPropertiesView(QVBoxLayout * lo)
{
	lo->addWidget(new RPMaterialWidget());
}

void RPMainWindow::buildSceneGraphView(QVBoxLayout * lo)
{
	lo->addWidget(new RPUISceneGraphWidget());
}

void RPMainWindow::saveWindowSettings()
{
	QSettings settings("Vitaminew","RedPillEditor");

	settings.setValue("initialized",true);
	settings.setValue("dockState",saveState());
	settings.setValue("windowGeometry",saveGeometry());
	bool maximized = isMaximized();
	settings.setValue("maximized",maximized);
	if (!maximized) {
		settings.setValue("position", pos());
		settings.setValue("size", size());
	}
	QString style = qApp->styleSheet();
	settings.setValue("stylesheet", style);

	// TODO: material library, asset library
}

void RPMainWindow::restoreWindowSettings()
{
	QSettings settings("Vitaminew","RedPillEditor");

	setTabPosition(Qt::RightDockWidgetArea,QTabWidget::West);
	setTabPosition(Qt::LeftDockWidgetArea,QTabWidget::East);
	setTabPosition(Qt::BottomDockWidgetArea,QTabWidget::West);

	QFile file(":/style/main_style.qss");
	QString stylesheet = "";
	if(file.open(QFile::ReadOnly)) {
	   stylesheet = QString(file.readAll());
	}


	if (settings.value("initialized",false).toBool()) {
		QString styleSheetSettings = settings.value("stylesheet",stylesheet).toString();
		if (styleSheetSettings!="") {
			stylesheet = styleSheetSettings;
		}
		qApp->setStyleSheet(stylesheet);
		restoreGeometry(settings.value("windowGeometry",saveGeometry()).toByteArray());
		restoreState(settings.value("dockState",saveState()).toByteArray());
		bool maximized = settings.value("maximized",true).toBool();
		if (maximized) {
			showMaximized();
		}
		else {
			QPoint position = settings.value("position", pos()).toPoint();
			move(position);
			resize(settings.value("size", size()).toSize());
			show();
		}

		RPAppMediator::get()->assetLibrary()->loadDefault();
		RPAppMediator::get()->materialLibrary()->loadDefault();

		std::string execPath = vwgl::System::get()->getExecutablePath();
		if (vwgl::System::get()->isMac()) {
			execPath = vwgl::System::get()->addPathComponent(execPath,"../../../");
		}
		std::string defaultLibraryPath = vwgl::System::get()->addPathComponent(execPath, "libraries");
		std::string assets = vwgl::System::get()->addPathComponent(defaultLibraryPath,"assets.vwlib");
		std::string materials = vwgl::System::get()->addPathComponent(defaultLibraryPath,"materials.vwlib");
		RPAppMediator::get()->assetLibrary()->open(assets);
		RPAppMediator::get()->materialLibrary()->open(materials);
	}
	else {
		addDockWidget(Qt::LeftDockWidgetArea,ui->sceneGraphDockWidget);
		addDockWidget(Qt::BottomDockWidgetArea,ui->assetsDockWidget);
		addDockWidget(Qt::BottomDockWidgetArea,ui->matlibDockWidget);

		this->tabifyDockWidget(ui->propertiesDockWidget,ui->solidPropertiesDockWidget);
		this->tabifyDockWidget(ui->solidPropertiesDockWidget,ui->materialEditorWidget);
		ui->propertiesDockWidget->raise();

		this->tabifyDockWidget(ui->assetsDockWidget,ui->matlibDockWidget);
		ui->assetsDockWidget->raise();


		showMaximized();
		qApp->setStyleSheet(stylesheet);

		RPAppMediator::get()->assetLibrary()->loadDefault();
		RPAppMediator::get()->materialLibrary()->loadDefault();

		std::string execPath = vwgl::System::get()->getExecutablePath();
		if (vwgl::System::get()->isMac()) {
			execPath = vwgl::System::get()->addPathComponent(execPath,"../../../");
		}
		std::string defaultLibraryPath = vwgl::System::get()->addPathComponent(execPath, "libraries");
		std::string assets = vwgl::System::get()->addPathComponent(defaultLibraryPath,"assets.vwlib");
		std::string materials = vwgl::System::get()->addPathComponent(defaultLibraryPath,"materials.vwlib");
		RPAppMediator::get()->assetLibrary()->open(assets);
		RPAppMediator::get()->materialLibrary()->open(materials);
	}	
}

void RPMainWindow::sceneChanged()
{
	RPSelectionManager * sel = RPAppMediator::get()->state()->selectionManager();
	if (sel && sel->selectedDrawable()) {
		vwgl::ptr<vwgl::BoundingBox> bbox = new vwgl::BoundingBox();
		vwgl::SolidList::iterator it;
		for (it=sel->selectedDrawable()->getSolidList().begin(); it!=sel->selectedDrawable()->getSolidList().end(); ++it) {
			bbox->addVertexData((*it)->getPolyList());
		}
		QString status = "Selection size (scale ignored): width=";
		status +=	QString::number(bbox->size().x()) + ", height=" +
					QString::number(bbox->size().y()) + ", depth=" +
					QString::number(bbox->size().z()) + "";
		_statusLabel->setText(status);

		QString polyCountText;
		if (sel->selectedItems().size()>0) {
			polyCountText = "Polygon count: ";
			int polyCount = 0;
			vwgl::SolidList::iterator it;
			bool showWarning = false;
			for (it=sel->selectedItems().begin(); it!=sel->selectedItems().end(); ++it) {
				vwgl::Solid * item = (*it).getPtr();
				int totalItems = item->getPolyList()->getIndexCount() / 3;
				polyCount += totalItems;
				if (totalItems*3>=vwgl::Math::maxValue(static_cast<short>(0))) {
					showWarning = true;
				}
			}
			polyCountText += QString::number(polyCount);
			if (showWarning) {
				polyCountText += " - WARNING: Some pieces exceeds the maximum number of polygons for WebGL/OpenGL ES";
			}
			_polygonCountLabel->setText(polyCountText);
		}
		else {
			_polygonCountLabel->setText("");
		}
	}
	else {
		_statusLabel->setText("Selection size: no selection");
		_polygonCountLabel->setText("");
	}
	updateGizmoVisibilityTools();
	bool highQualityRender = RPAppMediator::get()->sceneRenderer()->getRenderMode()==RPSceneRenderer::kRenderModeDeferred;
	ui->actionToggleHigh_Quality_Render->setChecked(highQualityRender);

	ui->actionShow_Hide_grid->setChecked(RPAppMediator::get()->sceneController()->isGridVisible());
	ui->actionToggleCoordinate_axis->setChecked(RPAppMediator::get()->sceneController()->isAxiVisible());
	_showGridCheck->setChecked(RPAppMediator::get()->sceneController()->isGridVisible());
	_showAxisCheck->setChecked(RPAppMediator::get()->sceneController()->isAxiVisible());
}

void RPMainWindow::selectionChanged()
{
	sceneChanged();
}

void RPMainWindow::moveEvent(QMoveEvent *)
{

}

void RPMainWindow::resizeEvent(QResizeEvent *)
{

}

void RPMainWindow::closeEvent(QCloseEvent * event)
{
    if (RPAppMediator::get()->unsavedChanges()) {
        if (RPAppMediator::get()->askSaveChanges()) {
            event->accept();
        }
        else {
            event->ignore();
        }
    }
    else {
        event->accept();
    }
}

vwgl::Node *  RPMainWindow::getDeleteObjectTarget()
{
	vwgl::Node * selected = RPAppMediator::get()->state()->selectionManager()->selectedNode();
	if (dynamic_cast<vwgl::DrawableNode*>(selected) && selected->getParent()) {
		// Returns the transform node
		return selected->getParent();
	}
	else if (dynamic_cast<vwgl::Light*>(selected) && selected->getParent()) {
		// Returns the transform node
		return selected->getParent();
	}
	else if (dynamic_cast<vwgl::TransformNode*>(selected)) {
		return selected;
	}
	else {
		return nullptr;
	}
}

void RPMainWindow::on_actionDelete_object_triggered()
{
	vwgl::Node * deleteTarget = getDeleteObjectTarget();
	if (deleteTarget) {
		QGLContext * ctx = RPAppMediator::get()->mainViewContext();
		RPAppMediator::get()->executeCommand(new RPDeleteObjectCommand(ctx,deleteTarget));
	}
}

void RPMainWindow::on_actionCenter_in_Selection_triggered()
{
	vwgl::Drawable * drw = RPAppMediator::get()->state()->selectionManager()->selectedDrawable();
	vwgl::Light * light = dynamic_cast<vwgl::Light*>(RPAppMediator::get()->state()->selectionManager()->selectedNode());
	vwgl::TransformNode * trx = nullptr;
	vwgl::TRSTransformStrategy * strategy = nullptr;
	if ( (drw && drw->getParent() &&
		 (trx=dynamic_cast<vwgl::TransformNode*>(drw->getParent()->getParent())) && (strategy=trx->getTransformStrategy<vwgl::TRSTransformStrategy>())) ||
		 (light && light->getGizmoDrawable() && (trx=dynamic_cast<vwgl::TransformNode*>(light->getParent())) && (strategy=trx->getTransformStrategy<vwgl::TRSTransformStrategy>()))) {
		vwgl::Vector3 targetPosition = strategy->getTranslate();
		vwgl::ptr<vwgl::BoundingBox> bbox = drw ? new vwgl::BoundingBox(drw):new vwgl::BoundingBox(light->getGizmoDrawable());
		vwgl::Vector3 size = bbox->size();
		vwgl::Vector3 scale = strategy->getScale();
		float distance = size.magnitude() * 1.5f * scale.magnitude();

		RPAppMediator::get()->sceneRenderer()->getCameraManipulator()->setCenter(targetPosition);
		RPAppMediator::get()->sceneRenderer()->getCameraManipulator()->setDistance(distance);
		RPAppMediator::get()->sceneRenderer()->getCameraManipulator()->setTransform();
		RPAppMediator::get()->sceneController()->notifyChange();
	}
}

void RPMainWindow::on_actionDirectional_triggered()
{
	RPAppMediator * app = RPAppMediator::get();
	RPCommand * cmd = new RPCreateLightCommand(app->mainViewContext(),
											   app->sceneController()->getSceneRoot(),
											   app->get()->sceneRenderer()->getCameraManipulator()->center(),
											   vwgl::Light::kTypeDirectional);
	app->executeCommand(cmd);
}

void RPMainWindow::on_actionSpot_triggered()
{
	RPAppMediator * app = RPAppMediator::get();
	RPCommand * cmd = new RPCreateLightCommand(app->mainViewContext(),
											   app->sceneController()->getSceneRoot(),
											   app->get()->sceneRenderer()->getCameraManipulator()->center(),
											   vwgl::Light::kTypeSpot);

	app->executeCommand(cmd);
}

void RPMainWindow::on_actionPoint_triggered()
{
	RPAppMediator * app = RPAppMediator::get();
	RPCommand * cmd = new RPCreateLightCommand(app->mainViewContext(),
											   app->sceneController()->getSceneRoot(),
											   app->get()->sceneRenderer()->getCameraManipulator()->center(),
											   vwgl::Light::kTypePoint);
	app->executeCommand(cmd);
}

void RPMainWindow::on_actionPlane_triggered()
{
	RPAppMediator * app = RPAppMediator::get();
	RPCommand * cmd = new RPCreatePlaneCommand(app->mainViewContext(),
										   app->sceneController()->getSceneRoot(),
										   1.0f, 1.0f);
	app->executeCommand(cmd);
}

void RPMainWindow::on_actionCube_triggered()
{
	RPAppMediator * app = RPAppMediator::get();
	RPCommand * cmd = new RPCreateCubeCommand(app->mainViewContext(),
										  app->sceneController()->getSceneRoot(),
										  1.0f, 1.0f, 1.0f);
	app->executeCommand(cmd);
}

void RPMainWindow::on_actionSphere_triggered()
{
	RPAppMediator * app = RPAppMediator::get();
	RPCommand * cmd = new RPCreateSphereCommand(app->mainViewContext(),
											app->sceneController()->getSceneRoot(),
											1.0f, 32, 32);
	app->executeCommand(cmd);
}

void RPMainWindow::on_actionSave_Scene_triggered()
{
	if (!RPAppMediator::get()->saveScene()) {
		on_actionSave_Scene_As_triggered();
	}
}

void RPMainWindow::on_actionSave_Scene_As_triggered()
{
    QString defaultDir = RPAppMediator::get()->settings()->getLastSaveDirectory();
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save scene"),
                                                    defaultDir,
                                                    tr("Scene file (*.vitscn)"));
	if (!fileName.isEmpty()) {
        RPAppMediator::get()->settings()->setLastSaveDirectory(fileName);
		RPAppMediator::get()->saveScene(fileName.toStdString());
	}
}

void RPMainWindow::on_actionOpen_Scene_triggered()
{
    RPAppMediator::get()->mainViewContext()->makeCurrent();
	bool execute = true;
	bool saveChanges = false;
	if (RPAppMediator::get()->unsavedChanges()) {
		QMessageBox::StandardButton reply;
		reply = QMessageBox::question(this, "Unsaved changes", "Do you want to save changes before continue?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);

		if (reply == QMessageBox::Yes) {
			saveChanges = true;
			execute = true;
		}
		else if (reply==QMessageBox::Cancel) {
			saveChanges = false;
			execute = false;
		}
		else {
			saveChanges = false;
			execute = true;
		}
	}
	if (execute) {
		if (saveChanges) {
			on_actionSave_Scene_triggered();
		}
        QString defaultDir = RPAppMediator::get()->settings()->getLastOpenDirectory();
        QString fileName = QFileDialog::getOpenFileName(this,
                                                        tr("Open Scene"),
                                                        defaultDir,
                                                        "Scene files (*.vitscn *.vitscnj)");
        //QString fileName = QFileDialog::getExistingDirectory(this, tr("Open Scene"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
		if (!fileName.isEmpty()) {
            RPAppMediator::get()->settings()->setLastOpenDirectory(fileName);
			RPAppMediator::get()->openScene(fileName.toStdString());
		}
	}
}

void RPMainWindow::on_actionNew_Scene_triggered()
{
    RPAppMediator::get()->mainViewContext()->makeCurrent();
	bool execute = true;
	bool saveChanges = false;
	if (RPAppMediator::get()->unsavedChanges()) {
		QMessageBox::StandardButton reply;
		reply = QMessageBox::question(this, "Unsaved changes", "Do you want to save changes before continue?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);

		if (reply == QMessageBox::Yes) {
			saveChanges = true;
			execute = true;
		}
		else if (reply==QMessageBox::Cancel) {
			saveChanges = false;
			execute = false;
		}
		else {
			saveChanges = false;
			execute = true;
		}
	}
	if (execute) {
		if (saveChanges) {
			on_actionSave_Scene_triggered();
		}
		RPAppMediator::get()->newScene();
	}
}

void RPMainWindow::on_actionDock_node_triggered()
{
    RPAppMediator * app = RPAppMediator::get();
    RPCommand * cmd = new RPCreateDockNodeCommand(app->sceneController()->getSceneRoot());
    app->executeCommand(cmd);
}

void RPMainWindow::on_actionShow_Hide_grid_triggered()
{
	RPAppMediator::get()->sceneController()->toggleGrid();
}

void RPMainWindow::on_actionToggleCoordinate_axis_triggered()
{
	RPAppMediator::get()->sceneController()->toggleAxis();
}

void RPMainWindow::on_actionSet_UI_Style_triggered()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Scene"), "", "Style files (*.qss)");
	if (!fileName.isEmpty()) {
		QFile file(fileName);
		if(file.open(QFile::ReadOnly)) {
		   QString StyleSheet = QString(file.readAll());
		   qApp->setStyleSheet(StyleSheet);
		}
	}
}

void RPMainWindow::on_actionDark_triggered()
{
	QFile file(":/style/main_style.qss");
	if(file.open(QFile::ReadOnly)) {
	   QString StyleSheet = QString(file.readAll());
	   qApp->setStyleSheet(StyleSheet);
	}
}

void RPMainWindow::on_actionLight_triggered()
{
	QFile file(":/style/light.qss");
	if(file.open(QFile::ReadOnly)) {
	   QString StyleSheet = QString(file.readAll());
	   qApp->setStyleSheet(StyleSheet);
	}
}

void RPMainWindow::appQuit()
{
    qApp->exit();
}

void RPMainWindow::on_actionWhite_triggered()
{
	QFile file(":/style/white.qss");
	if(file.open(QFile::ReadOnly)) {
	   QString StyleSheet = QString(file.readAll());
	   qApp->setStyleSheet(StyleSheet);
	}
}

void RPMainWindow::on_actionTransform_node_triggered()
{
	RPAppMediator * app = RPAppMediator::get();
	RPCommand * cmd = new RPCreateTransformCommand(app->mainViewContext(), app->sceneController()->getSceneRoot());
	app->executeCommand(cmd);
}

void RPMainWindow::updateGizmoVisibilityTools()
{
	RPState * s = RPAppMediator::get()->state();
	ui->actionToggleAxis_Orientation->setChecked(s->axisGizmoEnabled());
	ui->actionToggleDirectional_lights->setChecked(s->directionalLightGizmoEnabled());
	ui->actionTogglePoint_Lights->setChecked(s->pointLightGizmoEnabled());
	ui->actionToggleSpot_Lights->setChecked(s->spotLightGizmoEnabled());
	ui->actionToggleJoints->setChecked(s->jointGizmoEnabled());
	ui->actionToggleBounding_Boxes->setChecked(s->drawableBoundingBoxEnabled());
}

void RPMainWindow::on_actionToggleDirectional_lights_triggered()
{
	RPAppMediator::get()->state()->toggleDirectionalLightGizmo();
}

void RPMainWindow::on_actionTogglePoint_Lights_triggered()
{
	RPAppMediator::get()->state()->togglePointLightGizmo();
}

void RPMainWindow::on_actionToggleSpot_Lights_triggered()
{
	RPAppMediator::get()->state()->toggleSpotLightGizmo();
}

void RPMainWindow::on_actionToggleAxis_Orientation_triggered()
{
	RPAppMediator::get()->state()->toggleAxisGizmo();
}

void RPMainWindow::on_actionToggleJoints_triggered()
{
	RPAppMediator::get()->state()->toggleJointGizmo();
}

void RPMainWindow::on_actionToggleBounding_Boxes_triggered()
{
	RPAppMediator::get()->state()->toggleDrawableBBoxGizmo();
}

void RPMainWindow::on_actionToggleAll_gizmos_triggered()
{
	RPAppMediator::get()->state()->showAllGizmos();
}

void RPMainWindow::on_actionToggleHide_All_triggered()
{
	RPAppMediator::get()->state()->hideAllGizmos();
}

void RPMainWindow::on_actionDuplicate_triggered()
{
	RPSelectionManager * sel = RPAppMediator::get()->state()->selectionManager();
	RPCommand * cmd = nullptr;
	vwgl::Light * l = dynamic_cast<vwgl::Light*>(sel->selectedNode());

	if (sel->selectedDrawable()) {
		cmd = new RPDuplicateDrawableCommand(RPAppMediator::get()->mainViewContext(),
											 sel->selectedDrawable());
	}
	else if (l) {
		cmd = new RPDuplicateLightCommand(RPAppMediator::get()->mainViewContext(),l);
	}

	if (cmd) {
		RPAppMediator::get()->executeCommand(cmd);
	}
}

void RPMainWindow::on_actionToggleHigh_Quality_Render_triggered()
{
	RPAppMediator::get()->sceneRenderer()->toggleRenderMode();
	RPAppMediator::get()->sceneController()->notifyChange();
}

void RPMainWindow::prepareToQuit()
{
	saveWindowSettings();
}


void RPMainWindow::on_actionPreferences_triggered()
{
	RPAppMediator::get()->settings()->showPreferencesWindow();
}

void RPMainWindow::on_actionAbout_triggered()
{
	if (_aboutDialog==nullptr) {
		_aboutDialog = new RPAboutDialog();
	}
	_aboutDialog->show();
	_aboutDialog->setFocus();
}

void RPMainWindow::on_actionClear_Selection_triggered()
{
    RPAppMediator::get()->state()->selectionManager()->clearSelection();
}

void RPMainWindow::on_actionSelect_All_Items_triggered()
{
    RPAppMediator::get()->state()->selectionManager()->selectAllItems();
}

void RPMainWindow::on_actionSelect_Visible_Pieces_triggered()
{
    RPAppMediator::get()->state()->selectionManager()->selectVisibleItems();
}

void RPMainWindow::on_actionShow_Selected_Pieces_triggered()
{
    RPSelectionManager * sel = RPAppMediator::get()->state()->selectionManager();
    if (sel->selectedItems().size()>0) {
        RPCommand * cmd = new RPChangeItemVisibilityCommand(sel->selectedItems(),true);
        RPAppMediator::get()->executeCommand(cmd);
    }
}

void RPMainWindow::on_actionHide_Selected_Pieces_triggered()
{
    RPSelectionManager * sel = RPAppMediator::get()->state()->selectionManager();
    if (sel->selectedItems().size()>0) {
        RPCommand * cmd = new RPChangeItemVisibilityCommand(sel->selectedItems(),false);
        RPAppMediator::get()->executeCommand(cmd);
    }
}

void RPMainWindow::on_actionOrthographic_2_triggered()
{
    RPAppMediator::get()->sceneController()->setOrtographicCamera();
}

void RPMainWindow::on_actionPerspective_2_triggered()
{
    RPAppMediator::get()->sceneController()->setPerspectiveCamera();
}

void RPMainWindow::on_actionTop_triggered()
{
    vwgl::MouseTargetManipulator * man = RPAppMediator::get()->sceneRenderer()->getCameraManipulator();
    if (man) {
        man->setPitch(vwgl::Math::degreesToRadians(-90.0f));
        man->setYaw(0.0f);
        man->setTransform();
        RPAppMediator::get()->sceneController()->notifyChange();
    }
}

void RPMainWindow::on_actionLeft_triggered()
{
    vwgl::MouseTargetManipulator * man = RPAppMediator::get()->sceneRenderer()->getCameraManipulator();
    if (man) {
        man->setPitch(0.0f);
        man->setYaw(vwgl::Math::degreesToRadians(90.0f));
        man->setTransform();
        RPAppMediator::get()->sceneController()->notifyChange();
    }
}

void RPMainWindow::on_actionFront_triggered()
{
    vwgl::MouseTargetManipulator * man = RPAppMediator::get()->sceneRenderer()->getCameraManipulator();
    if (man) {
        man->setPitch(0.0f);
        man->setYaw(0.0f);
        man->setTransform();
        RPAppMediator::get()->sceneController()->notifyChange();
    }
}

void RPMainWindow::exportThreadDone(bool)
{
    RPAppMediator::get()->closeProcessWindow();
}

void RPMainWindow::reloadPlugins()
{
	QList<RPPluginAction*>::iterator aIt;
	RPEditorPluginMap::iterator pIt;
	QMenu * pluginsMenu = ui->menuPlugins;
	_pluginMenus.clear();
	pluginsMenu->clear();

	for (aIt=_pluginActions.begin(); aIt!=_pluginActions.end(); ++aIt) {
		RPPluginAction * action = *aIt;
		delete action;
	}
	_pluginActions.clear();

	for (pIt=RPEditorPluginRegister::get()->itBegin(); pIt!=RPEditorPluginRegister::get()->itEnd(); ++pIt) {
		RPEditorPlugin * plugin = pIt->second;
		RPPluginAction * action = new RPPluginAction(plugin);
		_pluginActions.push_back(action);
		QString menuName = QString::fromStdString(plugin->menuName());
		QString submenuName = QString::fromStdString(plugin->submenuName());
		QMenu * menu = nullptr;
		if (submenuName!="") {
			menu = _pluginMenus[menuName];
			if (!menu) {
				menu = new QMenu();
				menu->setTitle(menuName);
				pluginsMenu->addMenu(menu);
				_pluginMenus[menuName] = menu;
			}
			action->setText(submenuName);
		}
		else {
			menu = pluginsMenu;
			action->setText(menuName);
		}
		menu->addAction(action);
	}

	if (_pluginActions.size()==0) {
		pluginsMenu->hide();
	}
}

void RPMainWindow::on_actionDelete_piece_triggered()
{
	vwgl::SolidList & items = RPAppMediator::get()->state()->selectionManager()->selectedItems();
	vwgl::SolidList::iterator it;
	if (items.size()>0) {
		for (it=items.begin(); it!=items.end(); ++it) {
			RPCommand * cmd = new RPDeleteItemCommand((*it).getPtr());
			RPAppMediator::get()->executeCommand(cmd);
		}

	}
}
