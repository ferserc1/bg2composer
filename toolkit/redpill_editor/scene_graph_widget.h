#ifndef SCENE_GRAPH_WIDGET_H
#define SCENE_GRAPH_WIDGET_H

#include <QWidget>
#include "order_tree_view.h"
#include <QStandardItemModel>

#include "properties_view.h"

#include <vwgl/node_visitor.hpp>

namespace Ui {
class RPUISceneGraphWidget;
}

class RPUISceneGraphWidget : public QWidget, public RPPropertiesView, public vwgl::NodeVisitor {
	Q_OBJECT
public:
	explicit RPUISceneGraphWidget(QWidget *parent = 0);
	~RPUISceneGraphWidget();


	bool eventFilter(QObject *, QEvent *);

private:
	Ui::RPUISceneGraphWidget *ui;

	RPUIOrderTreeView * _treeView;
	QStandardItemModel * _model;
	bool _additiveSelection;

	void updateView();

	QStandardItem * _currentItem;	// Used by the node visitor to store the current group node
	vwgl::Node * _selectedNode;
	QStandardItem * _selectedItem;
	virtual void visit(vwgl::Node * node);

	void saveTreeState(QVariantList & states);
	void restoreTreeStates(QVariantList & states);

protected slots:
	void itemSelected(QItemSelection selected,QItemSelection deselected);
	void orderChanged(QModelIndex & source, QModelIndex & dst);
};

#endif // SCENE_GRAPH_WIDGET_H
