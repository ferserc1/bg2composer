#include "scene_controller.h"

#include "app_mediator.h"

#include <iostream>

#include <vwgl/shadow_light.hpp>
#include <vwgl/system.hpp>
#include <vwgl/math.hpp>
#include <vwgl/loader.hpp>

#include <vwgl/sphere.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/transform_node.hpp>

#include "grid_drawable.h"
#include "axis_drawable.h"

#include <QThread>

void RPSaveThread::run() {
    bool status = vwgl::Loader::get()->writeNode(_path, _sceneRoot);
    emit saveDone(status);
}

RPSceneController::RPSceneController()
{
	_openVisitor = new RPOpenSceneVisitor();
    _orthographicScale = 5.0f;
    _orthographicCameraZoom = 1.0f;
    _saveThread = new RPSaveThread();
    connect(_saveThread,SIGNAL(saveDone(bool)),this,SLOT(sceneSaved(bool)));
}

void RPSceneController::registerView(RPSceneView *sceneView)
{
	if (getSceneViewIterator(sceneView)==_views.end()) {
		_views.push_back(sceneView);
		sceneView->_controller = this;
		sceneView->_blockUpdate = false;
	}
}

void RPSceneController::unregisterView(RPSceneView * sceneView)
{
	SceneViewList::iterator it = getSceneViewIterator(sceneView);
	if (it!=_views.end()) {
		_views.erase(it);
	}
}

void RPSceneController::notifyChange()
{
    RPAppMediator::get()->mainViewContext()->makeCurrent();
	for (SceneViewList::iterator it=_views.begin(); it!=_views.end(); ++it) {
		if (!(*it)->_blockUpdate) (*it)->sceneChanged();
	}
}

void RPSceneController::notifySelectionChanged()
{
	for (SceneViewList::iterator it=_views.begin(); it!=_views.end(); ++it) {
		if (!(*it)->_blockUpdate) (*it)->selectionChanged();
	}
}

RPSceneController::SceneViewList::iterator RPSceneController::getSceneViewIterator(RPSceneView * sceneView)
{
	return std::find(_views.begin(), _views.end(), sceneView);
}

void RPSceneController::showGrid()
{
	_grid->show();
	notifyChange();
}

void RPSceneController::hideGrid()
{
	_grid->hide();
	notifyChange();
}

void RPSceneController::showAxis()
{
	_axis->show();
	notifyChange();
}

void RPSceneController::hideAxis()
{
	_axis->hide();
	notifyChange();
}

void RPSceneController::close()
{
	_hiddenScene = nullptr;
	_sceneRoot = nullptr;
	_mainCameraTransform = nullptr;
	_mainCamera = nullptr;
	_projectionMethod = nullptr;
	_defaultCubeMap = nullptr;
	vwgl::CameraManager::get()->finalize();
	vwgl::LightManager::get()->finalize();
	_filePath = "";
}

void RPSceneController::newScene()
{
	close();
	createHiddenNode();
	createDefaultScene();
}

void RPSceneController::createHiddenNode()
{
	_hiddenScene = new vwgl::Group();
	RPGridDrawable * grid = new RPGridDrawable(110.0f,21,21);
	grid->getGenericMaterial()->setPolygonMode(vwgl::Material::kLine);
	grid->getGenericMaterial()->setCullFace(false);
	grid->getGenericMaterial()->setLightEmission(1.0f);
	grid->getGenericMaterial()->setCastShadows(false);
	grid->getGenericMaterial()->setReceiveShadows(false);
	grid->getGenericMaterial()->setDiffuse(vwgl::Color(0.9f,0.9f,0.95f,1.0f));
	vwgl::TransformNode * gridTrx = new vwgl::TransformNode();
	gridTrx->addChild(new vwgl::DrawableNode(grid));
	_hiddenScene->addChild(gridTrx);
	_grid = grid;

	RPAxisDrawable * axis = new RPAxisDrawable(100.0f);
	vwgl::TransformNode * axisTrx = new vwgl::TransformNode();
	axisTrx->addChild(new vwgl::DrawableNode(axis));
	_hiddenScene->addChild(axisTrx);
	_axis = axis;
}

void RPSceneController::createDefaultScene() {
	std::string resourcePath = vwgl::System::get()->getResourcesPath();
	_sceneRoot = new vwgl::Group();
	_hiddenScene->addChild(_sceneRoot.getPtr());
	_mainCameraTransform = new vwgl::TransformNode();
	_mainCamera = new vwgl::Camera();
	_sceneRoot->addChild(_mainCameraTransform.getPtr());
	_mainCameraTransform->addChild(_mainCamera.getPtr());
    _projectionMethod = new vwgl::OpticalProjectionMethod(35.0f);
    _mainCamera->setProjectionMethod(_projectionMethod.getPtr());

	// Default light
    vwgl::ShadowLight * l = new vwgl::ShadowLight();
	l->getProjectionMatrix().ortho(-4.0,4.0,-4.0,4.0,1.0,50.0);
    l->setShadowStrength(0.5f);
	vwgl::PolarTransformStrategy * strategy = new vwgl::PolarTransformStrategy();
	vwgl::TransformNode * ltrx = new vwgl::TransformNode(strategy);
	strategy->setOrientation(vwgl::Math::degreesToRadians(-45.0f));
	strategy->setElevation(vwgl::Math::degreesToRadians(-45.0f));
	strategy->setDistance(-20.0f);
	ltrx->addChild(l);
	_sceneRoot->addChild(ltrx);

	// Default cube map
	// TODO: swap posy and negy when the cubemap bug is fixed in the graphic engine
	_defaultCubeMap = vwgl::Loader::get()->loadCubemap(resourcePath + "cubemap_posx.jpg",
													   resourcePath + "cubemap_negx.jpg",
													   resourcePath + "cubemap_negy.jpg",
													   resourcePath + "cubemap_posy.jpg",
													   resourcePath + "cubemap_posz.jpg",
													   resourcePath + "cubemap_negz.jpg");
}

void RPSceneController::setOrtographicCamera()
{
    if (_mainCamera.valid()) {
        _mainCamera->setProjectionMethod(nullptr);
        updateOrthographicZoom();
    }
}

void RPSceneController::setPerspectiveCamera()
{
    if (_mainCamera.valid()) {
        _mainCamera->setProjectionMethod(_projectionMethod.getPtr());
        notifyChange();
    }
}

void RPSceneController::updateOrthographicZoom()
{
    if (isOrtographicCamera()) {
        vwgl::Viewport vp = RPAppMediator::get()->sceneRenderer()->getViewport();
        vwgl::Vector2 baseSize = vwgl::Vector2(vp.aspectRatio(),1.0f);
        baseSize.scale(_orthographicScale * _orthographicCameraZoom);
        _mainCamera->getProjectionMatrix().ortho(-baseSize.width(),baseSize.width(),-baseSize.height(),baseSize.height(),-100.0,100.0);
        notifyChange();
    }
}

void RPSceneController::open(const std::string & file)
{
	close();
	createHiddenNode();
	_sceneRoot = dynamic_cast<vwgl::Group*>(vwgl::Loader::get()->loadNode(file));
	if (_sceneRoot.valid()) {
		_hiddenScene->addChild(_sceneRoot.getPtr());
		std::string resourcePath = vwgl::System::get()->getResourcesPath();
		_mainCamera = vwgl::CameraManager::get()->getMainCamera();
		if (!_mainCamera) {
			_mainCamera = new vwgl::Camera();
			_mainCameraTransform = new vwgl::TransformNode();
			_sceneRoot->addChild(_mainCameraTransform.getPtr());
		}
		else {
			_mainCameraTransform = dynamic_cast<vwgl::TransformNode*>(_mainCamera->getParent());
		}
		_projectionMethod = new vwgl::OpticalProjectionMethod(35.0f);
		_mainCamera->setProjectionMethod(_projectionMethod.getPtr());


		_defaultCubeMap = vwgl::Loader::get()->loadCubemap(resourcePath + "cubemap_posx.jpg",
														   resourcePath + "cubemap_negx.jpg",
														   resourcePath + "cubemap_posy.jpg",
														   resourcePath + "cubemap_negy.jpg",
														   resourcePath + "cubemap_posz.jpg",
														   resourcePath + "cubemap_negz.jpg");

		_openVisitor->setDefaultCubemap(_defaultCubeMap.getPtr());
		_openVisitor->visit(_sceneRoot.getPtr());
		_filePath = file;
    }
	else {
		createDefaultScene();
	}
}

void RPSceneController::saveAs(const std::string & file)
{
	_filePath = file;
	save();
}

bool RPSceneController::save()
{
	if (_filePath=="") {
		return false;
	}
	else {
        RPAppMediator::get()->showProcessWindow("Saving scene");
        _saveThread->configure(_filePath,_sceneRoot.getPtr());
        _saveThread->start();
        //bool status = vwgl::Loader::get()->writeNode(_filePath, _sceneRoot.getPtr());
        return true;
	}
}

void RPSceneController::sceneSaved(bool)
{
    RPAppMediator::get()->closeProcessWindow();
}

void RPSceneController::initialize()
{
	newScene();
}

void RPSceneController::onFrame()
{
	for (SceneViewList::iterator it=_views.begin(); it!=_views.end(); ++it) {
		if (!(*it)->_blockUpdate) (*it)->onFrame();
	}
}

void RPSceneController::onResize(int width, int height)
{
    if (_projectionMethod.valid()) {
        _projectionMethod->setViewport(vwgl::Viewport(0,0,width,height));
        for (SceneViewList::iterator it=_views.begin(); it!=_views.end(); ++it) {
            if (!(*it)->_blockUpdate) (*it)->onResize(width,height);
        }
    }
}
