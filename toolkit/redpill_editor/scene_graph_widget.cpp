#include "scene_graph_widget.h"
#include "ui_scene_graph_widget.h"
#include "app_mediator.h"

#include <QTreeWidgetItem>
#include <QtGui>

#include <vwgl/group.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/light.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/projector.hpp>
#include <vwgl/dock_node.hpp>

#include "drawable_3d_gizmo_node.h"
#include "scene_graph_commands.h"

Q_DECLARE_METATYPE(vwgl::Node*)
Q_DECLARE_METATYPE(vwgl::Solid*)

RPUISceneGraphWidget::RPUISceneGraphWidget(QWidget *parent) :
	QWidget(parent),
	RPPropertiesView(),
	ui(new Ui::RPUISceneGraphWidget)
{
	_model = nullptr;
	ui->setupUi(this);
	_treeView = ui->treeView;
	connect(_treeView,SIGNAL(itemOrdered(QModelIndex&,QModelIndex&)),this,SLOT(orderChanged(QModelIndex&,QModelIndex&)));

	updateView();

	_treeView->viewport()->installEventFilter(this);
}

RPUISceneGraphWidget::~RPUISceneGraphWidget()
{
	delete ui;
	delete _model;
}

bool RPUISceneGraphWidget::eventFilter(QObject *, QEvent * evt)
{
	if (evt->type()==QEvent::MouseButtonPress) {
		QMouseEvent * event = static_cast<QMouseEvent*>(evt);
		if (event->button()==Qt::MouseButton::RightButton) {
			_additiveSelection = true;
		}
		else {
			_additiveSelection = false;
		}
		return false;
	}
	return false;
}

void RPUISceneGraphWidget::updateView()
{
	_selectedNode = RPAppMediator::get()->state()->selectionManager()->selectedNode();
	RPDrawable3DGizmoNode * gizmo = dynamic_cast<RPDrawable3DGizmoNode*>(_selectedNode);
	if (gizmo) {
		_selectedNode = gizmo->getSubject();
	}

	if (_model!=nullptr) delete _model;
	_model = new QStandardItemModel(this);
	_model->setColumnCount(1);

	_currentItem = nullptr;
	_selectedItem = nullptr;
	visit(RPAppMediator::get()->sceneController()->getSceneRoot());
	_model->appendRow(_currentItem);

	_treeView->setModel(_model);

	_treeView->expandAll();

	if (_selectedItem) {
		_treeView->selectionModel()->select(_selectedItem->index(),QItemSelectionModel::ClearAndSelect);
	}
	connect(_treeView->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(itemSelected(QItemSelection,QItemSelection)));
}

void RPUISceneGraphWidget::visit(vwgl::Node * node)
{
	vwgl::Group * grp = dynamic_cast<vwgl::Group*>(node);
	vwgl::TransformNode * trx = dynamic_cast<vwgl::TransformNode*>(node);
	vwgl::DrawableNode * drw = dynamic_cast<vwgl::DrawableNode*>(node);
	vwgl::Light * l = dynamic_cast<vwgl::Light*>(node);
	vwgl::Camera * c = dynamic_cast<vwgl::Camera*>(node);
	vwgl::Projector * proj = dynamic_cast<vwgl::Projector*>(node);
	vwgl::DockNode * dock = dynamic_cast<vwgl::DockNode*>(node);
	RPDrawable3DGizmoNode * gizmo = dynamic_cast<RPDrawable3DGizmoNode*>(node);


	QStandardItem * item = nullptr;
	if (gizmo) {
		// Los gizmos no se representan
	}
	else if (trx) {
		item = new QStandardItem(QIcon(":/images/dark-icons/translate_tool.png"),"Transform Group");
		if (trx->getName()!="") {
			item->setText(QString::fromStdString(trx->getName()));
		}
	}
	else if (drw) {
		item = new QStandardItem(QIcon(":/images/dark-icons/drawable.png"),"Drawable node");
		if (drw->getDrawable()) {
			item->setText(QString::fromStdString(drw->getDrawable()->getName()));
			vwgl::SolidList::iterator it;
			for (it=drw->getDrawable()->getSolidList().begin(); it!=drw->getDrawable()->getSolidList().end(); ++it) {
				vwgl::Solid * solid = (*it).getPtr();
				QString imageFile = ":/images/dark-icons/drawable.png";
				std::string name = "Piece";
				if (solid->getPolyList()->getName()!="") {
					name = solid->getPolyList()->getName();
				}
				if (solid->getGenericMaterial()->getSelectedMode()) {
					imageFile = ":/images/dark-icons/drawable-selected.png";
				}
				if (!solid->isVisible()) {
					imageFile = solid->getGenericMaterial()->getSelectedMode() ? ":/images/dark-icons/drawable-hidden-selected.png":":/images/dark-icons/drawable-hidden.png";
					name = "** " + name + " **";
				}

				QStandardItem * subitem = new QStandardItem(QIcon(imageFile),QString::fromStdString(name));

				QVariant data;
				data.setValue(solid);
				subitem->setData(data);
				item->appendRow(subitem);
			}
		}
	}
	else if (l) {
		item = new QStandardItem(QIcon(":/images/dark-icons/light_1.png"),"Light");
		switch(l->getType()) {
			case vwgl::Light::kTypeDirectional:
				item->setText("Directional Light");
				break;
			case vwgl::Light::kTypeSpot:
				item->setText("Spot Light");
				break;
			case vwgl::Light::kTypePoint:
				item->setText("Point Light");
				break;
			case vwgl::Light::kTypeDisabled:
				break;
		}
		if (l->getName()!="") {
			item->setText(QString::fromStdString(l->getName()));
		}
	}
	else if (c) {
		item = new QStandardItem(QIcon(":/images/dark-icons/photo_camera_2.png"),"Camera");
	}
	else if (proj) {
		item = new QStandardItem(QIcon(":/images/dark-icons/camera.png"),"Projector");
	}
	else if (dock) {
		item = new QStandardItem("Dock Node");
		if (dock->getName()!="") {
			item->setText("Dock Node (" + QString::fromStdString(dock->getName()) + ")");
		}
	}
	else if (grp) {
		item = new QStandardItem("Group");
		if (grp->getName()!="") {
			item->setText(QString::fromStdString(grp->getName()));
		}
	}
	else {
		item = new QStandardItem("Generic node");
	}

	if (item) {
		QVariant data;
		data.setValue(node);
		item->setData(data);

		if (_selectedNode==node) {
			_selectedItem = item;
		}


		if (_currentItem==nullptr) {
			_currentItem = item;
			item->setText("Scene");
		}
		else {
			_currentItem->appendRow(item);
		}

		if (grp) {
			QStandardItem * myParentItem = _currentItem;
			_currentItem = item;
			vwgl::NodeList::iterator it;
			for (it=grp->getNodeList().begin(); it!=grp->getNodeList().end(); ++it) {
				visit((*it).getPtr());
			}
			_currentItem = myParentItem;
		}
	}
}

void RPUISceneGraphWidget::itemSelected(QItemSelection selected,QItemSelection deselected)
{
	QModelIndex index = _treeView->selectionModel()->currentIndex();

	QVariant data = index.data(Qt::UserRole + 1);
	if (data.canConvert<vwgl::Node*>()) {
		vwgl::Node * node = data.value<vwgl::Node*>();
		RPAppMediator::get()->state()->selectionManager()->setSelectedNode(node);
	}
	else if (data.canConvert<vwgl::Solid*>()) {
		vwgl::Solid * item = data.value<vwgl::Solid*>();
		if (!_additiveSelection) RPAppMediator::get()->state()->selectionManager()->clearSelection(false);
		RPAppMediator::get()->state()->selectionManager()->addSelection(item->getDrawable(),item,true);
	}
}

void RPUISceneGraphWidget::orderChanged(QModelIndex & source, QModelIndex & dst)
{
	QVariant sourceVar = source.data(Qt::UserRole + 1);
	QVariant dstVar = dst.data(Qt::UserRole + 1);
	if (sourceVar.canConvert<vwgl::Node*>() && dstVar.canConvert<vwgl::Node*>()) {
		vwgl::Node * source = sourceVar.value<vwgl::Node*>();
		vwgl::Node * dst = dstVar.value<vwgl::Node*>();

		vwgl::Group * parent = dynamic_cast<vwgl::Group*>(dst);
		vwgl::TransformNode * childTrx = dynamic_cast<vwgl::TransformNode*>(source);
		if (parent && childTrx) {
			RPCommand * cmd = new RPChangeParentCommand(parent,source);
			RPAppMediator::get()->executeCommand(cmd);
		}
	}
}
