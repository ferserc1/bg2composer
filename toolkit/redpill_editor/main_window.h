#ifndef RPMAINWINDOW_H
#define RPMAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>

#include <vwgl/node.hpp>
#include <vwgl/transform_visitor.hpp>
#include "scene_view.h"
#include "about_dialog.h"
#include "scene_controller.h"
#include "plugin_action.h"


class RPExportDrawableThread : public RPSaveThread {
    Q_OBJECT
protected:
    virtual void run();
};

namespace Ui {
class RPMainWindow;
}

class RPMainWindow : public QMainWindow,
		public RPSceneView
{
	Q_OBJECT

public:
	explicit RPMainWindow(QWidget *parent = 0);
	~RPMainWindow();

	virtual void moveEvent(QMoveEvent *);
	virtual void resizeEvent(QResizeEvent *);
	virtual void closeEvent(QCloseEvent *);

	virtual void sceneChanged();
	virtual void selectionChanged();

private slots:
	void on_actionProperties_triggered();
	void on_actionImport_triggered();
	void on_actionTranslate_triggered();
	void on_actionRotate_triggered();
	void on_actionScale_triggered();
	void on_actionSelect_triggered();
	void on_actionUndo_triggered();
	void on_actionRedo_triggered();
	void on_actionExport_Selected_triggered();
	void on_actionMaterial_Editor_triggered();
	void on_actionPieceProperties_triggered();
	void on_actionScene_View_triggered();
	void on_actionDelete_object_triggered();
	void on_actionCenter_in_Selection_triggered();
	void on_actionDirectional_triggered();
	void on_actionSpot_triggered();
	void on_actionPoint_triggered();
	void on_actionPlane_triggered();
	void on_actionCube_triggered();
	void on_actionSphere_triggered();
	void on_actionSave_Scene_triggered();
	void on_actionSave_Scene_As_triggered();
	void on_actionOpen_Scene_triggered();
	void on_actionNew_Scene_triggered();
    void on_actionDock_node_triggered();
	void on_actionShow_Hide_grid_triggered();
	void on_actionToggleCoordinate_axis_triggered();
	void on_actionSet_UI_Style_triggered();
	void on_actionDark_triggered();
	void on_actionLight_triggered();

    void appQuit();

	void on_actionWhite_triggered();
	void on_actionTransform_node_triggered();
	void on_actionToggleDirectional_lights_triggered();
	void on_actionTogglePoint_Lights_triggered();
	void on_actionToggleSpot_Lights_triggered();
	void on_actionToggleAxis_Orientation_triggered();
	void on_actionToggleJoints_triggered();
	void on_actionToggleAll_gizmos_triggered();
	void on_actionToggleHide_All_triggered();
	void on_actionDuplicate_triggered();
	void on_actionToggleHigh_Quality_Render_triggered();

	void prepareToQuit();

	void on_actionToggleBounding_Boxes_triggered();

	void on_actionPreferences_triggered();

	void on_actionAbout_triggered();

	void on_actionMaterial_Library_triggered();

	void on_actionAsset_Library_triggered();

    void on_actionClear_Selection_triggered();

    void on_actionSelect_All_Items_triggered();

    void on_actionSelect_Visible_Pieces_triggered();

    void on_actionShow_Selected_Pieces_triggered();

    void on_actionHide_Selected_Pieces_triggered();

    void exportThreadDone(bool status);

    void on_actionOrthographic_2_triggered();

    void on_actionPerspective_2_triggered();

    void on_actionTop_triggered();

    void on_actionLeft_triggered();

    void on_actionFront_triggered();

	void on_actionDelete_piece_triggered();

private:
	Ui::RPMainWindow *ui;

	void buildObjectPropertiesView(QVBoxLayout * lo);
	void buildPiecePropertiesView(QVBoxLayout * lo);
	void buildMaterialPropertiesView(QVBoxLayout * lo);
	void buildSceneGraphView(QVBoxLayout * lo);

	void saveWindowSettings();
	void restoreWindowSettings();

	vwgl::Node * getDeleteObjectTarget();

	QLabel * _statusLabel;
	QLabel * _polygonCountLabel;
	QCheckBox * _showGridCheck;
	QCheckBox * _showAxisCheck;
	QPushButton * _showGizmosBtn;
	QPushButton * _hideGizmosBtn;
	vwgl::TransformVisitor * _trVisitor;
	RPAboutDialog * _aboutDialog;

    RPExportDrawableThread * _exportThread;

	//QString windowStateFilePath();
	//QString dockStateFilePath();

	void updateGizmoVisibilityTools();

	void reloadPlugins();
	QList<RPPluginAction*> _pluginActions;
	QHash<QString,QMenu*> _pluginMenus;
};

#endif // RPMAINWINDOW_H
