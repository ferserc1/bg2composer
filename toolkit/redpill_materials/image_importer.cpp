#include "image_importer.h"
#include "app_mediator.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QDir>
#include <QMessageBox>
#include <QFileInfo>
#include <QTextStream>

#include <vwgl/vector.hpp>

void ImportImagesThread::cancel()
{

}

int ImportImagesThread::getTotalItems()
{
    int total;
    _lock.lockForRead();
    total = _totalItems;
    _lock.unlock();
    return total;
}

int ImportImagesThread::getCompletedItems()
{
    int completed;
    _lock.lockForRead();
    completed = _completedItems;
    _lock.unlock();
    return completed;
}

bool ImportImagesThread::isDone()
{
    int completed;
    int total;
    _lock.lockForRead();
    completed = _completedItems;
    total = _totalItems;
    _lock.unlock();
    return completed==total;
}

void ImportImagesThread::run()
{

}

void ImportImagesThread::importImage(const QString & texturePath, const QString & thumbPath, const QString & thumbFormat, const QString & imageFile)
{

}

ImageImporter::ImageImporter(QWidget * parent):
    QDialog(parent)
{
    QVBoxLayout * layout = new QVBoxLayout(this);
    setLayout(layout);

    layout->addWidget(new QLabel("Import materials from images"));

    _texturePathPicker = new RPUIFilePicker();
    _texturePathPicker->setLabel("Texture folder");
    _texturePathPicker->setType(RPUIFilePicker::kTypeFolder);
    layout->addWidget(_texturePathPicker);

    _thumbPathPicker = new RPUIFilePicker();
    _thumbPathPicker->setLabel("Thumbnail folder");
    _thumbPathPicker->setType(RPUIFilePicker::kTypeFolder);
    layout->addWidget(_thumbPathPicker);

    _textureFormat = new RPUITextInput();
    _textureFormat->setLabel("Texture format");
    _textureFormat->setValue(QString("jpg"));
    layout->addWidget(_textureFormat);

    _thumbFormat = new RPUITextInput();
    _thumbFormat->setLabel("Thumbnail format");
    _thumbFormat->setValue(QString("jpg"));
    layout->addWidget(_thumbFormat);

	_thumbSuffix = new RPUITextInput();
	_thumbSuffix->setLabel("Thumbnail suffix");
	_thumbSuffix->setValue(QString("_thumb"));
	layout->addWidget(_thumbSuffix);

	_importNormalMaps = new RPUIBooleanPicker();
	_importNormalMaps->setLabel("Import normal maps");
	_importNormalMaps->setValue(true);
	layout->addWidget(_importNormalMaps);

	_normalMapFormat = new RPUITextInput();
	_normalMapFormat->setLabel("Normal map format");
	_normalMapFormat->setValue(QString("png"));
	layout->addWidget(_normalMapFormat);

	_normalMapSuffix = new RPUITextInput();
	_normalMapSuffix->setLabel("Normal map suffix");
	_normalMapSuffix->setValue(QString("_nm"));
	layout->addWidget(_normalMapSuffix);

    _copySettings = new RPUIBooleanPicker();
	_copySettings->setLabel("Copy other settings from the currently selected material");
    layout->addWidget(_copySettings);

    _statusLabel = new QLabel("");
    layout->addWidget(_statusLabel);

    _importButton = new QPushButton("Import");


    connect(_importButton,SIGNAL(clicked()),this,SLOT(import()));
    _cancelButton = new QPushButton("Cancel");
    connect(_cancelButton,SIGNAL(clicked()),this,SLOT(cancel()));

    QDialogButtonBox * buttonBox = new QDialogButtonBox();
       layout->addWidget(buttonBox);
       buttonBox->addButton(_importButton,QDialogButtonBox::ActionRole);
       buttonBox->addButton(_cancelButton,QDialogButtonBox::ActionRole);
    setModal(true);
}

void ImageImporter::import()
{
    if (_texturePathPicker->getPath().isEmpty()) {
        QMessageBox::information(this,"Select a path","You must select a folder containing the texture images",QMessageBox::Ok);
    }
    else {
        QString textureType = _textureFormat->getValue();
        QStringList nameFilter(QString("*.") + textureType);
        QDir directory(_texturePathPicker->getPath());
        QStringList textureFiles = directory.entryList(nameFilter);
        if (textureFiles.size()==0) {
            QMessageBox::information(this,"Images not found",QString("Could not found any image of type \"")
                                     + textureType
                                     + QString("\" at folder \"")
                                     + _texturePathPicker->getPath() + QString("\""), QMessageBox::Ok);
        }
        else {

            QStringList::iterator it;
            for (it=textureFiles.begin(); it!=textureFiles.end(); ++it) {
                importImage(_texturePathPicker->getPath(),
                            _thumbPathPicker->getPath(),
                            _thumbFormat->getValue(),
                            *it);
            }
            AppMediator::get()->document()->notifyChange();
            close();
        }
    }
}

void ImageImporter::cancel()
{
    close();
}

void ImageImporter::importImage(const QString & texturePath, const QString & thumbPath, const QString & thumbFormat, const QString & imageFile)
{
	QString textureFullPath = texturePath + QString("/") + imageFile;
	QFileInfo imageInfo(textureFullPath);
	QString imageName = imageInfo.completeBaseName();
	QString rapportFullPath = texturePath + QString("/") + imageInfo.baseName() + ".rap";

	QString thumbSuffix = _thumbSuffix->getValue();
	QString nmapSuffix = _normalMapSuffix->getValue();

	if ((!thumbSuffix.isEmpty() && imageName.endsWith(thumbSuffix)) ||
		(!nmapSuffix.isEmpty() && imageName.endsWith(nmapSuffix))){
		return;
	}

	QString thumbFullPath = thumbPath + QString("/") + imageName +
			QString(thumbSuffix) +
			QString(".") + thumbFormat;

	QString nmapFullPath = "";
	if (_importNormalMaps->getValue()) {
		nmapFullPath = texturePath + QString("/") + imageName +
				QString(nmapSuffix) +
				QString(".") + _normalMapFormat->getValue();
	}

    Document * doc = AppMediator::get()->document();
    vwgl::library::Material * mat = _copySettings->getValue() ? doc->createMaterial(false):doc->createEmptyMaterial(false);
    if (mat) {
		vwgl::Vector2 scale = importRapport(rapportFullPath);
        mat->setId(imageName.toStdString());
        mat->setName(imageName.toStdString());
        mat->setIcon(thumbFullPath.toStdString());

		mat->getMaterialModifier().setTexture(textureFullPath.toStdString());
		mat->getMaterialModifier().setTextureScale(scale);

		if (!nmapFullPath.isEmpty()) {
			mat->getMaterialModifier().setNormalMap(nmapFullPath.toStdString());
		}
    }
}

vwgl::Vector2 ImageImporter::importRapport(const QString & rapportUrl)
{
	vwgl::Vector2 scale(1.0);
	static float s_MapSize = 2.0f;
	static float s_RapportScale = 0.01f;

	QFile inputFile(rapportUrl);
	if (inputFile.open(QIODevice::ReadOnly)) {
		QTextStream in(&inputFile);
		int lines = 0;
		while (!in.atEnd() && lines<2) {
			QString line = in.readLine();
			float scaleComponent = s_MapSize / (line.toFloat() * s_RapportScale);

			if (lines==0) {
				scale.x(scaleComponent);
			}
			else if (lines==1) {
				scale.y(scaleComponent);
			}

			++lines;
		}
		inputFile.close();
	}

	return scale;
}
