#ifndef IMAGE_IMPORTER_H
#define IMAGE_IMPORTER_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QThread>
#include <QReadWriteLock>

#include "file_picker.h"
#include "text_input.h"
#include "boolean_picker.h"

#include <vwgl/vector.hpp>

class ImportImagesThread : public QThread {
    Q_OBJECT;
public:
    ImportImagesThread() :_totalItems(0), _completedItems(0) {}

    inline void setTexturePath(const QString & texturePath) { _texturePath = texturePath; }
    inline void setThumbnailPath(const QString & thumbPath) { _thumbPath = thumbPath; }
    inline void setTextureFormat(const QString & textureFormat) { _textureFormat = textureFormat; }
    inline void setThumbnailFormat(const QString & thumbFormat) { _thumbFormat = thumbFormat; }
    inline void setCopyMaterialSettings(bool copy) { _copySettings = copy; }

    void cancel();
    int getTotalItems();
    int getCompletedItems();
    bool isDone();

protected:
    virtual void run();

    QString _texturePath;
    QString _thumbPath;
    QString _textureFormat;
    QString _thumbFormat;
    bool _copySettings;

    int _totalItems;
    int _completedItems;

    QReadWriteLock _lock;

    void importImage(const QString & texturePath, const QString & thumbPath, const QString & thumbFormat, const QString & imageFile);
};

class ImageImporter : public QDialog {
    Q_OBJECT
public:
    ImageImporter(QWidget * parent = 0);

public slots:
    void import();
    void cancel();

protected:
    QPushButton * _importButton;
    QPushButton * _cancelButton;

    RPUIFilePicker * _texturePathPicker;
    RPUIFilePicker * _thumbPathPicker;
    RPUITextInput * _textureFormat;
    RPUITextInput * _thumbFormat;
	RPUITextInput * _thumbSuffix;
	RPUITextInput * _normalMapFormat;
	RPUITextInput * _normalMapSuffix;
	RPUIBooleanPicker * _importNormalMaps;

    RPUIBooleanPicker * _copySettings;
    QLabel * _statusLabel;

    ImportImagesThread _importThread;

    void importImage(const QString & texturePath, const QString & thumbPath, const QString & thumbFormat, const QString & imageFile);

	vwgl::Vector2 importRapport(const QString & rapportUrl);
};

#endif // IMAGE_IMPORTER_H
