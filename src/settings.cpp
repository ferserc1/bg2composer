

#include <bg2e/settings.hpp>

#include <bg2e/closure_action.hpp>

#include <vwgl/scene/light.hpp>
#include <vwgl/system.hpp>
#include <vwgl/dictionary_loader.hpp>
#include <vwgl/dictionary_writter.hpp>
#include <vwgl/loader.hpp>

namespace bg2e {

using namespace vwgl;

Settings::Settings()
	:QObject(0)
	,_settingsWindow(nullptr)
	,_canvasColor(vwgl::Color(0.0,0.0,0.0,1.0))
    ,_api(vwgl::Graphics::kApiOpenGLAdvanced)
{
}

void Settings::init(vwgl::scene::Renderer * highQuality, vwgl::scene::Renderer * lowQuality) {
	vwgl::Loader::get()->registerReader(new vwgl::DictionaryLoader());
	vwgl::Loader::get()->registerWritter(new vwgl::DictionaryWritter());

	_highQualityRender = highQuality;
	_lowQualityRenderer = lowQuality;
}

SettingsWindow * Settings::settingsWindow() {
	if (_settingsWindow==nullptr) {
		_settingsWindow = new SettingsWindow(this);
		_settingsWindow->setAttribute(Qt::WA_DeleteOnClose);
		connect(_settingsWindow,SIGNAL(finished(int)),this,SLOT(settingsWindowClosed(int)));
	}
	return _settingsWindow;
}

void Settings::settingsWindowClosed(int) {
	_settingsWindow = nullptr;
}

vwgl::SSAOMaterial * Settings::ssao() {
	return highQualityRenderer().getFilter<vwgl::SSAOMaterial>();
}

vwgl::SSRayTraceMaterial * Settings::ssrt() {
	return highQualityRenderer().getFilter<vwgl::SSRayTraceMaterial>();
}

vwgl::ShadowRenderPassMaterial * Settings::shadows() {
	return highQualityRenderer().getFilter<vwgl::ShadowRenderPassMaterial>();
}

vwgl::DeferredGBufferMaterial * Settings::gbuffers() {
	return highQualityRenderer().getFilter<vwgl::DeferredGBufferMaterial>();
}

void Settings::setShadowQuality(const vwgl::Size2Di & size) {
	_shadowQuality = size;
	vwgl::scene::Light::setShadowMapSize(size);
}

void Settings::setCanvasColor(const vwgl::Color & col) {
	_canvasColor = col;
	highQualityRenderer().setClearColor(_canvasColor);
	lowQualityRenderer().setClearColor(_canvasColor);
}

void Settings::setAntialiasingEnabled(bool e) {
	_highQualityRender->getRenderSettings().setAntialiasingEnabled(e);

}

void Settings::setMaterialLibraryPath(const std::string & path) {
	_materialLibraryPath = path;
}

void Settings::setTessellationEnabled(bool e) {
	_tessellation = e;
	Vector4 levels = _tessellation ? Vector4(300.0f):Vector4(1.0f);
    if (gbuffers()) gbuffers()->setMaxTessellationLevels(levels);
}

void Settings::setReflectionsQuality(int q) {
    if (ssrt()) ssrt()->setQuality(static_cast<SSRayTraceMaterial::Quality>(q));
}

void Settings::saveSettings() {
	std::string path = getSettingsPath();
	std::cout << "Saving settings to " << path << std::endl;
	SSAOMaterial * ssao = this->ssao();
	SSRayTraceMaterial * ssrt = this->ssrt();
	ShadowRenderPassMaterial * shadows = this->shadows();

	using namespace vwgl;
	ptr<Dictionary> settings = new Dictionary();
	ptr<Dictionary> ssaoDict = new Dictionary();
	settings->getObject()["ssao"] = ssaoDict.getPtr();

	ssaoDict->getObject()["enabled"] = new Dictionary(ssao->isEnabled());
	ssaoDict->getObject()["kernelSize"] = new Dictionary(ssao->getKernelSize());
	ssaoDict->getObject()["sampleRadius"] = new Dictionary(ssao->getSampleRadius());
	ssaoDict->getObject()["color"] = new Dictionary();
	ssaoDict->getObject()["color"]->getObject()["r"] = new Dictionary(ssao->getColor().r());
	ssaoDict->getObject()["color"]->getObject()["g"] = new Dictionary(ssao->getColor().g());
	ssaoDict->getObject()["color"]->getObject()["b"] = new Dictionary(ssao->getColor().b());
	ssaoDict->getObject()["color"]->getObject()["a"] = new Dictionary(ssao->getColor().a());
	ssaoDict->getObject()["blurIterations"] = new Dictionary(ssao->getBlurIterations());
	ssaoDict->getObject()["maxDistance"] = new Dictionary(ssao->getMaxDistance());

	ptr<Dictionary> ssrtDict = new Dictionary();
	settings->getObject()["ssrt"] = ssrtDict.getPtr();


    if (ssrt) ssrtDict->getObject()["quality"] = new Dictionary(ssrt->getQuality());
    else ssrtDict->getObject()["quality"] = new Dictionary(vwgl::SSRayTraceMaterial::kQualityLow);

	ptr<Dictionary> shadowsDict = new Dictionary();
	settings->getObject()["shadows"] = shadowsDict.getPtr();

	switch (shadows->getShadowType()) {
	case ShadowRenderPassMaterial::kShadowTypeHard:
		shadowsDict->getObject()["type"] = new Dictionary("hard");
		break;
	case ShadowRenderPassMaterial::kShadowTypeSoft:
		shadowsDict->getObject()["type"] = new Dictionary("soft");
		break;
	case ShadowRenderPassMaterial::kShadowTypeSoftStratified:
		shadowsDict->getObject()["type"] = new Dictionary("stratified");
		break;
	}
	shadowsDict->getObject()["quality"] = new Dictionary(static_cast<double>(getShadowQuality().width()));
	shadowsDict->getObject()["blurIterations"] = new Dictionary(static_cast<double>(shadows->getBlurIterations()));

	settings->getObject()["canvasColor"] = new Dictionary();
	settings->getObject()["canvasColor"]->getObject()["r"] = new Dictionary(_canvasColor.r());
	settings->getObject()["canvasColor"]->getObject()["g"] = new Dictionary(_canvasColor.g());
	settings->getObject()["canvasColor"]->getObject()["b"] = new Dictionary(_canvasColor.b());
	settings->getObject()["canvasColor"]->getObject()["a"] = new Dictionary(_canvasColor.a());

	settings->getObject()["antialiasing"] = new Dictionary(isAntialiasingEnabled());

	settings->getObject()["materialLibraryPath"] = new Dictionary(_materialLibraryPath);

	settings->getObject()["tessellation"] = new Dictionary(isTessellationEnabled());

	for (auto ext : _settingsExtensions) {
		ext->onSaveSettings(settings.getPtr());
	}

    switch (_api) {
    case vwgl::Graphics::kApiOpenGLBasic:
        settings->getObject()["api"] = new Dictionary("kApiOpenGLBasic");
        break;
    case vwgl::Graphics::kApiOpenGL:
        settings->getObject()["api"] = new Dictionary("kApiOpenGL");
        break;
    case vwgl::Graphics::kApiOpenGLAdvanced:
        settings->getObject()["api"] = new Dictionary("kApiOpenGLAdvanced");
        break;
    default:
        break;
    }

	Loader::get()->writeDictionary(path,settings.getPtr());
}

void Settings::loadSettings() {
	std::string path = getSettingsPath();
	std::cout << "Loading settings from " << path << std::endl;

	using namespace vwgl;
	Loader::get()->registerReader(new vwgl::DictionaryLoader());
	ptr<Dictionary> settings = Loader::get()->loadDictionary(path);
	if (settings.valid()) {
		ptr<Dictionary> ssaoDict = settings->getObject()["ssao"];
		if (ssaoDict.valid()) {
			ssao()->setEnabled(ssaoDict->getObject()["enabled"]->getBoolean());
			ssao()->setKernelSize(static_cast<int>(ssaoDict->getObject()["kernelSize"]->getNumber()));
			ssao()->setSampleRadius(ssaoDict->getObject()["sampleRadius"]->getNumber());
			float r = ssaoDict->getObject()["color"]->getObject()["r"]->getNumber();
			float g = ssaoDict->getObject()["color"]->getObject()["g"]->getNumber();
			float b = ssaoDict->getObject()["color"]->getObject()["b"]->getNumber();
			float a = ssaoDict->getObject()["color"]->getObject()["a"]->getNumber();
			ssao()->setColor(Color(r,g,b,a));
			ssao()->setBlurIterations(static_cast<int>(ssaoDict->getObject()["blurIterations"]->getNumber()));
			ssao()->setMaxDistance(ssaoDict->getObject()["maxDistance"]->getNumber());
		}

		ptr<Dictionary> ssrtDict = settings->getObject()["ssrt"];
        if (ssrtDict.valid() && ssrt()) {
			int quality = static_cast<int>(ssrtDict->getObject()["quality"]->getNumber());
			if (quality==0) {
				quality = SSRayTraceMaterial::kQualityHigh;
			}
			ssrt()->setQuality(static_cast<SSRayTraceMaterial::Quality>(quality));
		}
        else if (ssrt()) {
			ssrt()->setQuality(SSRayTraceMaterial::kQualityHigh);
		}

		ptr<Dictionary> shadowsDict = settings->getObject()["shadows"];
		if (shadowsDict.valid()) {
			ptr<Dictionary> type = shadowsDict->getObject()["type"];
			std::string shadowType = type->getString();
			if (shadowType=="hard") {
				shadows()->setShadowType(ShadowRenderPassMaterial::kShadowTypeHard);
			}
			else if (shadowType=="soft") {
				shadows()->setShadowType(ShadowRenderPassMaterial::kShadowTypeSoft);
			}
			else if (shadowType=="stratified") {
				shadows()->setShadowType(ShadowRenderPassMaterial::kShadowTypeSoftStratified);
			}
			int quality = static_cast<int>(shadowsDict->getObject()["quality"]->getNumber());
			setShadowQuality(Size2Di(quality!=0 ? quality:512));
			int blur = static_cast<int>(shadowsDict->getObject()["blurIterations"]->getNumber());
			shadows()->setBlurIterations(blur!=0 ? blur:1);
		}

		ptr<Dictionary> canvasColorDict = settings->getObject()["canvasColor"];
		if (canvasColorDict.valid()) {
			float r = canvasColorDict->getObject()["r"]->getNumber();
			float g = canvasColorDict->getObject()["g"]->getNumber();
			float b = canvasColorDict->getObject()["b"]->getNumber();
			float a = canvasColorDict->getObject()["a"]->getNumber();
			_canvasColor = vwgl::Color(r,g,b,a);
			highQualityRenderer().setClearColor(_canvasColor);
			lowQualityRenderer().setClearColor(_canvasColor);
		}

		ptr<Dictionary> aa = settings->getObject()["antialiasing"];
		if (aa.valid()) {
			setAntialiasingEnabled(aa->getBoolean());
		}
		else {
			setAntialiasingEnabled(true);
		}

		ptr<Dictionary> matLib = settings->getObject()["materialLibraryPath"];
		if (matLib.valid()) {
			_materialLibraryPath = matLib->getString();
		}

		ptr<Dictionary> tess = settings->getObject()["tessellation"];
		if (tess.valid()) {
			setTessellationEnabled(tess->getBoolean());
		}
		else {
			setTessellationEnabled(true);
		}
	}
	else {
		// Setup default settings
        if (ssao()) {
            ssao()->setEnabled(true);
            ssao()->setKernelSize(16);
            ssao()->setSampleRadius(0.34f);
            ssao()->setBlurIterations(1);
            ssao()->setMaxDistance(100);
        }

        if (ssrt()) {
            ssrt()->setQuality(SSRayTraceMaterial::kQualityHigh);
        }


		shadows()->setShadowType(ShadowRenderPassMaterial::kShadowTypeHard);
		shadows()->setBlurIterations(1);
		setShadowQuality(Size2Di(1024));

		_canvasColor = Color(43.0f/255.0f,47.0f/255.0f,57.0f/255.0f,1.0f);
		highQualityRenderer().setClearColor(_canvasColor);
		lowQualityRenderer().setClearColor(_canvasColor);

		setAntialiasingEnabled(true);
		setTessellationEnabled(true);
	}

	for (auto ext : _settingsExtensions) {
		ext->onLoadSettings(settings.getPtr());
	}
}

vwgl::Graphics::Api Settings::getApi() {
    std::string path = getSettingsPath();
    std::cout << "Loading API settings from " << path << std::endl;

    using namespace vwgl;
    Loader::get()->registerReader(new vwgl::DictionaryLoader());
    ptr<Dictionary> settings = Loader::get()->loadDictionary(path);
    if (settings.valid()) {
        ptr<Dictionary> api = settings->getObject()["api"];
        if (api.valid() && !api->getString().empty()) {
            std::string apiStr = api->getString();
            if (apiStr=="kApiOpenGL") {
                _api = vwgl::Graphics::kApiOpenGL;
            }
            else if (apiStr=="kApiOpenGLBasic") {
                _api = vwgl::Graphics::kApiOpenGLBasic;
            }
            else {
                _api = vwgl::Graphics::kApiOpenGLAdvanced;
            }
        }
    }

    return _api;
}

std::string Settings::getSettingsPath() {
	std::string execPath = vwgl::System::get()->getExecutablePath();
	if (vwgl::System::get()->isMac()) {
		execPath += "../../../";
	}
	execPath += "settings.json";
	return execPath;
}

}
