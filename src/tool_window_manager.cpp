#include <bg2e/tool_window_manager.hpp>

#include <QDockWidget>

namespace bg2e {

ToolWindowManager::ToolWindowManager(QMainWindow * mainWindow)
	:_mainWindow(mainWindow)
{
}

void ToolWindowManager::addToolWindow(const QString & name, DockSide side, ui::ToolWindow * wnd)  {
	wnd->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetClosable);
	wnd->setAllowedAreas(Qt::RightDockWidgetArea | Qt::LeftDockWidgetArea | Qt::BottomDockWidgetArea);
	_toolWindows[name] = wnd;
	switch (side) {
	case kDockLeft:
		_mainWindow->addDockWidget(Qt::LeftDockWidgetArea, wnd);
		break;
	case kDockRight:
		_mainWindow->addDockWidget(Qt::RightDockWidgetArea, wnd);
		break;
	case kDockBottom:
		_mainWindow->addDockWidget(Qt::BottomDockWidgetArea, wnd);
		break;
	}
}

void ToolWindowManager::tabify(ui::ToolWindow * first, ui::ToolWindow * second) {
	_mainWindow->tabifyDockWidget(first, second);
}

}
