
#include <bg2e/save_thread.hpp>


#include <QVBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QMessageBox>
#include <QApplication>

namespace bg2e {

bool PostErrorWrapper::event(QEvent * event) {
	if (event->type()==QEvent::User) {
		QMessageBox::warning(nullptr, QObject::tr("Error"), _message);
		return true;
	}
	return false;
}

void SaveThread::showProgress() {
	if (!_progressDialog) {
		_progressDialog = new QDialog();

		QVBoxLayout * layout = new QVBoxLayout();
		_progressDialog->setLayout(layout);

		layout->addWidget(new QLabel("Processing..."));

		QProgressBar * progress = new QProgressBar();
		layout->addWidget(progress);
		progress->setMinimumWidth(200);
		progress->setMinimum(0);
		progress->setMaximum(0);
		_progressDialog->setModal(true);
		_progressDialog->setWindowFlags( ( (_progressDialog->windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowCloseButtonHint) );

		_progressDialog->show();
	}

}

void SaveThread::hideProgress() {
	if (_progressDialog) {
		_progressDialog->close();
		delete _progressDialog;
		_progressDialog = nullptr;
	}
}

void SaveThread::postErrorMessage(const QString & message) {
	PostErrorWrapper err(message);
	err.moveToThread(QApplication::instance()->thread());

	QCoreApplication::postEvent(&err, new QEvent(QEvent::User));
}

void SaveThread::setDoneCallback(std::function<void()> cb) {
	_done.setCallback(cb);
}

void SaveThread::setErrorCallback(std::function<void()> cb) {
	_error.setCallback(cb);
}

void SaveThread::run() {
	_saveStatus = false;
	if (_threadCallback) { _threadCallback(); }

	if (_saveStatus) {
		QCoreApplication::postEvent(&_done, new QEvent(QEvent::User));
	}
	else if (!_saveStatus) {
		QCoreApplication::postEvent(&_error, new QEvent(QEvent::User));
	}
}


}
