
#include <bg2e/version.hpp>

#include <sstream>

#include "../version"

namespace bg2e {

int Version::s_major = s_bg2e_version_major;
int Version::s_minor = s_bg2e_version_minor;
int Version::s_build = s_bg2e_version_build;
std::string Version::s_versionString;
std::string Version::s_revision;

Version::Version() {

}

Version::~Version() {

}

const std::string & Version::getRevision() {
	return s_bg2e_version_revision;
}

const std::string & Version::getVersionString() {
	if (s_versionString=="") {
		std::stringstream sstream;
		sstream << s_major << "." << s_minor << "." << s_build << " " << Version::getRevision();
		s_versionString = sstream.str();
	}
	return s_versionString;
}

}
