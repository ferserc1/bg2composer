
#include <QGroupBox>

#include <QVBoxLayout>
#include <QMessageBox>

#include <bg2e/settings_window.hpp>
#include <bg2e/settings.hpp>

#include <vwgl/app/message_box.hpp>
#include <vwgl/ss_raytrace.hpp>

namespace bg2e {

SettingsWindow::SettingsWindow(Settings * settings)
	:QDialog(0)
	,_settings(settings)
{
	setModal(false);
    setWindowFlags(Qt::WindowStaysOnTopHint);
	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);

    _api = new ui::ComboBoxField(tr("Render API"));
    _api->addItem("OpenGL");
    _api->addItem("OpenGL Advanced");
    _api->onComboBoxChanged([&](ui::ComboBoxField * field) {
        vwgl::Graphics::Api api;
        switch (field->currentIndex()) {
        case 0:
            api = vwgl::Graphics::kApiOpenGL;
            break;
        case 1:
            api = vwgl::Graphics::kApiOpenGLAdvanced;
            break;
        }
        _settings->setApi(api);
        QMessageBox::information(this, QObject::tr("Restart needed"), QObject::tr("You need to restart bg2 Composer to use the new API"));
    });
    this->layout()->addWidget(_api);

	buildShadowSettings();
	buildSSAOSettings();
	buildOtherSettings();
	updateUISettings();
}

void SettingsWindow::buildSSAOSettings() {
	QGroupBox * ssaoBox = new QGroupBox("Screen Space Ambient Occlusion");
	QVBoxLayout * layout = new QVBoxLayout();
	ssaoBox->setLayout(layout);
	this->layout()->addWidget(ssaoBox);

	_ssaoEnabled = new ui::BooleanField(tr("Enabled"));
	_ssaoEnabled->onValueChanged([&](ui::BooleanField * field) {
		_settings->ssao()->setEnabled(field->value());
		updateSSAO();
	});
	layout->addWidget(_ssaoEnabled);

	_ssaoKernelSize = new ui::SliderField(tr("Kernel size"));
	_ssaoKernelSize->setRange(4,64);
	_ssaoKernelSize->setValue(64);
	_ssaoKernelSize->onSliderChanged([&](ui::SliderField * field) {
		_settings->ssao()->setKernelSize(field->value());
		updateGL();
	});
	layout->addWidget(_ssaoKernelSize);

	_ssaoSampleRadius = new ui::FloatSliderField(tr("Sample radius"));
	_ssaoSampleRadius->setRange(0.0f, 5.0f);
	_ssaoSampleRadius->setValue(0.25f);
	_ssaoSampleRadius->onSliderChanged([&](ui::FloatSliderField * field) {
		_settings->ssao()->setSampleRadius(field->value());
		updateGL();
	});
	layout->addWidget(_ssaoSampleRadius);

	_ssaoColor = new ui::ColorField("Color");
	_ssaoColor->setColor(vwgl::Color::black());
	_ssaoColor->onColorChanged([&](ui::ColorField * field) {
		_settings->ssao()->setColor(field->getEngineColor());
		updateGL();
	});
	layout->addWidget(_ssaoColor);

	_ssaoBlurIterations = new ui::SliderField("Blur iterations");
	_ssaoBlurIterations->setRange(1,8);
	_ssaoBlurIterations->setValue(4);
	_ssaoBlurIterations->onSliderChanged([&](ui::SliderField * field) {
		_settings->ssao()->setBlurIterations(field->value());
		updateGL();
	});
	layout->addWidget(_ssaoBlurIterations);

	_ssaoMaxDistance = new ui::FloatSliderField("Maximum distance");
	_ssaoMaxDistance->setRange(0.0f, 100.0f);
	_ssaoMaxDistance->setValue(50.0f);
	_ssaoMaxDistance->onSliderChanged([&](ui::FloatSliderField * field) {
		_settings->ssao()->setMaxDistance(field->value());
		updateGL();
	});
	layout->addWidget(_ssaoMaxDistance);


	layout->addSpacerItem(new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Expanding));
}

void SettingsWindow::buildShadowSettings() {
	QGroupBox * shadowBox = new QGroupBox("Shadows");
	QVBoxLayout * layout = new QVBoxLayout();
	shadowBox->setLayout(layout);
	this->layout()->addWidget(shadowBox);

	_shadowQuality = new ui::ComboBoxField("Shadow Map Quality");
	_shadowQuality->addItem("Low (512x512)");
	_shadowQuality->addItem("Medium (1024x1024)");
	_shadowQuality->addItem("High (2048x2048)");
	_shadowQuality->addItem("Very High (4096x4096)");
	_shadowQuality->addItem("Extreme (8192x8192)");
	_shadowQuality->onComboBoxChanged([&](ui::ComboBoxField * field) {
		vwgl::Size2Di quality;
		switch (field->currentIndex()) {
		case 0:
			quality.set(512,512);
			break;
		case 1:
			quality.set(1024,1024);
			break;
		case 2:
			quality.set(2048,2048);
			break;
		case 3:
			quality.set(4096,4096);
			break;
		case 4:
			quality.set(8192,8192);
			break;
		}
		_settings->setShadowQuality(quality);
		updateGL();
	});

	layout->addWidget(_shadowQuality);

	_shadowType = new ui::ComboBoxField("Shadow type");
	_shadowType->addItem("Hard shadows");
	_shadowType->addItem("Soft shadows");
	_shadowType->addItem("Stratified shadows");
	_shadowType->onComboBoxChanged([&](ui::ComboBoxField * field) {
		auto type = vwgl::ShadowRenderPassMaterial::kShadowTypeHard;
		if (field->currentIndex()==1) {
			type = vwgl::ShadowRenderPassMaterial::kShadowTypeSoft;
		}
		else if (field->currentIndex()==2) {
			type = vwgl::ShadowRenderPassMaterial::kShadowTypeSoftStratified;
		}
		_settings->shadows()->setShadowType(type);
		updateGL();
	});
	layout->addWidget(_shadowType);

	_shadowBlurIterations = new ui::SliderField("Blur iterations");
	_shadowBlurIterations->setRange(0,8);
	_shadowBlurIterations->onSliderChanged([&](ui::SliderField * field) {
		_settings->shadows()->setBlurIterations(field->value());
		updateGL();
	});
	layout->addWidget(_shadowBlurIterations);
}

void SettingsWindow::buildOtherSettings() {
	QGroupBox * otherBox = new QGroupBox("Other settings");
	QVBoxLayout * layout = new QVBoxLayout();
	otherBox->setLayout(layout);
	this->layout()->addWidget(otherBox);

	_canvasColor = new ui::ColorField("Canvas color");
	_canvasColor->onColorChanged([&](ui::ColorField * field) {
		_settings->setCanvasColor(field->getEngineColor());
		updateGL();
	});
	layout->addWidget(_canvasColor);

	_antialiasing = new ui::BooleanField("Anti Aliasing");
	_antialiasing->onValueChanged([&](ui::BooleanField * field) {
		_settings->setAntialiasingEnabled(field->value());
		updateGL();
	});
	layout->addWidget(_antialiasing);

	_tessellation = new ui::BooleanField("Tessellation");
	_tessellation->onValueChanged(([&](ui::BooleanField * field) {
		_settings->setTessellationEnabled(field->value());
		updateGL();
	}));
	layout->addWidget(_tessellation);

	_reflectionQuality = new ui::ComboBoxField("Reflections Quality");
	_reflectionQuality->addItem("Low");
	_reflectionQuality->addItem("Medium");
	_reflectionQuality->addItem("High");
	_reflectionQuality->addItem("Extreme");
	_reflectionQuality->onComboBoxChanged([&](ui::ComboBoxField * field) {
		switch (field->currentIndex()) {
		case 0:
			_settings->setReflectionsQuality(vwgl::SSRayTraceMaterial::kQualityLow);
			break;
		case 1:
			_settings->setReflectionsQuality(vwgl::SSRayTraceMaterial::kQualityMedium);
			break;
		case 2:
			_settings->setReflectionsQuality(vwgl::SSRayTraceMaterial::kQualityHigh);
			break;
		case 3:
			_settings->setReflectionsQuality(vwgl::SSRayTraceMaterial::kQualityExtreme);
			break;
		}
		updateGL();
	});
	layout->addWidget(_reflectionQuality);
}

void SettingsWindow::updateOtherSettings() {
	_canvasColor->setColor(_settings->getCanvasColor());
	_antialiasing->setValue(_settings->isAntialiasingEnabled());
	_tessellation->setValue(_settings->isTessellationEnabled());
	switch (_settings->getReflectionsQuality()) {
	case vwgl::SSRayTraceMaterial::kQualityLow:
		_reflectionQuality->setCurrentIndex(0);
		break;
	case vwgl::SSRayTraceMaterial::kQualityMedium:
		_reflectionQuality->setCurrentIndex(1);
		break;
	case vwgl::SSRayTraceMaterial::kQualityHigh:
		_reflectionQuality->setCurrentIndex(2);
		break;
	case vwgl::SSRayTraceMaterial::kQualityExtreme:
		_reflectionQuality->setCurrentIndex(3);
		break;
	default:
		break;
	}
	updateGL();
}

void SettingsWindow::updateSSAO() {
	vwgl::SSAOMaterial * ssao = _settings->ssao();
	_ssaoEnabled->setValue(ssao->isEnabled());
	_ssaoKernelSize->setEnabled(ssao->isEnabled());
	_ssaoSampleRadius->setEnabled(ssao->isEnabled());
	_ssaoColor->setEnabled(ssao->isEnabled());
	_ssaoBlurIterations->setEnabled(ssao->isEnabled());
	_ssaoMaxDistance->setEnabled(ssao->isEnabled());

	_ssaoKernelSize->setValue(ssao->getKernelSize());
	_ssaoSampleRadius->setValue(ssao->getSampleRadius());
	_ssaoColor->setColor(ssao->getColor());
	_ssaoBlurIterations->setValue(ssao->getBlurIterations());
	_ssaoMaxDistance->setValue(ssao->getMaxDistance());
	updateGL();
}

void SettingsWindow::updateShadowSettings() {
	switch (_settings->getShadowQuality().width()) {
	case 512:
		_shadowQuality->setCurrentIndex(0);
		break;
	case 1024:
		_shadowQuality->setCurrentIndex(1);
		break;
	case 2048:
		_shadowQuality->setCurrentIndex(2);
		break;
	case 4096:
		_shadowQuality->setCurrentIndex(3);
		break;
	case 8192:
		_shadowQuality->setCurrentIndex(4);
		break;
	}

	switch (_settings->shadows()->getShadowType()) {
	case vwgl::ShadowRenderPassMaterial::kShadowTypeHard:
		_shadowType->setCurrentIndex(0);
		break;
	case vwgl::ShadowRenderPassMaterial::kShadowTypeSoft:
		_shadowType->setCurrentIndex(1);
		break;
	case vwgl::ShadowRenderPassMaterial::kShadowTypeSoftStratified:
		_shadowType->setCurrentIndex(2);
		break;
	}

	_shadowBlurIterations->setValue(_settings->shadows()->getBlurIterations());
}

void SettingsWindow::updateUISettings() {
    switch (_settings->getApi()) {
    case vwgl::Graphics::kApiOpenGL:
        _api->setCurrentIndex(0);
        break;
    case vwgl::Graphics::kApiOpenGLAdvanced:
        _api->setCurrentIndex(1);
        break;
    }

	updateSSAO();
	updateShadowSettings();
	updateOtherSettings();
	updateGL();
}

void SettingsWindow::updateGL() {
	_settings->notifySettingsChanged();
	_settings->saveSettings();
}

}
