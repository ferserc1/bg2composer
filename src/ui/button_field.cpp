
#include <bg2e/ui/button_field.hpp>

namespace bg2e {
namespace ui {

ButtonField::ButtonField(const QString & title)
	:EditField()
{
	build();
	setLabel("");
	setTitle(title);
}

void ButtonField::build() {
	_button = new QPushButton();
	layout()->addWidget(_button);
	hboxLayout()->addSpacerItem(new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Minimum));
	connect(_button,SIGNAL(clicked()),this,SLOT(btnClicked()));
}

void ButtonField::btnClicked() {
	if (_onButtonPress) {
		_onButtonPress();
	}
	emit(clicked());
}

}
}
