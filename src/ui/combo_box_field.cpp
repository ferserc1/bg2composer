#include <bg2e/ui/combo_box_field.hpp>

#include <iostream>

namespace bg2e {
namespace ui {

ComboBoxField::ComboBoxField()
	:EditField()
{
	build();
}

ComboBoxField::ComboBoxField(const QString & label)
	:EditField()
{
	build();
	setLabel(label);
}

void ComboBoxField::addItem(const QString &item) {
	_comboBox->addItem(item);
}

void ComboBoxField::build() {
	_comboBox = new QComboBox();
	connect(_comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(indexChanged(int)));
	layout()->addWidget(_comboBox);
	hboxLayout()->addSpacerItem(new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Minimum));
}

void ComboBoxField::indexChanged(int) {
	if (!signalsBlocked()) {
		if (_onComboBoxChanged) {
			_onComboBoxChanged(this);
		}
		emit(comboBoxChanged(this));
	}
}

}
}
