
#include <bg2e/ui/vector_field.hpp>

#include <QDoubleValidator>
#include <float.h>

namespace bg2e {
namespace ui {

VectorLabels::VectorLabels()
	:EditField()
{
	setLabel("");
	build();
}

VectorLabels::VectorLabels(const QString & label)
	:EditField()
{
	setLabel(label);
	build();
}

void VectorLabels::setComponents(int c) {
	_components = c;
	switch (_components) {
	case 4:
		_w->setVisible(true);
	case 3:
		_z->setVisible(true);
	case 2:
		_y->setVisible(true);
	case 1:
		_x->setVisible(true);
		break;

	default:
		break;
	}
}

void VectorLabels::build() {
	QString qss = "QLabel { qproperty-alignment: 'AlignCenter'; }";
	_x = new QLabel("x");
	_x->setStyleSheet(qss);
	_y = new QLabel("y");
	_y->setStyleSheet(qss);
	_z = new QLabel("z");
	_z->setStyleSheet(qss);
	_w = new QLabel("w");
	_w->setStyleSheet(qss);
	layout()->addWidget(_x);
	layout()->addWidget(_y);
	layout()->addWidget(_z);
	layout()->addWidget(_w);
	_x->setMinimumHeight(25);
	setComponents(4);
}

VectorField::VectorField()
	:_preventUpdate(false)
{
	build();
}

VectorField::VectorField(const QString & label)
	:_preventUpdate(false)
{
	build();
	setLabel(label);
	setComponentLabel(0,"x");
	setComponentLabel(1,"y");
	setComponentLabel(2,"z");
	setComponentLabel(3,"w");
	setVector(vwgl::Vector4(0.0f));
}

void VectorField::setKeyIncrement(float inc) {
	for (int i=0; i<4; ++i) {
		_fields[i]->setKeyIncrement(inc);
	}
}

void VectorField::setWheelIncrement(float inc) {
	for (int i=0; i<4; ++i) {
		_fields[i]->setWheelIncrement(inc);
	}
}

void VectorField::build() {
	for (int i=0; i<4; ++i) {
		_fields[i] = new NumericTextWidget();
		_labels[i] = new QLabel();
		layout()->addWidget(_labels[i]);
		layout()->addWidget(_fields[i]);
	}
	connect(_fields[0],SIGNAL(textChanged(QString)),this,SLOT(xChanged(QString)));
	connect(_fields[1],SIGNAL(textChanged(QString)),this,SLOT(yChanged(QString)));
	connect(_fields[2],SIGNAL(textChanged(QString)),this,SLOT(zChanged(QString)));
	connect(_fields[3],SIGNAL(textChanged(QString)),this,SLOT(wChanged(QString)));
}

void VectorField::configureField() {
	if (!_preventUpdate) {
		disableSignals();
		switch (_type) {
		case kVector2:
			_fields[0]->setText(QString::number(_vector2.x()));
			_fields[1]->setText(QString::number(_vector2.y()));
			_fields[2]->hide();
			_labels[2]->hide();
			_fields[3]->hide();
			_labels[3]->hide();
			break;
		case kVector3:
			_fields[0]->setText(QString::number(_vector3.x()));
			_fields[1]->setText(QString::number(_vector3.y()));
			_fields[2]->setText(QString::number(_vector3.z()));
			_fields[2]->show();
			_labels[2]->show();
			_fields[3]->hide();
			_labels[3]->hide();
			break;
		case kVector4:
			_fields[0]->setText(QString::number(_vector4.x()));
			_fields[1]->setText(QString::number(_vector4.y()));
			_fields[2]->setText(QString::number(_vector4.z()));
			_fields[3]->setText(QString::number(_vector4.w()));
			_fields[2]->show();
			_labels[2]->show();
			_fields[3]->show();
			_labels[3]->show();
			break;
		}
		_fields[0]->setCursorPosition(0);
		_fields[1]->setCursorPosition(0);
		_fields[2]->setCursorPosition(0);
		_fields[3]->setCursorPosition(0);
		enableSignals();
	}
}

void VectorField::disableSignals() {
	for (int i=0; i<4; ++i) {
		_fields[i]->blockSignals(true);
	}
}

void VectorField::enableSignals() {
	for (int i=0; i<4; ++i) {
		_fields[i]->blockSignals(false);
	}
}

void VectorField::xChanged(const QString &) {
	sendSignals();
}

void VectorField::yChanged(const QString &) {
	sendSignals();
}

void VectorField::zChanged(const QString &) {
	sendSignals();
}

void VectorField::wChanged(const QString &) {
	sendSignals();
}

void VectorField::sendSignals() {
	_preventUpdate = true;
	_vector4.set(_fields[0]->text().toFloat(),
				_fields[1]->text().toFloat(),
				_fields[2]->text().toFloat(),
				_fields[3]->text().toFloat());
	_vector3 = _vector4.xyz();
	_vector2 = _vector4.xy();
	emit(vectorChanged(this));
	if (_onVectorChanged) {
		_onVectorChanged(this);
	}
	_preventUpdate = false;
}

}
}
