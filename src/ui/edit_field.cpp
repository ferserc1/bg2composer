
#include <bg2e/ui/edit_field.hpp>

#include <QHBoxLayout>

namespace bg2e {
namespace ui {

EditField::EditField()
	:QWidget(nullptr)
{
	QHBoxLayout * layout = new QHBoxLayout();
	layout->setMargin(2);
	setLayout(layout);

	_label = new QLabel("Label");
	_label->setAlignment(Qt::AlignRight);
	_label->setMaximumWidth(100);
	_label->setMinimumWidth(100);
	layout->addWidget(_label);
}
	
}
}
