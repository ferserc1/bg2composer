
#include <bg2e/ui/file_field.hpp>

#include <QFileDialog>
#include <QFileInfo>

namespace bg2e {
namespace ui {

QString FileField::s_defaultPath = "";

QString FileField::openFileDialog(const QString & title, const QString & filter) {
	QString fileName = QFileDialog::getOpenFileName(nullptr,title, s_defaultPath, filter);
	if (!fileName.isEmpty()) {
		QFileInfo info(fileName);
		s_defaultPath = info.absolutePath();
	}
	return fileName;
}

QString FileField::saveFileDialog(const QString & title, const QString & filter) {
	QString fileName = QFileDialog::getSaveFileName(nullptr, title,s_defaultPath,filter);
	if (!fileName.isEmpty()) {
		QFileInfo info(fileName);
		s_defaultPath = info.absolutePath();
	}
	return fileName;
}

QString FileField::openDirectoryDialog(const QString & title) {
	QString fileName = QFileDialog::getExistingDirectory(nullptr, title, s_defaultPath);
	if (!fileName.isEmpty()) {
		s_defaultPath = fileName;
	}
	return fileName;
}

std::string FileField::stdStringPath(const QString & path) {
#ifdef Q_OS_WIN
     std::string utfPath = path.toLocal8Bit().constData();
#else
     std::string utfPath = path.toStdString();
#endif
     return utfPath;
}

FileField::FileField()
	:EditField()
	,_dialogMessage("Open file")
	,_pickFolderMode(false)
{
	build();
}

FileField::FileField(const QString & label)
	:EditField()
	,_dialogMessage("Open file")
	,_pickFolderMode(false)
{
	build();
	setLabel(label);
}

void FileField::build() {
	_lineEdit = new QLineEdit();
	connect(_lineEdit,SIGNAL(textChanged(QString)),this,SLOT(textChanged(QString)));
	hboxLayout()->addWidget(_lineEdit);

	_btn = new QPushButton(tr("Select"));
	hboxLayout()->addWidget(_btn);
	connect(_btn,SIGNAL(clicked()),this,SLOT(pickButtonClick()));
}

void FileField::pickButtonClick() {
	QString path = _pickFolderMode ? openDirectoryDialog(_dialogMessage) : openFileDialog(_dialogMessage,_filter);
	if (!path.isEmpty()) {
		_lineEdit->setText(path);
	}
}

void FileField::textChanged(const QString &) {
	if (_onFileChanged) {
		_onFileChanged(this);
	}
	emit(fileChanged(this));
}

}
}
