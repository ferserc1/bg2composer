
#include <bg2e/ui/about_panel.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

namespace bg2e {
namespace ui {

AboutPanel::AboutPanel(const QString & title, const QString & header, const QString & body, const QString & version, const QString & icon)
	:QDialog(nullptr)
{
	setWindowTitle(title);

	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);

	QLabel * lb = new QLabel(header);
	lb->setStyleSheet("font-size: 19px;");
	layout->addWidget(lb);

	QWidget * logo = new QWidget();
	QHBoxLayout * logoLo = new QHBoxLayout();
	logo->setLayout(logoLo);
	logoLo->addSpacerItem(new QSpacerItem(10,10,QSizePolicy::Expanding, QSizePolicy::Expanding));
	lb = new QLabel();
	lb->setPixmap(QPixmap(icon));
	logoLo->addWidget(lb);
	logoLo->addSpacerItem(new QSpacerItem(10,10,QSizePolicy::Expanding,QSizePolicy::Expanding));
	layout->addWidget(logo);
	logo->setStyleSheet("background-color: transparent;");


	lb = new QLabel(body);
	lb->setWordWrap(true);
	lb->setStyleSheet("font-size: 10px;");
	layout->addWidget(lb);

#ifdef Q_OS_MAC
	QString platform = "OS X";
#elif defined(Q_OS_WIN32)
	QString platform = "Windows";
#endif
	lb = new QLabel(version + " - " + platform);
	layout->addWidget(lb);

	layout->addSpacerItem(new QSpacerItem(20,20,QSizePolicy::Expanding,QSizePolicy::Expanding));
	setModal(true);
}

void AboutPanel::mousePressEvent(QMouseEvent *) {
	close();
}

void AboutPanel::logoClicked() {
	close();
}

}
}
