
#include <iostream>

#include <QMouseEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <bg2e/ui/tree_view.hpp>


namespace bg2e {
namespace ui {

TreeView::TreeView()
	:QTreeView(nullptr)
{
}

void TreeView::mousePressEvent(QMouseEvent * event) {
	_sourceIndex = indexAt(event->pos());
	QTreeView::mousePressEvent(event);
}

void TreeView::mouseReleaseEvent(QMouseEvent * event) {
	_destinationIndex = QModelIndex();
	_sourceIndex = QModelIndex();
	QTreeView::mouseReleaseEvent(event);
}

void TreeView::dropEvent(QDropEvent * event) {
	_destinationIndex = indexAt(event->pos());

	if (_sourceIndex.isValid()) {
		emit(moveNode(_sourceIndex,_destinationIndex));
	}
	_sourceIndex = QModelIndex();
	_destinationIndex = QModelIndex();
}

}
}
