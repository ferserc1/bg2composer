
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/closure_action.hpp>

#include <iostream>

namespace bg2e {
namespace ui {

MenuField::MenuField(const QString & title)
	:EditField()
{
	build();
	setLabel("");
	setTitle(title);
}

void MenuField::build() {
	_button = new QPushButton();
	_button->setMaximumHeight(22);
	layout()->addWidget(_button);
	_menu = new QMenu();
	_button->setMenu(_menu);
	hboxLayout()->addSpacerItem(new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Minimum));
}

void MenuField::addAction(QAction *action) {
	_menu->addAction(action);
}

void MenuField::addSeparator() {
	_menu->addSeparator();
}

}
}
