
#include <bg2e/ui/text_field.hpp>

#include <QVBoxLayout>

namespace bg2e {
namespace ui {

TextField::TextField()
	:EditField()
{
	build();
}

TextField::TextField(const QString & label)
	:EditField()
{
	build();
	setLabel(label);
}	

void TextField::build() {
	_lineEdit = new QLineEdit();
	layout()->addWidget(_lineEdit);
	_lineEdit->setMinimumHeight(22);
	_lineEdit->setMinimumWidth(180);
	connect(_lineEdit,SIGNAL(textChanged(QString)),this,SLOT(editTextChanged(QString)));
	connect(_lineEdit,SIGNAL(editingFinished()),this,SLOT(editTextFinished()));
}

void TextField::editTextChanged(const QString &) {
	if (_onTextChanged) {
		_onTextChanged(this);
	}
	emit(textChanged(this));
}

void TextField::editTextFinished() {
	if (_onEditFinished) {
		_onEditFinished(this);
	}
	emit(editFinished(this));
}

}
}
