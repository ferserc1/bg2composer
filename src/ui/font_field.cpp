
#include <bg2e/ui/font_field.hpp>

#include <vwgl/font_manager.hpp>
#include <vwgl/loader.hpp>

#include <QFileDialog>
#include <QFileInfo>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QButtonGroup>
#include <QIntValidator>

namespace bg2e {
namespace ui {

CreateFontWidget::CreateFontWidget(QWidget * parent)
	:QDialog(parent)
{
	setModal(true);
	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);

	_fontType = new ui::ComboBoxField("Font type");
	_fontType->addItem("Sans serif");
	_fontType->addItem("Serif");
	_fontType->addItem("Monospace serif");
	_fontType->addItem("Monospace sans serif");
	_fontType->addItem("Custom");
	_fontType->onComboBoxChanged([&](ui::ComboBoxField *) {
		if (_fontType->currentIndex()==4) {
			_customFont->show();
		}
		else {
			_customFont->hide();
		}
	});
	layout->addWidget(_fontType);

	_customFont = new ui::FileField("Select Font");
	_customFont->setFilter("*.ttf *.cff *.woff *.otf *.otc");
	_customFont->hide();
	layout->addWidget(_customFont);

	QIntValidator * validator = new QIntValidator();
	validator->setBottom(8);
	validator->setTop(300);
	_size = new ui::TextField("Size");
	_size->setValidator(new QIntValidator());
	_size->setText("50");
	layout->addWidget(_size);

	_resolution = new ui::TextField("Resolution");
	validator = new QIntValidator();
	validator->setBottom(64);
	validator->setTop(256);
	_resolution->setValidator(validator);
	_resolution->setText("128");
	layout->addWidget(_resolution);

	layout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Maximum,QSizePolicy::Expanding));

	QWidget * buttons = new QWidget();
	buttons->setStyleSheet("background-color: transparent; ");
	layout->addWidget(buttons);
	_acceptButton = new QPushButton(tr("Create"));
	_cancelButton = new QPushButton(tr("Cancel"));
	QHBoxLayout * buttonsLayout = new QHBoxLayout();
	buttons->setLayout(buttonsLayout);
	buttonsLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum));
	buttonsLayout->addWidget(_acceptButton);
	buttonsLayout->addWidget(_cancelButton);
	connect(_acceptButton,SIGNAL(clicked()),this,SLOT(acceptClicked()));
	connect(_cancelButton,SIGNAL(clicked()),this,SLOT(cancelClicked()));
}

void CreateFontWidget::acceptClicked() {
	if (_createFontClosure) {
		vwgl::ptr<vwgl::TextFont> newFont;
		int size = _size->text().toInt();
		int resolution = _resolution->text().toInt();
		std::string fontPath = "";
		switch (_fontType->currentIndex()) {
		case 0:
			fontPath = vwgl::TextFont::getDefaultFontPath(vwgl::TextFont::kTypeSansSerif);
			break;
		case 1:
			fontPath = vwgl::TextFont::getDefaultFontPath(vwgl::TextFont::kTypeSerif);
			break;
		case 2:
			fontPath = vwgl::TextFont::getDefaultFontPath(vwgl::TextFont::kTypeSerif | vwgl::TextFont::kTypeMonospace);
			break;
		case 3:
			fontPath = vwgl::TextFont::getDefaultFontPath(vwgl::TextFont::kTypeSansSerif | vwgl::TextFont::kTypeMonospace);
			break;
		case 4:
			fontPath = _customFont->getPath();
			break;
		}

		newFont = vwgl::Loader::get()->loadFont(fontPath, size, resolution);
		if (newFont.valid()) {
			_createFontClosure(newFont.getPtr());
			close();
		}
	}
	else {
		close();
	}
}

void CreateFontWidget::cancelClicked() {
	if (_cancelClosure) {
		_cancelClosure(nullptr);
	}
	close();
}

FontList * FontList::s_singleton = nullptr;

FontList * FontList::get() {
	if (s_singleton==nullptr) {
		s_singleton = new FontList();
	}
	return s_singleton;
}

FontList::FontList()
	:QDialog(nullptr)
	,_onFontSelected(nullptr)
	,_createFont(nullptr)
{
	setModal(true);
	hide();

	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);

	_list = new QListWidget();
	connect(_list,SIGNAL(currentRowChanged(int)),this,SLOT(fontSelectionChanged(int)));
	layout->addWidget(_list);

	QHBoxLayout * buttonLayout = new QHBoxLayout();
	_deleteUnused = new QPushButton(tr("Delete Unused"));
	_addButton = new QPushButton(tr("Add New"));
	_acceptButton = new QPushButton(tr("Accept"));
	_closeButton = new QPushButton(tr("Close"));
	QWidget * buttons = new QWidget();
	buttons->setStyleSheet("background-color: transparent;");
	buttons->setLayout(buttonLayout);
	layout->addWidget(buttons);
	buttonLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum));
	buttonLayout->addWidget(_deleteUnused);
	buttonLayout->addWidget(_addButton);
	buttonLayout->addWidget(_acceptButton);
	buttonLayout->addWidget(_closeButton);
	connect(_deleteUnused, SIGNAL(clicked()), this, SLOT(deleteUnused()));
	connect(_addButton, SIGNAL(clicked()), this, SLOT(addClicked()));
	connect(_acceptButton, SIGNAL(clicked()), this, SLOT(selectClicked()));
	connect(_closeButton, SIGNAL(clicked()), this, SLOT(closeClicked()));
}

FontList::~FontList()
{

}

void FontList::show() {
	QDialog::show();
	reloadFonts();
}

void FontList::reloadFonts() {
	_list->clear();
	vwgl::FontManager::get()->eachFont([&](const std::string & fontName, vwgl::TextFont *) {
		_list->addItem(QString::fromStdString(fontName));
	});
}

vwgl::TextFont * FontList::selectedFont() {
	QListWidgetItem * item = _list->currentItem();
	vwgl::TextFont * font = nullptr;
	if (item) {
		std::string fontName = item->text().toStdString();
		font = vwgl::FontManager::get()->getFont(fontName);
	}
	return font;
}

void FontList::deleteUnused() {
	vwgl::FontManager::get()->clearUnused();
	reloadFonts();
}

void FontList::addClicked() {
	if (!_createFont) {
		_createFont = new CreateFontWidget(this);
	}
	_createFont->show();
	_createFont->onCreateFont([&](vwgl::TextFont *) {
		reloadFonts();
	});

	_createFont->onCancel([&](vwgl::TextFont *) {

	});
}

void FontList::closeClicked() {
	hide();
}

void FontList::selectClicked() {
	vwgl::TextFont * font = selectedFont();
	if (font && _onFontSelected) {
		_onFontSelected(_list->currentItem()->text().toStdString(), font);
	}
	hide();
}

void FontList::fontSelectionChanged(int) {
	if (selectedFont()) {
		_acceptButton->setEnabled(true);
	}
	else {
		_acceptButton->setEnabled(false);
	}
}

vwgl::TextFont * FontList::getDefaultFont() {
	vwgl::ptr<vwgl::TextFont> defaultFont;
	vwgl::FontManager::get()->eachFont([&](const std::string &, vwgl::TextFont * font) {
		if (!defaultFont.valid()) {
			defaultFont = font;
		}
	});

	if (!defaultFont.valid()) {
		defaultFont = vwgl::Loader::get()->loadFont(vwgl::TextFont::getDefaultFontPath(), 50, 128);
	}
	return defaultFont.release();
}


FontField::FontField()
	:EditField()
{
	build();
}

FontField::FontField(const QString & label)
	:EditField()
{
	build();
	setLabel(label);
}

void FontField::build() {
	_fontName = new QLabel();
	hboxLayout()->addWidget(_fontName);

	_btn = new QPushButton(tr("Select"));
	hboxLayout()->addWidget(_btn);
	connect(_btn,SIGNAL(clicked()),this,SLOT(pickButtonClick()));
}

void FontField::setFont(vwgl::TextFont * font) {
	std::string fontName = "";
	vwgl::FontManager::get()->eachFont([&](const std::string &name, vwgl::TextFont * f) {
		if (font==f) {
			fontName = name;
		}
	});

	_font = font;
	_fontName->setText(QString::fromStdString(fontName));
}

void FontField::setFontName(const std::string & name) {
	_font = nullptr;
	vwgl::FontManager::get()->eachFont([&](const std::string & n, vwgl::TextFont * f) {
		if (name==n) {
			_font = f;
		}
	});
	_fontName->setText(QString::fromStdString(name));
}

void FontField::pickButtonClick() {
	FontList::get()->show();
	FontList::get()->onFontSelected([&](const std::string & name, vwgl::TextFont * font) {
		_fontName->setText(QString::fromStdString(name));
		_font = font;
		if (_onFontChanged) {
			_onFontChanged(this);
		}
	});
}


}
}
