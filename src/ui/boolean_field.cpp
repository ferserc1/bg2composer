
#include <bg2e/ui/boolean_field.hpp>

namespace bg2e {
namespace ui {

BooleanField::BooleanField()
	:EditField()
{
	build();
	setLabel("");
	setCheckLabel("Boolean Field");
}

BooleanField::BooleanField(const QString & label)
	:EditField()
{
	build();
	setLabel("");
	setCheckLabel(label);
}

void BooleanField::build()
{
	_checkBox = new QCheckBox();
	layout()->addWidget(_checkBox);
	connect(_checkBox,SIGNAL(stateChanged(int)),this,SLOT(checkStateChanged(int)));
}

void BooleanField::checkStateChanged(int) {
	if (_onValueChanged) {
		_onValueChanged(this);
	}
	emit(valueChanged(this));
}


}
}
