
#include <bg2e/ui/scene_tree_model.hpp>
#include <bg2e/ui/scene_tree_item.hpp>

namespace bg2e {
namespace ui {

using namespace vwgl;

SceneTreeModel::SceneTreeModel(vwgl::scene::Node * root, QObject * parent)
	:QAbstractItemModel(parent)
	,_sceneRoot(root)
{

}

SceneTreeModel::~SceneTreeModel()
{

}

void SceneTreeModel::refresh() {
	emit(layoutAboutToBeChanged());
	emit(layoutChanged());
}

QVariant SceneTreeModel::data(const QModelIndex & index, int role) const {
	if (!index.isValid()) {
		return QVariant();
	}

	if (role != Qt::DisplayRole) {
		return QVariant();
	}

	vwgl::scene::Node * item = static_cast<vwgl::scene::Node*>(index.internalPointer());
	if (!item) {
		return QVariant();
	}

	return QVariant(item->getName().c_str());
}

Qt::ItemFlags SceneTreeModel::flags(const QModelIndex & index) const {
	if(!index.isValid()) {
		return Qt::ItemIsDropEnabled;
	}

	Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
	return flags;
}

Qt::DropActions SceneTreeModel::supportedDragActions() const {
	return Qt::CopyAction;
}

QVariant SceneTreeModel::headerData(int, Qt::Orientation, int) const {
	return QVariant();
}

QModelIndex SceneTreeModel::index(int row, int column, const QModelIndex & parent) const {
	if (!hasIndex(row, column, parent)) {
		return QModelIndex();
	}

	vwgl::scene::Node * parentItem;

	if (!parent.isValid()) {
		parentItem = _sceneRoot;
	}
	else {
		parentItem = static_cast<vwgl::scene::Node*>(parent.internalPointer());
	}

	vwgl::scene::Node * childItem = parentItem->children()[row].getPtr();
	if (childItem) {
		ui::SceneTreeItem * treeItem = childItem->getComponent<ui::SceneTreeItem>();
		if (!treeItem) {
			treeItem = new ui::SceneTreeItem();
			childItem->addComponent(treeItem);
		}
		if (treeItem) {
			treeItem->setModelIndex(createIndex(row, column, childItem));
		}
		return createIndex(row, column, childItem);
	}
	else {
		return QModelIndex();
	}
}

QModelIndex SceneTreeModel::parent(const QModelIndex & index) const {
	if (!index.isValid()) {
		return QModelIndex();
	}

	vwgl::scene::Node * childItem = static_cast<vwgl::scene::Node*>(index.internalPointer());
	vwgl::scene::Node * parentItem = childItem ? childItem->parent():nullptr;

	if (parentItem && parentItem!=_sceneRoot) {
		return createIndex(getNodeRow(parentItem), 0, parentItem);
	}
	return QModelIndex();
}

int SceneTreeModel::rowCount(const QModelIndex & parent) const {
	vwgl::scene::Node * parentItem;

	if (!parent.isValid()) {
		parentItem = _sceneRoot;
	}
	else {
		parentItem = static_cast<vwgl::scene::Node*>(parent.internalPointer());
	}

	return parentItem->children().size();
}

int SceneTreeModel::columnCount(const QModelIndex & ) const {
	return 1;
}

int SceneTreeModel::getNodeRow(vwgl::scene::Node * item) {
	int row = 0;
	if (item && item->parent()) {
		vwgl::scene::Node * parent = item->parent();
		vwgl::scene::Node::NodeVector::iterator it = std::find(parent->children().begin(), parent->children().end(), item);
		row = std::distance(parent->children().begin(), it);
	}
	return row;
}

}
}
