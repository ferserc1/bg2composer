
#include <bg2e/ui/slider_field.hpp>

namespace bg2e {
namespace ui {

SliderField::SliderField()
	:EditField()
{
	build();
}

SliderField::SliderField(const QString & label)
	:EditField()
{
	build();
	setLabel(label);
}

void SliderField::build() {
	_slider = new QSlider(Qt::Horizontal);
	connect(_slider,SIGNAL(sliderMoved(int)),this,SLOT(sliderMoved(int)));
	connect(_slider,SIGNAL(sliderReleased()),this,SLOT(sliderChanged()));
	layout()->addWidget(_slider);
	_label = new QLabel();
	_label->setMaximumWidth(70);
	_label->setMinimumWidth(70);
	layout()->addWidget(_label);
	updateLabel();
}

void SliderField::updateLabel() {
	_label->setText(QString::number(_slider->value()));
}

void SliderField::sliderMoved(int) {
	if (_onSliderChanged) {
		_onSliderChanged(this);
	}
	emit(sliderValueChanged(this));
	updateLabel();
}

void SliderField::sliderChanged() {
	if (_onSliderEnd) {
		_onSliderEnd(this);
	}
	emit(sliderValueChanged(this));
	updateLabel();
}


// Float slider

float FloatSliderField::s_valueScale = 1000.0f;

FloatSliderField::FloatSliderField()
	:EditField()
{
	build();
}

FloatSliderField::FloatSliderField(const QString & label)
	:EditField()
{
	build();
	setLabel(label);
}

void FloatSliderField::build() {
	_slider = new QSlider(Qt::Horizontal);
	connect(_slider,SIGNAL(sliderMoved(int)),this,SLOT(sliderMoved(int)));
	connect(_slider,SIGNAL(sliderReleased()),this,SLOT(sliderChanged()));
	layout()->addWidget(_slider);
	_label = new QLabel();
	_label->setMaximumWidth(70);
	_label->setMinimumWidth(70);
	layout()->addWidget(_label);
	updateLabel();
}

void FloatSliderField::updateLabel() {
	_label->setText(QString::number(static_cast<float>(_slider->value()) / s_valueScale));
}

void FloatSliderField::sliderMoved(int) {
	if (_onSliderChanged) {
		_onSliderChanged(this);
	}
	emit(sliderValueChanged(this));
	updateLabel();
}

void FloatSliderField::sliderChanged() {
	if (_onSliderEnd) {
		_onSliderEnd(this);
	}
	emit(sliderValueChanged(this));
	updateLabel();
}

}
}
