
#include <bg2e/ui/numeric_text_widget.hpp>
#include <float.h>

#include <QWheelEvent>
#include <QKeyEvent>

#include <regex>

namespace bg2e {
namespace ui {

NumericTextWidget::NumericTextWidget()
	:QLineEdit(0)
	,_type(kDouble)
	,_keyIncrement(1.0f)
	,_minFlt(-FLT_MAX)
	,_maxFlt(FLT_MAX)
	,_decimals(5)
{
	setValue(0);
}

void NumericTextWidget::keyPressEvent(QKeyEvent * evt) {
	float value = text().toFloat();
	float mult = evt->modifiers().testFlag(Qt::ShiftModifier) ? 10.0f:
				evt->modifiers().testFlag(Qt::ControlModifier) ? 0.1f:1.0f;

	if (evt->key()==Qt::Key_Up) {
		value += _keyIncrement * mult;
		if (value<=_maxFlt) {
			setValue(value);
		}
	}
	else if (evt->key()==Qt::Key_Down) {
		value -= _keyIncrement * mult;
		if (value>=_minFlt) {
			setValue(value);
		}
	}
	else {
		QLineEdit::keyPressEvent(evt);
	}
}

void NumericTextWidget::changeEvent(QEvent * evt) {
	if (check()) {
		QLineEdit::changeEvent(evt);
	}
}

bool NumericTextWidget::check() {
	std::regex floatRE("-{0,1}\\d+\\.\\d+");
	std::regex intRE("-{0,1}\\d+");
	std::string value = text().toStdString();
	if (std::regex_match(value, intRE)) {
		int intValue = atoi(text().toStdString().c_str());
		if (intValue<static_cast<int>(_minFlt)) {
			return false;
		}
		else if (intValue>static_cast<int>(_maxFlt)) {
			return false;
		}
		else {
			_lastValueOk = text();
		}
		return true;
	}
	else if (_type==kDouble && std::regex_match(value,floatRE)) {
		float floatValue = static_cast<float>(atof(text().toStdString().c_str()));
		if (floatValue<_minFlt) {
			return false;
		}
		else if (floatValue>_maxFlt) {
			return false;
		}
		else {
			_lastValueOk = text();
		}
		return true;
	}
	else {
		return false;
	}
}

}
}
