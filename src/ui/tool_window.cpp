
#include <bg2e/ui/tool_window.hpp>
#include <QVBoxLayout>
#include <QFrame>
#include <QVBoxLayout>
#include <QLayoutItem>

#include <iostream>

namespace bg2e {
namespace ui {

ToolWindow::ToolWindow(const QString &title)
	:QDockWidget(title,nullptr)
{
	_scroll = new QScrollArea();
	_scroll->setWidgetResizable(true);
	_scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setWidget(_scroll);
	_scroll->setBackgroundRole(QPalette::Dark);

	_centralWidget = new QWidget();
	_scroll->setWidget(_centralWidget);
	_centralWidgetLayout = new QVBoxLayout();
	_centralWidget->setLayout(_centralWidgetLayout);
	_centralWidget->setSizeIncrement(QSizePolicy::Expanding,QSizePolicy::Expanding);
}

void ToolWindow::addField(EditField * field) {
	_centralWidget->layout()->addWidget(field);
}

void ToolWindow::addWidget(QWidget * widget) {
	_centralWidget->layout()->addWidget(widget);
}

void ToolWindow::addTitle(const QString & text) {
	_centralWidget->layout()->addWidget(new QLabel(text));
}

void ToolWindow::addTitle(QLabel * label) {
	_centralWidget->layout()->addWidget(label);
}

void ToolWindow::clear() {
	QLayoutItem *wItem;
	while ((wItem = _centralWidget->layout()->takeAt(0))) {
		if (wItem->widget()) {
			if (dynamic_cast<QFrame*>(wItem->widget())) {
				// The QFrame is used as separator, and must be deleted each time. See ToolWindow::addSeparator()
				delete wItem->widget();
			}
			else {
				wItem->widget()->setParent(nullptr);
			}
			_centralWidget->layout()->removeItem(wItem);
			delete wItem;
		}
	}
}

void ToolWindow::addSeparator() {
	QFrame * line = new QFrame();
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	_centralWidgetLayout->addWidget(line);
}

void ToolWindow::endWidget() {
	_centralWidgetLayout->addSpacerItem(new QSpacerItem(50,50,QSizePolicy::Expanding, QSizePolicy::Expanding));
}

}
}
