
#include <bg2e/ui/color_field.hpp>

#include <QPalette>
#include <QIntValidator>

namespace bg2e {
namespace ui {

ColorField::ColorField()
	:EditField()
	,_rValue(255), _gValue(255), _bValue(255), _aValue(255)
	,_colorDialog(nullptr)
{
	build();
}

ColorField::ColorField(const QString & label)
	:EditField()
	,_rValue(255), _gValue(255), _bValue(255), _aValue(255)
	,_colorDialog(nullptr)
{
	build();
	setLabel(label);
}

void ColorField::build() {	
	_rField = createField("r");
	_gField = createField("g");
	_bField = createField("b");
	_aField = createField("a");

	connect(_rField,SIGNAL(textChanged(QString)),this,SLOT(rChanged(QString)));
	connect(_gField,SIGNAL(textChanged(QString)),this,SLOT(gChanged(QString)));
	connect(_bField,SIGNAL(textChanged(QString)),this,SLOT(bChanged(QString)));
	connect(_aField,SIGNAL(textChanged(QString)),this,SLOT(aChanged(QString)));

	_selectorButton = new QPushButton();
	_selectorButton->setMinimumWidth(30);
	connect(_selectorButton, SIGNAL(clicked()),this,SLOT(buttonPress()));
	layout()->addWidget(_selectorButton);

	updateButtonBackground();
	updateTextFieldValues();
}

NumericTextWidget * ColorField::createField(const QString & label) {
	NumericTextWidget * field = new NumericTextWidget();
	field->setType(NumericTextWidget::kInteger);
	field->setMinValue(0);
	field->setMaxValue(255);
	field->setWheelIncrement(1);
	field->setKeyIncrement(10);
	QLabel * labelField = new QLabel();
	labelField->setText(label);
	layout()->addWidget(labelField);
	layout()->addWidget(field);
	labelField->setMinimumWidth(10);
	labelField->setMaximumWidth(10);
	field->setMinimumWidth(30);
	field->setMaximumWidth(30);
	return field;
}

void ColorField::updateButtonBackground() {
	_selectorButton->setStyleSheet(QString("background-color: rgb(%1,%2,%3);")
								   .arg(QString::number(_rValue),
										QString::number(_gValue),
										QString::number(_bValue)));
}

void ColorField::updateTextFieldValues() {
	disableSignals();
	_rField->setText(QString::number(_rValue));
	_gField->setText(QString::number(_gValue));
	_bField->setText(QString::number(_bValue));
	_aField->setText(QString::number(_aValue));
	enableSignals();
}

int ColorField::parseColorValue(const QString & val, int prevValue, QLineEdit * field) {
	bool status = true;
	int value = val.toInt(&status);
	if (!status && value<256) {
		value = prevValue;
		field->setText(QString::number(value));
	}
	return value;
}

void ColorField::disableSignals() {
	_rField->blockSignals(true);
	_gField->blockSignals(true);
	_bField->blockSignals(true);
	_aField->blockSignals(true);
}

void ColorField::enableSignals() {
	_rField->blockSignals(false);
	_gField->blockSignals(false);
	_bField->blockSignals(false);
	_aField->blockSignals(false);
}

void ColorField::callColorChanged() {
	if (_onColorChanged) {
		// C++11 mode
		_onColorChanged(this);
	}
	// Qt mode
	emit(colorChanged(this));
}

void ColorField::rChanged(const QString & val) {
	_rValue = val.toInt();
	updateButtonBackground();
	callColorChanged();
}

void ColorField::gChanged(const QString & val) {
	_gValue = val.toInt();
	updateButtonBackground();
	callColorChanged();
}

void ColorField::bChanged(const QString & val) {
	_bValue = val.toInt();
	updateButtonBackground();
	callColorChanged();
}

void ColorField::aChanged(const QString & val) {
	_aValue = val.toInt();
	updateButtonBackground();
	callColorChanged();
}

void ColorField::buttonPress() {
	if (!_colorDialog) {
		_colorDialog = new QColorDialog();
		_colorDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
		_colorDialog->setModal(true);
		_colorDialog->setOption(QColorDialog::ShowAlphaChannel,true);
		_colorDialog->setAttribute(Qt::WA_DeleteOnClose);
		connect(_colorDialog,SIGNAL(currentColorChanged(QColor)),this,SLOT(currentColorChanged(QColor)));
		connect(_colorDialog,SIGNAL(finished(int)),this,SLOT(colorDialogClosed(int)));
	}
	QColor color(_rValue, _gValue, _bValue, _aValue);
	_colorDialog->setCurrentColor(color);
	if (_colorDialog->isVisible()) {
		_colorDialog->hide();
	}
	else {
		_colorDialog->show();
	}
}

void ColorField::colorDialogClosed(int) {
	_colorDialog = nullptr;
}

void ColorField::currentColorChanged(const QColor & color) {
	_rValue = color.red();
	_gValue = color.green();
	_bValue = color.blue();
	_aValue = color.alpha();
	disableSignals();
	updateTextFieldValues();
	updateButtonBackground();
	enableSignals();
	callColorChanged();
}

}
}
