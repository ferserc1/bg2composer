
#include <bg2e/closure_action.hpp>

namespace bg2e {

ClosureAction::ClosureAction(std::function<void()> closure)
	:QAction(nullptr), _closure(closure)
{
	connect(this,SIGNAL(triggered()),this,SLOT(actionTriggered()));
}

ClosureAction::ClosureAction(const QString & text, std::function<void()> closure)
	:QAction(text, nullptr), _closure(closure)
{
	connect(this,SIGNAL(triggered()),this,SLOT(actionTriggered()));
}

ClosureAction::ClosureAction(const QIcon & icon, std::function<void()> closure)
	:QAction(icon,"",nullptr), _closure(closure)
{
	connect(this, SIGNAL(triggered()),this,SLOT(actionTriggered()));
}

ClosureAction::ClosureAction(const QIcon & icon, const QString & text, std::function<void()> closure)
	:QAction(icon, text, nullptr), _closure(closure)
{
	connect(this,SIGNAL(triggered()),this,SLOT(actionTriggered()));
}

ClosureAction::ClosureAction(const QString & text, const QKeySequence & seq, std::function<void()> closure)
	:QAction(text, nullptr), _closure(closure)
{
	setShortcut(seq);
	connect(this,SIGNAL(triggered()),this,SLOT(actionTriggered()));
}

ClosureAction::ClosureAction(const QIcon & icon, const QKeySequence & seq, std::function<void()> closure)
	:QAction(icon,"",nullptr), _closure(closure)
{
	setShortcut(seq);
	connect(this, SIGNAL(triggered()),this,SLOT(actionTriggered()));
}

ClosureAction::ClosureAction(const QIcon & icon, const QString & text, const QKeySequence & seq, std::function<void()> closure)
	:QAction(icon, text, nullptr), _closure(closure)
{
	setShortcut(seq);
	connect(this,SIGNAL(triggered()),this,SLOT(actionTriggered()));
}


void ClosureAction::actionTriggered() {
	_closure();
}

}
