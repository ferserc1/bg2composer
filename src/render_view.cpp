

#include <QMouseEvent>
#include <QGLFormat>
#undef min
#undef max
#include <QDateTime>

#include <bg2e/render_view.hpp>

#include <vwgl/opengl.hpp>

#include <vwgl/mouse.hpp>
#include <vwgl/keyboard.hpp>
#include <vwgl/app/mouse_event.hpp>
#include <vwgl/app/keyboard_event.hpp>

#include <vwgl/state.hpp>
#include <vwgl/graphics.hpp>


namespace bg2e {

void setKeyboardEvent(QKeyEvent * event, vwgl::app::KeyboardEvent & kbEvent) {
	kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyNull);
	if (event->modifiers() & Qt::ControlModifier) {
		kbEvent.keyboard().setModifier(vwgl::Keyboard::kCtrlKey);
	}
	if (event->modifiers() & Qt::AltModifier) {
		kbEvent.keyboard().setModifier(vwgl::Keyboard::kAltKey);
	}
	if (event->modifiers() & Qt::ShiftModifier) {
		kbEvent.keyboard().setModifier(vwgl::Keyboard::kShiftKey);
	}
	if ( (event->key()>=Qt::Key_0 && event->key()<=Qt::Key_9) ||
		 (event->key()>=Qt::Key_A && event->key()<=Qt::Key_Z)) {
		kbEvent.keyboard().setKey(static_cast<vwgl::Keyboard::KeyCode>(event->key()));
		kbEvent.keyboard().setCharacter(static_cast<unsigned char>(event->key()));
	}
	else {
		kbEvent.keyboard().setCharacter('\0');
		switch (event->key()) {
			case Qt::Key_Plus:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyAdd);
				kbEvent.keyboard().setCharacter('+');
				break;
			case Qt::Key_Minus:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeySub);
				kbEvent.keyboard().setCharacter('-');
				break;
			case Qt::Key_Escape:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyEsc);
				break;
			case Qt::Key_Space:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeySpace);
				kbEvent.keyboard().setCharacter(' ');
				break;
			case Qt::Key_Delete:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyDel);
				break;
			case Qt::Key_PageDown:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyAvPag);
				break;
			case Qt::Key_PageUp:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyRePag);
				break;
			case Qt::Key_End:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyEnd);
				break;
			case Qt::Key_Backspace:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyBack);
				break;
			case Qt::Key_Up:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyUp);
				break;
			case Qt::Key_Down:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyDown);
				break;
			case Qt::Key_Left:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyLeft);
				break;
			case Qt::Key_Right:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyRight);
				break;
			case Qt::Key_Tab:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyTab);
				kbEvent.keyboard().setCharacter('\t');
				break;
			case Qt::Key_Return:
				kbEvent.keyboard().setKey(vwgl::Keyboard::kKeyReturn);
				kbEvent.keyboard().setCharacter('\r');
				break;

			default:
					break;
		}
	}
}

RenderView::RenderView(bg2e::WindowController * windowController, QGLFormat fmt, QWidget * parent)
	:QGLWidget(fmt, parent)
	,_paused(true)
	,_singleStep(false)
	,_eventRefresh(false)
	,_leftButton(false)
	,_rightButton(false)
	,_middleButton(false)
	,_initGLFunction(nullptr)
	,_lastTime(0)
	,_windowController(windowController)
	,_updateRenderFrames(0)
{

	setMinimumSize(QSize(640,480));
	setFocusPolicy(Qt::StrongFocus);

	_timer = new QTimer();
	connect(_timer,SIGNAL(timeout()),this,SLOT(animate()));
	_timer->start(30);

	setMouseTracking(true);
}

RenderView::~RenderView() {
	_windowController->destroy();
	_windowController = nullptr;
}

void RenderView::initializeGL() {
	// Global OpenGL initialization
	if (_initGLFunction) {
		_initGLFunction(context());
	}

	// Window controller OpenGL initialization
	_windowController->initGL();
}

void RenderView::frame() {
	uint elapsed = QDateTime::currentMSecsSinceEpoch();
	float delta = 0.0f;
	if (_lastTime!=0) {
		delta = static_cast<float>(elapsed - _lastTime)/1000;
	}
	_lastTime = elapsed;

	_windowController->frame(delta);
}

void RenderView::paintGL() {
	_windowController->draw();

	if (_updateRenderFrames>0) {
		play();
		if (_singleStep) --_updateRenderFrames;
	}
	else {
		pause();
	}
}

void RenderView::resizeGL(int width, int height) {
	_lastSize.set(width, height);
	_windowController->reshape(width,height);
}

void RenderView::mousePressEvent(QMouseEvent * event) {
	vwgl::app::MouseEvent evt;

	if (event->button()==Qt::LeftButton) _leftButton = true;
	if (event->button()==Qt::MiddleButton) _middleButton = true;
	if (event->button()==Qt::RightButton) _rightButton = true;
	qreal pixelRatio = window()->windowHandle()->devicePixelRatio();
	evt.setPos(vwgl::Position2Di(event->x() * pixelRatio, event->y() * pixelRatio));
	setButtonMask(evt);
	_windowController->mouseDown(evt);
	updateRenderView(10);
}

void RenderView::mouseReleaseEvent(QMouseEvent * event) {
	vwgl::app::MouseEvent evt;
	if (event->button()==Qt::LeftButton) _leftButton = false;
	if (event->button()==Qt::MiddleButton) _middleButton = false;
	if (event->button()==Qt::RightButton) _rightButton = false;
	qreal pixelRatio = window()->windowHandle()->devicePixelRatio();
	evt.setPos(vwgl::Position2Di(event->x() * pixelRatio, event->y() * pixelRatio));
	setButtonMask(evt);
	_windowController->mouseUp(evt);
	updateRenderView(10);
}

void RenderView::mouseMoveEvent(QMouseEvent * event) {
	vwgl::app::MouseEvent evt;
	qreal pixelRatio = window()->windowHandle()->devicePixelRatio();
	evt.setPos(vwgl::Position2Di(event->x() * pixelRatio, event->y() * pixelRatio));
	setButtonMask(evt);
	_windowController->mouseMove(evt);
	if (evt.mouse().anyButtonPressed()) {
		_windowController->mouseDrag(evt);
	}
	updateRenderView(10);
}

void RenderView::wheelEvent(QWheelEvent * event) {
	if (event->orientation()==Qt::Vertical) {
		vwgl::app::MouseEvent evt;
		qreal pixelRatio = window()->windowHandle()->devicePixelRatio();
		evt.setPos(vwgl::Position2Di(event->x() * pixelRatio, event->y() * pixelRatio));
        evt.setDelta(vwgl::Vector2(0.0f, static_cast<float>(event->delta() * -0.01f)));
		setButtonMask(evt);
		_windowController->mouseWheel(evt);
		updateRenderView(5);
	}
}

void RenderView::keyPressEvent(QKeyEvent * e)
{
	vwgl::app::KeyboardEvent evt;
	setKeyboardEvent(e,evt);
	if (evt.keyboard().key()!=vwgl::Keyboard::kKeyNull) {
		_windowController->keyDown(evt);
		_eventRefresh = true;
	}
	updateRenderView(4);
}

void RenderView::keyReleaseEvent(QKeyEvent * e)
{
	vwgl::app::KeyboardEvent evt;
	setKeyboardEvent(e,evt);
	if (evt.keyboard().key()!=vwgl::Keyboard::kKeyNull) {
		_windowController->keyUp(evt);
		_eventRefresh = false;
	}
	updateRenderView(4);
}

void RenderView::animate()
{
	if (!_paused) {
		frame();
	}
	if (!_paused || _eventRefresh) {
		updateGL();
	}
	_paused = _paused || _singleStep;
}

void RenderView::setButtonMask(vwgl::app::MouseEvent & event)
{
	if (_leftButton) event.mouse().setMouseDown(vwgl::Mouse::kLeftButton);
	if (_rightButton) event.mouse().setMouseDown(vwgl::Mouse::kRightButton);
	if (_middleButton) event.mouse().setMouseDown(vwgl::Mouse::kMiddleButton);
}


}

