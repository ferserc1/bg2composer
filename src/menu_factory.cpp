
#include <bg2e/menu_factory.hpp>
#include <bg2e/ui/file_field.hpp>
#include <QMenu>
#include <QMenuBar>

#include <iostream>

namespace bg2e {

MenuFactory::MenuFactory(QMainWindow * wnd)
	:_mainWindow(wnd)
{
}

void MenuFactory::initMenu(const QString & name) {
	QMenuBar * menuBar = _mainWindow->menuBar();
	QString path = name + "/";
	_menuMap[path] = menuBar->addMenu(name);
}

void MenuFactory::addAction(const QString & menuPathString, QAction * action) {
	QStringList menuPath = menuPathString.split('/');
	QStringList::iterator it;
	QString currentPath;
	QMenu * currentMenu = nullptr;
	QMenuBar * menuBar = _mainWindow->menuBar();

	for (it=menuPath.begin(); it!=menuPath.end(); ++it) {
		currentPath = currentPath + *it + "/";
		if (*it==menuPath.back()) {
			// Action
			if (currentMenu) {
				if (action==nullptr && !currentMenu->isEmpty()) {
					// Only add separator if the menu is not empty
					currentMenu->addSeparator();
				}
				else if (action) {
					currentMenu->addAction(action);
				}
			}
			else {
                std::cerr << "WARNING: invalid menu path specified: " << ui::FileField::stdStringPath(menuPathString) << std::endl;
			}
		}
		else {
			// Menu
			if (_menuMap.find(currentPath)==_menuMap.end()) {
				if (!currentMenu) {
					currentMenu = menuBar->addMenu(*it);
					_menuMap[currentPath] = currentMenu;
				}
				else {
					QMenu * newMenu = new QMenu(*it);
					currentMenu->addMenu(newMenu);
					currentMenu = newMenu;
					_menuMap[currentPath] = currentMenu;
				}
			}
			else {
				currentMenu = _menuMap[currentPath];
			}
		}
	}
}

}
