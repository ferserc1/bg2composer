
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/material_settings_window.hpp>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>

#include <vwgl/scene/cubemap.hpp>

namespace bg2e {
namespace editor {

MaterialSettingsWindow::MaterialSettingsWindow()
	:ToolWindow(QObject::tr("Material Settings"))
	,_blockUpdate(false)
{
	build();

	AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
	AppDelegate::get()->commandManager().registerObserver(this);
	AppDelegate::get()->settings().registerObserver(this);
}

void MaterialSettingsWindow::build() {
	setObjectName("MaterialSettingsWindow");
	addTitle(tr("Color properties"));

	ui::ButtonField * applyButton = new ui::ButtonField(tr("Apply to Selection"));
	applyButton->onButtonClicked([&]() {
		applyModifier();
	});
	addField(applyButton);

	addSeparator();

	_cullFace = new ui::BooleanField(tr("Cull faces"));
	_cullFace->onValueChanged([&](ui::BooleanField *) {
		applyCullFace();
	});
	addField(_cullFace);

	addSeparator();

	_diffuseColor = new ui::ColorField(tr("Diffuse"));
	_diffuseColor->onColorChanged([&](ui::ColorField *) { applyModifier(); });
	addField(_diffuseColor);

	_specularColor = new ui::ColorField(tr("Specular"));
	_specularColor->onColorChanged([&](ui::ColorField*) { applyModifier(); });
	addField(_specularColor);

	addSeparator();
	addTitle(tr("Shininess"));

	_shininess = new ui::FloatSliderField(tr("Amount"));
	_shininess->setRange(0.0f,255.0f);
	_shininess->onSliderChanged([&](ui::FloatSliderField*) { applyModifier(); });
	addField(_shininess);

	_shininessMask = new ui::FileField(tr("Mask"));
	_shininessMask->setFilter("Image files (*.jpg *.png *.gif *.jpeg)");
	_shininessMask->onFileChanged([&](ui::FileField*) { applyModifier(); });
	addField(_shininessMask);

	_shininessChannel = new ui::ComboBoxField(tr("Channel"));
	_shininessChannel->addItem(tr("Red"));
	_shininessChannel->addItem(tr("Green"));
	_shininessChannel->addItem(tr("Blue"));
	_shininessChannel->addItem(tr("Alpha"));
	_shininessChannel->onComboBoxChanged([&](ui::ComboBoxField*) { applyModifier(); });
	addField(_shininessChannel);

	_shininessInvert = new ui::BooleanField(tr("Invert values"));
	_shininessInvert->onValueChanged([&](ui::BooleanField*) { applyModifier(); });
	addField(_shininessInvert);
	addSeparator();

	addTitle(tr("Light emission"));

	_lightEmission = new ui::FloatSliderField(tr("Amount"));
	_lightEmission->setRange(0.0f,1.0f);
	_lightEmission->onSliderChanged([&](ui::FloatSliderField*) { applyModifier(); });
	addField(_lightEmission);

	_lightEmissionMask = new ui::FileField(tr("Mask"));
	_lightEmissionMask->setFilter("Image files (*.jpg *.png *.gif *.jpeg)");
	_lightEmissionMask->onFileChanged([&](ui::FileField*) { applyModifier(); });
	addField(_lightEmissionMask);

	_lightEmissionChannel = new ui::ComboBoxField(tr("Channel"));
	_lightEmissionChannel->addItem(tr("Red"));
	_lightEmissionChannel->addItem(tr("Green"));
	_lightEmissionChannel->addItem(tr("Blue"));
	_lightEmissionChannel->addItem(tr("Alpha"));
	_lightEmissionChannel->onComboBoxChanged([&](ui::ComboBoxField*) { applyModifier(); });
	addField(_lightEmissionChannel);

	_lightEmissionInvert = new ui::BooleanField(tr("Invert values"));
	_lightEmissionInvert->onValueChanged([&](ui::BooleanField*) { applyModifier(); });
	addField(_lightEmissionInvert);
	addSeparator();

	addTitle(tr("Texture properties"));

	_texture = new ui::FileField(tr("Diffuse"));
	_texture->setFilter("Image files (*.jpg *.png *.gif *.jpeg)");
	_texture->onFileChanged([&](ui::FileField*) { applyModifier(); });
	addField(_texture);

	_alphaCutoff = new ui::FloatSliderField(tr("Alpha cutoff"));
	_alphaCutoff->setRange(0.0f, 1.0f);
	_alphaCutoff->onSliderChanged([&](ui::FloatSliderField*) { applyModifier();});
	addField(_alphaCutoff);

	_textureOffset = new ui::VectorField(tr("Offset"));
	_textureOffset->setVector(vwgl::Vector2(0.0f));
	_textureOffset->onVectorChanged([&](ui::VectorField*) { applyModifier(); });
	addField(_textureOffset);

	_textureScale = new ui::VectorField(tr("Scale"));
	_textureScale->setVector(vwgl::Vector2(1.0f));
	_textureScale->onVectorChanged([&](ui::VectorField*) { applyModifier(); });
	addField(_textureScale);

	_lightMap = new ui::FileField(tr("Light map"));
	_lightMap->setFilter("Image files (*.jpg *.png *.gif *.jpeg)");
	_lightMap->onFileChanged([&](ui::FileField*) { applyModifier(); });
	addField(_lightMap);

	_normalMap = new ui::FileField(tr("Normal map"));
	_normalMap->setFilter("Image files (*.jpg *.png *.gif *.jpeg)");
	_normalMap->onFileChanged([&](ui::FileField*) { applyModifier(); });
	addField(_normalMap);
	_normalMapOffset = new ui::VectorField(tr("Offset"));
	_normalMapOffset->setVector(vwgl::Vector2(0.0f));
	_normalMapOffset->onVectorChanged([&](ui::VectorField*) { applyModifier(); });
	addField(_normalMapOffset);

	_normalMapScale = new ui::VectorField(tr("Scale"));
	_normalMapScale->setVector(vwgl::Vector2(1.0f));
	_normalMapScale->onVectorChanged([&](ui::VectorField*) { applyModifier(); });
	addField(_normalMapScale);

	addSeparator();

	_reflectionAmount = new ui::FloatSliderField(tr("Reflection"));
	_reflectionAmount->setRange(0.0f, 1.0f);
	_reflectionAmount->onSliderChanged([&](ui::FloatSliderField*) { applyModifier(); });
	addField(_reflectionAmount);

	_reflectionMask = new ui::FileField(tr("Reflection mask"));
	_reflectionMask->setFilter("Image files (*.jpg *.png *.gif *.jpeg)");
	_reflectionMask->onFileChanged([&](ui::FileField*) { applyModifier(); });
	addField(_reflectionMask);

	_reflectionChannel = new ui::ComboBoxField(tr("Channel"));
	_reflectionChannel->addItem(tr("Red"));
	_reflectionChannel->addItem(tr("Green"));
	_reflectionChannel->addItem(tr("Blue"));
	_reflectionChannel->addItem(tr("Alpha"));
	_reflectionChannel->onComboBoxChanged([&](ui::ComboBoxField*) { applyModifier(); });
	addField(_reflectionChannel);

	_reflectionInvert = new ui::BooleanField(tr("Invert values"));
	_reflectionInvert->onValueChanged([&](ui::BooleanField*) { applyModifier(); });
	addField(_reflectionInvert);
	addSeparator();

	_castShadows = new ui::BooleanField(tr("Cast shadows"));
	_castShadows->onValueChanged([&](ui::BooleanField*) { applyModifier(); });
	addField(_castShadows);

	_receiveShadows = new ui::BooleanField(tr("Receive shadows"));
	_receiveShadows->onValueChanged([&](ui::BooleanField*) { applyModifier(); });
	addField(_receiveShadows);

	addSeparator();

	addTitle(tr("Height Map"));
	_tessellationWarning = new QLabel(tr("Warning: tessellation is disabled in view settings"));
	_tessellationWarning->setStyleSheet("QLabel { background-color: #F00000; }");
	_tessellationWarning->setMinimumHeight(30);
	addTitle(_tessellationWarning);
	_heightMap = new ui::FileField(tr("Image"));
	_heightMap->onFileChanged([&](ui::FileField*) {
		applyModifier();
		updateDisplacementNormals();
	});
	addField(_heightMap);

	_heightMapUVSet = new ui::ComboBoxField(tr("Map to"));
	_heightMapUVSet->addItem(tr("UV Set 0"));
	_heightMapUVSet->addItem(tr("UV Set 1"));
	_heightMapUVSet->onComboBoxChanged([&](ui::ComboBoxField*) { applyModifier(); });
	addField(_heightMapUVSet);

	_heightFactor = new ui::FloatSliderField(tr("Height factor"));
	_heightFactor->setRange(-2.0f, 2.0f);
	_heightFactor->setValue(0.2f);
	_heightFactor->onSliderChanged([&](ui::FloatSliderField*) { applyModifier(); });
	_heightFactor->onSliderEnd([&](ui::FloatSliderField*) { updateDisplacementNormals(); });
	addField(_heightFactor);

	addSeparator();

	addTitle("Tessellation");
	ui::VectorLabels * tessLabels = new ui::VectorLabels();
	tessLabels->setLabels("nearest","near","far","farthest");
	addField(tessLabels);
	_tessellationDistances = new ui::VectorField("Distances");
	_tessellationDistances->setComponentLabel(0,"");
	_tessellationDistances->setComponentLabel(1,"");
	_tessellationDistances->setComponentLabel(2,"");
	_tessellationDistances->setComponentLabel(3,"");
	_tessellationDistances->setVector(vwgl::Vector4(5.0f, 10.0f, 20.0f, 30.0f));
	_tessellationDistances->onVectorChanged([&](ui::VectorField*) { applyModifier(); });
	addField(_tessellationDistances);

	_tessellationLevels = new ui::VectorField("Subdivisions");
	_tessellationLevels->setVector(vwgl::Vector4(1.0f));
	_tessellationLevels->setComponentLabel(0,"");
	_tessellationLevels->setComponentLabel(1,"");
	_tessellationLevels->setComponentLabel(2,"");
	_tessellationLevels->setComponentLabel(3,"");
	_tessellationLevels->onVectorChanged([&](ui::VectorField*) { applyModifier(); });
	addField(_tessellationLevels);

	endWidget();

	hidePanel();
}

void MaterialSettingsWindow::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	if (_blockUpdate) return;
	if (sel.getMaterialList().size()) {

		vwgl::GenericMaterial * mat = dynamic_cast<vwgl::GenericMaterial*>(sel.getMaterialList().back().getPtr());

		_cullFace->setValue(mat->getCullFace());

		_diffuseColor->setColor(mat->getDiffuse());
		_alphaCutoff->setValue(mat->getAlphaCutoff());
		_specularColor->setColor(mat->getSpecular());
		_shininess->setValue(mat->getShininess());

		_shininessMask->setPath(QString::fromStdString(mat->getShininessMask() ? mat->getShininessMask()->getFileName():""));
		_shininessChannel->setCurrentIndex(mat->getShininessMaskChannel());
		_shininessInvert->setValue(mat->getInvertShininessMask());

		_lightEmission->setValue(mat->getLightEmission());
		_lightEmissionMask->setPath(QString::fromStdString(mat->getLightEmissionMask() ? mat->getLightEmissionMask()->getFileName():""));
		_lightEmissionChannel->setCurrentIndex(mat->getLightEmissionMaskChannel());
		_lightEmissionInvert->setValue(mat->getInvertLightEmissionMask());

		_texture->setPath(QString::fromStdString(mat->getTexture() ? mat->getTexture()->getFileName():""));
		_textureOffset->setVector(mat->getTextureOffset());
		_textureScale->setVector(mat->getTextureScale());

		_lightMap->setPath(QString::fromStdString(mat->getLightMap() ? mat->getLightMap()->getFileName():""));

		_normalMap->setPath(QString::fromStdString(mat->getNormalMap() ? mat->getNormalMap()->getFileName():""));
		_normalMapOffset->setVector(mat->getNormalMapOffset());
		_normalMapScale->setVector(mat->getNormalMapScale());

		_reflectionAmount->setValue(mat->getReflectionAmount());
		_reflectionMask->setPath(QString::fromStdString(mat->getReflectionMask() ? mat->getReflectionMask()->getFileName():""));
		_reflectionChannel->setCurrentIndex(mat->getReflectionMaskChannel());
		_reflectionInvert->setValue(mat->getInvertReflectionMask());

		_castShadows->setValue(mat->getCastShadows());
		_receiveShadows->setValue(mat->getReceiveShadows());

		_heightMap->setPath(QString::fromStdString(mat->getDisplacementMapPath()));
		_heightFactor->setValue(mat->getDisplacementFactor());
		_heightMapUVSet->setCurrentIndex(mat->getDisplacementUVSet());
		_tessellationDistances->setVector(mat->getTessellationDistances());
		_tessellationLevels->setVector(mat->getTessellationLevels());

		onSettingsChanged(&AppDelegate::get()->settings());

		showPanel();
	}
	else {

		_cullFace->setValue(false);

		_diffuseColor->setColor(vwgl::Color::black());
		_alphaCutoff->setValue(0.5f);
		_specularColor->setColor(vwgl::Color::black());
		_shininess->setValue(0.0f);

		_shininessMask->setPath("");
		_shininessChannel->setCurrentIndex(0);
		_shininessInvert->setValue(false);

		_lightEmission->setValue(0.0f);
		_lightEmissionMask->setPath("");
		_lightEmissionChannel->setCurrentIndex(0);
		_lightEmissionInvert->setValue(false);

		_texture->setPath("");
		_textureOffset->setVector(vwgl::Vector2(0.0f));
		_textureScale->setVector(vwgl::Vector2(1.0f));

		_lightMap->setPath("");

		_normalMap->setPath("");
		_normalMapOffset->setVector(vwgl::Vector2(0.0f));
		_normalMapScale->setVector(vwgl::Vector2(1.0f));

		_reflectionAmount->setValue(0.f);
		_reflectionMask->setPath("");
		_reflectionChannel->setCurrentIndex(0);
		_reflectionInvert->setValue(false);

		_castShadows->setValue(false);
		_receiveShadows->setValue(false);

		_heightMap->setPath("");
		_heightFactor->setValue(0.0f);
		_heightMapUVSet->setCurrentIndex(0);
		_tessellationDistances->setVector(vwgl::Vector4(5.0f, 10.f, 20.0f, 30.f));
		_tessellationLevels->setVector(vwgl::Vector4(1.0f));

		hidePanel();
	}
}

void MaterialSettingsWindow::didExecuteCommand(vwgl::app::Command * cmd) {
	if (dynamic_cast<vwgl::cmd::MaterialCommand*>(cmd)) {
		selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
	}
}

void MaterialSettingsWindow::didUndo(vwgl::app::Command * cmd) {
	if (dynamic_cast<vwgl::cmd::MaterialCommand*>(cmd)) {
		selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
	}
}

void MaterialSettingsWindow::didRedo(vwgl::app::Command * cmd) {
	if (dynamic_cast<vwgl::cmd::MaterialCommand*>(cmd)) {
		selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
	}
}

void MaterialSettingsWindow::onSettingsChanged(Settings * settings) {
	if (settings->isTessellationEnabled()) {
		_heightMap->setEnabled(true);
		_heightFactor->setEnabled(true);
		_heightMapUVSet->setEnabled(true);
		_tessellationDistances->setEnabled(true);
		_tessellationLevels->setEnabled(true);
		_tessellationWarning->setVisible(false);
	}
	else {
		_heightMap->setEnabled(false);
		_heightFactor->setEnabled(false);
		_heightMapUVSet->setEnabled(false);
		_tessellationDistances->setEnabled(false);
		_tessellationLevels->setEnabled(false);
		_tessellationWarning->setVisible(true);
	}
}

void MaterialSettingsWindow::applyModifier() {
	vwgl::MaterialModifier mod;
	_blockUpdate = true;

	mod.setDiffuse(_diffuseColor->getEngineColor());
	mod.setAlphaCutoff(_alphaCutoff->value());
	mod.setSpecular(_specularColor->getEngineColor());
	mod.setShininess(_shininess->value());

	std::string shininessMask = _shininessMask->getPath();
	int shininessMaskChannel = _shininessChannel->currentIndex();
	bool invertShininess = _shininessInvert->value();
	mod.setShininessMask(shininessMask, shininessMaskChannel, invertShininess);

	mod.setLightEmission(_lightEmission->value());
	std::string leMask = _lightEmissionMask->getPath();
	int leMaskChannel = _lightEmissionChannel->currentIndex();
	bool leShininess = _lightEmissionInvert->value();
	mod.setLightEmissionMask(leMask, leMaskChannel, leShininess);

	mod.setTexture(_texture->getPath());
	mod.setTextureOffset(_textureOffset->getVector2());
	mod.setTextureScale(_textureScale->getVector2());

	mod.setLightMap(_lightMap->getPath());

	mod.setNormalMap(_normalMap->getPath());
	mod.setNormalMapOffset(_normalMapOffset->getVector2());
	mod.setNormalMapScale(_normalMapScale->getVector2());

	mod.setReflectionAmount(_reflectionAmount->value());
	mod.setReflectionMask(_reflectionMask->getPath(), _reflectionChannel->currentIndex(), _reflectionInvert->value());

	mod.setCastShadows(_castShadows->value());
	mod.setReceiveShadows(_receiveShadows->value());

	mod.setDisplacementFactor(_heightFactor->value());
	mod.setDisplacementUV(_heightMapUVSet->currentIndex());
	mod.setDisplacementMap(_heightMap->getPath());
	mod.setTessellationDistances(_tessellationDistances->getVector4());
	mod.setTessellationLevels(_tessellationLevels->getVector4());

	vwgl::cmd::ApplyMaterialModifierCommand * cmd = new vwgl::cmd::ApplyMaterialModifierCommand(AppDelegate::get()->renderView()->context(), mod);
	AppDelegate::get()->inputMediator().selectionHandler().eachMaterial([&](vwgl::Material*mat) {
		vwgl::GenericMaterial * gm = dynamic_cast<vwgl::GenericMaterial*>(mat);
		if (gm) {
			cmd->addMaterial(gm);
		}
	});
	AppDelegate::get()->commandManager().execute(cmd);
	AppDelegate::get()->updateRenderView();
	_blockUpdate = false;

	AppDelegate::get()->inputMediator().selectionHandler().eachDrawable([&](vwgl::scene::Drawable * drw) {
		if (_reflectionAmount->value()>0.0f && drw->node()->getComponent<vwgl::scene::Cubemap>()==nullptr) {
			drw->node()->addComponent(new vwgl::scene::Cubemap());
			drw->node()->init();
		}
		else if (_reflectionAmount->value()==0.0f && drw->node()->getComponent<vwgl::scene::Cubemap>()!=nullptr){
			drw->node()->removeComponent(drw->node()->getComponent<vwgl::scene::Cubemap>());
		}
	});

	AppDelegate::get()->statusBar().updateNow();
}

void MaterialSettingsWindow::applyCullFace() {
	_blockUpdate = true;
	cmd::SetCullFaceCommand * cmd = new cmd::SetCullFaceCommand(AppDelegate::get()->renderView()->context(), _cullFace->value());
	AppDelegate::get()->inputMediator().selectionHandler().eachMaterial([&](vwgl::Material * mat) {
		vwgl::GenericMaterial * gm = dynamic_cast<vwgl::GenericMaterial*>(mat);
		if (gm) {
			cmd->addMaterial(gm);
		}
	});
	AppDelegate::get()->commandManager().execute(cmd);
	AppDelegate::get()->updateRenderView();
	_blockUpdate = false;
}

void MaterialSettingsWindow::updateDisplacementNormals() {
	AppDelegate::get()->inputMediator().selectionHandler().eachMaterial([&](vwgl::Material * mat) {
		vwgl::GenericMaterial * gm = dynamic_cast<vwgl::GenericMaterial*>(mat);
		if (gm) {
			gm->updateDisplacementNormals();
		}
		AppDelegate::get()->updateRenderView();
	});
}

}
}
