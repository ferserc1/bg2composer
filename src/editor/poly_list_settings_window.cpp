
#include <QListWidget>

#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/poly_list_settings_window.hpp>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>

#include <vwgl/scene/cubemap.hpp>
#include <vwgl/cmd/polylist_commands.hpp>

namespace vwgl {
namespace cmd {

class PListDuplicateCommand : public PolyListDrawableCommand {
public:
	PListDuplicateCommand(plain_ptr ctx) :PolyListDrawableCommand(ctx) {}

	void doCommand() {
		if (_plistVector.size() == 0) throw std::runtime_error("Error executing PListDuplicateCommand: empty polyList vector found");
		if (_drawableVector.size() == 0) throw std::runtime_error("Error executing PListDuplicateCommand: empty parent drawable list found");

		initialize();

		for (DrawablePolyList & drw : _drawableVector) {
			drw.eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform & trx) {
				ptr<PolyList> newPlist = new PolyList(plist);
				newPlist->setName(plist->getName() + " clone");
				ptr<Material> newMat = new GenericMaterial();//mat->clone();
				vwgl::Transform newTrx = trx;
				drw.drawable()->addPolyList(newPlist.getPtr(), newMat.getPtr(), newTrx);
				_undoPlist.push_back(newPlist.getPtr());
			});
			vwgl::scene::Node * node = drw.drawable()->node();
			if (node && node->getComponent<manipulation::Selectable>()) {
				node->getComponent<manipulation::Selectable>()->drawableChanged(drw.drawable());
			}
		}
	}
	void undoCommand() {
		for (DrawablePolyList & drw : _drawableVector) {
			for (auto undoPlist : _undoPlist) {
				try {
					drw.drawable()->tryRemovePolyList(undoPlist.getPtr());
				}
				catch (vwgl::scene::DrawableElementNotFoundException &) {
				}
			}
			vwgl::scene::Node * node = drw.drawable()->node();
			if (node && node->getComponent<manipulation::Selectable>()) {
				node->getComponent<manipulation::Selectable>()->drawableChanged(drw.drawable());
			}
		}
		_undoPlist.clear();
	}

protected:
	virtual ~PListDuplicateCommand() {}

	std::vector<ptr<PolyList> > _undoPlist;
};

class PListDetachCommand : public PolyListDrawableCommand {
public:
	PListDetachCommand(plain_ptr ctx) :PolyListDrawableCommand(ctx) {}

	void doCommand() {
		if (_plistVector.size() == 0) throw std::runtime_error("Error executing PListDuplicateCommand: empty polyList vector found");
		if (_drawableVector.size() == 0) throw std::runtime_error("Error executing PListDuplicateCommand: empty parent drawable list found");

		initialize();

		for (DrawablePolyList & drw : _drawableVector) {
			vwgl::scene::Drawable * srcDrw = drw.drawable();
			vwgl::scene::Node * srcNode = srcDrw->node();
			vwgl::scene::Node * newParent = srcNode ? srcNode->parent():nullptr;

			if (newParent) {
				std::string name = srcDrw->getName().empty() ? srcNode->getName():srcDrw->getName();
				name = "Detached plist of " + name;
				ptr<vwgl::scene::Drawable> newDrawable = new vwgl::scene::Drawable();
				newDrawable->setName(name);
				ptr<vwgl::scene::Node> newNode = new vwgl::scene::Node(name);
				newNode->addComponent(newDrawable.getPtr());
				newParent->addChild(newNode.getPtr());

				if (srcNode->getComponent<vwgl::scene::Transform>()) {
					ptr<vwgl::scene::Transform> newTrx = srcNode->getComponent<vwgl::scene::Transform>()->clone();
					newNode->addComponent(newTrx.getPtr());
				}

				bg2e::editor::AppDelegate::get()->scene().prepareSceneNode(newNode.getPtr());

				drw.eachElement([&](PolyList * plist, Material * mat, vwgl::Transform & trx) {
					newDrawable->addPolyList(plist,mat,trx);
					srcDrw->removePolyList(plist);
				});

				newNode->init();

				if (srcNode->getComponent<manipulation::Selectable>()) {
					srcNode->getComponent<manipulation::Selectable>()->drawableChanged(srcDrw);
				}

				this->_undoDrawables[srcDrw] = newDrawable.getPtr();
			}
		}
	}

	void undoCommand() {
		for (auto drws : _undoDrawables) {
			ptr<vwgl::scene::Drawable> dstDrawable = drws.first;
			ptr<vwgl::scene::Drawable> undoDrawable = drws.second;

			undoDrawable->eachElement([&](PolyList * plist, Material * material, Transform & trx) {
				dstDrawable->addPolyList(plist, material, trx);
			});
			if (dstDrawable->node()->getComponent<manipulation::Selectable>()) {
				dstDrawable->node()->getComponent<manipulation::Selectable>()->drawableChanged(dstDrawable.getPtr());
			}

			vwgl::scene::Node * parent = undoDrawable->node()->parent();
			parent->removeChild(undoDrawable->node());
		}
		_undoDrawables.clear();
	}

protected:
	virtual ~PListDetachCommand() {}

	std::map<vwgl::scene::Drawable*, vwgl::scene::Drawable *> _undoDrawables;
};

}
}
namespace bg2e {
namespace editor {

PolyList::VertexBufferType bufferType(int uvSet) {
	switch (uvSet) {
	case 0:
		return PolyList::kTexCoord0;
	case 1:
		return PolyList::kTexCoord1;
	case 2:
		return PolyList::kTexCoord2;
	default:
		return PolyList::kTexCoord0;
	}
}

int PolyListSettingsWindow::s_plistIndex = 0;

PolyListSettingsWindow::PolyListSettingsWindow()
	:ToolWindow(QObject::tr("PolyList Settings"))
	,_preventUpdate(false)
{
	build();

	AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
	AppDelegate::get()->commandManager().registerObserver(this);
}

void PolyListSettingsWindow::build() {
	setObjectName("PolyListSettingsWindow");

	_list = new QListWidget();
	_list->setSelectionMode(QAbstractItemView::ExtendedSelection);
	addWidget(_list);
	_list->setMaximumHeight(300);
	_list->setMinimumHeight(200);
	connect(_list,SIGNAL(itemSelectionChanged()),this,SLOT(plistSelectionChanged()));

	_name = new ui::TextField(tr("Name"));
	_name->onEditFinished([&](ui::TextField *) {
		if (_list->currentItem() && _list->selectedItems().size()==1) {
			executeCommand(new cmd::PListSetNameCommand(AppDelegate::get()->renderView()->context(),_name->getText()));
			//selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
			QListWidgetItem * selectedItem = _list->currentItem();
			selectedItem->setText(_name->text());
		}
	});
	addField(_name);

	_groupName = new ui::TextField(tr("Group name"));
	_groupName->onEditFinished([&](ui::TextField *) {
		executeCommand(new cmd::PListSetGroupNameCommand(AppDelegate::get()->renderView()->context(),_groupName->getText()));
	});
	addField(_groupName);

	_visible = new ui::BooleanField(tr("Visible"));
	_visible->onValueChanged([&](ui::BooleanField *) {
		executeCommand(new cmd::PListSetVisibleCommand(AppDelegate::get()->renderView()->context(),_visible->value()));
		QModelIndexList indexes = _list->selectionModel()->selectedIndexes();
		foreach (QModelIndex index, indexes) {
			QListWidgetItem * item = _list->item(index.row());
			QIcon icon = _visible->value() ? QIcon(":/icons/light-icons/visible-icon.png"):QIcon(":/icons/light-icons/not-visible-icon.png");
			item->setIcon(icon);
		}
	});
	addField(_visible);


	addSeparator();
	addTitle(tr("Swap UVs"));

	_swapUVfrom = new ui::ComboBoxField(tr("From UV Set"));
	_swapUVfrom->addItem("0"); _swapUVfrom->addItem("1"); _swapUVfrom->addItem("2");
	_swapUVfrom->setCurrentIndex(0);
	_swapUVfrom->onComboBoxChanged([&](ui::ComboBoxField *) {
		if (_swapUVfrom->currentIndex()==_swapUVTo->currentIndex()) {
			_swapUVTo->setCurrentIndex(_swapUVTo->currentIndex()==0 ? 1:0);
		}
	});
	addField(_swapUVfrom);

	_swapUVTo = new ui::ComboBoxField(tr("To UV Set"));
	_swapUVTo->addItem("0"); _swapUVTo->addItem("1"); _swapUVTo->addItem("2");
	_swapUVTo->setCurrentIndex(1);
	_swapUVTo->onComboBoxChanged([&](ui::ComboBoxField *) {
		if (_swapUVfrom->currentIndex()==_swapUVTo->currentIndex()) {
			_swapUVfrom->setCurrentIndex(_swapUVfrom->currentIndex()==0 ? 1:0);
		}
	});
	addField(_swapUVTo);

	ui::ButtonField * swapButton = new ui::ButtonField(tr("Swap"));
	swapButton->onButtonClicked([&]() {
		executeCommand(new cmd::PListSwapUVsCommand(AppDelegate::get()->renderView()->context(), bufferType(_swapUVfrom->currentIndex()), bufferType(_swapUVTo->currentIndex())));
	});

	addField(swapButton);

	addSeparator();
	_actions = new ui::MenuField(tr("Actions"));
	_actions->addAction(new ClosureAction(tr("Flip Faces"),[&]() {
		executeCommand(new cmd::PListFlipFacesCommand(AppDelegate::get()->renderView()->context()));
	}));
	_actions->addAction(new ClosureAction(tr("Flip normals"), [&]() {
		executeCommand(new cmd::PListFlipNormalsCommand(AppDelegate::get()->renderView()->context()));
	}));
	_actions->addSeparator();
	_actions->addAction(new ClosureAction(tr("Delete"), [&]() {
		deletePlist(AppDelegate::get()->inputMediator().selectionHandler());
	}));
	_actions->addAction(new ClosureAction(tr("Detach"), [&]() {
		detachPlist(AppDelegate::get()->inputMediator().selectionHandler());
	}));
	_actions->addAction(new ClosureAction(tr("Duplicate"), [&]() {
		duplicatePlist(AppDelegate::get()->inputMediator().selectionHandler());
	}));
	_actions->addSeparator();
	_actions->addAction(new ClosureAction(tr("Fix Luxology Modo material names"), [&]() {
		eachSelectedPList([&](vwgl::PolyList * plist) {
			std::string name = plist->getName();
			std::string prefix = "Material-";
			size_t start_pos = name.find(prefix);
			if(start_pos != std::string::npos) {
				name.replace(start_pos, prefix.length(), "");
			}
			cmd::PListSetNameCommand * cmd = new cmd::PListSetNameCommand(AppDelegate::get()->renderView()->context(), name);
			cmd->addPolyList(plist);
			AppDelegate::get()->commandManager().execute(cmd);
		});
	}));

	addField(_actions);


	_findName = new ui::TextField("Replace Name");
	addField(_findName);
	_replaceName = new ui::TextField("With Name");
	addField(_replaceName);

	ui::ButtonField * replaceBtn = new ui::ButtonField("Replace");
	replaceBtn->onButtonClicked([&]() {
		bool exec = false;
		cmd::PListSetNameCommand * cmd = new cmd::PListSetNameCommand(AppDelegate::get()->renderView()->context(), _replaceName->getText());
		eachSelectedPList([&](vwgl::PolyList * plist) {
			std::string name = plist->getName();
			if (name==_findName->getText()) {
				cmd->addPolyList(plist);
				exec = true;
			}
		});
		if (exec) {
			AppDelegate::get()->commandManager().execute(cmd);
		}
	});
	addField(replaceBtn);



	endWidget();
	hidePanel();
}

void PolyListSettingsWindow::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	if (_preventUpdate) return;
	if (sel.getPolyListList().size()>0) {
		_list->clear();
		sel.eachPolyList([&](vwgl::PolyList * plist) {
			if (plist->getName()=="") {
				std::stringstream nameGenerator;
				nameGenerator << "PolyList_" << s_plistIndex;
				++s_plistIndex;
				plist->setName(nameGenerator.str());
			}
			QString name = QString::fromStdString(plist->getName());
			QIcon icon = plist->isVisible() ? QIcon(":/icons/light-icons/visible-icon.png"):QIcon(":/icons/light-icons/not-visible-icon.png");
			_list->addItem(new QListWidgetItem(icon,name));
		});

		blockFieldSignals(true);
		_name->setText("");
		_groupName->setText("");
		_visible->setValue(false);

		_name->setEnabled(false);
		_groupName->setEnabled(false);
		_visible->setEnabled(false);
		_swapUVfrom->setEnabled(false);
		_swapUVTo->setEnabled(false);
		_actions->setEnabled(false);


		showPanel();

		_list->setCurrentRow(0);
		_list->selectAll();
		plistSelectionChanged();
		blockFieldSignals(false);
	}
	else {
		_list->clear();
		hidePanel();
	}
}

void PolyListSettingsWindow::didExecuteCommand(vwgl::app::Command * cmd) {
	if (dynamic_cast<vwgl::cmd::PolyListCommand*>(cmd)) {
		selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
	}
}

void PolyListSettingsWindow::didUndo(vwgl::app::Command * cmd) {
	if (dynamic_cast<vwgl::cmd::PolyListCommand*>(cmd)) {
		selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
	}
}

void PolyListSettingsWindow::didRedo(vwgl::app::Command * cmd) {
	if (dynamic_cast<vwgl::cmd::PolyListCommand*>(cmd)) {
		selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
	}
}

void PolyListSettingsWindow::plistSelectionChanged() {
	vwgl::manipulation::SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
	std::vector<PolyList*> plistVector;
	blockFieldSignals(true);
	AppDelegate::get()->drawPlistSelection().clearSelection();
	if (_list->currentItem() && sel.getPolyListList().size()==static_cast<size_t>(_list->count())) {
		QModelIndexList indexes = _list->selectionModel()->selectedIndexes();
		std::vector<int> indexList;
		foreach (QModelIndex index, indexes) {
			int position = index.row();
			if (sel.getPolyListList().size()>static_cast<size_t>(position)) {
				manipulation::SelectionHandler::PolyListList::iterator p_it = std::next(sel.getPolyListList().begin(), position);
				plistVector.push_back((*p_it).getPtr());
				AppDelegate::get()->drawPlistSelection().addSelection((*p_it).getPtr());
			}
		}
		if (AppDelegate::get()->drawPlistSelection().selectionCount()==sel.getPolyListList().size()) {
			AppDelegate::get()->drawPlistSelection().clearSelection();
		}
		AppDelegate::get()->updateRenderView();
	}

	if (plistVector.size()>0) {
		_name->setText("");
		_name->setEnabled(true);
		_groupName->setText("");
		_groupName->setEnabled(true);
		_visible->setEnabled(true);
		_swapUVfrom->setEnabled(true);
		_swapUVTo->setEnabled(true);
		_actions->setEnabled(true);


		std::vector<PolyList*>::iterator it;
		std::string name = plistVector.size() ? plistVector.front()->getName():"";
		std::string groupName = plistVector.size() ? plistVector.front()->getGroupName():"";
		bool visible = true;
		for (it=plistVector.begin(); it!=plistVector.end(); ++it) {
			PolyList * plist = *it;
			if (name!=plist->getName()) {
				name = QObject::tr("< multiple values >").toStdString();
				_name->setEnabled(false);
			}
			if (groupName!=plist->getGroupName()) {
				groupName = QObject::tr("< multiple values >").toStdString();
			}
			visible = plist->isVisible() && visible;
		}

		_name->setText(QString::fromStdString(name));
		_groupName->setText(QString::fromStdString(groupName));
		_visible->setValue(visible);
	}
	else {
		_name->setText("");
		_name->setEnabled(false);
		_groupName->setText("");
		_groupName->setEnabled(false);
		_visible->setEnabled(false);
		_swapUVfrom->setEnabled(false);
		_swapUVTo->setEnabled(false);
		_actions->setEnabled(false);
	}
	blockFieldSignals(false);
}

void PolyListSettingsWindow::deletePlist(vwgl::manipulation::SelectionHandler & sel) {
	cmd::PListDeleteCommand * cmd = new cmd::PListDeleteCommand(AppDelegate::get()->renderView()->context());
	AppDelegate::get()->renderView()->context()->makeCurrent();
	eachSelectedPList([&](vwgl::PolyList * plist) {
		cmd->addPolyList(plist);
	});
	//sel.eachPolyList([&](PolyList * plist) {
	//	cmd->addPolyList(plist);
	//});
	sel.eachDrawable([&](vwgl::scene::Drawable * drw) {
		cmd->addParentDrawable(drw);
	});

	AppDelegate::get()->commandManager().execute(cmd);
	AppDelegate::get()->inputMediator().selectionHandler().clearSelection();
	AppDelegate::get()->updateRenderView();
}

void PolyListSettingsWindow::detachPlist(vwgl::manipulation::SelectionHandler & sel) {
	cmd::PListDetachCommand * cmd = new cmd::PListDetachCommand(AppDelegate::get()->renderView()->context());
	AppDelegate::get()->renderView()->context()->makeCurrent();
	eachSelectedPList([&](vwgl::PolyList * plist) {
		cmd->addPolyList(plist);
	});
	sel.eachDrawable([&](vwgl::scene::Drawable * drw) {
		cmd->addParentDrawable(drw);
	});

	AppDelegate::get()->commandManager().execute(cmd);
	AppDelegate::get()->inputMediator().selectionHandler().clearSelection();
	AppDelegate::get()->updateRenderView();
}

void PolyListSettingsWindow::duplicatePlist(vwgl::manipulation::SelectionHandler & sel) {
	cmd::PListDuplicateCommand * cmd = new cmd::PListDuplicateCommand(AppDelegate::get()->renderView()->context());
	AppDelegate::get()->renderView()->context()->makeCurrent();
	eachSelectedPList([&](vwgl::PolyList * plist) {
		cmd->addPolyList(plist);
	});
	sel.eachDrawable([&](vwgl::scene::Drawable * drw) {
		cmd->addParentDrawable(drw);
	});

	AppDelegate::get()->commandManager().execute(cmd);
	AppDelegate::get()->inputMediator().selectionHandler().clearSelection();
	AppDelegate::get()->updateRenderView();
}

void PolyListSettingsWindow::executeCommand(vwgl::cmd::PolyListCommand * cmd) {
	_preventUpdate = true;
	manipulation::SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
	std::vector<PolyList*> plistVector;
	if (_list->currentItem() && sel.getPolyListList().size()==static_cast<size_t>(_list->count())) {
		QModelIndexList indexes = _list->selectionModel()->selectedIndexes();
		foreach (QModelIndex index, indexes) {
			int position = index.row();
			if (sel.getPolyListList().size()>static_cast<size_t>(position)) {
				manipulation::SelectionHandler::PolyListList::iterator p_it = std::next(sel.getPolyListList().begin(), position);
				plistVector.push_back((*p_it).getPtr());
			}
		}
	}

	std::vector<PolyList*>::iterator it;
	for (it=plistVector.begin(); it!=plistVector.end(); ++it) {
		PolyList * plist = *it;
		cmd->addPolyList(plist);
	}

	AppDelegate::get()->commandManager().execute(cmd);
	AppDelegate::get()->updateRenderView();
	_preventUpdate = false;
}

void PolyListSettingsWindow::blockFieldSignals(bool block) {
	_name->blockSignals(block);
	_groupName->blockSignals(block);
	_visible->blockSignals(block);
	_list->blockSignals(block);
}

void PolyListSettingsWindow::eachSelectedPList(std::function<void(vwgl::PolyList * plist)> cb) {
	manipulation::SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
	std::vector<PolyList*> plistVector;
	if (_list->currentItem() && sel.getPolyListList().size()==static_cast<size_t>(_list->count())) {
		QModelIndexList indexes = _list->selectionModel()->selectedIndexes();
		foreach (QModelIndex index, indexes) {
			int position = index.row();
			if (sel.getPolyListList().size()>static_cast<size_t>(position)) {
				manipulation::SelectionHandler::PolyListList::iterator p_it = std::next(sel.getPolyListList().begin(), position);
				plistVector.push_back((*p_it).getPtr());
			}
		}
	}

	std::vector<PolyList*>::iterator it;
	for (it=plistVector.begin(); it!=plistVector.end(); ++it) {
		cb(*it);
	}
}

}
}
