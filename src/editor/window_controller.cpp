
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/window_controller.hpp>

#include <vwgl/state.hpp>

#define UNUSED(arg) (void)arg;

namespace bg2e {
namespace editor {

using namespace vwgl;

WindowController::WindowController()
{
	
}

WindowController::~WindowController() {
	
}

void WindowController::initGL() {
	State::get()->enableDepthTest();
}

void WindowController::reshape(int w, int h) {
	Size2Di size(w,h);
	AppDelegate::get()->renderer().resize(size);
	AppDelegate::get()->inputMediator().resize(size);
}

void WindowController::draw() {
	AppDelegate::get()->renderer().update();
	AppDelegate::get()->renderer().draw();
	AppDelegate::get()->drawPlistSelection().draw();
	AppDelegate::get()->inputMediator().drawGizmos();
	AppDelegate::get()->renderer().drawUI();

	AppDelegate::get()->inputMediator().endFrame();
}

void WindowController::frame(float delta) {
	AppDelegate::get()->renderer().frame(delta);
}

void WindowController::destroy() {

}

void WindowController::keyUp(const vwgl::app::KeyboardEvent & evt) {
	AppDelegate::get()->inputMediator().keyUp(evt);
}

void WindowController::keyDown(const vwgl::app::KeyboardEvent & evt) {
	AppDelegate::get()->inputMediator().keyDown(evt);
}

void WindowController::mouseDown(const vwgl::app::MouseEvent & evt) {
	AppDelegate::get()->inputMediator().mouseDown(evt);
}

void WindowController::mouseDrag(const vwgl::app::MouseEvent & evt) {
	AppDelegate::get()->inputMediator().mouseDrag(evt);
}

void WindowController::mouseMove(const vwgl::app::MouseEvent & evt) {
	if (!evt.mouse().anyButtonPressed()) {
		AppDelegate::get()->inputMediator().mouseMove(evt);
	}
}

void WindowController::mouseUp(const vwgl::app::MouseEvent & evt) {
	AppDelegate::get()->inputMediator().mouseUp(evt);
}

void WindowController::mouseWheel(const vwgl::app::MouseEvent & evt) {
	AppDelegate::get()->inputMediator().mouseWheel(evt);
}

}
}
