
#include <bg2e/editor/status_bar.hpp>
#include <bg2e/editor/app_delegate.hpp>

#include <vwgl/boundingbox.hpp>

#include <QHBoxLayout>

namespace bg2e {
namespace editor {

using namespace vwgl;

StatusBar::StatusBar()
	:QStatusBar(nullptr)
	,_warningWindow(nullptr)
{
	_gridVisible = new QCheckBox(tr("Show grid"));
	_gridVisible->setChecked(true);
	_axisVisible = new QCheckBox(tr("Show axis"));
	_axisVisible->setChecked(true);
	_inlineText = new QLabel();
	_selectionText = new QLabel();
	_gridSizeText = new QLabel();
	_warningText = new QLabel();
	_warningText->setStyleSheet("color: red;");
	_warningText->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	_infoButton = new QPushButton("+ info");
	_infoButton->hide();

	connect(_infoButton,SIGNAL(clicked()),this,SLOT(showMoreInfo()));

	addPermanentWidget(_axisVisible);
	addPermanentWidget(_gridVisible);
	addPermanentWidget(_gridSizeText);
	addPermanentWidget(_selectionText);
	addPermanentWidget(_inlineText);
	addPermanentWidget(_warningText,1);
	addPermanentWidget(_infoButton);

	connect(_axisVisible,SIGNAL(clicked(bool)),this,SLOT(axisToggle(bool)));
	connect(_gridVisible,SIGNAL(clicked(bool)),this,SLOT(gridToggle(bool)));

	AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
	AppDelegate::get()->inputMediator().gizmoManager().registerObserver(this);
}

void StatusBar::updateNow() {
	selectionChanged(AppDelegate::get()->inputMediator().selectionHandler());
}

void StatusBar::gridToggle(bool checked) {
	if (checked) {
		AppDelegate::get()->grid()->show();
	}
	else {
		AppDelegate::get()->grid()->hide();
	}
	AppDelegate::get()->updateRenderView();
}

void StatusBar::axisToggle(bool checked) {
	if (checked) {
		AppDelegate::get()->axis()->show();
	}
	else {
		AppDelegate::get()->axis()->hide();
	}
	AppDelegate::get()->updateRenderView();
}

void StatusBar::selectionChanged(vwgl::manipulation::SelectionHandler &sender) {
	_warningLog = "";
	bool webglPolyWarning = false;
	bool textureWarning = false;
	int totalPolys = 0;
	sender.eachPolyList([&](PolyList * plist) {
		int numIndexes = plist->getIndexCount();
		int numPolys = numIndexes / 3;
		totalPolys += numPolys;
		if (numIndexes>0x7FFF) {	// WebGL stores the index buffer in a short type, so, the maximum number of indexes are 0x7FFF
			webglPolyWarning = true;
			_warningLog += tr("The polyList ") + QString::fromStdString(plist->getName()) + " " + tr("Exceeds the maximum number of triangles allowed in WebGL") + "\n";
		}
	});
	sender.eachMaterial([&](Material * mat) {
		GenericMaterial * gmat = dynamic_cast<GenericMaterial*>(mat);
		if (gmat) {
			textureWarning = !checkPowerOfTwo(gmat->getShininessMask()) || textureWarning;
			textureWarning = !checkPowerOfTwo(gmat->getLightEmissionMask()) || textureWarning;
			textureWarning = !checkPowerOfTwo(gmat->getTexture()) || textureWarning;
			textureWarning = !checkPowerOfTwo(gmat->getLightMap()) || textureWarning;
			textureWarning = !checkPowerOfTwo(gmat->getNormalMap()) || textureWarning;
		}
	});
	if (sender.getNodeList().size()==1) {
		vwgl::ptr<vwgl::scene::Node> node = sender.getNodeList().front();
		vwgl::ptr<vwgl::BoundingBox> bbox = new vwgl::BoundingBox(node.getPtr());
		vwgl::Vector3 size = bbox->size();
		_selectionText->setText(tr("Selected item size: x=") + QString::number(size.x(),'g',4) + ", y=" +
								QString::number(size.y(),'g',4) + ", z=" +
								QString::number(size.z(),'g',4) + " " + tr("meters"));
	}
	else {
		_selectionText->setText("");
	}

	if (totalPolys>0) {
		_polygonsText = QString(" ") + QString::number(totalPolys) + " " + tr("triangles selected");
		if (webglPolyWarning) {
			_polygonsWarning = tr("Some pieces exceed the maximum number of polygons for WebGL use.");
		}
		else {
			_polygonsWarning = "";
		}
	}
	else {
		_polygonsText = "";
		_polygonsWarning = "";
	}

	if (textureWarning) {
		_texturesText = "";
		_texturesWarning = tr("They are potential problems with some textures.");
	}
	else {
		_texturesText = "";
		_texturesWarning = "";
	}

	updateText();
	updateWarning();
}

void StatusBar::gizmoMoved(vwgl::manipulation::GizmoManager &) {
	updateNow();
}

void StatusBar::updateText() {
	_inlineText->setText(_polygonsText);
}

void StatusBar::updateWarning() {
	if (!_polygonsWarning.isEmpty() || !_texturesWarning.isEmpty()) {
		_warningText->setText(tr("WARNING: ") + _polygonsWarning + " " + _texturesWarning);
		_infoButton->show();
	}
	else {
		_warningText->setText("");
		_infoButton->hide();
	}
}

bool StatusBar::checkPowerOfTwo(vwgl::Texture * tex) {
	if (tex) {
		if (!vwgl::Math::isPowerOfTwo(tex->getWidth()) || !vwgl::Math::isPowerOfTwo(tex->getHeight())) {
			_warningLog += tr("The texture file ") + QString::fromStdString(tex->getFileName()) + tr(" may cause problems because its width and/or height is not power of two") + "\n";
			return false;
		}
	}
	return true;
}

void StatusBar::showMoreInfo() {
	if (_warningWindow) {
		_warningWindow->close();
		delete _warningWindow;
		_warningWindow = nullptr;
	}
	_warningWindow = new QDialog(AppDelegate::get()->mainWindow());
	QVBoxLayout * layout = new QVBoxLayout();
	_warningWindow->setLayout(layout);

	QScrollArea * scroll = new QScrollArea();
	layout->addWidget(scroll);
	scroll->setMinimumHeight(400);
	scroll->setMinimumWidth(500);

	scroll->setLayout(new QVBoxLayout());
	scroll->setStyleSheet("background-color: transparent");
	QLabel * warningLabel = new QLabel(_warningLog);
	warningLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	warningLabel->setWordWrap(true);
	scroll->layout()->addWidget(warningLabel);
	QWidget * buttonGroup = new QWidget();
	QHBoxLayout * buttonLayout = new QHBoxLayout();
	buttonGroup->setLayout(buttonLayout);
	layout->addWidget(buttonGroup);

	QPushButton * closeButton = new QPushButton(tr("Close"));

	buttonGroup->setStyleSheet("background-color: transparent");
	buttonLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding, QSizePolicy::Minimum));
	buttonLayout->addWidget(closeButton);
	connect(closeButton,SIGNAL(clicked()),this,SLOT(closeMessage()));

	_warningWindow->setModal(true);
	_warningWindow->open();
}

void StatusBar::closeMessage() {
	if (_warningWindow) {
		_warningWindow->close();
		delete _warningWindow;
		_warningWindow = nullptr;
	}
}

}
}
