
#include <bg2e/editor/draw_plist_selection.hpp>

#include <vwgl/graphics.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/polylist.hpp>


namespace bg2e {
namespace editor {

using namespace vwgl;

std::string DrawPlistSelectionMaterial::s_vshader =
	"attribute vec4 in_Position;\n"
	
	"uniform mat4 uMVPMatrix;\n"
	
	"void main(void) {\n"
	"	gl_Position = uMVPMatrix * in_Position;\n"
	"}\n";

std::string DrawPlistSelectionMaterial::s_fshader =

	"uniform vec4 uDrawColor;\n"
	
	"void main(void) {\n"
	"	gl_FragColor = uDrawColor;\n"
	"}\n";

std::string DrawPlistSelectionMaterial::s_vshader_gl3 =
	"#version 150\n"
	"in vec4 in_Position;\n"
	
	"uniform mat4 uMVPMatrix;\n"
		
	"void main(void) {\n"
	"	gl_Position = uMVPMatrix * in_Position;\n"
	"}\n";

std::string DrawPlistSelectionMaterial::s_fshader_gl3 =
	"#version 150\n"
	
	"out vec4 out_Color;\n"
	
	"uniform vec4 uDrawColor;\n"
	
	"void main(void) {\n"
	"	out_Color = uDrawColor;\n"
	"}\n";

DrawPlistSelectionMaterial::DrawPlistSelectionMaterial()
{
	setReuseShaderKey("draw_plist_selection_material");
	initShader();
}

DrawPlistSelectionMaterial::~DrawPlistSelectionMaterial() {

}

void DrawPlistSelectionMaterial::initShader() {
	if (Graphics::get()->getApi() == Graphics::kApiOpenGL || Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLBasic) {
		getShader()->attachShader(Shader::kTypeVertex, s_vshader);
		getShader()->attachShader(Shader::kTypeFragment, s_fshader);
	}
	else if (Graphics::get()->getApi() == Graphics::kApiOpenGLAdvanced) {
		getShader()->attachShader(Shader::kTypeVertex, s_vshader_gl3);
		getShader()->attachShader(Shader::kTypeFragment, s_fshader_gl3);
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Color");
	}
	else {
		std::cerr << "Warnings: DrawPlistSelectionMaterial shader is not compatible with the current API" << std::endl;
	}
	getShader()->link();

	loadVertexAttrib("in_Position");

	getShader()->initUniformLocation("uMVPMatrix");
	getShader()->initUniformLocation("uDrawColor");
}

void DrawPlistSelectionMaterial::setupUniforms() {
	vwgl::Matrix4 mv = vwgl::State::get()->modelViewMatrix();
	vwgl::Matrix4 mvp = vwgl::State::get()->projectionMatrix();
	mvp.mult(mv);
	getShader()->setUniform("uMVPMatrix", mvp);

	Color c = Color::red();
	c.a(0.5f);
	getShader()->setUniform("uDrawColor",c);
}

DrawPlistSelectionVisitor::DrawPlistSelectionVisitor(DrawPlistSelection * mgr)
	:_manager(mgr)
{

}

void DrawPlistSelectionVisitor::visit(vwgl::scene::Node * node) {
	node->willDraw();
	DrawPlistSelectionMaterial * mat = material();

	vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
	if (drw) {
		drw->eachElement([&](vwgl::PolyList * plist, vwgl::Material *, vwgl::Transform & transform) {
			if (this->_manager->isSelected(plist)) {
				vwgl::State::get()->pushModelMatrix();
				vwgl::State::get()->modelMatrix().mult(transform.getMatrix());


				mat->bindPolyList(plist);
				mat->activate();

				plist->drawElements();

				mat->deactivate();

				vwgl::State::get()->popModelMatrix();
			}
		});
	}
	
}

void DrawPlistSelectionVisitor::didVisit(vwgl::scene::Node * node) {
	node->didDraw();
}

DrawPlistSelectionMaterial * DrawPlistSelectionVisitor::material() {
	if (!_mat.valid()) {
		_mat = new DrawPlistSelectionMaterial();
	}
	return _mat.getPtr();
}

DrawPlistSelection::DrawPlistSelection()
{
	_visitor = new DrawPlistSelectionVisitor(this);
}

DrawPlistSelection::~DrawPlistSelection() {
	delete _visitor;
}

void DrawPlistSelection::draw() {
	if (_sceneRoot.valid()) {
		vwgl::State::get()->enableBlend(vwgl::State::kBlendFunctionAlpha);
		vwgl::State::get()->clearBuffers(vwgl::State::kBufferDepth);
		vwgl::scene::Camera::applyTransform(vwgl::scene::Camera::getMainCamera());
		_sceneRoot->accept(*_visitor);
		vwgl::State::get()->disableBlend();
	}
}

}
}
