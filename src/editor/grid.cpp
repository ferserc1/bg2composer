
#include <iostream>

#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/grid.hpp>

#include <vwgl/generic_material.hpp>
#include <vwgl/scene/camera.hpp>

namespace bg2e {
namespace editor {
	
using namespace vwgl;

Grid::Grid()
	:_visible(true)
	,_updateGrid(true)
	,_size(20)
	,_gridSize(100.0f)
{
	
}

Grid::~Grid()
{
	
}

void Grid::update() {
	vwgl::scene::Camera * cam = vwgl::scene::Camera::getMainCamera();
	if (cam) {
		Vector3 pos = cam->getTransform().getPosition();
		float dist = pos.distance(Vector3());
		float scale = roundf(dist / 10.0f) * 30.0f;
		if (scale<50.0f) {
			scale = 50.0f;
		}
		if (scale!=this->getSize()) {
			setGridSize(scale);
		}
	}

	if (_updateGrid) {
		_polyList.clear();
		_material.clear();
		_transform.clear();

		ptr<PolyList> plist = new PolyList();

		float width = _gridSize;
		float height = _gridSize;
		float x = width / 2.0f;
		float y = height / 2.0f;
		float incX = width / static_cast<float>(_size);
		float incY = height / static_cast<float>(_size);
		float offsetX = incX / 2.0f;
		float offsetY = incY / 2.0f;
		AppDelegate::get()->statusBar().setGridSize(incX);

		for (int i=0; i<=_size; ++i) {

			// Horizontal line
			plist->addVertex(vwgl::Vector3(offsetX -x, 0.0f,offsetY - y + incY * static_cast<float>(i)));
			plist->addVertex(vwgl::Vector3(offsetX + x, 0.0f,offsetY - y + incY * static_cast<float>(i)));

			// Vertical line
			plist->addVertex(vwgl::Vector3(offsetX -x + incX * static_cast<float>(i), 0.0f,offsetY - y));
			plist->addVertex(vwgl::Vector3(offsetX -x + incX * static_cast<float>(i), 0.0f,offsetY + y));

			plist->addNormal(Vector3(0.0f, 1.0f, 0.0f));
			plist->addNormal(Vector3(0.0f, 1.0f, 0.0f));
			plist->addNormal(Vector3(0.0f, 1.0f, 0.0f));
			plist->addNormal(Vector3(0.0f, 1.0f, 0.0f));

			plist->addTexCoord0(Vector2(0.0f));
			plist->addTexCoord0(Vector2(0.0f));
			plist->addTexCoord0(Vector2(0.0f));
			plist->addTexCoord0(Vector2(0.0f));

			plist->addIndex(i * 4); plist->addIndex(i * 4 + 1);
			plist->addIndex(i * 4 + 2); plist->addIndex(i * 4 + 3);

		}

		plist->buildPolyList();
		plist->setDrawMode(vwgl::PolyList::kLines);

		ptr<GenericMaterial> mat = new GenericMaterial();
		mat->setCullFace(false);
		mat->setCastShadows(false);
		mat->setLightEmission(1.0f);
		mat->setReceiveShadows(false);
		addPolyList(plist.getPtr(),mat.getPtr(),Transform());

		_updateGrid = false;
	}
}

void Grid::draw() {
	if (_visible) {
		Drawable::draw();
	}
}

}
}
