
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/component_settings_window.hpp>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>

#include <bg2e/components/registry.hpp>

#include <vwgl/scene/scene.hpp>
#include <vwgl/cmd/scene_commands.hpp>

#include <algorithm>

namespace bg2e {
namespace editor {

using namespace vwgl::manipulation;
using namespace vwgl::scene;

class AddComponentWithFactoryCommand : public app::ContextCommand {
public:
	AddComponentWithFactoryCommand(plain_ptr ctx, components::ComponentUIFactory * factory) :app::ContextCommand(ctx), _factory(factory) {}


	inline void addTargetNode(vwgl::scene::Node * node) {
		bool addComponent = true;
		ptr<vwgl::scene::Component> newComp = _factory->create();
		node->eachComponent([&](vwgl::scene::Component * comp) {
			if (typeid(comp)==typeid(newComp.getPtr())) {
				addComponent = true;
			}
		});
		if (addComponent) {
			_targetNodes.push_back(node);
			_components.push_back(newComp.getPtr());
		}
	}



	void doCommand() {
		if (_targetNodes.size()==0) throw std::runtime_error("Could not add component: the node list is empty");
		vwgl::scene::Node::NodeVector::iterator it = _targetNodes.begin();
		std::vector<ptr<vwgl::scene::Component> >::iterator cit = _components.begin();

		for (;it!=_targetNodes.end(); ++it, ++cit) {
			vwgl::scene::Node * node = (*it).getPtr();
			vwgl::scene::Component * comp = (*cit).getPtr();
			node->addComponent(comp);
			_factory->configure(node, comp);
			node->init();
		}
	}

	void undoCommand() {
		vwgl::scene::Node::NodeVector::iterator it = _targetNodes.begin();
		std::vector<ptr<vwgl::scene::Component> >::iterator cit = _components.begin();

		for (;it!=_targetNodes.end(); ++it, ++cit) {
			vwgl::scene::Node * node = (*it).getPtr();
			vwgl::scene::Component * comp = (*cit).getPtr();
			node->removeComponent(comp);
		}
	}

protected:
	components::ComponentUIFactory * _factory;
	vwgl::scene::Node::NodeVector _targetNodes;
	std::vector<ptr<vwgl::scene::Component> > _components;
};

ComponentSettingsWindow::ComponentSettingsWindow()
	:ToolWindow(QObject::tr("Component Settings"))
	,_browser(nullptr)
{
	build();
	_nodeName = new ui::TextField(tr("Node name"));
	_nodeName->onEditFinished([&](ui::TextField * textField) {
		SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
		if (sel.getNodeList().size()>0) {
			ptr<cmd::SetNodeNameCommand> cmd = new cmd::SetNodeNameCommand(textField->getText());
			sel.eachNode([&](vwgl::scene::Node * node) {
				cmd->addTargetNode(node);
			});
			AppDelegate::get()->commandManager().execute(cmd.getPtr());
			AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
		}
	});
	_enabled = new ui::BooleanField(tr("Enabled"));
	_enabled->onValueChanged([&](ui::BooleanField * field) {
		SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
		if (sel.getNodeList().size()>0) {
			ptr<cmd::SetNodeEnabledCommand> cmd = new cmd::SetNodeEnabledCommand(field->value());
			sel.eachNode([&](vwgl::scene::Node * node) {
				cmd->addTargetNode(node);
			});
			AppDelegate::get()->commandManager().execute(cmd.getPtr());
			AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
		}
	});
	AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
}

void ComponentSettingsWindow::selectionChanged(SelectionHandler &sender) {
	clear();
	addField(_nodeName);
	addField(_enabled);
	_nodeName->hide();
	_enabled->hide();
	addSeparator();
	bool multiple = false;
	std::string lastName;
	sender.eachNode([&](vwgl::scene::Node * node) {
		_nodeName->show();
		_enabled->show();
		if (multiple) {
			if (lastName!=node->getName()) {
				lastName = "<multiple values>";
			}
		}
		else {
			lastName = node->getName();
		}
		_nodeName->setText(QString::fromStdString(lastName));
		_enabled->setValue(node->isEnabled());
		showComponentSettings(node);
		multiple = true;
	});


	QWidget * footerWidget = new QWidget();
	QHBoxLayout * footerLayout = new QHBoxLayout();
	footerWidget->setLayout(footerLayout);

	footerLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum));
	QPushButton * addBtn = new QPushButton(tr("Add Component"));
	footerLayout->addWidget(addBtn);
	connect(addBtn,SIGNAL(clicked()),this,SLOT(addButtonPressed()));

	addWidget(footerWidget);

	endWidget();
}

void ComponentSettingsWindow::build() {
	setObjectName("ComponentSettingsWindow");
	setMinimumWidth(350);
	components::Registry::get()->registerFactories();
	endWidget();
}

void ComponentSettingsWindow::showComponentSettings(vwgl::scene::Node * node) {
	node->eachComponent([&](Component * component) {
		QWidget * widget = components::Registry::get()->getWidgetForComponent(component);
		QObjectList::const_iterator it = std::find(_centralWidget->children().begin(),_centralWidget->children().end(),widget);
		if (widget && it==_centralWidget->children().end()) {
			_centralWidget->layout()->addWidget(widget);
			widget->show();
			addSeparator();
		}
		else if (widget && widget->parent()==nullptr) {
			widget->hide();
		}
	});
}

void ComponentSettingsWindow::addButtonPressed() {
	if (!_browser) {
		_browser = new components::ComponentBrowser(AppDelegate::get()->mainWindow());
		_browser->setDoneCallback([](components::ComponentUIFactory * factory) {
			vwgl::manipulation::SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
			plain_ptr ctx = AppDelegate::get()->renderView()->context();
			AddComponentWithFactoryCommand * cmd = new AddComponentWithFactoryCommand(ctx,factory);

			sel.eachNode([&](vwgl::scene::Node * node) {
				cmd->addTargetNode(node);
			});

			AppDelegate::get()->commandManager().execute(cmd);
			AppDelegate::get()->updateRenderView();
			AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
		});
	}
	_browser->show();

}

}
}
