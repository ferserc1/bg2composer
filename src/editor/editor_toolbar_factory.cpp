
#include <bg2e/editor/editor_toolbar_factory.hpp>
#include <bg2e/editor/manipulator_gizmo_toolbar.hpp>
#include <bg2e/editor/creation_toolbar.hpp>
#include <bg2e/editor/panels_toolbar.hpp>
#include <bg2e/editor/animation_toolbar.hpp>

namespace bg2e {
namespace editor {

void EditorToolBarFactory::build(QMainWindow * window) {
	window->addToolBar(new ManipulatorGizmoToolBar("Manipulation Gizmos"));
	window->addToolBar(new CreationToolBar("Creation Tools"));
	window->addToolBar(new PanelsToolBar("Panels"));
	window->addToolBar(new AnimationToolBar("Animation"));

}

}
}
