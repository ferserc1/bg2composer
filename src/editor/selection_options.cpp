
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/selection_options.hpp>
#include <bg2e/closure_action.hpp>

#include <vwgl/cmd/delete_command.hpp>
#include <vwgl/scene/transform_visitor.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/boundingbox.hpp>
#include <vwgl/manipulation/transform_controller.hpp>

#include <vector>

namespace bg2e {
namespace editor {

class SelectDrawableVisitor : public vwgl::scene::NodeVisitor {
public:
	virtual void visit(vwgl::scene::Node * n) {
		vwgl::scene::Drawable * drw = n->getComponent<vwgl::scene::Drawable>();
		if (drw) {
			_result.push_back(drw);
		}
	}

	inline void eachDrawable(std::function<void(vwgl::scene::Drawable *)> cb) {
		for (auto d_ptr : _result) {
			cb(d_ptr.getPtr());
		}
	}

protected:
	std::vector<vwgl::ptr<vwgl::scene::Drawable> > _result;
};

SelectionOptions::SelectionOptions()
	:_selectionWindow(nullptr)
{

}

void SelectionOptions::init() {
	AppDelegate::get()->commandManager().registerObserver(this);
	AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
	ClosureAction * selectAll = new ClosureAction(QObject::tr("Select All Drawables"),[&]() {
		SelectDrawableVisitor visitor;
		AppDelegate::get()->scene().getSceneRoot()->accept(visitor);
		visitor.eachDrawable([&](vwgl::scene::Drawable * drw) {
			selectDrawable(drw);
		});

		AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
		AppDelegate::get()->updateRenderView();
	});
	selectAll->setShortcut(QKeySequence::SelectAll);
	AppDelegate::get()->menuFactory().addAction("Selection/:selectAll", selectAll);

	AppDelegate::get()->menuFactory().addSeparator("Selection/:separator");

	ClosureAction * centerOnSelection = new ClosureAction(QObject::tr("Zoom all extents"), [&]() {
		centerSelection();
	});
	centerOnSelection->setShortcut(QKeySequence(Qt::Key_Z));
	AppDelegate::get()->menuFactory().addAction("Selection/:centerView", centerOnSelection);

	ClosureAction * selectByName = new ClosureAction(QObject::tr("Select by Name"), QKeySequence(Qt::CTRL + Qt::Key_F), [&]() {
		if (_selectionWindow==nullptr) {
			_selectionWindow = new SelectionWindow();
		}
		_selectionWindow->show();
	});
	AppDelegate::get()->menuFactory().addAction("Selection/:selectByName",selectByName);

	AppDelegate::get()->menuFactory().addSeparator("Selection/:separator");

	checkEnabled();
}

void SelectionOptions::checkEnabled() {
}

void SelectionOptions::selectDrawable(vwgl::scene::Drawable *drw) {
	vwgl::manipulation::SelectionHandler & selection = AppDelegate::get()->inputMediator().selectionHandler();

	if (drw) {
		vwgl::scene::Node* node = drw->node();
		selection.lockNotifications();
		selection.setAdditiveSelection(true);

		drw->eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform &) {
			selection.selectPolyList(plist);
			selection.selectMaterial(mat);
		});
		selection.selectDrawable(drw);
		if (node) selection.selectNode(node);
		selection.setAdditiveSelection(false);
		selection.unlockNotifications();
		AppDelegate::get()->inputMediator().gizmoManager().setManipulateObject(node);
	}
}

void SelectionOptions::centerSelection() {
	vwgl::manipulation::SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
	vwgl::ptr<vwgl::BoundingBox> bbox = new vwgl::BoundingBox();
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::TransformVisitor visitor;
		vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
		node->acceptReverse(visitor);
		vwgl::Vector4 center = visitor.getMatrix().multVector(vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f));
		if (drw) {
			vwgl::ptr<vwgl::BoundingBox> drwBBox = new vwgl::BoundingBox(drw);
			vwgl::Vector3 max = center.xyz() + drwBBox->max();
			vwgl::Vector3 min = center.xyz() + drwBBox->min();
			bbox->addVertex(max);
			bbox->addVertex(min);
		}
		else {
			bbox->addVertex(center.xyz());
		}
	});

	vwgl::scene::Camera * cam = vwgl::scene::Camera::getMainCamera();
	vwgl::manipulation::TargetInputController * ctrl = nullptr;
	if (cam && cam->node() &&
		(ctrl = cam->node()->getComponent<vwgl::manipulation::TargetInputController>())) {
		ctrl->setCenter(bbox->center());
		vwgl::Vector3 size = bbox->size();
		float minDistance = 2.0f;
		float max = size.x()>size.y() ? size.x():size.y();
		max = size.z()>max ? size.z():max;
		max = minDistance>max ? minDistance:max;
		ctrl->setDistance(max * 1.5f);
		AppDelegate::get()->updateRenderView();
	}

}

}
}
