
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/closure_action.hpp>
#include <bg2e/editor/gizmo_view_options.hpp>

#include <vwgl/manipulation/gizmo_manager.hpp>

#include <bg2e/scene/collider.hpp>

namespace bg2e {
namespace editor {

using namespace vwgl::manipulation;

void GizmoViewOptions::init() {
	bool dirLight = AppDelegate::get()->inputMediator().gizmoManager().getIcon(GizmoManager::kIconLightDirectional);
	_directionalLight = new ClosureAction("Directional Light", [&]() { this->updateGizmoVisibility(); });
	_directionalLight->setCheckable(true);
	_directionalLight->setChecked(dirLight);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option", _directionalLight);

	bool spotLight = AppDelegate::get()->inputMediator().gizmoManager().getIcon(GizmoManager::kIconLightSpot);
	_spotLight = new ClosureAction("Spot Light", [&]() { this->updateGizmoVisibility(); });
	_spotLight->setCheckable(true);
	_spotLight->setChecked(spotLight);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option", _spotLight);

	bool pointLight = AppDelegate::get()->inputMediator().gizmoManager().getIcon(GizmoManager::kIconLightSpot);
	_pointLight =  new ClosureAction("Point Light", [&]() { this->updateGizmoVisibility(); });
	_pointLight->setCheckable(true);
	_pointLight->setChecked(pointLight);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option", _pointLight);

	bool camera = AppDelegate::get()->inputMediator().gizmoManager().getIcon(GizmoManager::kIconCamera);
	_camera =  new ClosureAction("Camera", [&]() { this->updateGizmoVisibility(); });
	_camera->setCheckable(true);
	_camera->setChecked(camera);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option",_camera);

	bool trx = AppDelegate::get()->inputMediator().gizmoManager().getIcon(GizmoManager::kIconTransform);
	_transform =  new ClosureAction("Transform", [&]() { this->updateGizmoVisibility(); });
	_transform->setCheckable(true);
	_transform->setChecked(trx);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option",_transform);

	bool drw = AppDelegate::get()->inputMediator().gizmoManager().getIcon(GizmoManager::kIconDrawable);
	_drawable = new ClosureAction("Drawable", [&]() { this->updateGizmoVisibility(); });
	_drawable->setCheckable(true);
	_drawable->setChecked(drw);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option", _drawable);

	bool joint = AppDelegate::get()->inputMediator().gizmoManager().getIcon(GizmoManager::kIconJoint);
	_joint = new ClosureAction("Joint", [&]() { this->updateGizmoVisibility(); });
	_joint->setCheckable(true);
	_joint->setChecked(joint);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option",_joint);

	bool collider = bg2e::scene::Collider::isShowGizmos();
	_collider = new ClosureAction("Collider", [&]() { this->updateGizmoVisibility(); });
	_collider->setCheckable(true);
	_collider->setChecked(collider);
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option",_collider);

	AppDelegate::get()->menuFactory().addSeparator("View/Gizmos/:separator");
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option", new ClosureAction("Show All Gizmos", [&]() {
		this->showAllGizmos();
	}));
	AppDelegate::get()->menuFactory().addAction("View/Gizmos/:option", new ClosureAction("Hide All Gizmos", [&]() {
		this->hideAllGizmos();
	}));

	_stats = new ClosureAction("Statistics", [&]() {
		AppDelegate::get()->stats()->setVisible(_stats->isChecked());
	});
	_stats->setCheckable(true);
	_stats->setChecked(false);
	AppDelegate::get()->menuFactory().addAction("View/:stats", _stats);
}

void GizmoViewOptions::showGizmo(vwgl::manipulation::GizmoManager::GizmoIcon icon) {
	switch (icon) {
	case GizmoManager::kIconLightDirectional:
		_directionalLight->setChecked(true);
		break;
	case GizmoManager::kIconLightSpot:
		_spotLight->setChecked(true);
		break;
	case GizmoManager::kIconLightPoint:
		_pointLight->setChecked(true);
		break;
	case GizmoManager::kIconCamera:
		_camera->setChecked(true);
		break;
	case GizmoManager::kIconTransform:
		_transform->setChecked(true);
		break;
	case GizmoManager::kIconDrawable:
		_drawable->setChecked(true);
		break;
	case GizmoManager::kIconJoint:
		_joint->setChecked(true);
		break;
	default:
		break;
	}
	updateGizmoVisibility();
}

void GizmoViewOptions::hideGizmo(vwgl::manipulation::GizmoManager::GizmoIcon icon) {
	switch (icon) {
	case GizmoManager::kIconLightDirectional:
		_directionalLight->setChecked(false);
		break;
	case GizmoManager::kIconLightSpot:
		_spotLight->setChecked(false);
		break;
	case GizmoManager::kIconLightPoint:
		_pointLight->setChecked(false);
		break;
	case GizmoManager::kIconCamera:
		_camera->setChecked(false);
		break;
	case GizmoManager::kIconTransform:
		_transform->setChecked(false);
		break;
	case GizmoManager::kIconDrawable:
		_drawable->setChecked(false);
		break;
	case GizmoManager::kIconJoint:
		_joint->setChecked(false);
		break;
	default:
		break;
	}
	updateGizmoVisibility();
}

void GizmoViewOptions::showAllGizmos() {
	_directionalLight->setChecked(true);
	_spotLight->setChecked(true);
	_pointLight->setChecked(true);
	_camera->setChecked(true);
	_transform->setChecked(true);
	_drawable->setChecked(true);
	_joint->setChecked(true);
	_collider->setChecked(true);
	updateGizmoVisibility();
}

void GizmoViewOptions::hideAllGizmos() {
	_directionalLight->setChecked(false);
	_spotLight->setChecked(false);
	_pointLight->setChecked(false);
	_camera->setChecked(false);
	_transform->setChecked(false);
	_drawable->setChecked(false);
	_joint->setChecked(false);
	_collider->setChecked(false);
	updateGizmoVisibility();
}

void GizmoViewOptions::updateGizmoVisibility() {
	GizmoManager & mgr = AppDelegate::get()->inputMediator().gizmoManager();
	if (_directionalLight->isChecked()) {
		mgr.showGizmo(GizmoManager::kIconLightDirectional);
	}
	else {
		mgr.hideGizmo(GizmoManager::kIconLightDirectional);
	}

	if (_spotLight->isChecked()) {
		mgr.showGizmo(GizmoManager::kIconLightSpot);
	}
	else {
		mgr.hideGizmo(GizmoManager::kIconLightSpot);
	}

	if (_pointLight->isChecked()) {
		mgr.showGizmo(GizmoManager::kIconLightPoint);
	}
	else {
		mgr.hideGizmo(GizmoManager::kIconLightPoint);
	}

	if (_camera->isChecked()) {
		mgr.showGizmo(GizmoManager::kIconCamera);
	}
	else {
		mgr.hideGizmo(GizmoManager::kIconCamera);
	}

	if (_transform->isChecked()) {
		mgr.showGizmo(GizmoManager::kIconTransform);
	}
	else {
		mgr.hideGizmo(GizmoManager::kIconTransform);
	}

	if (_drawable->isChecked()) {
		mgr.showGizmo(GizmoManager::kIconDrawable);
	}
	else {
		mgr.hideGizmo(GizmoManager::kIconDrawable);
	}

	if (_joint->isChecked()) {
		mgr.showGizmo(GizmoManager::kIconJoint);
	}
	else {
		mgr.hideGizmo(GizmoManager::kIconJoint) ;
	}

	if (_collider->isChecked()) {
		bg2e::scene::Collider::showGizmos();
	}
	else {
		bg2e::scene::Collider::hideGizmos();
	}
	AppDelegate::get()->updateRenderView();
}

}
}
