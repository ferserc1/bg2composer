
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/edit_options.hpp>
#include <bg2e/closure_action.hpp>

#include <vwgl/cmd/delete_command.hpp>

namespace bg2e {
namespace editor {

EditOptions::EditOptions() {

}

void EditOptions::init() {
	AppDelegate::get()->commandManager().registerObserver(this);
	AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
	_undo = new ClosureAction(QObject::tr("Undo"),[&]() {
		AppDelegate::get()->commandManager().undo();
		AppDelegate::get()->updateRenderView();
	});
	_undo->setShortcut(QKeySequence::Undo);
	AppDelegate::get()->menuFactory().addAction("Edit/:undo", _undo);

	_redo = new ClosureAction(QObject::tr("Redo"),[&]() {
		AppDelegate::get()->commandManager().redo();
		AppDelegate::get()->updateRenderView();
	});
	_redo->setShortcut(QKeySequence::Redo);
	AppDelegate::get()->menuFactory().addAction("Edit/:redo",_redo);

	AppDelegate::get()->menuFactory().addSeparator("Edit/:separator");

	_delete = new ClosureAction(QObject::tr("Delete"), []() {
		plain_ptr ctx = AppDelegate::get()->renderView()->context();
		vwgl::cmd::DeleteCommand * cmd = new vwgl::cmd::DeleteCommand(ctx);
		AppDelegate::get()->inputMediator().selectionHandler().eachNode([&](vwgl::scene::Node * node) {
			cmd->addNodeToDelete(node);
		});
		AppDelegate::get()->commandManager().execute(cmd);
		AppDelegate::get()->inputMediator().selectionHandler().clearSelection();
		AppDelegate::get()->updateRenderView();
	});
	_delete->setShortcut(QKeySequence("Ctrl+Backspace"));
	AppDelegate::get()->menuFactory().addAction("Edit/:delete", _delete);

	checkEnabled();
}

void EditOptions::checkEnabled() {
	_undo->setEnabled(AppDelegate::get()->commandManager().canUndo());
	_redo->setEnabled(AppDelegate::get()->commandManager().canRedo());
	_delete->setEnabled(AppDelegate::get()->inputMediator().selectionHandler().numberOfSelectedNodes()!=0);
}

}
}
