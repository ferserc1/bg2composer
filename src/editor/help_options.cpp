
#include <bg2e/editor/help_options.hpp>


#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/ui/about_panel.hpp>

#include <bg2e/closure_action.hpp>

#include <bg2e/version.hpp>

#include <vwgl/cmd/delete_command.hpp>

namespace bg2e {
namespace editor {

HelpOptions::HelpOptions() {

}

void HelpOptions::init() {
	_about = new ClosureAction(QObject::tr("About"),[&]() {
		QString title = QObject::tr("About bg2 Composer");
		QString header = QObject::tr("Business Grade Graphic Engine - Composer");
		QString body = QObject::tr("Copyright 2015 Vitaminew C.B.\nThis software is published under the GNU Public License (GPL) V3.\n"
								   "Composer GUI is powered by QT.\n"
								   "bg2 Engine is powered by VitaminewGL graphic library.\n"
								   "VitaminewGL uses the following third party libraries:\n"
								   " - irrXML: to load XML, used to parse Collada files.\n"
								   " - SOIL image library (only on Windows platform) to load textures from images.\n"
								   " - JSONBox: to load and write JSON objects.\n"
								   "This is an Open Source project developed by Fernando Serrano Carpena:\n"
								   "   http://bitbucket.org/ferserc1\n"
								   "   http://apache.vitaminew.com/~bg2"
								   "\n\nThis software contains Autodesk® FBX® code developed by Autodesk, Inc. "
								   "Copyright 2008 Autodesk, Inc. All rights, reserved. Such code is provided \"as is\""
								   " and Autodesk, Inc. disclaims any and all warranties, whether express or "
								   "implied, including without limitation the implied warranties of merchantability, "
								   "fitness for a particular purpose or non-infringement of third party rights. In no "
								   "event shall Autodesk, Inc. be liable for any direct, indirect, incidental,"
								   " special, exemplary, or consequential damages (including, but not limited "
								   "to, procurement of substitute goods or services; loss of use, data, or"
								   " profits; or business interruption) however caused and on any theory of "
								   "liability, whether in contract, strict liability, or tort (including "
								   "negligence or otherwise) arising in any way out of such code.");
		QString version = "Version: " + QString::fromStdString(Version::getVersionString());
		QString icon = ":/resources/icon256.png";
		ui::AboutPanel * about = new ui::AboutPanel(title,header,body,version,icon);
		about->show();
	});
	AppDelegate::get()->menuFactory().addAction("Help/:about", _about);
}

}
}
