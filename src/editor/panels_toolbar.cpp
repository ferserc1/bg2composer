
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/panels_toolbar.hpp>
#include <bg2e/closure_action.hpp>
#include <bg2e/menu_factory.hpp>

#include <vwgl/app/app.hpp>

#include <QIcon>

namespace bg2e {
namespace editor {

using namespace vwgl;
void PanelsToolBar::build() {
	setObjectName("ManipulatorGizmoToolBar");

	ClosureAction * sceneTree = new ClosureAction(QIcon(":/icons/light-icons/scene_graph_1.png"), QObject::tr("Scene Tree"), []() {
		if (AppDelegate::get()->toolWindowManager().toolWindow("sceneTreeWindow")->isVisible()) {
			AppDelegate::get()->toolWindowManager().toolWindow("sceneTreeWindow")->hide();
		}
		else {
			AppDelegate::get()->toolWindowManager().toolWindow("sceneTreeWindow")->show();
		}
	});
	AppDelegate::get()->menuFactory().addAction("Window/Panels/:sceneTree", sceneTree);
	addAction(sceneTree);

	addSeparator();

	ClosureAction * components = new ClosureAction(QIcon(":/icons/light-icons/piece.png"), QObject::tr("Component Settings"), []() {
		if (AppDelegate::get()->toolWindowManager().toolWindow("componentSettings")->isVisible()) {
			AppDelegate::get()->toolWindowManager().toolWindow("componentSettings")->hide();
		}
		else {
			AppDelegate::get()->toolWindowManager().toolWindow("componentSettings")->show();
		}
	});
	AppDelegate::get()->menuFactory().addAction("Window/Panels/:component", components);
	addAction(components);

	ClosureAction * materials = new ClosureAction(QIcon(":/icons/light-icons/material.png"), QObject::tr("Material Settings"), []() {
		if (AppDelegate::get()->toolWindowManager().toolWindow("materialSettings")->isVisible()) {
			AppDelegate::get()->toolWindowManager().toolWindow("materialSettings")->hide();
		}
		else {
			AppDelegate::get()->toolWindowManager().toolWindow("materialSettings")->show();
		}
	});
	AppDelegate::get()->menuFactory().addAction("Window/Panels/:material", materials);
	addAction(materials);

	ClosureAction * plist = new ClosureAction(QIcon(":/icons/light-icons/plist-settings.png"), QObject::tr("PolyList Settings"), []() {
		if (AppDelegate::get()->toolWindowManager().toolWindow("polyListSettings")->isVisible()) {
			AppDelegate::get()->toolWindowManager().toolWindow("polyListSettings")->hide();
		}
		else {
			AppDelegate::get()->toolWindowManager().toolWindow("polyListSettings")->show();
		}
	});
	AppDelegate::get()->menuFactory().addAction("Window/Panels/:plist", plist);
	addAction(plist);

	addSeparator();

	ClosureAction * materialsAction = new ClosureAction(QIcon(":/icons/light-icons/material_library.png"),"Material Library", QKeySequence("M"), [&]() {
		if (AppDelegate::get()->materialLibrary().getLibraryInspector()->isVisible()) {
			AppDelegate::get()->materialLibrary().getLibraryInspector()->hide();
		}
		else {
			AppDelegate::get()->materialLibrary().getLibraryInspector()->show();
		}
		AppDelegate::get()->updateRenderView();
	});
	addAction(materialsAction);
	AppDelegate::get()->menuFactory().addAction("Window/:materials", materialsAction);
}

}
}
