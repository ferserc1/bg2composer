
#include <QIcon>

#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/creation_toolbar.hpp>
#include <bg2e/closure_action.hpp>
#include <bg2e/menu_factory.hpp>

#include <vwgl/app/app.hpp>

#include <vwgl/cmd/scene_commands.hpp>
#include <vwgl/scene/primitive_factory.hpp>
#include <vwgl/cmd/import_command.hpp>
#include <vwgl/loader.hpp>

namespace bg2e {
namespace editor {

using namespace vwgl;
void CreationToolBar::build() {
	setObjectName("CreationToolBar");

	QAction * action = new ClosureAction(QIcon(":/icons/light-icons/add_node_2.png"),"Empty Node", QKeySequence(), [&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		child->addComponent(new manipulation::Selectable());
		child->setName("New node");
		vwgl::scene::Node * parent = currentNode();
		ptr<app::Command> cmd = new cmd::AddNodeCommand(context(), child.getPtr(), parent);
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		child->init();
		AppDelegate::get()->updateRenderView();
		AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Scene Tree/:name",action);
	AppDelegate::get()->menuFactory().addSeparator("Create/Scene Tree");

	addSeparator();

	action = new ClosureAction(QIcon(":/icons/light-icons/component-transform.png"),"Transform Component", QKeySequence(), [&]() {
		ptr<cmd::AddComponentCommand<vwgl::scene::Transform> > cmd = new cmd::AddComponentCommand<vwgl::scene::Transform>(context());
		AppDelegate::get()->inputMediator().selectionHandler().eachNode([&](vwgl::scene::Node * node) {
			vwgl::scene::Transform * trx = cmd->addTargetNode(node);
			if (trx) {
				trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
			}
		});
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		AppDelegate::get()->updateRenderView();
		AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Component/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/component-light.png"),"Light Component", QKeySequence(), [&]() {
		ptr<cmd::AddComponentCommand<vwgl::scene::Light> > cmd = new cmd::AddComponentCommand<vwgl::scene::Light>(context());
		AppDelegate::get()->inputMediator().selectionHandler().eachNode([&](vwgl::scene::Node * node) {
			vwgl::scene::Light * light = cmd->addTargetNode(node);
			if (light) {
				light->setType(vwgl::scene::Light::kTypePoint);
			}
		});
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		AppDelegate::get()->updateRenderView();
		AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Component/:name",action);


	action = new ClosureAction(QIcon(":/icons/light-icons/component-drawable.png"),"Drawable Component", QKeySequence(), [&]() {

		QString path = ui::FileField::openFileDialog(QObject::tr("Import object"),"scene objects (*.vwglb *.dae *.obj *.fbx)");
		if (!path.isEmpty()) {
			ptr<vwgl::scene::Drawable> importDrawable = Loader::get()->loadModel(ui::FileField::stdStringPath(path));

			if (!importDrawable.valid()) {
				QMessageBox::warning(nullptr, QObject::tr("Error"), QObject::tr("Error importing object"));
			}
			else {
				ptr<cmd::AddComponentCommand<vwgl::scene::Drawable> > cmd = new cmd::AddComponentCommand<vwgl::scene::Drawable>(context());
				AppDelegate::get()->inputMediator().selectionHandler().eachNode([&](vwgl::scene::Node * node) {
					vwgl::scene::Drawable * drw = cmd->addTargetNode(node);
					if (drw) {
						importDrawable->eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, Transform &) {
							GenericMaterial * gmat = dynamic_cast<GenericMaterial*>(mat);
							if (gmat) {
								MaterialModifier mod = gmat->getModifierWithMask(MaterialModifier::kModifierAll);
								GenericMaterial * newMat = new GenericMaterial();
                                newMat->applyModifier(mod,System::get()->getPath(ui::FileField::stdStringPath(path)));
								drw->addPolyList(new PolyList(plist),newMat);
							}
							else {
								drw->addPolyList(new PolyList(plist));
							}

						});
					}
				});
				AppDelegate::get()->commandManager().execute(cmd.getPtr());
				AppDelegate::get()->updateRenderView();
				AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
			}
		}
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Component/:name",action);


	action = new ClosureAction(QIcon(":/icons/light-icons/component-box.png"),"Cube Component", QKeySequence(), [&]() {
		ptr<cmd::AddCubeComponentCommand> cmd = new cmd::AddCubeComponentCommand(context());
		AppDelegate::get()->inputMediator().selectionHandler().eachNode([&](vwgl::scene::Node * node) {
			cmd->addTargetNode(node);
		});
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		AppDelegate::get()->updateRenderView();
		AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Component/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/component-plane.png"),"Plane Component", QKeySequence(), [&]() {
		ptr<cmd::AddPlaneComponentCommand> cmd = new cmd::AddPlaneComponentCommand(context());
		AppDelegate::get()->inputMediator().selectionHandler().eachNode([&](vwgl::scene::Node * node) {
			cmd->addTargetNode(node);
		});
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		AppDelegate::get()->updateRenderView();
		AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Component/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/component-sphere.png"),"Sphere Component", QKeySequence(), [&]() {
		ptr<cmd::AddSphereComponentCommand> cmd = new cmd::AddSphereComponentCommand(context(),1.0f, 50);
		AppDelegate::get()->inputMediator().selectionHandler().eachNode([&](vwgl::scene::Node * node) {
			cmd->addTargetNode(node);
		});
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		AppDelegate::get()->updateRenderView();
		AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Component/:name",action);

	addSeparator();
	AppDelegate::get()->menuFactory().addSeparator("Create");
	action = new ClosureAction(QIcon(":/icons/light-icons/transform_node.png"),tr("Transform"),QKeySequence(),[&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		vwgl::scene::Transform * trx = new vwgl::scene::Transform();
		trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
		child->addComponent(trx);
		child->setName("Transform node");
		createNode(child.getPtr());
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);

	AppDelegate::get()->menuFactory().addSeparator("Create/Node");
	action = new ClosureAction(QIcon(":/icons/light-icons/drawable.png"),tr("Cube"),QKeySequence(),[&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		vwgl::scene::Transform * trx = new vwgl::scene::Transform();
		trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
		child->addComponent(trx);
		child->addComponent(vwgl::scene::PrimitiveFactory::cube());
		child->setName("Cube node");
		createNode(child.getPtr());
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/sphere_1.png"),tr("Sphere"),QKeySequence(),[&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		vwgl::scene::Transform * trx = new vwgl::scene::Transform();
		trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
		child->addComponent(trx);
		child->addComponent(vwgl::scene::PrimitiveFactory::sphere());
		child->setName("Sphere node");
		createNode(child.getPtr());
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/plane.png"),tr("Plane"),QKeySequence(),[&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		vwgl::scene::Transform * trx = new vwgl::scene::Transform();
		trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
		child->addComponent(trx);
		child->addComponent(vwgl::scene::PrimitiveFactory::plane());
		child->setName("Plane node");
		createNode(child.getPtr());
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/object.png"),tr("Import Object"),QKeySequence(),[&]() {
		QString path = ui::FileField::openFileDialog(QObject::tr("Import object"),"scene objects (*.vwglb *.dae *.obj *.fbx)");
		if (!path.isEmpty()) {
            bool isPrefab = System::get()->getExtension(ui::FileField::stdStringPath(path))=="vwglb";
			vwgl::plain_ptr ctx = AppDelegate::get()->renderView()->context();
            vwgl::cmd::ImportCommand * cmd = new vwgl::cmd::ImportCommand(ctx,ui::FileField::stdStringPath(path),currentNode(),vwgl::Vector3(0.0f, 0.0f, 0.0f),isPrefab);
			if (!AppDelegate::get()->commandManager().execute(cmd)) {
				QMessageBox::warning(nullptr, QObject::tr("Error"), QObject::tr("Error importing object"));
			}
			else {
				vwgl::scene::Node * node = cmd->importNode();
				AppDelegate::get()->scene().prepareSceneNode(node);
				node->init();
				AppDelegate::get()->updateRenderView();
				AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
			}
		}
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);

	AppDelegate::get()->menuFactory().addSeparator("Create/Node");

	action = new ClosureAction(QIcon(":/icons/light-icons/light_1.png"),tr("Point Light"),QKeySequence(),[&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		vwgl::scene::Transform * trx = new vwgl::scene::Transform();
		trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
		child->addComponent(trx);
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->setType(vwgl::scene::Light::kTypePoint);
		child->addComponent(light);
		child->setName("Point Light");
		createNode(child.getPtr());
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/spot_light.png"),tr("Spot Light"),QKeySequence(),[&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		vwgl::scene::Transform * trx = new vwgl::scene::Transform();
		trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
		child->addComponent(trx);
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->setType(vwgl::scene::Light::kTypeSpot);
		child->addComponent(light);
		child->setName("Spot Light");
		createNode(child.getPtr());
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/directional_light_2.png"),tr("Directional Light"),QKeySequence(),[&]() {
		ptr<vwgl::scene::Node> child = new vwgl::scene::Node();
		vwgl::scene::Transform * trx = new vwgl::scene::Transform();
		trx->getTransform().setTransformStrategy(new TRSTransformStrategy());
		child->addComponent(trx);
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->setType(vwgl::scene::Light::kTypeDirectional);
		child->addComponent(light);
		child->setName("Directional Light");
		createNode(child.getPtr());
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Create/Node/:name",action);


}

vwgl::scene::Node * CreationToolBar::currentNode() const {
	return AppDelegate::get()->inputMediator().selectionHandler().getNodeList().size()>0 ?
				AppDelegate::get()->inputMediator().selectionHandler().getNodeList().front().getPtr() :
				AppDelegate::get()->scene().getSceneRoot();
}

vwgl::plain_ptr CreationToolBar::context() const {
	return AppDelegate::get()->renderView()->context();
}

void CreationToolBar::createNode(vwgl::scene::Node * node) {
	ptr<vwgl::scene::Node> child = node;
	child->addComponent(new manipulation::Selectable());
	vwgl::scene::Node * parent = currentNode();
	ptr<app::Command> cmd = new cmd::AddNodeCommand(context(), child.getPtr(), parent);
	AppDelegate::get()->commandManager().execute(cmd.getPtr());
	child->init();
	AppDelegate::get()->updateRenderView();
	AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
}

}
}
