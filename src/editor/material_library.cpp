
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/material_library.hpp>

#include <bg2e/ui/file_field.hpp>

#include <vwgl/loader.hpp>
#include <vwgl/library_reader.hpp>
#include <vwgl/cmd/material_commands.hpp>
#include <vwgl/library/library_visitor.hpp>

#include <iostream>

namespace bg2e {
namespace editor {

using namespace vwgl;

MaterialLibrary::MaterialLibrary()
{
	AppDelegate::get()->scene().registerObserver(this);
}

void MaterialLibrary::init() {
	_libraryInspector = new components::LibraryInspectorComponent();

	_libraryInspector->onOpenLibrary([&](components::LibraryInspectorComponent *) {
		QString path = ui::FileField::openFileDialog("Open Library", "Library files (*.vwlib)");
		if (!path.isEmpty()) {
            openLibrary(ui::FileField::stdStringPath(path));
			AppDelegate::get()->updateRenderView(3);
		}
	});

	_libraryInspector->onEnterGroup([&](components::LibraryInspectorComponent *) {
		AppDelegate::get()->updateRenderView(3);
	});

	_libraryInspector->onSelectItem([&](components::LibraryInspectorComponent*,vwgl::library::Node * node) {
		vwgl::library::Material * mat = dynamic_cast<vwgl::library::Material*>(node);
		if (mat) {
			applyMaterial(mat);
		}
	});

	Loader::get()->registerReader(new vwgl::LibraryLoader());
	std::string path = AppDelegate::get()->settings().getMaterialLibraryPath();
	if (path.empty()) {
		path = System::get()->getExecutablePath();
		if (System::get()->isMac()) {
			path = System::get()->addPathComponent(path, "../../..");
		}
		path = System::get()->addPathComponent(path,"materials.vwlib");
	}
	openLibrary(path);
}

bool MaterialLibrary::openLibrary(const std::string & path) {
	library::Group * newLib = dynamic_cast<vwgl::library::Group*>(Loader::get()->loadLibrary(path));
	if (newLib) {
		AppDelegate::get()->settings().setMaterialLibraryPath(path);
		AppDelegate::get()->settings().saveSettings();
		_libraryPath = path;
		_library = newLib;
		_libraryInspector->setLibrary(_library.getPtr());
		return true;
	}
	else {
		AppDelegate::get()->settings().setMaterialLibraryPath("");
		_libraryPath = "";
		_library = new library::Group();
		_libraryInspector->setLibrary(_library.getPtr());
		return false;
	}
}

vwgl::library::Material * MaterialLibrary::find(const std::string & searchString) {
	vwgl::library::FindNodeVisitor<vwgl::library::Material> visitor(searchString,
																	vwgl::library::FindNodeVisitor<vwgl::library::Material>::kSearchName);
	visitor.visit(_library.getPtr());
	return visitor.getResult();
}

void MaterialLibrary::sceneDidLoad(vwgl::scene::Node * node) {
	if (node) {
		node->addComponent(_libraryInspector.getPtr());
	}
}

void MaterialLibrary::applyMaterial(vwgl::library::Material * material) {
	Q_UNUSED(material);

	vwgl::manipulation::SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
	vwgl::plain_ptr ctx = AppDelegate::get()->renderView()->context();
	vwgl::ptr<vwgl::cmd::ApplyMaterialModifierCommand> cmd = new vwgl::cmd::ApplyMaterialModifierCommand(ctx, material->getMaterialModifier());
	cmd->setResourcesPath(_library->getPath());
	bool execute = false;
	sel.eachMaterial([&](vwgl::Material * mat) {
		vwgl::GenericMaterial * genMat = dynamic_cast<vwgl::GenericMaterial*>(mat);
		if (genMat) {
			cmd->addMaterial(genMat);
			execute = true;
		}
	});

	if (execute) {
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		AppDelegate::get()->updateRenderView();
	}
}

}
}
