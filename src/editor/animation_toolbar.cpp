
#include <QIcon>

#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/animation_toolbar.hpp>
#include <bg2e/closure_action.hpp>
#include <bg2e/menu_factory.hpp>

#include <vwgl/app/app.hpp>

#include <vwgl/cmd/scene_commands.hpp>
#include <vwgl/scene/primitive_factory.hpp>
#include <vwgl/cmd/import_command.hpp>
#include <vwgl/loader.hpp>

namespace bg2e {
namespace editor {

void AnimationToolBar::build() {
	setObjectName("AnimationToolBar");

	QAction * action = new ClosureAction(QIcon(":/icons/light-icons/play.png"),"Play", QKeySequence(), [&]() {
		AppDelegate::get()->dynamics().clear();
		AppDelegate::get()->scene().getSceneRoot()->accept(AppDelegate::get()->dynamics());
		AppDelegate::get()->dynamics().play();
		AppDelegate::get()->renderView()->play();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Animation/:name",action);

	addSeparator();

	action = new ClosureAction(QIcon(":/icons/light-icons/pause.png"),"Pause", QKeySequence(), [&]() {
		AppDelegate::get()->dynamics().pause();
		AppDelegate::get()->renderView()->pause();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Animation/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/stop.png"),"Stop", QKeySequence(), [&]() {
		AppDelegate::get()->dynamics().stop();
		AppDelegate::get()->renderView()->pause();
		AppDelegate::get()->updateRenderView();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Animation/:name",action);
}


}
}
