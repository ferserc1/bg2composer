
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/main_window_builder.hpp>
#include <bg2e/tool_window_manager.hpp>
#include <bg2e/ui/tool_window.hpp>
#include <bg2e/closure_action.hpp>
#include <bg2e/editor/status_bar.hpp>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>

#include <QRegExpValidator>

namespace bg2e {
namespace editor {

void MainWindowBuilder::build(QMainWindow * wnd) {
	_componentSettings = new ComponentSettingsWindow();
	AppDelegate::get()->toolWindowManager().addToolWindow("componentSettings", ToolWindowManager::kDockRight, _componentSettings);

	_materialSettings = new MaterialSettingsWindow();
	AppDelegate::get()->toolWindowManager().addToolWindow("materialSettings", ToolWindowManager::kDockRight, _materialSettings);

	_polyListSettings = new PolyListSettingsWindow();
	AppDelegate::get()->toolWindowManager().addToolWindow("polyListSettings", ToolWindowManager::kDockRight, _polyListSettings);

	_sceneTreeWindow = new SceneTreeWindow();
	AppDelegate::get()->toolWindowManager().addToolWindow("sceneTreeWindow", ToolWindowManager::kDockLeft, _sceneTreeWindow);


	AppDelegate::get()->menuFactory().addSeparator("Window");
	AppDelegate::get()->menuFactory().addAction("Window/GUI Style/:style",new ClosureAction(QObject::tr("Dark"), []() {
		QFile file(":/style/style/main_style.qss");
		if(file.open(QFile::ReadOnly)) {
		   QString StyleSheet = QString(file.readAll());
		   qApp->setStyleSheet(StyleSheet);
		}
	}));
	AppDelegate::get()->menuFactory().addAction("Window/GUI Style/:style",new ClosureAction(QObject::tr("Light"), []() {
		QFile file(":/style/style/white.qss");
		if(file.open(QFile::ReadOnly)) {
		   QString StyleSheet = QString(file.readAll());
		   qApp->setStyleSheet(StyleSheet);
		}
	}));

	AppDelegate::get()->menuFactory().addSeparator("Window");
	AppDelegate::get()->menuFactory().addAction("Window/:action", new ClosureAction(QObject::tr("Font Manager"), []() {
		ui::FontList::get()->onFontSelected(nullptr);
		ui::FontList::get()->show();
	}));

	AppDelegate::get()->toolWindowManager().tabify(_componentSettings, _materialSettings);
	AppDelegate::get()->toolWindowManager().tabify(_materialSettings, _polyListSettings);
}

}
}
