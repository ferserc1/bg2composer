
#include <iostream>

#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/axis.hpp>

#include <vwgl/generic_material.hpp>
#include <vwgl/scene/camera.hpp>

namespace bg2e {
namespace editor {
	
using namespace vwgl;

static void addLine(vwgl::scene::Drawable * drw, const vwgl::Vector3 & axis, float size, const Color & c) {
	ptr<vwgl::PolyList> plist = new vwgl::PolyList();
	ptr<vwgl::GenericMaterial> mat = new vwgl::GenericMaterial();
	mat->setCullFace(false);
	mat->setCastShadows(false);
	mat->setDiffuse(c);
	mat->setLightEmission(1.0f);
	mat->setReceiveShadows(false);

	plist->addVertex(Vector3(axis).scale(size));
	plist->addVertex(Vector3(0.0f));

	plist->addNormal(Vector3(axis));
	plist->addNormal(Vector3(axis));

	plist->addTexCoord0(Vector2(0.0f));
	plist->addTexCoord0(Vector2(0.0f));

	plist->addIndex(0); plist->addIndex(1);
	plist->buildPolyList();
	plist->setDrawMode(PolyList::kLines);

	drw->addPolyList(plist.getPtr(),mat.getPtr());
}

Axis::Axis()
	:_visible(true)
{
	
}

Axis::~Axis()
{
	
}

void Axis::update() {
	if (getPolyListVector().size()==0) {
		// TODO: build the grid
		float size = 1000.0f;
		addLine(this, Vector3(1.0f, 0.0f, 0.0f), size, Color::red());
		addLine(this, Vector3(0.0f, 1.0f, 0.0f), size, Color::green());
		addLine(this, Vector3(0.0f, 0.0f, 1.0f), size, Color::blue());



	}
}

void Axis::draw() {
	if (_visible) {
		Drawable::draw();
	}
}

}
}
