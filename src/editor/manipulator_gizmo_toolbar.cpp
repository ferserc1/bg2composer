
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/manipulator_gizmo_toolbar.hpp>
#include <bg2e/closure_action.hpp>
#include <bg2e/menu_factory.hpp>

#include <vwgl/app/app.hpp>

#include <QIcon>

namespace bg2e {
namespace editor {

using namespace vwgl;
void ManipulatorGizmoToolBar::build() {
	setObjectName("ManipulatorGizmoToolBar");

	AppDelegate::get()->menuFactory().addSeparator("Selection/Tools");
	QAction * action = new ClosureAction(QIcon(":/icons/light-icons/select_tool.png"),"Select Tool", QKeySequence(Qt::Key_Q), []() {
		AppDelegate::get()->inputMediator().setGizmo(app::InputMediator::kGizmoNone);
		AppDelegate::get()->updateRenderView();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Selection/Tools/:name",action);
	AppDelegate::get()->menuFactory().addSeparator("Selection/Tools");

	addSeparator();

	action = new ClosureAction(QIcon(":/icons/light-icons/translate_tool.png"),"Translate Tool", QKeySequence(Qt::Key_W), []() {
		AppDelegate::get()->inputMediator().setGizmo(app::InputMediator::kGizmoTranslate);
		AppDelegate::get()->updateRenderView();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Selection/Tools/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/rotate_tool.png"),"Rotate Tool", QKeySequence(Qt::Key_E), []() {
		AppDelegate::get()->inputMediator().setGizmo(app::InputMediator::kGizmoRotate);
		AppDelegate::get()->updateRenderView();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Selection/Tools/:name",action);

	action = new ClosureAction(QIcon(":/icons/light-icons/scale_icon_3.png"),"Scale Tool", QKeySequence(Qt::Key_R), []() {
		AppDelegate::get()->inputMediator().setGizmo(app::InputMediator::kGizmoScale);
		AppDelegate::get()->updateRenderView();
	});
	addAction(action);
	AppDelegate::get()->menuFactory().addAction("Selection/Tools/:name",action);

}

}
}
