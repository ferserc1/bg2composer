
#include <QApplication>
#include <QMessageBox>

#include <bg2e/editor/app_delegate.hpp>

#include <vwgl/graphics.hpp>
#include <vwgl/system.hpp>
#include <vwgl/ssao.hpp>
#include <vwgl/shadow_render_pass.hpp>
#include <vwgl/scene/light.hpp>

#include <bg2e/editor/window_controller.hpp>
#include <bg2e/editor/main_window.hpp>
#include <bg2e/editor/main_window_builder.hpp>
#include <bg2e/editor/ui_settings.hpp>

#include <vwgl/font_manager.hpp>
#include <vwgl/manipulation/stats.hpp>

namespace bg2e {
namespace editor {

AppDelegate AppDelegate::s_singleton;

AppDelegate::AppDelegate()
	:QObject(0)
	,AppDelegateBase()
{
	_scene.setAppDelegate(this);
}

AppDelegate::~AppDelegate() {

}

void AppDelegate::configureMainWindow(MainWindow * wnd) {
	_mainWindow = wnd;
	wnd->_renderView = createRenderView();
	wnd->setCentralWidget(_renderView);

	_menuFactory = new MenuFactory(wnd);
	menuFactory().initMenu("File");
	menuFactory().initMenu("Edit");
	menuFactory().initMenu("View");
	menuFactory().initMenu("Create");
	menuFactory().initMenu("Selection");
	menuFactory().initMenu("Window");
	menuFactory().initMenu("Animation");
	menuFactory().initMenu("Help");
	_toolWindowManager = new ToolWindowManager(wnd);

	settings().registerExtension(&_uiSettings);

	AppDelegate::get()->menuFactory().addAction("View/:settings", new ClosureAction("Graphic Settings",[&]() {
		settings().settingsWindow()->show();
	}));

	_hqRendererAction = new ClosureAction("High Quality Render", [&]() {
		_hqRendererAction->blockSignals(true);
		if (AppDelegate::get()->_renderer==AppDelegate::get()->_highQualityRenderer.getPtr()) {
			AppDelegate::get()->setRenderPath(AppDelegate::kQualityLow);
			_hqRendererAction->setChecked(false);
		}
		else {
			AppDelegate::get()->setRenderPath(AppDelegate::kQualityHigh);
			_hqRendererAction->setChecked(true);
		}
		_hqRendererAction->blockSignals(false);
		AppDelegate::get()->updateRenderView();
	});
	_hqRendererAction->setCheckable(true);
	_hqRendererAction->setChecked(getRenderPath()==AppDelegate::kQualityHigh);
	menuFactory().addAction("View/:renderer", _hqRendererAction);

	editor::MainWindowBuilder builder;
	_statusBar = new StatusBar();
	wnd->setStatusBar(_statusBar);
	builder.build(wnd);

	_fileOptions.init();
	_editOptions.init();
	_selectionOptions.init();
	_helpOptions.init();

	connect(qApp,SIGNAL(aboutToQuit()),this,SLOT(prepareToQuit()));
}

void AppDelegate::initGL(QGLContext * context) {
	_mainContext = context;

	if (vwgl::System::get()->isMac()) {
		std::string shaderPath = vwgl::System::get()->getDefaultShaderPath();
		shaderPath = vwgl::System::get()->addPathComponent(shaderPath,"shaders");
		vwgl::System::get()->setDefaultShaderPath(shaderPath);

		std::string resourcesPath = vwgl::System::get()->getResourcesPath();
		resourcesPath = vwgl::System::get()->addPathComponent(resourcesPath,"resources");
		vwgl::System::get()->setResourcesPath(resourcesPath);
	}
	vwgl::Graphics::get()->initContext();

	_scene.loadReaders();
	_scene.loadWriters();

	initRenderer();
	_settings.registerObserver(this);
	_settings.init(_highQualityRenderer.getPtr(),_lowQualityRenderer.getPtr());
	_settings.loadSettings();

	_inputMediator.init();
	_gizmoViewOptions.init();

	inputMediator().setGizmo(app::InputMediator::kGizmoTranslate);

	_materialLibrary.init();

	_scene.registerObserver(this);
	_scene.initGL();

	commandManager().setCurrentContextCallback([](vwgl::plain_ptr) {
		AppDelegate::get()->renderView()->makeCurrent();
	});
}

void AppDelegate::setRenderPath(RenderQuality q) {
	switch (q) {
	case kQualityHigh:
		_renderer = _highQualityRenderer.getPtr();
		break;
	case kQualityLow:
		_renderer = _lowQualityRenderer.getPtr();
		break;
	}
	_inputMediator.setRenderer(_renderer);
	if (_stats.valid() && scene().getSceneRoot()) {
		_stats->setRenderer(_renderer);
	}
}

bool AppDelegate::sceneWillUnload(vwgl::scene::Node * sceneRoot) {
	if (_stats.valid()) {
		sceneRoot->removeComponent(_stats.getPtr());
	}
	return true;
}

void AppDelegate::sceneDidLoad(vwgl::scene::Node * sceneRoot) {
	_highQualityRenderer->setSceneRoot(sceneRoot);
	_lowQualityRenderer->setSceneRoot(sceneRoot);

	if (!_grid.valid()) {
		_grid = new Grid();
	}
	sceneRoot->addComponent(_grid.getPtr());
	if (!_axis.valid()) {
		_axis = new Axis();
	}
	sceneRoot->addComponent(_axis.getPtr());

	_renderer->init();
	_inputMediator.setRenderer(_renderer);
	if (!_stats.valid()) {
		_stats = new vwgl::manipulation::Stats(_renderer);
		_stats->setVisible(false);
	}
	else {
		_stats->setRenderer(_renderer);
	}
	sceneRoot->addComponent(_stats.getPtr());

	drawPlistSelection().setSceneRoot(sceneRoot);
}

void AppDelegate::sceneDidUnload() {
	_lowQualityRenderer->setSceneRoot(nullptr);
	_highQualityRenderer->setSceneRoot(nullptr);
	vwgl::FontManager::get()->clearUnused();
	vwgl::TextureManager::get()->clearUnused();
	inputMediator().commandManager().clearCommandStack();
}

void AppDelegate::destroyGL() {
	_scene.close();
}

void AppDelegate::initRenderer() {
	_highQualityRenderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathDeferred);
	_lowQualityRenderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathForward);

	// TODO: Load this from settings
	setRenderPath(kQualityHigh);
}

RenderView * AppDelegate::createRenderView() {
    vwgl::Graphics::Api api = _settings.getApi();
    bool advancedOpenGL = api==vwgl::Graphics::kApiOpenGLAdvanced;
	QGLFormat glFormat;

    vwgl::Graphics::get()->useApi(api);

	if (advancedOpenGL) {
		glFormat.setVersion(3,3);
		glFormat.setProfile(QGLFormat::CoreProfile);
		glFormat.setSampleBuffers(true);
	}

	_renderView = new RenderView(new bg2e::editor::WindowController(), glFormat);
	_renderView->setInitGLFunction([&](vwgl::plain_ptr ctx) {
		initGL(vwgl::native_cast<QGLContext*>(ctx));
	});
	return _renderView;
}

void AppDelegate::saveWindowSettings()
{
	QSettings settings("Vitaminew","bg2e_editor");

	settings.setValue("initialized",true);
	settings.setValue("dockState",_mainWindow->saveState());
	settings.setValue("windowGeometry",_mainWindow->saveGeometry());
	bool maximized = _mainWindow->isMaximized();
	settings.setValue("maximized",maximized);
	if (!maximized) {
		settings.setValue("position", _mainWindow->pos());
		settings.setValue("size", _mainWindow->size());
	}
	QString style = qApp->styleSheet();
	settings.setValue("stylesheet", style);
	this->settings().saveSettings();
}

void AppDelegate::restoreWindowSettings()
{
	QSettings settings("Vitaminew","bg2e_editor");

	_mainWindow->setTabPosition(Qt::RightDockWidgetArea,QTabWidget::West);
	_mainWindow->setTabPosition(Qt::LeftDockWidgetArea,QTabWidget::East);
	_mainWindow->setTabPosition(Qt::BottomDockWidgetArea,QTabWidget::West);

	QFile file(":/style/style/main_style.qss");
	QString stylesheet = "";
	if(file.open(QFile::ReadOnly)) {
	   stylesheet = QString(file.readAll());
	}


	if (settings.value("initialized",false).toBool()) {
		QString styleSheetSettings = settings.value("stylesheet",stylesheet).toString();
		if (styleSheetSettings!="") {
			stylesheet = styleSheetSettings;
		}
		qApp->setStyleSheet(stylesheet);
		_mainWindow->restoreGeometry(settings.value("windowGeometry",_mainWindow->saveGeometry()).toByteArray());
		_mainWindow->restoreState(settings.value("dockState",_mainWindow->saveState()).toByteArray());
		bool maximized = settings.value("maximized",true).toBool();
		if (maximized) {
			_mainWindow->showMaximized();
		}
		else {
			QPoint position = settings.value("position", _mainWindow->pos()).toPoint();
			_mainWindow->move(position);
			_mainWindow->resize(settings.value("size", _mainWindow->size()).toSize());
			_mainWindow->show();
		}
	}
	else {

		_mainWindow->showMaximized();
		qApp->setStyleSheet(stylesheet);
	}
}

void AppDelegate::prepareToQuit() {
	std::cout << "Quit bg2 composer" << std::endl;
	saveWindowSettings();
}

}
}
