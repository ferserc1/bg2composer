
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/ui_settings.hpp>

namespace bg2e {
namespace editor {

using namespace vwgl;

void UISettings::onLoadSettings(vwgl::Dictionary * settings) {
	std::cout << "Loading UI settings" << std::endl;
	bool loadDefaultGizmos = true;
	if (settings) {
		ptr<Dictionary> uiDict = settings->getObject()["uiSettings"];

		if (uiDict.valid()) {
			ptr<Dictionary> gizmoVisibility = uiDict->getObject()["gizmoVisibility"];
			ptr<Dictionary> defaultFilePath = uiDict->getObject()["defaultFilePath"];
			if (gizmoVisibility.valid() && gizmoVisibility->isNumber()) {
				loadDefaultGizmos = false;
				AppDelegate::get()->inputMediator().gizmoManager().setVisibleGizmos(static_cast<unsigned int>(gizmoVisibility->getNumber()));
			}

			if (defaultFilePath.valid()) {
				ui::FileField::setDefaultPath(QString::fromStdString(defaultFilePath->getString()));
			}
		}
	}


	if (loadDefaultGizmos) {
		AppDelegate::get()->inputMediator().gizmoManager().setVisibleGizmos(
					manipulation::GizmoManager::kIconCamera |
					manipulation::GizmoManager::kIconLightDirectional |
					manipulation::GizmoManager::kIconLightSpot |
					manipulation::GizmoManager::kIconLightPoint |
					manipulation::GizmoManager::kIconTransform |
					manipulation::GizmoManager::kIconJoint);
	}
}

void UISettings::onSaveSettings(vwgl::Dictionary * settings) {
	std::cout << "Saving UI settings" << std::endl;

	ptr<Dictionary> uiDict = new Dictionary();
	settings->getObject()["uiSettings"] = uiDict.getPtr();

	uiDict->getObject()["gizmoVisibility"] = new Dictionary(AppDelegate::get()->inputMediator().gizmoManager().getGizmoVisibilityMask());
	uiDict->getObject()["defaultFilePath"] = new Dictionary(ui::FileField::stdStringPath(ui::FileField::getDefaultPath()));
}

}
}
