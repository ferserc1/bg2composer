
#include <QGroupBox>

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/screenshot_window.hpp>
#include <bg2e/settings.hpp>

#include <vwgl/system.hpp>
#include <vwgl/cmd/import_command.hpp>
#include <vwgl/boundingbox.hpp>

namespace bg2e {
namespace editor {

ScreenshotWindow::ScreenshotWindow()
	:QDialog(0)
{
	setModal(true);
	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);

	layout->addWidget(new QLabel(tr("Save Screenshot")));


	_size = new ui::VectorField("Image size");
	_size->setVector(vwgl::Vector2(1024.0f,768.0f));
	layout->addWidget(_size);

	QHBoxLayout * buttonLayout = new QHBoxLayout();
	_saveButton = new QPushButton(tr("Save"));
	_cancelButton = new QPushButton(tr("Cancel"));

	QWidget * buttons = new QWidget();
	buttons->setStyleSheet("background-color: transparent;");
	buttons->setLayout(buttonLayout);
	layout->addWidget(buttons);
	buttonLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum));
	buttonLayout->addWidget(_saveButton);
	buttonLayout->addWidget(_cancelButton);


	connect(_saveButton, SIGNAL(clicked()), this, SLOT(save()));
	connect(_cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
}

void ScreenshotWindow::save() {
	QString path = ui::FileField::saveFileDialog(QObject::tr("Save screenshot"),"*.jpg");
	if (!path.isEmpty()) {
		std::string cstrPath = ui::FileField::stdStringPath(path);
		if (System::get()->getExtension(cstrPath)!="jpg") {
			cstrPath += ".jpg";
		}
		AppDelegate::get()->renderView()->context()->makeCurrent();

		// TODO: If we take an screenshot before send a mouseDown/mouseUp events, the
		// viewport breaks, why? This patch solves this issue, but it would be better a more elegant solution
		AppDelegate::get()->inputMediator().mouseDown(vwgl::app::MouseEvent());
		AppDelegate::get()->inputMediator().mouseUp(vwgl::app::MouseEvent());


		bool gridVisible = AppDelegate::get()->grid()->isVisible();
		bool axisVisible = AppDelegate::get()->axis()->isVisible();
		AppDelegate::get()->grid()->hide();
		AppDelegate::get()->axis()->hide();
		Size2Di size(static_cast<int>(_size->getVector2().x()),
					 static_cast<int>(_size->getVector2().y()));
		vwgl::ptr<vwgl::Image> image = AppDelegate::get()->renderer().screenshot(size);
		AppDelegate::get()->grid()->setVisible(gridVisible);
		AppDelegate::get()->axis()->setVisible(axisVisible);
		if (image.valid()) {
			image->saveJPG(cstrPath);
		}
	}

	hide();
}

void ScreenshotWindow::cancel() {
	hide();
}

}
}
