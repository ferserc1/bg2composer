
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/selection_window.hpp>

#include <vwgl/scene/find_node_visitor.hpp>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QPushButton>

#include <regex>

namespace bg2e {
namespace editor {

SelectionWindow::SelectionWindow()
	:QDialog(nullptr)
{
	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);
	setModal(false);
	setWindowFlags(Qt::WindowStaysOnTopHint);

	_patternField = new ui::TextField("Search pattern");
	layout->addWidget(_patternField);

	_addToSelection = new ui::BooleanField("Add to selection");
	layout->addWidget(_addToSelection);

	QHBoxLayout * hlayout = new QHBoxLayout;
	QWidget * btnGroup = new QWidget(nullptr);
	btnGroup->setStyleSheet("background-color: transparent;");
	layout->addWidget(btnGroup);
	btnGroup->setLayout(hlayout);

	hlayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
	ui::ButtonField * accept = new ui::ButtonField("Search and Select");
	hlayout->addWidget(accept);
	accept->onButtonClicked([&]() {
		if (!_patternField->getText().empty()) {
			selectPattern();
		}
	});

	ui::ButtonField * close = new ui::ButtonField("Close");
	hlayout->addWidget(close);
	close->onButtonClicked([&]() {
		hide();
	});
}

void SelectionWindow::selectPattern() {
	vwgl::scene::FindNodeWithComponentVisitor<vwgl::scene::Drawable> findVisitor;
	AppDelegate::get()->scene().getSceneRoot()->accept(findVisitor);

	AppDelegate::get()->inputMediator().selectionHandler().lockNotifications();
	if (!_addToSelection->value()) {
		AppDelegate::get()->inputMediator().selectionHandler().clearSelection();
	}
	AppDelegate::get()->inputMediator().selectionHandler().setAdditiveSelection(true);
	findVisitor.eachComponent([&](vwgl::scene::Node * node, vwgl::scene::Drawable * drw) {
		addToSelection(node, drw);
	});

	AppDelegate::get()->inputMediator().selectionHandler().setAdditiveSelection(false);
	AppDelegate::get()->inputMediator().selectionHandler().unlockNotifications();
	AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();

	hide();
}

void SelectionWindow::addToSelection(vwgl::scene::Node * node, vwgl::scene::Drawable * drw) {
	vwgl::manipulation::SelectionHandler & sel = AppDelegate::get()->inputMediator().selectionHandler();
	bool addDrawable = false;
	std::regex re(_patternField->getText(), std::regex_constants::icase | std::regex_constants::ECMAScript);

	drw->eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform &) {
		std::smatch match;
		if (std::regex_search(plist->getName(), match, re)) {
			addDrawable = true;
			sel.selectPolyList(plist);
			sel.selectMaterial(mat);
		}
	});
	if (addDrawable) {
		sel.selectDrawable(drw);
		sel.selectNode(node);
	}
}

}
}
