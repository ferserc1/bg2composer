
#include <QMenu>
#include <QToolBar>

#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/main_window.hpp>

namespace bg2e {
namespace editor {

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
#ifdef Q_OS_MAC
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint);
#endif
	AppDelegate::get()->configureMainWindow(this);

	setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::TabPosition::East);
	setTabPosition(Qt::RightDockWidgetArea, QTabWidget::TabPosition::West);
	setTabPosition(Qt::BottomDockWidgetArea, QTabWidget::TabPosition::North);
	_toolbarFactory.build(this);

	AppDelegate::get()->restoreWindowSettings();


	int w = geometry().width();
	int h = geometry().height();
	resize(w + 1, h);
	resize(w, h);
}

MainWindow::~MainWindow()
{

}

void MainWindow::closeEvent(QCloseEvent * evt) {
	QMessageBox::StandardButton reply = QMessageBox::question(nullptr,
															  tr("Quit Composer"),
															  tr("Do you want to save changes in the scene before quit?"),
															  QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
	if (reply == QMessageBox::Yes
		&& !AppDelegate::get()->scene().save()) {
		QString path = ui::FileField::saveFileDialog(QObject::tr("Save scene as"),"scene files (*.vitscn *.vitbundle)");
		if (!path.isEmpty()) {
			if (!AppDelegate::get()->scene().saveAs(ui::FileField::stdStringPath(path))) {
				QMessageBox::warning(nullptr, QObject::tr("Error"),QObject::tr("Error saving file"));
				evt->ignore();
			}
		}
		else {
			evt->ignore();
		}
	}
	else if (reply == QMessageBox::Cancel) {
		evt->ignore();
	}
}


}
}
