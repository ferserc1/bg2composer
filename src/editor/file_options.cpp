
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/file_options.hpp>
#include <bg2e/ui/file_field.hpp>

#include <QObject>

#include <vwgl/cmd/import_command.hpp>
#include <vwgl/loader.hpp>

namespace bg2e {
namespace editor {

FileOptions::FileOptions()
	:_importWindow(nullptr)
	,_screenshotWindow(nullptr)
{

}

void FileOptions::init() {
	_newScene = new ClosureAction(QObject::tr("New Scene"), [&]() { newScene(); });
	_newScene->setShortcut(QKeySequence::New);
	AppDelegate::get()->menuFactory().addAction("File/:new", _newScene);

	_openScene = new ClosureAction(QObject::tr("Open Scene"), [&]() { openScene(); });
	_openScene->setShortcut(QKeySequence::Open);
	AppDelegate::get()->menuFactory().addAction("File/:open", _openScene);

	AppDelegate::get()->menuFactory().addSeparator("File/:separator");

	_importObject = new ClosureAction(QObject::tr("Import"), [&]() { importObject(); });
	AppDelegate::get()->menuFactory().addAction("File/:import", _importObject);
	_importMultipleObjects = new ClosureAction(QObject::tr("Import multiple"), [&]() { importMultiple(); });
	AppDelegate::get()->menuFactory().addAction("File/:importMultiple", _importMultipleObjects);
	_exportObject = new ClosureAction(QObject::tr("Export Selection"),[&]() { exportObject(); });
	AppDelegate::get()->menuFactory().addAction("File/:export", _exportObject);

	AppDelegate::get()->menuFactory().addSeparator("File");

	_saveScene = new ClosureAction(QObject::tr("Save Scene"), [&]() { saveScene(); });
	_saveScene->setShortcut(QKeySequence::Save);
	AppDelegate::get()->menuFactory().addAction("File/:save", _saveScene);

	_saveSceneAs = new ClosureAction(QObject::tr("Save Scene As..."), [&]() { saveSceneAs(); });
	_saveSceneAs->setShortcut(QKeySequence::SaveAs);
	AppDelegate::get()->menuFactory().addAction("File/:save_as",_saveSceneAs);

	_closeScene = new ClosureAction(QObject::tr("Close Scene"), [&]() { closeScene(); });
	AppDelegate::get()->menuFactory().addAction("File/:close", _closeScene);

	AppDelegate::get()->menuFactory().addSeparator("File");
	_exportScene = new ClosureAction(QObject::tr("Export Scene"),[&]() { exportSceneAs(); });
	AppDelegate::get()->menuFactory().addAction("File/:export", _exportScene);

	_saveScreenshot = new ClosureAction(QObject::tr("Save Screenshot"), [&]() { saveScreenshot(); });
	AppDelegate::get()->menuFactory().addAction("File/:saveScreenshot", _saveScreenshot);

	AppDelegate::get()->menuFactory().addSeparator("File");
	_quit = new ClosureAction(QObject::tr("Quit"),[&]() {
		bool exitApp = true;
		QMessageBox::StandardButton reply = QMessageBox::question(nullptr,
																  QObject::tr("Quit Composer"),
																  QObject::tr("Do you want to save changes in the scene before quit?"),
																  QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
		if (reply == QMessageBox::Yes
			&& !AppDelegate::get()->scene().save()) {
			QString path = ui::FileField::saveFileDialog(QObject::tr("Save scene as"),"scene files (*.vitscn *.vitbundle)");
			if (!path.isEmpty()) {
				if (!AppDelegate::get()->scene().saveAs(ui::FileField::stdStringPath(path))) {
					QMessageBox::warning(nullptr, QObject::tr("Error"),QObject::tr("Error saving file"));
					exitApp = false;
				}
			}
			else {
				exitApp = false;
			}
		}
		else if (reply == QMessageBox::Cancel) {
			exitApp = false;
		}

		if (exitApp) QApplication::exit(0);
	});
	AppDelegate::get()->menuFactory().addAction("File/:quit", _quit);
}

void FileOptions::newScene() {
	AppDelegate::get()->scene().createDefault();
}

void FileOptions::openScene() {
	QString path = ui::FileField::openFileDialog(QObject::tr("Open scene"),"scene files (*.vitscn *.vitscnj)");
	if (!path.isEmpty()) {
        AppDelegate::get()->scene().open(ui::FileField::stdStringPath(path));
	}
}

void FileOptions::saveScene() {
	if (!AppDelegate::get()->scene().save()) {
		QString path = ui::FileField::saveFileDialog(QObject::tr("Save scene as"),"scene files (*.vitscn *.vitbundle)");
		if (!path.isEmpty()) {
            if (!AppDelegate::get()->scene().saveAs(ui::FileField::stdStringPath(path))) {
				QMessageBox::warning(nullptr, QObject::tr("Error"),QObject::tr("Error saving file"));
			}
		}
	}
}

void FileOptions::saveSceneAs() {
	QString path = ui::FileField::saveFileDialog(QObject::tr("Save scene as"),"scene files (*.vitscn *.vitbundle)");
	if (!path.isEmpty()) {
        if (!AppDelegate::get()->scene().saveAs(ui::FileField::stdStringPath(path))) {
			QMessageBox::warning(nullptr, QObject::tr("Error"),QObject::tr("Error saving file"));
		}
	}
}

void FileOptions::importObject() {
	QString path = ui::FileField::openFileDialog(QObject::tr("Import object"),"scene objects (*.vwglb *.dae *.obj *.fbx)");
	if (!path.isEmpty()) {
        bool isPrefab = System::get()->getExtension(ui::FileField::stdStringPath(path))=="vwglb";
		vwgl::plain_ptr ctx = AppDelegate::get()->renderView()->context();
        vwgl::scene::Node * sceneRoot = AppDelegate::get()->scene().getSceneRoot();
        std::string utfPath = path.toLocal8Bit().constData();
        vwgl::cmd::ImportCommand * cmd = new vwgl::cmd::ImportCommand(ctx,ui::FileField::stdStringPath(path),sceneRoot,vwgl::Vector3(0.0f, 0.0f, 0.0f),isPrefab);
		if (!AppDelegate::get()->commandManager().execute(cmd)) {
			QMessageBox::warning(nullptr, QObject::tr("Error"), QObject::tr("Error importing object"));
		}
		else {
			vwgl::scene::Node * node = cmd->importNode();
			AppDelegate::get()->scene().prepareSceneNode(node);
			node->init();
			AppDelegate::get()->updateRenderView();
		}
	}
}

void FileOptions::importMultiple() {
	if (_importWindow==nullptr) {
		_importWindow = new MultiImportWindow();
	}
	_importWindow->show();
}

void FileOptions::exportObject() {
	int numDrawables = AppDelegate::get()->inputMediator().selectionHandler().getDrawableList().size();
	if (numDrawables==1) {
		QString path = ui::FileField::saveFileDialog(QObject::tr("Export selection"),"scene object (*.vwglb)");
		if (!path.isEmpty()) {
			AppDelegate::get()->renderView()->context()->makeCurrent();
			ptr<vwgl::scene::Drawable> drawable = AppDelegate::get()->inputMediator().selectionHandler().getDrawableList().front();
            if (!doExportObject(ui::FileField::stdStringPath(path), drawable.getPtr())) {
				QMessageBox::warning(nullptr, QObject::tr("Error exporting file"),QObject::tr("Could not export file at selected location."));
			}
		}
	}
	else if (numDrawables>1) {
		QString path = ui::FileField::openDirectoryDialog(QObject::tr("Export selection"));
		if (!path.isEmpty()) {
			int saveIndex = 0;
			std::vector<std::string> errors;
			AppDelegate::get()->inputMediator().selectionHandler().eachDrawable([&](vwgl::scene::Drawable * drw) {
				std::string name = drw->getName();
				if (name.empty() && drw->node()) {
					name = drw->node()->getName();
				}
				if (name.empty()) {
					std::stringstream newName;
					newName << "Object-" << saveIndex;
					name = newName.str();
				}
                std::string fullPath = System::get()->addPathComponent(ui::FileField::stdStringPath(path),name);
				System::get()->mkdir(fullPath);
				fullPath = System::get()->addPathComponent(fullPath,name);
				fullPath = System::get()->addExtension(fullPath, "vwglb");
				AppDelegate::get()->renderView()->context()->makeCurrent();
				if (!doExportObject(fullPath, drw)) {
					errors.push_back(fullPath);
				}
			});

			if (errors.size()>0) {
				std::string errorsString;
				for (auto p : errors) {
					errorsString += p + "\n";
				}
				QMessageBox::warning(nullptr, QObject::tr("Errors found"),QObject::tr("There were errors exporting selection") + QString::fromStdString(errorsString));
			}
		}
	}

}

void FileOptions::closeScene() {
	AppDelegate::get()->scene().createDefault();
}

void FileOptions::exportSceneAs() {
	QString path = ui::FileField::saveFileDialog(QObject::tr("Export scene as"),"scene files (*.osg)");
	if (!path.isEmpty()) {
        if (!AppDelegate::get()->scene().saveAs(ui::FileField::stdStringPath(path))) {
			QMessageBox::warning(nullptr, QObject::tr("Error"),QObject::tr("Error saving file"));
		}
	}
}

void FileOptions::saveScreenshot() {
	if (_screenshotWindow==nullptr) {
		_screenshotWindow = new ScreenshotWindow();
	}
	_screenshotWindow->show();
}

bool FileOptions::doExportObject(const std::string & path, vwgl::scene::Drawable * drw) {
	if (!drw) {
		return false;
	}

	ptr<vwgl::scene::Node> drwNode = drw->node();
	if (drwNode.valid()) {
		return Loader::get()->writeNode(path, drwNode.getPtr());
	}
	else {
		return Loader::get()->writeModel(path,drw);
	}
}

}
}
