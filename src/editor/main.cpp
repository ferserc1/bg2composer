#include <bg2e/editor/main_window.hpp>
#include <bg2e/version.hpp>

#include <QApplication>
#include <QtGui>
#include <iostream>

#include <vwgl/system.hpp>

int main(int argc, char *argv[])
{
	QDir appLocation(argv[0]);
#ifdef Q_OS_MAC
	appLocation.cdUp();
#endif
	appLocation.cdUp();
	QString plugins = appLocation.absolutePath() + "/PlugIns";
	QStringList paths;

	paths << plugins
		  << plugins + "/platforms"
		  << plugins + "/imageformats"
		  << plugins + "/printsupport";

	QApplication::setLibraryPaths(paths);
	QApplication::setDesktopSettingsAware(false);
	QApplication::setStyle(QStyleFactory::create("Fusion"));

	std::cout << "BG2E Editor - Version " << bg2e::Version::getVersionString() << std::endl;

	QApplication a(argc, argv);
	bg2e::editor::MainWindow w;
	w.show();

	qApp->setAttribute(Qt::AA_UseHighDpiPixmaps);

	return a.exec();
}
