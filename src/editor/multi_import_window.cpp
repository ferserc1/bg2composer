
#include <QGroupBox>

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/multi_import_window.hpp>
#include <bg2e/settings.hpp>

#include <vwgl/system.hpp>
#include <vwgl/cmd/import_command.hpp>
#include <vwgl/boundingbox.hpp>

namespace bg2e {
namespace editor {

class MultiFileImporter {
public:
	MultiFileImporter() {

	}

	inline void setPrefix(const std::string & p) { _prefix = p; }
	inline void setPostfix(const std::string & p) { _postfix = p; }

	void import(const std::string & path) {
		if (vwgl::System::get()->directoryExists(path)) {
			std::vector<std::string> fileList;
			std::vector<std::string> dirList;
			vwgl::System::get()->listDirectory(path, fileList, dirList);

			for (auto file : fileList) {
				auto ext = vwgl::System::get()->getExtension(file);
				if (ext=="dae" || ext=="obj" || ext=="vwglb" || ext=="fbx") {
					importFile(vwgl::System::get()->addPathComponent(path,file));
				}
			}
		}
	}

protected:
	std::string _prefix;
	std::string _postfix;

	void importFile(const std::string & path) {
		std::string fileName = vwgl::System::get()->getFileName(path);
		std::string objectName = _prefix + fileName + _postfix;

		bool isPrefab = System::get()->getExtension(path)=="vwglb";
		vwgl::plain_ptr ctx = AppDelegate::get()->renderView()->context();
		vwgl::scene::Node * sceneRoot = AppDelegate::get()->scene().getSceneRoot();
		vwgl::cmd::ImportCommand * cmd = new vwgl::cmd::ImportCommand(ctx,path,sceneRoot,vwgl::Vector3(0.0f, 0.0f, 0.0f),isPrefab);
		if (!AppDelegate::get()->commandManager().execute(cmd)) {
			// TODO: añadir warning a la lista en la barra de estado
		}
		else {
			vwgl::scene::Node * node = cmd->importNode();
			AppDelegate::get()->scene().prepareSceneNode(node);
			node->init();

			// TODO: bounding box, sacar el centro, mover al centro, fix transform, desplazar a la posicón que estaba
			vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
			vwgl::scene::Transform * trx = node->getComponent<vwgl::scene::Transform>();
			if (drw && trx) {
				vwgl::ptr<vwgl::BoundingBox> bbox = new vwgl::BoundingBox(drw);
				vwgl::Vector3 position = bbox->center();
				vwgl::Vector3 size = bbox->size();
				position.scale(-1.0f);
				position.y(position.y() + size.y()/2);
				vwgl::Matrix4 matrix = vwgl::Matrix4::makeTranslation(position);
				drw->applyTransform(matrix);
				std::string name = drw->getName();
				drw->setName(_prefix + name + _postfix);
				node->setName(drw->getName());

				vwgl::TRSTransformStrategy * s = dynamic_cast<vwgl::TRSTransformStrategy*>(trx->getTransform().getTransformStrategy());
				if (s) {
					s->translate(position.scale(-1.0f));
				}
			}


			AppDelegate::get()->updateRenderView();
		}
	}
};

MultiImportWindow::MultiImportWindow()
	:QDialog(0)
{
	setModal(true);
	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);

	layout->addWidget(new QLabel(tr("Import multiple files")));

	_folder = new ui::FileField(tr("Folder path"));
	_folder->setFolderPickerMode(true);
	layout->addWidget(_folder);

	_prefix = new ui::TextField(tr("Name prefix"));
	layout->addWidget(_prefix);

	_postfix = new ui::TextField(tr("Name postfix"));
	layout->addWidget(_postfix);


	QHBoxLayout * buttonLayout = new QHBoxLayout();
	_importButton = new QPushButton(tr("Import"));
	_cancelButton = new QPushButton(tr("Cancel"));

	QWidget * buttons = new QWidget();
	buttons->setStyleSheet("background-color: transparent;");
	buttons->setLayout(buttonLayout);
	layout->addWidget(buttons);
	buttonLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum));
	buttonLayout->addWidget(_importButton);
	buttonLayout->addWidget(_cancelButton);


	connect(_importButton, SIGNAL(clicked()), this, SLOT(import()));
	connect(_cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
}

void MultiImportWindow::import() {
	MultiFileImporter importer;
	importer.setPrefix(_prefix->getText());
	importer.setPostfix(_postfix->getText());
	importer.import(_folder->getPath());

	hide();
}

void MultiImportWindow::cancel() {
	hide();
}

}
}
