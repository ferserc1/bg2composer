
#include <functional>

#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/editor/scene_tree_window.hpp>

#include <bg2e/ui/scene_tree_model.hpp>

#include <vwgl/scene/cubemap.hpp>
#include <vwgl/cmd/scene_commands.hpp>

namespace bg2e {
namespace editor {

SceneTreeWindow::SceneTreeWindow()
	:ToolWindow(QObject::tr("Scene Tree"))
	,_model(nullptr)
{
	build();

	AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
	AppDelegate::get()->commandManager().registerObserver(this);
	AppDelegate::get()->scene().registerObserver(this);
}

void SceneTreeWindow::build() {
	setObjectName("SceneTreeWindow");
	setMinimumWidth(300);

	_treeView = new ui::TreeView();
	_treeView->setSelectionMode(QAbstractItemView::ExtendedSelection);
	_treeView->setContextMenuPolicy(Qt::CustomContextMenu);

	_treeView->setDragEnabled(true);
	_treeView->setAcceptDrops(true);
	_treeView->setDropIndicatorShown(true);

	connect(_treeView,SIGNAL(expanded(QModelIndex)),this,SLOT(expandNode(QModelIndex)));
	connect(_treeView,SIGNAL(collapsed(QModelIndex)),this,SLOT(collapseNode(QModelIndex)));
	connect(_treeView,SIGNAL(clicked(QModelIndex)),this,SLOT(nodeClicked(QModelIndex)));
	connect(_treeView,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(contextClick(QPoint)));
	connect(_treeView,SIGNAL(moveNode(QModelIndex,QModelIndex)),this,SLOT(moveNode(QModelIndex,QModelIndex)));
	addWidget(_treeView);

	_centralWidget->layout()->setMargin(0);

//	endWidget();
}

void SceneTreeWindow::reload() {
	if (!_model) {
		_model = new ui::SceneTreeModel(AppDelegate::get()->scene().getSceneRoot());
		_treeView->setModel(_model);
	}
	else {
		_model->refresh();
	}

	restoreExpandedState();
}

void SceneTreeWindow::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	_treeView->selectionModel()->clear();
	sel.eachNode([&](vwgl::scene::Node * node) {
		ui::SceneTreeItem * treeItem = node->getComponent<ui::SceneTreeItem>();
		if (treeItem) {
			_treeView->selectionModel()->select(treeItem->modelIndex(),QItemSelectionModel::Select);
		}
	});
}

void SceneTreeWindow::didExecuteCommand(vwgl::app::Command * ) {
	reload();
}

void SceneTreeWindow::didUndo(vwgl::app::Command * ) {
	reload();
}

void SceneTreeWindow::didRedo(vwgl::app::Command * ) {
	reload();
}

void SceneTreeWindow::sceneDidLoad(vwgl::scene::Node *) {
	_treeView->blockSignals(true);
	_treeView->setModel(nullptr);
	delete this->_model;
	this->_model = nullptr;
	_treeView->blockSignals(false);
	reload();
}

void SceneTreeWindow::sceneDidUnload() {
}

void SceneTreeWindow::restoreExpandedState()
{
	restoreChildren(AppDelegate::get()->scene().getSceneRoot());
}

void SceneTreeWindow::expandNode(const QModelIndex & index) {
	ui::SceneTreeItem * item = treeItem(index);
	if (item) {
		item->setExpanded(true);
	}
}

void SceneTreeWindow::collapseNode(const QModelIndex &index) {
	ui::SceneTreeItem * item = treeItem(index);
	if (item) {
		item->setExpanded(false);
	}
}

void SceneTreeWindow::nodeClicked(const QModelIndex & index) {
	handleSelection(index, QGuiApplication::keyboardModifiers() & Qt::ControlModifier);
}

void SceneTreeWindow::contextClick(const QPoint &point) {
	QModelIndex index = _treeView->indexAt(point);
	handleSelection(index,true);
}

void SceneTreeWindow::moveNode(const QModelIndex & source, const QModelIndex & destination) {
	vwgl::ptr<vwgl::scene::Node> sourceNode = static_cast<vwgl::scene::Node*>(source.internalPointer());
	vwgl::ptr<vwgl::scene::Node> destinationNode = static_cast<vwgl::scene::Node*>(destination.internalPointer());
	if (!destinationNode.valid()) {
		destinationNode = AppDelegate::get()->scene().getSceneRoot();
	}

	if (sourceNode.valid() && destinationNode.valid()) {
		vwgl::plain_ptr ctx = editor::AppDelegate::get()->renderView()->context();
		vwgl::ptr<vwgl::app::Command> cmd = new vwgl::cmd::AddNodeCommand(ctx, sourceNode.getPtr(), destinationNode.getPtr());
		AppDelegate::get()->commandManager().execute(cmd.getPtr());
		reload();
		AppDelegate::get()->updateRenderView();
	}
}

ui::SceneTreeItem * SceneTreeWindow::treeItem(const QModelIndex & index) {
	vwgl::scene::Node * node = nullptr;
	ui::SceneTreeItem * treeItem = nullptr;

	if (index.isValid()) {
		node = static_cast<vwgl::scene::Node*>(index.internalPointer());
	}

	if (node) {
		treeItem = node->getComponent<ui::SceneTreeItem>();
		if (!treeItem) {
			treeItem = new ui::SceneTreeItem();
			treeItem->setModelIndex(index);
			node->addComponent(treeItem);
		}
	}

	return treeItem;
}

void SceneTreeWindow::restoreChildren(vwgl::scene::Node * root) {
	ui::SceneTreeItem * item = root->getComponent<ui::SceneTreeItem>();
	if (item) {
		_treeView->blockSignals(true);
		_treeView->setExpanded(item->modelIndex(), item->isExpanded());
		_treeView->blockSignals(false);
	}
	root->iterateChildren([&](vwgl::scene::Node * node) {
		restoreChildren(node);
	});
}

void SceneTreeWindow::handleSelection(const QModelIndex & index, bool additive) {
	vwgl::manipulation::SelectionHandler & selection = AppDelegate::get()->inputMediator().selectionHandler();
	vwgl::scene::Node * targetNode = static_cast<vwgl::scene::Node*>(index.internalPointer());
	if (targetNode) {
		selection.lockNotifications();
		if (!additive) {
			selection.clearSelection();
		}
		selection.setAdditiveSelection(true);
		vwgl::scene::Drawable * drw = targetNode->getComponent<vwgl::scene::Drawable>();
		if (drw) {
			drw->eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform &) {
				selection.selectPolyList(plist);
				selection.selectMaterial(mat);
			});
            selection.selectDrawable(drw);
		}
		selection.selectNode(targetNode);
		selection.setAdditiveSelection(false);
		selection.unlockNotifications();
		AppDelegate::get()->inputMediator().gizmoManager().setManipulateObject(targetNode);
		selection.notifyObservers();
		AppDelegate::get()->updateRenderView();
	}
}

}
}
