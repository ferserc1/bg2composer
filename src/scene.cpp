
#include <QMessageBox>

#include <bg2e/scene.hpp>

#include <bg2e/plugins/fbx_plugin.hpp>

#include <vwgl/loader.hpp>
#include <vwgl/scene_loader.hpp>
#include <vwgl/scene_writter.hpp>
#include <vwgl/vwglb_plugin.hpp>
#include <vwgl/collada_plugin.hpp>
#include <vwgl/colladaloader.hpp>
#include <vwgl/obj_reader.hpp>
#include <vwgl/freetype_reader.hpp>

#include <vwgl/scene/forward_renderer.hpp>
#include <vwgl/scene/deferred_renderer.hpp>
#include <vwgl/scene/primitive_factory.hpp>
#include <vwgl/scene/light.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/manipulation/transform_controller.hpp>
#include <vwgl/cmd/import_command.hpp>
#include <vwgl/osg_plugin.hpp>

#include <vwgl/ssao.hpp>

#include <algorithm>



namespace bg2e {

void Scene::registerObserver(ISceneObserver * o) {
	SceneObserverVector::iterator item = std::find(_observers.begin(), _observers.end(), o);
	if (item==_observers.end()) {
		_observers.push_back(o);
	}
}

void Scene::unregisterObserver(ISceneObserver * o) {
	SceneObserverVector::iterator item = std::find(_observers.begin(), _observers.end(), o);
	if (item!=_observers.end()) {
		_observers.erase(item);
	}
}

Scene::Scene()
{
}

void Scene::loadReaders() {
	// Register readers
	vwgl::Loader::get()->registerReader(new vwgl::VWGLBModelReader());
	vwgl::Loader::get()->registerReader(new vwgl::ColladaModelReader());
	vwgl::Loader::get()->registerReader(new vwgl::ObjModelReader());
	vwgl::Loader::get()->registerReader(new vwgl::VWGLBPrefabReader());
	vwgl::Loader::get()->registerReader(new vwgl::SceneReader());
	vwgl::Loader::get()->registerReader(new vwgl::FreetypeReader());
	vwgl::Loader::get()->registerReader(new plugins::FBXReader());
}

void Scene::loadWriters() {
	// Register writters
	vwgl::Loader::get()->registerWritter(new vwgl::VWGLBModelWritter());
	vwgl::Loader::get()->registerWritter(new vwgl::VWGLBPrefabWritter());
	vwgl::Loader::get()->registerWritter(new vwgl::SceneWritter());
	vwgl::Loader::get()->registerWritter(new vwgl::OSGWritter());
}

void Scene::initGL() {
	_sceneChanged = false;
	createDefault();
}

void Scene::createDefault() {
	if (close()) {
		eachObserver([](ISceneObserver * o) {
			o->sceneWillLoad();
			return true;
		});

		_sceneRoot = new vwgl::scene::Node("Scene root");

		vwgl::scene::Node * light = new vwgl::scene::Node("Main Light");
		_sceneRoot->addChild(light);

		light->addComponent(new vwgl::scene::Light());
		light->getComponent<vwgl::scene::Light>()->setShadowBias(0.0029f);
		light->getComponent<vwgl::scene::Light>()->setShadowStrength(0.8f);
		light->getComponent<vwgl::scene::Light>()->setAmbient(vwgl::Color(0.2f,0.2f,0.25f,1.0f));
		light->getComponent<vwgl::scene::Light>()->setDiffuse(vwgl::Color(0.93f,0.93f,0.85f,1.0f));

		light->addComponent(new vwgl::scene::Transform());
		vwgl::TRSTransformStrategy * strategy = new vwgl::TRSTransformStrategy();
		strategy->translate(vwgl::Vector3(0.0f, 3.0f, 0.0f));
		strategy->rotateX(-vwgl::Math::kPiOver4);
		strategy->rotateY(vwgl::Math::kPiOver4);
		light->getComponent<vwgl::scene::Transform>()->getTransform().setTransformStrategy(strategy);
		light->addComponent(new vwgl::manipulation::Selectable());


		vwgl::scene::Node * camera = new vwgl::scene::Node("Camera");
		camera->addComponent(new vwgl::scene::Camera());
		this->prepareMainCamera(camera);

		_sceneRoot->addChild(camera);

		eachObserver([&](ISceneObserver * o) {
			o->sceneDidLoad(_sceneRoot.getPtr());
			return true;
		});

		sendResizeEvent();
	}
}

bool Scene::open(const std::string & scenePath) {
	if (close()) {
		_appDelegate->getMainGLContext()->makeCurrent();
		_sceneRoot = vwgl::Loader::get()->loadScene(scenePath);
		if (_sceneRoot.valid()) {
			_currentScenePath = scenePath;
			_sceneRoot->accept(*this);
			_sceneChanged = false;
			vwgl::scene::Camera * cam = vwgl::scene::Camera::getMainCamera();
			if (!cam) {
				cam = new vwgl::scene::Camera();
			}
			if (!cam->node()) {
				vwgl::scene::Node * camNode = new vwgl::scene::Node("Camera");
				camNode->addComponent(cam);
				_sceneRoot->addChild(camNode);
			}
			prepareMainCamera(cam->node());
			eachObserver([&](ISceneObserver * o) {
				o->sceneDidLoad(_sceneRoot.getPtr());
				return true;
			});

			sendResizeEvent();
		}
		else {
			createDefault();
			return false;
		}
	}
	return true;
}

bool Scene::save() {
	bool status = false;
	if (_currentScenePath!="") {
		status = true;
		_appDelegate->getMainGLContext()->makeCurrent();

		_saveThread.showProgress();
		_saveThread.setThreadCallback([&]() {
			bool status = vwgl::Loader::get()->write(_currentScenePath,_sceneRoot.getPtr());
			_sceneChanged = !status;
			_saveThread.setSaveStatus(status);
		});

		_saveThread.setDoneCallback([&]() {
			_appDelegate->getMainGLContext()->makeCurrent();
			_saveThread.hideProgress();
		});

		_saveThread.setErrorCallback([&]() {
			_appDelegate->getMainGLContext()->makeCurrent();
			QMessageBox::warning(nullptr,"Error","Error saving scene");
			_saveThread.hideProgress();
		});

		_saveThread.start();
	}
	return status;
}

bool Scene::saveAs(const std::string & scenePath) {
	std::cout << "Save as" << std::endl;
	_currentScenePath = scenePath;
	save();
	return true;
}

bool Scene::exportAs(const std::string & scenePath) {
	std::cout << "Export" << std::endl;
	_appDelegate->getMainGLContext()->makeCurrent();

	_saveThread.showProgress();
	_saveThread.setThreadCallback([&]() {
		bool status = vwgl::Loader::get()->write(scenePath,_sceneRoot.getPtr());
		_saveThread.setSaveStatus(status);
	});

	_saveThread.setDoneCallback([&]() {
		_appDelegate->getMainGLContext()->makeCurrent();
		_saveThread.hideProgress();
	});

	_saveThread.setErrorCallback([&]() {
		_appDelegate->getMainGLContext()->makeCurrent();
		QMessageBox::warning(nullptr,"Error","Error saving scene");
		_saveThread.hideProgress();
	});

	_saveThread.start();

	return true;
}


bool Scene::close() {
	bool cancelClose = false;

	if (_sceneChanged) {
		auto reply = QMessageBox::question(nullptr, QObject::tr("Save scene"),
										   QObject::tr("The scene have unsaved changes. Do you want to save scene before proceed?"),
										   QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
		if (reply == QMessageBox::Yes) {
			cancelClose = !save();
		}
		else if (reply==QMessageBox::Cancel){
			cancelClose = true;
		}
	}

	if (!cancelClose) {
		eachObserver([&](ISceneObserver * o) {
			if (!o->sceneWillUnload(_sceneRoot.getPtr())) {
				cancelClose = true;
				return false;
			}
			return true;
		});
		if (!cancelClose) {
			_sceneRoot = nullptr;
			eachObserver([](ISceneObserver * o) {
				o->sceneDidUnload();
				return true;
			});
		}
	}

	return !cancelClose;
}

void Scene::visit(vwgl::scene::Node * node) {
	if (node->getComponent<vwgl::scene::Camera>()) {
		vwgl::scene::Camera::setMainCamera(node->getComponent<vwgl::scene::Camera>());
	}
	prepareSceneNode(node);
}

void Scene::prepareMainCamera(vwgl::scene::Node * camera) {
	camera->getComponent<vwgl::scene::Camera>()->setProjectionMethod(new vwgl::OpticalProjectionMethod());
	camera->addComponent(new vwgl::scene::Transform());
	vwgl::manipulation::TargetInputController * ctrl = new vwgl::manipulation::TargetInputController();
	ctrl->setRotation(vwgl::Vector2(-15.0f,45.0f));
	camera->addComponent(ctrl);
}

void Scene::prepareSceneNode(vwgl::scene::Node * node) {
	node->addComponent(new vwgl::manipulation::Selectable());
	vwgl::scene::Transform * trx = node->getComponent<vwgl::scene::Transform>();
	vwgl::TRSTransformStrategy * strategy = trx ? trx->getTransform().getTransformStrategy<vwgl::TRSTransformStrategy>():nullptr;
	if (trx && !strategy) {
		vwgl::Vector3 position = trx->getTransform().getMatrix().getPosition();
		vwgl::TRSTransformStrategy * strategy = new vwgl::TRSTransformStrategy();
		strategy->translate(position);
		trx->getTransform().setTransformStrategy(strategy);
	}
}

void Scene::sendResizeEvent() {
	RenderView * renderView = _appDelegate->renderView();
	// Force resize scene
	int w = renderView->geometry().width();
	int h = renderView->geometry().height();
	renderView->resize(w + 1, h);
	renderView->resize(w, h);
}

}
