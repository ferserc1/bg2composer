
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/components/rigid_body_ui.hpp>

#include <bg2e/scene/rigid_body.hpp>
#include <vwgl/cmd/scene_commands.hpp>

#include <QDoubleValidator>

namespace bg2e {
namespace components {

class RigidBodyCommand : public vwgl::app::Command {
public:
	inline void addTarget(bg2e::scene::RigidBody*t) { _targets.push_back(t); }

protected:
	std::vector<vwgl::ptr<bg2e::scene::RigidBody> > _targets;
};

class SetMassCommand : public RigidBodyCommand {
public:
	SetMassCommand(float mass) :_mass(mass) {}

	virtual void doCommand() {
		_undoMass.clear();
		for (auto rb : _targets) {
			_undoMass.push_back(rb->getMass());
			rb->setMass(_mass);
		}
	}

	virtual void undoCommand() {
		std::vector<float>::iterator it = _undoMass.begin();
		for (auto rb : _targets) {
			rb->setMass(*it);
			++it;
		}
	}

protected:
	float _mass;
	std::vector<float> _undoMass;
};

using namespace vwgl;

ComponentUIFactory rigidBodyRegister("Rigid Body",
	[](Registry * registry) {
		registry->registerWidget(typeid(bg2e::scene::RigidBody).hash_code(),new RigidBodyUI());
	},
	[]() {
		return new bg2e::scene::RigidBody();
	});


RigidBodyUI::RigidBodyUI()
	:ComponentField("Rigid Body")
{
	_mass = new ui::TextField("Mass");
	_mass->setValidator(new QDoubleValidator());
	_mass->onEditFinished([&](ui::TextField *) {
		vwgl::ptr<SetMassCommand> cmd = new SetMassCommand(_mass->text().toFloat());
		vwgl::manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
		sel.eachNode([&](vwgl::scene::Node * node) {
			vwgl::ptr<bg2e::scene::RigidBody> rb = node->getComponent<bg2e::scene::RigidBody>();
			if (rb.valid()) {
				cmd->addTarget(rb.getPtr());
			}
		});
		editor::AppDelegate::get()->commandManager().execute(cmd.getPtr());
		editor::AppDelegate::get()->updateRenderView();
	});
	addField(_mass);
}

void RigidBodyUI::selectionChanged(vwgl::manipulation::SelectionHandler & selection) {
	if (selection.getNodeList().size()>0) {
		vwgl::scene::Node * lastNode = selection.getNodeList().back().getPtr();
		bg2e::scene::RigidBody * rb = nullptr;

		if (lastNode && (rb=lastNode->getComponent<bg2e::scene::RigidBody>())) {
			_mass->setText(QString::number(rb->getMass()));
		}
	}
}

void RigidBodyUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<bg2e::scene::RigidBody> *cmd = new cmd::RemoveComponentCommand<bg2e::scene::RigidBody>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		bg2e::scene::RigidBody * c = node->getComponent<bg2e::scene::RigidBody>();
		if (c) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

}
}
