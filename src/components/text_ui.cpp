
#include <bg2e/components/text_ui.hpp>

#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/ui/font_field.hpp>

#include <vwgl/cmd/scene_commands.hpp>
#include <vwgl/scene/text.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/font_manager.hpp>

namespace bg2e {
namespace components {

using namespace vwgl;

ComponentUIFactory textRegister("Text",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::Text).hash_code(),new TextUI());
	},
	[]() {
		return new vwgl::scene::Text();
	},
	[](vwgl::scene::Node *, vwgl::scene::Component * comp) {
		ptr<vwgl::scene::Text> text = dynamic_cast<vwgl::scene::Text*>(comp);
		ptr<TextFont> font = ui::FontList::get()->getDefaultFont();
		text->setFont(font.getPtr());
		text->setText("Hello, World!");
	});


TextUI::TextUI()
	:ComponentField("Text")
{
	buildUI();
}

void TextUI::buildUI() {
	_text = new ui::TextField("Text");
	_text->onEditFinished([&](ui::TextField *) {
		cmd::TextSetTextCommand * cmd = new cmd::TextSetTextCommand(context(),_text->getText());
		execute(cmd);
	});
	addField(_text);

	_font = new ui::FontField("Font");
	_font->onFontChanged([&](ui::FontField*) {
		if (_font->getFont()) {
			cmd::TextSetFontCommand * cmd = new cmd::TextSetFontCommand(context(), _font->getFont());
			execute(cmd);
		}
	});
	addField(_font);

	_color = new ui::ColorField(tr("Color"));
	_color->onColorChanged([&](ui::ColorField *) {
		applyProperties();
	});
	addField(_color);

	_specular = new ui::ColorField(tr("Specular"));
	_specular->onColorChanged([&](ui::ColorField *) {
		applyProperties();
	});
	addField(_specular);

	_shininess = new ui::FloatSliderField(tr("Shininess"));
	_shininess->setRange(0.0f, 255.0f);
	_shininess->onSliderChanged([&](ui::FloatSliderField *) {
		applyProperties();
	});
	addField(_shininess);

	_lightEmission = new ui::FloatSliderField(tr("Light emission"));
	_lightEmission->setRange(0.0f, 1.0f);
	_lightEmission->onSliderChanged([&](ui::FloatSliderField *) {
		applyProperties();
	});
	addField(_lightEmission);

	_castShadows = new ui::BooleanField(tr("Cast shadows"));
	_castShadows->onValueChanged([&](ui::BooleanField*) {
		applyProperties();
	});
	addField(_castShadows);

	_receiveShadows = new ui::BooleanField(tr("Receive shadows"));
	_receiveShadows->onValueChanged([&](ui::BooleanField*) {
		applyProperties();
	});
	addField((_receiveShadows));
}

void TextUI::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	bool hideUI = true;
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Text * text = node->getComponent<vwgl::scene::Text>();
		if (text) {
			hideUI = false;
			blockFields(true);
			_text->setText(QString::fromStdString(text->getText()));
			_font->setFont(text->getFont());
			_color->setColor(text->getColor());
			_specular->setColor(text->getSpecular());
			_shininess->setValue(text->getShininess());
			_lightEmission->setValue(text->getLightEmission());
			_castShadows->setValue(text->getCastShadows());
			_receiveShadows->setValue(text->getReceiveShadows());
			blockFields(false);
		}
	});

	if (hideUI) {
		hide();
	}
	else {
		show();
	}
}

void TextUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::Text> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::Text>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Text * c = node->getComponent<vwgl::scene::Text>();
		if (c) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

plain_ptr TextUI::context() {
	return editor::AppDelegate::get()->renderView()->context();
}

void TextUI::applyProperties() {
	vwgl::scene::TextProperties prop;
	prop.setColor(_color->getEngineColor());
	prop.setSpecular(_specular->getEngineColor());
	prop.setShininess(_shininess->value());
	prop.setLightEmission(_lightEmission->value());
	prop.setCastShadows(_castShadows->value());
	prop.setReceiveShadows(_receiveShadows->value());
	cmd::TextSetPropertiesCommand * cmd = new cmd::TextSetPropertiesCommand(context(),prop);
	execute(cmd);
}

void TextUI::execute(vwgl::cmd::TextCommand * cmd) {
	manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
	bool exec = false;
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Text * text = node->getComponent<vwgl::scene::Text>();
		if (text) {
			exec = true;
			cmd->addTargetText(text);
		}
	});

	if (exec) {
		editor::AppDelegate::get()->commandManager().execute(cmd);
		editor::AppDelegate::get()->updateRenderView();
	}
}

void TextUI::blockFields(bool b) {
	_text->blockSignals(b);
	_color->blockSignals(b);
	_specular->blockSignals(b);
	_shininess->blockSignals(b);
	_lightEmission->blockSignals(b);
	_castShadows->blockSignals(b);
	_receiveShadows->blockSignals(b);
}

}
}
