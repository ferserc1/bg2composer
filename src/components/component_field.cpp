

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/components/component_field.hpp>

namespace bg2e {
namespace components {
	
ComponentField::ComponentField(const QString & title)
	:QWidget(0)
{
	setLayout(new QVBoxLayout());
	editor::AppDelegate::get()->inputMediator().selectionHandler().registerObserver(this);
	editor::AppDelegate::get()->commandManager().registerObserver(this);

	QWidget * headerWidget = new QWidget();
	QHBoxLayout * headerLayout = new QHBoxLayout();
	headerWidget->setLayout(headerLayout);

	headerLayout->addWidget(new QLabel(tr(title.toStdString().c_str())));
	headerLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum));
	QPushButton * removeBtn = new QPushButton(tr("Remove"));
	headerLayout->addWidget(removeBtn);
	connect(removeBtn,SIGNAL(clicked()),this,SLOT(removeButtonPressed()));

	layout()->addWidget(headerWidget);
}

void ComponentField::addField(ui::EditField *field) {
	layout()->addWidget(field);
}

vwgl::plain_ptr ComponentField::context() {
	return editor::AppDelegate::get()->renderView()->context();
}

void ComponentField::removeButtonPressed() {
	onRemoveComponent(editor::AppDelegate::get()->inputMediator().selectionHandler());
	editor::AppDelegate::get()->updateRenderView();
	editor::AppDelegate::get()->inputMediator().selectionHandler().notifyObservers();
}

}
}
