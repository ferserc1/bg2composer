
#include <QDoubleValidator>

#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/components/light_ui.hpp>
#include <bg2e/closure_action.hpp>

#include <vwgl/scene/light.hpp>
#include <vwgl/cmd/light_commands.hpp>
#include <vwgl/cmd/scene_commands.hpp>

namespace bg2e {
namespace components {

using namespace vwgl;

ComponentUIFactory lightRegister("Light",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::Light).hash_code(),new LightUI());
	},
	[]() {
		return new vwgl::scene::Light();
	});

LightUI::LightUI()
	:ComponentField("Light")
	,_preventUpdate(false)
{
	_type = new ui::ComboBoxField(tr("Type"));
	_type->addItem(tr("Directional"));
	_type->addItem(tr("Point"));
	_type->addItem(tr("Spot"));
	_type->onComboBoxChanged([&](ui::ComboBoxField*) {
		applyChanges();
		selectionChanged(editor::AppDelegate::get()->inputMediator().selectionHandler());
	});
	addField(_type);

	_ambient = new ui::ColorField(tr("Ambient"));
	addField(_ambient);
	_diffuse = new ui::ColorField(tr("Diffuse"));
	addField(_diffuse);
	_specular = new ui::ColorField(tr("Specular"));
	addField(_specular);
	_ambient->onColorChanged([&](ui::ColorField*) { applyChanges(); });
	_diffuse->onColorChanged([&](ui::ColorField*) { applyChanges(); });
	_specular->onColorChanged([&](ui::ColorField*) { applyChanges(); });

	layout()->addWidget(new QLabel(tr("Attenuation")));
	_constantAtt = new ui::FloatSliderField(tr("Constant"));
	_constantAtt->setRange(0.0f, 5.0f);
	addField(_constantAtt);
	_linearAtt = new ui::FloatSliderField(tr("Linear"));
	_linearAtt->setRange(0.0f, 5.0f);
	addField(_linearAtt);
	_quadraticAtt = new ui::FloatSliderField(tr("Quadratic"));
	_quadraticAtt->setRange(0.0f, 10.0f);
	addField(_quadraticAtt);
	_cutoffDistance = new ui::FloatSliderField(tr("Cutoff distance"));
	_cutoffDistance->setRange(-1.0f, 10.0f);
	addField(_cutoffDistance);
	_constantAtt->onSliderChanged([&](ui::FloatSliderField *) { applyChanges(); });
	_linearAtt->onSliderChanged([&](ui::FloatSliderField *) { applyChanges(); });
	_quadraticAtt->onSliderChanged([&](ui::FloatSliderField *) { applyChanges(); });
	_cutoffDistance->onSliderChanged([&](ui::FloatSliderField *) { applyChanges(); });

	layout()->addWidget(new QLabel(tr("Shadows")));
	_castShadows = new ui::BooleanField(tr("Cast shadows"));
	addField(_castShadows);
	_castShadows->onValueChanged([&](ui::BooleanField*) { applyChanges(); });

	_shadowStrenght = new ui::FloatSliderField(tr("Strength"));
	_shadowStrenght->setRange(0.0f,1.0f);
	_shadowStrenght->onSliderChanged([&](ui::FloatSliderField *) { applyChanges(); });
	addField(_shadowStrenght);
	_shadowBias = new ui::FloatSliderField(tr("Bias"));
	_shadowBias->setRange(0.0f,0.05f);
	_shadowBias->onSliderChanged([&](ui::FloatSliderField *) { applyChanges(); });
	addField(_shadowBias);

	layout()->addWidget(new QLabel(tr("Spot properties")));
	_spotExponent = new ui::FloatSliderField(tr("Exponent"));
	_spotExponent->setRange(0.0f, 90.0f);
	_spotExponent->onSliderChanged([&](ui::FloatSliderField*) { applyChanges(); });
	addField(_spotExponent);
	_spotCutoff = new ui::FloatSliderField(tr("Cutoff angle"));
	_spotCutoff->setRange(0.0f, 90.0f);
	_spotCutoff->onSliderChanged([&](ui::FloatSliderField*) { applyChanges(); });
	addField(_spotCutoff);
}

void LightUI::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	vwgl::scene::Light * light = nullptr;
	sel.eachNode([&](vwgl::scene::Node * node) {
		if (node->getComponent<vwgl::scene::Light>() && !light) {
			light = node->getComponent<vwgl::scene::Light>();
		}
	});

	if (light) {
		_preventUpdate = true;
		vwgl::scene::Light::LightType type = light->getType();
		_castShadows->setValue(light->getCastShadows());
		_castShadows->setEnabled(true);
		_shadowStrenght->setValue(light->getShadowStrength());
		_shadowStrenght->setEnabled(true);
		_shadowBias->setValue(light->getShadowBias() * 10.0f);
		_shadowBias->setEnabled(true);

		_spotExponent->setValue(light->getSpotExponent());
		_spotExponent->setEnabled(true);
		_spotCutoff->setValue(light->getSpotCutoff());
		_spotCutoff->setEnabled(true);

		_constantAtt->setEnabled(true);
		_constantAtt->setValue(light->getConstantAttenuation());
		_linearAtt->setEnabled(true);
		_linearAtt->setValue(light->getLinearAttenuation());
		_quadraticAtt->setEnabled(true);
		_quadraticAtt->setValue(light->getExpAttenuation());
		_cutoffDistance->setEnabled(true);
		_cutoffDistance->setValue(light->getCutoffDistance());

		switch (type) {
		case vwgl::scene::Light::kTypeDirectional:
			_type->setCurrentIndex(0);
			_constantAtt->setEnabled(false);
			_linearAtt->setEnabled(false);
			_quadraticAtt->setEnabled(false);
			_cutoffDistance->setEnabled(false);
			_spotExponent->setEnabled(false);
			_spotCutoff->setEnabled(false);
			break;
		case vwgl::scene::Light::kTypePoint:
			_type->setCurrentIndex(1);
			_castShadows->setEnabled(false);
			_shadowStrenght->setEnabled(false);
			_shadowBias->setEnabled(false);
			_spotExponent->setEnabled(false);
			_spotCutoff->setEnabled(false);
			break;
		case vwgl::scene::Light::kTypeSpot:
			_type->setCurrentIndex(2);
			break;
		case vwgl::scene::Light::kTypeDisabled:
			break;
		}

		_ambient->setColor(light->getAmbient());
		_diffuse->setColor(light->getDiffuse());
		_specular->setColor(light->getSpecular());

		_preventUpdate = false;
	}
}

void LightUI::applyChanges() {
	if (!_preventUpdate) {
		manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
		vwgl::scene::LightModifier mod;
		editor::AppDelegate::get()->getMainGLContext()->makeCurrent();

		switch (_type->currentIndex()) {
		case 0:
			mod.setType(vwgl::scene::Light::kTypeDirectional);
			break;
		case 1:
			mod.setType(vwgl::scene::Light::kTypePoint);
			break;
		case 2:
			mod.setType(vwgl::scene::Light::kTypeSpot);
			break;
		}

		mod.setAmbient(_ambient->getEngineColor());
		mod.setDiffuse(_diffuse->getEngineColor());
		mod.setSpecular(_specular->getEngineColor());
		mod.setCastShadows(_castShadows->value());
		mod.setShadowStrength(_shadowStrenght->value());
		mod.setShadowBias(_shadowBias->value() / 10.0f);
		mod.setSpotExponent(_spotExponent->value());
		mod.setSpotCutoff(_spotCutoff->value());
		mod.setConstantAttenuation(_constantAtt->value());
		mod.setLinearAttenuation(_linearAtt->value());
		mod.setExpAttenuation(_quadraticAtt->value());
		mod.setCutoffDistance(_cutoffDistance->value());


		cmd::ApplyLightModifierCommand * cmd = new cmd::ApplyLightModifierCommand(editor::AppDelegate::get()->renderView()->context(), mod);

		sel.eachNode([&](vwgl::scene::Node * node) {
			if (node->getComponent<vwgl::scene::Light>()) {
				cmd->addLight(node->getComponent<vwgl::scene::Light>());
			}
		});

		editor::AppDelegate::get()->commandManager().execute(cmd);
		editor::AppDelegate::get()->updateRenderView();
	}
}

void LightUI::applyChanges(vwgl::scene::Light * light) {
	if (light) {

	}
}

void LightUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::Light> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::Light>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Light * l = node->getComponent<vwgl::scene::Light>();
		if (l) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

}
}
