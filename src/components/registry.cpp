
#include <bg2e/components/registry.hpp>

namespace bg2e {
namespace components {

Registry * Registry::s_registry = nullptr;

Registry::Registry() {
}

Registry::~Registry() {

}

void Registry::registerFactory(ComponentUIFactory* factory) {
	_componentUIFactoryVector.push_back(factory);
}

void Registry::registerFactories() {
	std::vector<ComponentUIFactory*>::iterator it;
	for (it = _componentUIFactoryVector.begin(); it!=_componentUIFactoryVector.end(); ++it) {
		(*it)->registerWidget();
	}
}

}
}
