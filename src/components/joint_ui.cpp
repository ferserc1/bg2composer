
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/components/joint_ui.hpp>

#include <vwgl/scene/joint.hpp>
#include <vwgl/cmd/scene_commands.hpp>
#include <vwgl/cmd/joint_command.hpp>

#include <functional>
#include <iostream>
#include <vwgl/scene/chain.hpp>

namespace bg2e {
namespace components {
	
JointUI::JointUI(const QString & title)
	:ComponentField(title)
{
	buildUI();
}

void JointUI::buildUI() {
	_offset = new ui::VectorField(tr("Offset"));
	_offset->setVector(vwgl::Vector3(0.0f, 0.0f, 0.0f));
	_offset->setKeyIncrement(0.1f);
	_offset->onVectorChanged([&](ui::VectorField *) {
		updateData();
	});
	addField(_offset);

	_rotation = new ui::VectorField("Rotation");
	_rotation->setVector(vwgl::Vector3(0.0f, 0.0f, 0.0f));
	_rotation->onVectorChanged([&](ui::VectorField *) {
		updateData();
	});
	addField(_rotation);
}

using namespace vwgl;

ComponentUIFactory inJointRegister("Input chain joint",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::InputChainJoint).hash_code(),new InputJointUI());
	},
	[]() {
		return new vwgl::scene::InputChainJoint();
	},
	[](vwgl::scene::Node * node, vwgl::scene::Component * comp) {
		vwgl::scene::InputChainJoint * jointComp = dynamic_cast<vwgl::scene::InputChainJoint*>(comp);
		vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
		if (jointComp && drw) {
			vwgl::physics::LinkJoint * joint = jointComp->getJoint();
			vwgl::ptr<vwgl::BoundingBox> bbox = new vwgl::BoundingBox(drw);
			joint->setOffset(vwgl::Vector3(-bbox->min().x(), -bbox->min().y(), -bbox->min().z()));
		}
	});

ComponentUIFactory outJointRegister("Output chain joint",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::OutputChainJoint).hash_code(),new OutputJointUI());
	},
	[]() {
		return new vwgl::scene::OutputChainJoint();
	},
	[](vwgl::scene::Node * node, vwgl::scene::Component * comp) {
		vwgl::scene::OutputChainJoint * jointComp = dynamic_cast<vwgl::scene::OutputChainJoint*>(comp);
		vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
		if (jointComp && drw) {
			vwgl::physics::LinkJoint * joint = jointComp->getJoint();
			vwgl::ptr<vwgl::BoundingBox> bbox = new vwgl::BoundingBox(drw);
			joint->setOffset(vwgl::Vector3(bbox->max().x(), bbox->min().y(), bbox->min().z()));
		}
	});

InputJointUI::InputJointUI()
	:JointUI("Input joint")
{
}

void InputJointUI::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::InputChainJoint * joint = node->getComponent<vwgl::scene::InputChainJoint>();
		if (joint) {
			_offset->setVector(joint->getJoint()->offset());
			Vector3 eulerRotation = joint->getJoint()->eulerRotation();
			eulerRotation.scale(Math::k180OverPi);
			_rotation->setVector(eulerRotation);
		}
	});
}

void InputJointUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::InputChainJoint> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::InputChainJoint>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::InputChainJoint* j = node->getComponent<vwgl::scene::InputChainJoint>();
		if (j) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

void InputJointUI::updateData() {
	Vector3 eulerRotation = _rotation->getVector3();
	eulerRotation.scale(Math::kPiOver180);
	cmd::EditJointCommand * cmd = new cmd::EditJointCommand(_offset->getVector3(),eulerRotation);
	manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::InputChainJoint* j = node->getComponent<vwgl::scene::InputChainJoint>();
		if (j) {
			cmd->addJoint(j);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
	editor::AppDelegate::get()->updateRenderView();
}

OutputJointUI::OutputJointUI()
	:JointUI("Output joint")
{
	_attachButton = new ui::ButtonField("Attach");
	manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
	_attachButton->onButtonClicked([&]() {
		this->_attachNode = sel.getNodeList().front();
	});
	addField(_attachButton);
}

void OutputJointUI::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	int selectedOutputs = 0;
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::OutputChainJoint * joint = node->getComponent<vwgl::scene::OutputChainJoint>();
		vwgl::scene::InputChainJoint * inJoint = node->getComponent<vwgl::scene::InputChainJoint>();
		if (joint) {
			_offset->setVector(joint->getJoint()->offset());
			Vector3 eulerRotation = joint->getJoint()->eulerRotation();
			eulerRotation.scale(Math::k180OverPi);
			_rotation->setVector(eulerRotation);
			++selectedOutputs;
		}

		if (inJoint && this->_attachNode.valid()) {
			editor::AppDelegate::get()->commandManager().execute(new AttachInputJointCommand(_attachNode.getPtr(), inJoint->node()));
			this->_attachNode = nullptr;
			editor::AppDelegate::get()->updateRenderView();
		}
	});

	if (selectedOutputs==1) {
		_attachButton->setEnabled(true);
	}
	else {
		_attachButton->setEnabled(false);
	}
	_attachNode = nullptr;
}

void OutputJointUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::OutputChainJoint> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::OutputChainJoint>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::OutputChainJoint * j = node->getComponent<vwgl::scene::OutputChainJoint>();
		if (j) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

void OutputJointUI::updateData() {
	Vector3 eulerRotation = _rotation->getVector3();
	eulerRotation.scale(Math::kPiOver180);
	cmd::EditJointCommand * cmd = new cmd::EditJointCommand(_offset->getVector3(),eulerRotation);
	manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::OutputChainJoint* j = node->getComponent<vwgl::scene::OutputChainJoint>();
		if (j) {
			cmd->addJoint(j);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
	editor::AppDelegate::get()->updateRenderView();
}

void AttachInputJointCommand::doCommand() {
	ptr<vwgl::scene::Node> chain = nullptr;
	_outParent = _outNode->parent();
	_inParent = _inNode->parent();
	if (!_outParent.valid() || !_inParent.valid()) {
		throw std::runtime_error("AttachInputJointCommand: Unexpected error. Parent node not found in output or input nodes.");
	}

	if (!_outParent->getComponent<vwgl::scene::Chain>()) {
		// The output joint parent is not a chain: create a new parent node
		chain = new vwgl::scene::Node("Chain");
		chain->addComponent(new vwgl::scene::Chain());
		vwgl::scene::Transform * trans = new vwgl::scene::Transform();
		trans->getTransform().setTransformStrategy(new vwgl::TRSTransformStrategy());
		if (_outNode->getComponent<vwgl::scene::Transform>()) {
			vwgl::scene::Transform * prevTrx = _outNode->getComponent<vwgl::scene::Transform>();
			if (prevTrx->getTransform().getTransformStrategy()) {
				_outNodeTransform = prevTrx->getTransform().getTransformStrategy()->clone();
				trans->getTransform().setTransformStrategy(prevTrx->getTransform().getTransformStrategy()->clone());
				prevTrx->getTransform().setTransformStrategy(new TRSTransformStrategy());
			}
		}
		chain->addComponent(trans);
		_outParent->addChild(chain.getPtr());
		chain->addChild(_outNode.getPtr());
		//chain->addChild;()
		_chain = chain;
	}
	else {
		chain = _outParent.getPtr();
	}

	chain->addChild(_inNode.getPtr());
	chain->moveChildNextTo(_outNode.getPtr(), _inNode.getPtr());
}

void AttachInputJointCommand::undoCommand() {
	// TODO: Implement undo
	if (_outParent.getPtr()!=_outNode->parent()) {
		_outParent->addChild(_outNode.getPtr());
	}
	_inParent->addChild(_inNode.getPtr());
	if (_chain.valid()) {
		_chain->parent()->removeChild(_chain.getPtr());
		_chain = nullptr;
	}
	if (_outNodeTransform.valid()) {
		_outNode->getComponent<vwgl::scene::Transform>()->getTransform().setTransformStrategy(_outNodeTransform.getPtr());
		_outNodeTransform = nullptr;
	}
}

}
}
