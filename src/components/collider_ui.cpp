
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/components/collider_ui.hpp>

#include <bg2e/physics/box_collider_shape.hpp>
#include <bg2e/physics/sphere_collider_shape.hpp>

#include <bg2e/scene/collider.hpp>
#include <vwgl/cmd/scene_commands.hpp>

#include <QDoubleValidator>

namespace bg2e {
namespace components {

using namespace vwgl;

class ColliderCommand : public vwgl::app::Command {
public:

	inline void addTarget(bg2e::scene::Collider * col) { if (col) _targets.push_back(col); }

protected:
	std::vector<vwgl::ptr<bg2e::scene::Collider> > _targets;
};

template <class T>
class SetColliderShapeCommand : public ColliderCommand {
public:
	SetColliderShapeCommand() {}

	virtual void doCommand() {
		clearUndoShapes();
		for (auto t : _targets) {
			bg2e::physics::ColliderShape * shape = t->getShape();
			if (shape) {
				_undoShapes.push_back(shape->clone());
			}
			else {
				_undoShapes.push_back(nullptr);
			}
			t->setShape(new T());
		}
	}

	virtual void undoCommand() {
		std::vector<bg2e::physics::ColliderShape*>::iterator it = _undoShapes.begin();
		for (auto t : _targets) {
			bg2e::physics::ColliderShape * undoShape = *it;
			t->setShape(undoShape);
			++it;
		}
	}

protected:
	std::vector<bg2e::physics::ColliderShape*> _undoShapes;

	void clearUndoShapes() {
		for (auto shape : _undoShapes) {
			if (shape) {
				delete shape;
			}
		}
		_undoShapes.clear();
	}
};

class SetColliderBoxSizeCommand : public ColliderCommand {
public:
	SetColliderBoxSizeCommand(const vwgl::Vector3 & size) :_size(size) {}

	virtual void doCommand() {
		_undoSize.clear();
		for (auto t : _targets) {
			bg2e::physics::BoxColliderShape * shape = dynamic_cast<bg2e::physics::BoxColliderShape*>(t->getShape());
			if (shape) {
				_undoSize.push_back(shape->getSize());
				shape->setSize(_size);
			}
			else {
				_undoSize.push_back(vwgl::Vector3());
			}
		}
	}

	virtual void undoCommand() {
		std::vector<vwgl::Vector3>::iterator it = _undoSize.begin();

		for (auto t : _targets) {
			bg2e::physics::BoxColliderShape * shape = dynamic_cast<bg2e::physics::BoxColliderShape*>(t->getShape());
			if (shape) {
				shape->setSize(*it);
			}
			++it;
		}
	}

protected:
	vwgl::Vector3 _size;
	std::vector<vwgl::Vector3> _undoSize;
};

class SetColliderSphereRadiusCommand : public ColliderCommand {
public:
	SetColliderSphereRadiusCommand(float r) :_radius(r) {}

	virtual void doCommand() {
		_undoRadius.clear();

		for (auto t : _targets) {
			bg2e::physics::SphereColliderShape * shape = dynamic_cast<bg2e::physics::SphereColliderShape*>(t->getShape());
			if (shape) {
				_undoRadius.push_back(shape->getRadius());
				shape->setRadius(_radius);
			}
			else {
				_undoRadius.push_back(0.0f);
			}
		}
	}

	virtual void undoCommand() {
		std::vector<float>::iterator it = _undoRadius.begin();
		for (auto t : _targets) {
			bg2e::physics::SphereColliderShape * shape = dynamic_cast<bg2e::physics::SphereColliderShape*>(t->getShape());
			if (shape) {
				shape->setRadius(*it);
			}
			++it;
		}
	}

protected:
	float _radius;
	std::vector<float> _undoRadius;
};

void executeColliderCommand(ColliderCommand * cmd) {
	vwgl::manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
	bool exec = false;

	sel.eachNode([&](vwgl::scene::Node * node) {
		bg2e::scene::Collider* col = node->getComponent<bg2e::scene::Collider>();
		if (col) {
			exec = true;
			cmd->addTarget(col);
		}
	});

	editor::AppDelegate::get()->commandManager().execute(cmd);
	editor::AppDelegate::get()->updateRenderView();
}


ComponentUIFactory colliderRegister("Collider",
	[](Registry * registry) {
		registry->registerWidget(typeid(bg2e::scene::Collider).hash_code(),new ColliderUI());
	},
	[]() {
		return new bg2e::scene::Collider(new bg2e::physics::BoxColliderShape());
	});


ColliderUI::ColliderUI()
	:ComponentField("Collider")
{
	_type = new ui::ComboBoxField("Collider type");
	_type->addItem("-");
	_type->addItem(tr("Box"));
	_type->addItem(tr("Sphere"));
	_type->onComboBoxChanged([&](ui::ComboBoxField*) {
		changeColliderType();
	});
	addField(_type);

	_boxSize = new ui::VectorField("Box size");
	_boxSize->setComponentLabel(0,tr("width"));
	_boxSize->setComponentLabel(1,tr("height"));
	_boxSize->setComponentLabel(2,tr("depth"));
	_boxSize->onVectorChanged([&](ui::VectorField*) {
		executeColliderCommand(new SetColliderBoxSizeCommand(_boxSize->getVector3()));
	});
	addField(_boxSize);

	_sphereRadius = new ui::TextField("Sphere radius");
	_sphereRadius->setValidator(new QDoubleValidator());
	_sphereRadius->onEditFinished([&](ui::TextField*) {
		executeColliderCommand(new SetColliderSphereRadiusCommand(_sphereRadius->text().toFloat()));
	});
	addField(_sphereRadius);
}

void ColliderUI::selectionChanged(vwgl::manipulation::SelectionHandler & selection) {
	if (selection.getNodeList().size()>0) {
		vwgl::scene::Node * lastNode = selection.getNodeList().back().getPtr();
		bg2e::scene::Collider * col = nullptr;

		if (lastNode && (col=lastNode->getComponent<bg2e::scene::Collider>())) {
			bg2e::physics::BoxColliderShape * boxShape = dynamic_cast<bg2e::physics::BoxColliderShape*>(col->getShape());
			bg2e::physics::SphereColliderShape * sphereShape = dynamic_cast<bg2e::physics::SphereColliderShape*>(col->getShape());

			if (boxShape) {
				_type->setCurrentIndex(1);
				_sphereRadius->hide();
				_boxSize->show();
				_boxSize->setVector(boxShape->getSize());
			}
			else if (sphereShape) {
				_type->setCurrentIndex(2);
				_sphereRadius->show();
				_boxSize->hide();
				_sphereRadius->setText(QString::number(sphereShape->getRadius()));
			}
			else {
				_type->setCurrentIndex(0);
				_sphereRadius->hide();
				_boxSize->hide();
			}
		}
	}
}

void ColliderUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<bg2e::scene::Collider> *cmd = new cmd::RemoveComponentCommand<bg2e::scene::Collider>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		bg2e::scene::Collider * c = node->getComponent<bg2e::scene::Collider>();
		if (c) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

void ColliderUI::changeColliderType() {
	vwgl::manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();

	ColliderCommand * cmd = nullptr;
	switch (_type->currentIndex()) {
	case 1:
		cmd = new SetColliderShapeCommand<bg2e::physics::BoxColliderShape>();
		break;
	case 2:
		cmd = new SetColliderShapeCommand<bg2e::physics::SphereColliderShape>();
		break;
	}

	sel.eachNode([&](vwgl::scene::Node * node) {
		bg2e::scene::Collider* col = node->getComponent<bg2e::scene::Collider>();
		if (col) {
			cmd->addTarget(col);
		}
	});

	editor::AppDelegate::get()->commandManager().execute(cmd);
	editor::AppDelegate::get()->updateRenderView();
	selectionChanged(sel);
}

}
}
