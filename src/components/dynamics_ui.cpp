
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/components/dynamics_ui.hpp>

#include <bg2e/scene/dynamics.hpp>
#include <vwgl/cmd/scene_commands.hpp>

namespace bg2e {
namespace components {

using namespace vwgl;

class SetGravityCommand : public vwgl::app::Command {
public:
	SetGravityCommand(const Vector3 & g) :_gravity(g) {}

	inline void addTarget(bg2e::scene::Dynamics* dyn) {
		_targets.push_back(dyn);
	}

	virtual void doCommand() {
		_undoGravity.clear();

		for (auto t : _targets) {
			bg2e::scene::Dynamics * dyn = t.getPtr();
			_undoGravity.push_back(dyn->getWorld().getGravity());
			dyn->getWorld().setGravity(_gravity);
		}
	}

	virtual void undoCommand() {
		std::vector<Vector3>::iterator it = _undoGravity.begin();
		for (auto t : _targets) {
			t->getWorld().setGravity(*it);
			++it;
		}
	}

protected:
	vwgl::Vector3 _gravity;
	std::vector<ptr<bg2e::scene::Dynamics> > _targets;
	std::vector<Vector3> _undoGravity;
};

ComponentUIFactory dynamicsRegister("Dynamics",
	[](Registry * registry) {
		registry->registerWidget(typeid(bg2e::scene::Dynamics).hash_code(),new DynamicsUI());
	},
	[]() {
		return new bg2e::scene::Dynamics();
	});


DynamicsUI::DynamicsUI()
	:ComponentField("Dynamics")
{
	_gravity = new ui::VectorField("Gravity");
	_gravity->onVectorChanged([&](ui::VectorField*) {
		vwgl::ptr<SetGravityCommand> cmd = new SetGravityCommand(_gravity->getVector3());
		bool exec = false;
		vwgl::manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
		sel.eachNode([&](vwgl::scene::Node * node) {
			bg2e::scene::Dynamics * dyn = node->getComponent<bg2e::scene::Dynamics>();
			if (dyn) {
				exec = true;
				cmd->addTarget(dyn);
			}
		});
		if (exec) {
			editor::AppDelegate::get()->commandManager().execute(cmd.getPtr());
		}
	});
	_gravity->setVector(Vector3(0.0f));
	addField(_gravity);
}

void DynamicsUI::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
	sel.eachNode([&](vwgl::scene::Node * node) {
		bg2e::scene::Dynamics * dyn = node->getComponent<bg2e::scene::Dynamics>();
		if (dyn) {
			_gravity->setVector(dyn->getWorld().getGravity());
		}
	});
}

void DynamicsUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<bg2e::scene::Dynamics> *cmd = new cmd::RemoveComponentCommand<bg2e::scene::Dynamics>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		bg2e::scene::Dynamics * c = node->getComponent<bg2e::scene::Dynamics>();
		if (c) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

}
}
