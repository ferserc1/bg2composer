
#include <bg2e/components/library_inspector.hpp>

#include <vwgl/gui/gui.hpp>
#include <vwgl/gui/localization.hpp>
#include <vwgl/state.hpp>
#include <vwgl/library/model.hpp>
#include <vwgl/library/material.hpp>
#include <vwgl/loader.hpp>

namespace bg2e {
namespace components {

LibraryInspectorComponent::LibraryInspectorComponent()
	:_visible(false)
	,_scrollTop(0)
	,_closeOnSelect(false)
	,_onOpenLibrary(nullptr)
	,_onEnterGroup(nullptr)
	,_onSelectItem(nullptr)
{
}

LibraryInspectorComponent::~LibraryInspectorComponent() {
}

void LibraryInspectorComponent::drawUI(vwgl::flat::Renderer & renderer) {
	if (_library.valid() && _visible) {
		vwgl::Viewport vp = vwgl::State::get()->getViewport();
		int margin = 40;
		int height = vp.height() - margin * 2;
		vwgl::gui::Layout containerLayout(margin, margin, 300, height);
		vwgl::gui::View view(renderer, containerLayout);

		vwgl::gui::Layout layout(3, 3, -3, 20);

		vwgl::gui::Controls::button("Open Library", layout, [&]() {
			if (_onOpenLibrary) {
				_onOpenLibrary(this);
			}
		});

		vwgl::gui::Controls::separator(layout.getNextDown());

		vwgl::Vector4i parentBounds;
		int w, h;
		view.getParentBounds(parentBounds);
		layout.getSize(parentBounds, w, h);
		drawSubview(renderer, layout.getNextDown().setHeight(height - h - margin * 2 - 10));

		vwgl::gui::Controls::separator(layout.getNextDown().setHeight(10));

		vwgl::gui::Controls::checkBox("Hide on select", layout.getNextDown(), _closeOnSelect);
	}
}

void LibraryInspectorComponent::drawSubview(vwgl::flat::Renderer & renderer, vwgl::gui::Layout & lo) {
	if (!_library.valid()) return;
	if (!_currentNode.valid()) {
		_currentNode = _library.getPtr();
	}

	vwgl::gui::ScrollView view(renderer, _scrollTop, lo.clone());

	std::function<void (int, int, vwgl::library::Node *, vwgl::gui::Layout &)> drawItem =
			[&](int cellSize, int margin, vwgl::library::Node * item, vwgl::gui::Layout & layout) {
				vwgl::library::Group * group = dynamic_cast<vwgl::library::Group*>(item);
				vwgl::library::Material * material = dynamic_cast<vwgl::library::Material*>(item);
				vwgl::library::Model * model = dynamic_cast<vwgl::library::Model*>(item);
				if (item && !item->getIcon().empty() && !group) {
					int w = layout.getRawWidth();
					int h = layout.getRawHeight();
					layout.setWidth(cellSize)
							.setHeight(cellSize);
					std::string url = vwgl::System::get()->addPathComponent(_library->getPath(), item->getIcon());
					vwgl::Texture * icon = vwgl::Loader::get()->loadTexture(url);
					vwgl::gui::Controls::imageButton(icon, layout, [&]() {
						selectItem(item);
					});
					layout.setX(cellSize + margin*2)
							.setWidth(w)
							.setHeight(h);
				}
				if (item==nullptr) {
					vwgl::gui::Controls::buttonLabel("..", layout, [&]() {
						setCurrentNode(_currentNode->getParent());
					});
				}
				else if (group) {
					vwgl::gui::Controls::buttonLabel(item->getName(), layout, [&]() {
						setCurrentNode(group);
					});
				}
				else if (material) {
					vwgl::gui::Controls::buttonLabel(item->getName(), layout, [&]() {
						selectItem(item);
					});
				}
				else if (model) {
					vwgl::gui::Controls::buttonLabel(item->getName(), layout, [&]() {
						selectItem(item);
					});
				}
				layout.setX(margin);
			};

	int rectSize = 30;
	int margin = 3;

	int skipPre = _scrollTop / (rectSize + margin);
	int maxItems = lo.getRawHeight() / (rectSize) - 2;
	int drawnItems = 0;
	vwgl::gui::Layout layout(margin, margin, -28, rectSize);
	int i = 0;
	for (vwgl::ptr<vwgl::library::Node> item : _currentNode->getNodeList()) {
		if (i>=skipPre && drawnItems<maxItems) {
			++drawnItems;
			if (_currentNode->getParent() && i==0) {
				drawItem(rectSize, margin, nullptr, layout);
			}
			else {
				drawItem(rectSize, margin, item.getPtr(), layout);
			}
		}
		layout.getNextDown();
		++i;
	}
	view.endScroll(layout, _scrollTop);
}

void LibraryInspectorComponent::selectItem(vwgl::library::Node * node) {
	if (_closeOnSelect) hide();
	if (_onSelectItem) _onSelectItem(this, node);
}

}
}
