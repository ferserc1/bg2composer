
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/components/transform_ui.hpp>

#include <vwgl/scene/transform.hpp>

#include <vwgl/cmd/transform_commands.hpp>
#include <vwgl/cmd/scene_commands.hpp>

namespace bg2e {
namespace components {

using namespace vwgl;

ComponentUIFactory transformRegister("Transform",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::Transform).hash_code(),new TransformUI());
	},
	[]() {
		return new vwgl::scene::Transform();
	});

TransformUI::TransformUI()
	:ComponentField("Transform")
{
	editor::AppDelegate::get()->inputMediator().gizmoManager().registerObserver(this);

	_menu = new ui::MenuField("Transform Actions");
	addField(_menu);
	_menu->addAction(new ClosureAction("Reset Position", [&]() {
		setTranslate(Vector3(0.0f, 0.0f, 0.0f));
	}));
	_menu->addAction(new ClosureAction("Reset Rotation", [&]() {
		setRotate(Vector3(0.0f, 0.0f, 0.0f));
	}));
	_menu->addAction(new ClosureAction("Reset Scale", [&]() {
		setScale(Vector3(1.0f, 1.0f, 1.0f));
	}));

	_translation = new ui::VectorField("Translation");
	_translation->setVector(vwgl::Vector3(0.0f, 0.0f, 0.0f));
	_translation->onVectorChanged([&](ui::VectorField * field) {
		setTranslate(field->getVector3());
	});
	addField(_translation);

	_rotation = new ui::VectorField("Rotation");
	_rotation->setVector(vwgl::Vector3(0.0f, 0.0f, 0.0f));
	_rotation->onVectorChanged([&](ui::VectorField * field) {
		setRotate(field->getVector3());
	});
	addField(_rotation);

	_scale = new ui::VectorField("Scale");
	_scale->setVector(vwgl::Vector3(1.0f, 1.0f, 1.0f));
	_scale->onVectorChanged([&](ui::VectorField * field ) {
		setScale(field->getVector3());
	});
	addField(_scale);
}

void TransformUI::selectionChanged(manipulation::SelectionHandler & selection) {
	if (selection.getNodeList().size()>0) {
		vwgl::scene::Node * lastNode = selection.getNodeList().back().getPtr();
		vwgl::scene::Transform * trx = nullptr;

		if (lastNode && (trx=lastNode->getComponent<vwgl::scene::Transform>())) {
			TRSTransformStrategy * strategy=trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
			if (strategy) {
				setSelection(strategy);
			}
			else {
				setSelection(trx->getTransform().getMatrix());
			}
		}
		else {
			_translation->hide();
			_rotation->hide();
			_scale->hide();
		}
	}
}

void TransformUI::didExecuteCommand(vwgl::app::Command *) {
	selectionChanged(editor::AppDelegate::get()->inputMediator().selectionHandler());
}

void TransformUI::didUndo(vwgl::app::Command *) {
	selectionChanged(editor::AppDelegate::get()->inputMediator().selectionHandler());
}

void TransformUI::didRedo(vwgl::app::Command *) {
	selectionChanged(editor::AppDelegate::get()->inputMediator().selectionHandler());
}

void TransformUI::gizmoMoved(vwgl::manipulation::GizmoManager &) {
	selectionChanged(editor::AppDelegate::get()->inputMediator().selectionHandler());
}

void TransformUI::setSelection(vwgl::TRSTransformStrategy * strategy) {
	_translation->setVector(strategy->getTranslate());
	_rotation->setVector(Vector3(strategy->getRotateX() * Math::k180OverPi,
								 strategy->getRotateY() * Math::k180OverPi,
								 strategy->getRotateZ() * Math::k180OverPi));
	_scale->setVector(strategy->getScale());

	_translation->show();
	_rotation->show();
	_scale->show();
}

void TransformUI::setSelection(const vwgl::Matrix4 & matrix) {
	Q_UNUSED(matrix);
	_translation->hide();
	_rotation->hide();
	_scale->hide();
}

void TransformUI::setTranslate(const vwgl::Vector3 & vector) {
	vwgl::scene::Transform * trx = transform();
	if (trx) {
		executeCommand(new cmd::SetTranslateCommand(trx,vector));
	}
}

void TransformUI::setRotate(const vwgl::Vector3 & vector) {
	vwgl::scene::Transform * trx = transform();
	if (trx) {
		vwgl::Vector3 rad = vector;
		rad.scale(vwgl::Math::kPiOver180);
		executeCommand(new cmd::SetRotateCommand(trx,rad));
	}
}

void TransformUI::setScale(const vwgl::Vector3 & vector) {
	vwgl::scene::Transform * trx = transform();
	if (trx) {
		executeCommand(new cmd::SetScaleCommand(trx,vector));
	}
}

vwgl::TRSTransformStrategy * TransformUI::strategy() {
	vwgl::scene::Transform * trx = transform();

	return trx!=nullptr ? trx->getTransform().getTransformStrategy<vwgl::TRSTransformStrategy>():nullptr;
}

vwgl::scene::Transform * TransformUI::transform() {
	manipulation::SelectionHandler & selection = editor::AppDelegate::get()->inputMediator().selectionHandler();
	vwgl::scene::Transform * trx = nullptr;

	if (selection.getNodeList().size()>0) {
		trx = selection.getNodeList().back()->getComponent<vwgl::scene::Transform>();
	}

	return trx;
}

void TransformUI::executeCommand(vwgl::app::Command * cmd) {
	editor::AppDelegate::get()->commandManager().execute(cmd);
	editor::AppDelegate::get()->updateRenderView();
}

void TransformUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::Transform> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::Transform>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Transform * trx = node->getComponent<vwgl::scene::Transform>();
		if (trx) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

}
}
