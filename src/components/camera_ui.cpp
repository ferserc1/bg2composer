
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/components/camera_ui.hpp>

#include <vwgl/scene/camera.hpp>
#include <vwgl/cmd/scene_commands.hpp>

namespace bg2e {
namespace components {

using namespace vwgl;

ComponentUIFactory cameraRegister("Camera",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::Camera).hash_code(),new CameraUI());
	},
	[]() {
		return new vwgl::scene::Camera();
	});


CameraUI::CameraUI()
	:ComponentField("Camera")
{
}

void CameraUI::selectionChanged(vwgl::manipulation::SelectionHandler &) {
	
}

void CameraUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::Camera> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::Camera>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Camera * c = node->getComponent<vwgl::scene::Camera>();
		if (c && c!=vwgl::scene::Camera::getMainCamera()) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

}
}
