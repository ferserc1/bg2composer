
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/components/chain_ui.hpp>

#include <vwgl/scene/chain.hpp>
#include <vwgl/cmd/scene_commands.hpp>

namespace bg2e {
namespace components {

using namespace vwgl;

ComponentUIFactory chainRegister("Chain",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::Chain).hash_code(),new ChainUI());
	},
	[]() {
		return new vwgl::scene::Chain();
	});

ChainUI::ChainUI()
	:ComponentField("Chain")
{
}

void ChainUI::selectionChanged(vwgl::manipulation::SelectionHandler &) {
	
}

void ChainUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::Chain> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::Chain>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Chain * c = node->getComponent<vwgl::scene::Chain>();
		if (c) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

}
}
