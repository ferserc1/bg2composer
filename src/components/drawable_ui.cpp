
#include <bg2e/editor/app_delegate.hpp>
#include <bg2e/components/drawable_ui.hpp>
#include <bg2e/ui/text_field.hpp>
#include <bg2e/closure_action.hpp>

#include <vwgl/loader.hpp>
#include <vwgl/cmd/drawable_commands.hpp>
#include <vwgl/cmd/export_command.hpp>
#include <vwgl/cmd/scene_commands.hpp>

namespace vwgl {
namespace cmd {

class DrwMergeCommand : public DrawableTransformCommand {
public:
	DrwMergeCommand(plain_ptr ctx) :DrawableTransformCommand(ctx) {}


	void doCommand() {
		if (targetSize()<2) {
			throw std::runtime_error("Could not execute DrwMergeCommand: not enough targets set");
		}
		else {
			Vector3 srcPosition;
			vwgl::scene::Drawable * destination = _drawableVector.front().getPtr();
			if (destination->transform()) {
				srcPosition = destination->transform()->getTransform().getMatrix().getPosition();
			}
			_undoPosition = srcPosition;

			beginDoCommand();
			eachDrawable([&](vwgl::scene::Drawable * drw) {
				pushTransform(drw);
				if (drw->transform()) {
					Matrix4 mat = drw->transform()->getTransform().getMatrix();
					drw->applyTransform(mat);
					drw->transform()->identity();
				}

				if (drw!=destination) {
					drw->eachElement([&](PolyList * plist, Material * mat, Transform & trx) {
						PolyList * plistClone = new PolyList(plist);
						destination->addPolyList(plistClone, mat->clone(), trx);
						_undoPlist.push_back(plistClone);
					});
					_undoParents.push_back(drw->node()->parent());
					_undoNodes.push_back(drw->node());
					drw->node()->parent()->removeChild(drw->node());
				}
			});

			if (destination->node()->getComponent<manipulation::Selectable>()) {
				destination->node()->getComponent<manipulation::Selectable>()->drawableChanged(destination);
			}

			// Mover a srcPosition
			if (destination->transform()) {
				Matrix4 posTrx;
				posTrx.identity().translate(-srcPosition.x(), -srcPosition.y(), -srcPosition.z());
				destination->applyTransform(posTrx);
				Transform trx = destination->transform()->getTransform();
				if (dynamic_cast<TRSTransformStrategy*>(trx.getTransformStrategy())) {
					dynamic_cast<TRSTransformStrategy*>(trx.getTransformStrategy())->translate(srcPosition);
				}
				else {
					trx.getMatrix().translate(srcPosition);
				}
			}
		}
	}

	void undoCommand() {
		vwgl::scene::Drawable * destination = _drawableVector.front().getPtr();

		if (destination->transform()) {
			Matrix4 posTrx;
			posTrx.identity().translate(_undoPosition);
			destination->applyTransform(posTrx);
			Transform trx = destination->transform()->getTransform();
			if (dynamic_cast<TRSTransformStrategy*>(trx.getTransformStrategy())) {
				dynamic_cast<TRSTransformStrategy*>(trx.getTransformStrategy())->translate(_undoPosition.scale(-1.0f));
			}
			else {
				trx.getMatrix().translate(_undoPosition.scale(-1.0f));
			}
		}

		for (size_t i = 0; i<_undoParents.size(); ++i) {
			_undoParents[i]->addChild(_undoNodes[i].getPtr());
		}

		for (ptr<PolyList> plist : _undoPlist) {
			destination->removePolyList(plist.getPtr());
		}

		eachDrawable([&](vwgl::scene::Drawable * drw) {
			restoreTransform(drw);
			if (drw->transform()) {
				Matrix4 mat = drw->transform()->getTransform().getMatrix();
				mat.invert();
				drw->applyTransform(mat);
			}
		});

		// Actualizar el selectable
		if (destination->node()->getComponent<manipulation::Selectable>()) {
			destination->node()->getComponent<manipulation::Selectable>()->drawableChanged(destination);
		}

		_undoParents.clear();
		_undoNodes.clear();
		_undoPlist.clear();
		_undoPosition.set(0.0f);
	}

protected:
	std::vector<ptr<vwgl::scene::Node> > _undoParents;
	std::vector<ptr<vwgl::scene::Node> > _undoNodes;
	std::vector<ptr<PolyList> > _undoPlist;
	Vector3 _undoPosition;
};

}
}

namespace bg2e {
namespace components {

using namespace vwgl;

ComponentUIFactory drawableRegister("Drawable",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::scene::Drawable).hash_code(),new DrawableUI());
	},
	[]() {
		return new vwgl::scene::Drawable();
	});

DrawableUI::DrawableUI()
	:ComponentField("Drawable")
{
	editor::AppDelegate * app = editor::AppDelegate::get();
	manipulation::SelectionHandler & selectionHandler = app->inputMediator().selectionHandler();

	_name = new ui::TextField(tr("Name"));
	connect(_name,SIGNAL(textChanged(ui::TextField*)),this,SLOT(nameChanged(ui::TextField*)));
	addField(_name);

	ui::MenuField * menu = new ui::MenuField(tr("Actions"));
	menu->addAction(new ClosureAction(tr("Switch UVs"), [&]() {
		switchUVs(selectionHandler);
	}));
	menu->addAction(new ClosureAction(tr("Flip Normals"), [&]() {
		flipNormals(selectionHandler);
	}));
	menu->addAction(new ClosureAction(tr("Flip Faces"), [&]() {
		flipFaces(selectionHandler);
	}));
	menu->addSeparator();
	menu->addAction(new ClosureAction(tr("Freeze Transform"), [&]() {
		freezeTransform(selectionHandler);
	}));
	menu->addAction(new ClosureAction(tr("Move To Center"), [&]() {
		moveToCenter(selectionHandler);
	}));
	menu->addAction(new ClosureAction(tr("Put On Floor"), [&]() {
		putOnFloor(selectionHandler);
	}));
	menu->addSeparator();
	menu->addAction(new ClosureAction(tr("Export Prefab"), [&]() {
		exportPrefab(selectionHandler);
	}));
	menu->addSeparator();
	menu->addAction(new ClosureAction(tr("Replace geometry with imported file"), [&]() {
		importObject(selectionHandler);
	}));
	menu->addSeparator();
	menu->addAction(new ClosureAction(tr("Merge selected drawables"), [&]() {
		mergeSelection(selectionHandler);
	}));
	addField(menu);
}

void DrawableUI::selectionChanged(vwgl::manipulation::SelectionHandler & sender) {
	QString name = "";
	bool nameInitialized = false;
	//editor::AppDelegate * app = editor::AppDelegate::get();

	sender.eachDrawable([&](vwgl::scene::Drawable * drw) {
		if (!nameInitialized) {
			name = QString::fromStdString(drw->getName());
			nameInitialized = true;
		}
		else if (name!=QString::fromStdString(drw->getName())) {
			name = QObject::tr("<multiple values>");
		}
	});

	_name->setText(name);
}

void DrawableUI::didUndo(vwgl::app::Command *) {
	selectionChanged(editor::AppDelegate::get()->inputMediator().selectionHandler());
}

void DrawableUI::didRedo(vwgl::app::Command *) {
	selectionChanged(editor::AppDelegate::get()->inputMediator().selectionHandler());
}

void DrawableUI::switchUVs(vwgl::manipulation::SelectionHandler & selection) {
	executeDrawableCommand(selection, new cmd::DrwSwitchUVsCommand(context(), PolyList::kTexCoord0, PolyList::kTexCoord1));
}

void DrawableUI::flipNormals(vwgl::manipulation::SelectionHandler & selection) {
	executeDrawableCommand(selection, new cmd::DrwFlipNormalsCommand(context()));
}

void DrawableUI::flipFaces(vwgl::manipulation::SelectionHandler & selection) {
	executeDrawableCommand(selection, new cmd::DrwFlipFacesCommand(context()));
}

void DrawableUI::freezeTransform(vwgl::manipulation::SelectionHandler & selection) {
	executeDrawableCommand(selection, new cmd::DrwFreezeTransformCommand(context()));
}

void DrawableUI::moveToCenter(vwgl::manipulation::SelectionHandler & selection) {
	executeDrawableCommand(selection, new cmd::DrwMoveToCenterCommand(context()));
}

void DrawableUI::putOnFloor(vwgl::manipulation::SelectionHandler & selection) {
	executeDrawableCommand(selection, new cmd::DrwPutOnFloorCommand(context()));
}

void DrawableUI::exportPrefab(vwgl::manipulation::SelectionHandler & selection) {
	QString dir = ui::FileField::openDirectoryDialog(tr("Select destination directory"));
	if (!dir.isEmpty()) {
        executeDrawableCommand(selection, new cmd::ExportCommand(context(), ui::FileField::stdStringPath(dir)));
	}
}

void DrawableUI::importObject(vwgl::manipulation::SelectionHandler & selection) {
	QString path = ui::FileField::openFileDialog(QObject::tr("Import object"),"scene objects (*.vwglb *.dae)");
	if (!path.isEmpty()) {
        ptr<vwgl::scene::Drawable> importDrawable = Loader::get()->loadModel(ui::FileField::stdStringPath(path));
		if (importDrawable.valid()) {
			executeDrawableCommand(selection, new cmd::DrwReplaceGeometryCommand(context(),importDrawable.getPtr()));
		}
		else {
			QMessageBox::warning(nullptr, QObject::tr("Error"), QObject::tr("Error importing object"));
		}
	}
}

void DrawableUI::nameChanged(ui::TextField * text) {
	executeDrawableCommand(editor::AppDelegate::get()->inputMediator().selectionHandler(), new cmd::DrwSetNameCommand(context(), text->getText()));
}

void DrawableUI::executeDrawableCommand(vwgl::manipulation::SelectionHandler & selection, vwgl::cmd::DrawableCommand * cmd) {
	selection.eachDrawable([&](vwgl::scene::Drawable * drawable) {
		cmd->addDrawable(drawable);
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
	editor::AppDelegate::get()->updateRenderView();
}

void DrawableUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
	cmd::RemoveComponentCommand<vwgl::scene::Drawable> *cmd = new cmd::RemoveComponentCommand<vwgl::scene::Drawable>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
		vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
		if (drw) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

void DrawableUI::mergeSelection(vwgl::manipulation::SelectionHandler & selection) {
	if (selection.getDrawableList().size()>=2) {
		cmd::DrwMergeCommand * cmd = new cmd::DrwMergeCommand(context());
		vwgl::scene::Node * parent = nullptr;
		QString errorMsg = "";
		selection.eachDrawable([&](vwgl::scene::Drawable * drw) {
			vwgl::scene::Node * thisParent = drw->node() ? drw->node()->parent():nullptr;
			if (thisParent==nullptr) {
				errorMsg = QObject::tr("Unexpected error: orphan node found");
			}
			if (parent==nullptr) {
				parent = thisParent;
			}
			if (parent!=thisParent) {
				errorMsg = QObject::tr("Could not merge drawables: all the nodes containing the  drawables must be children of the same parent");
			}
			cmd->addDrawable(drw);
		});
		if (errorMsg.isEmpty()) {
			editor::AppDelegate::get()->commandManager().execute(cmd);
			editor::AppDelegate::get()->inputMediator().selectionHandler().clearSelection();
			editor::AppDelegate::get()->updateRenderView();
		}
		else {
			QMessageBox::warning(nullptr, QObject::tr("Merge drawables"), errorMsg);
		}

	}
}

}
}
