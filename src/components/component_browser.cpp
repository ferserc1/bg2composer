
#include <bg2e/components/component_browser.hpp>
#include <bg2e/components/registry.hpp>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>


namespace bg2e {
namespace components {

ComponentBrowser::ComponentBrowser(QWidget * parent)
	:QDialog(parent)
	,_doneCallback(nullptr)
{
	setModal(true);
	QVBoxLayout * layout = new QVBoxLayout();
	setLayout(layout);

	// Create _okButton before create list, because _list can trigger the selectionChanged signal, and
	// this signal will disable the button
	_okButton = new QPushButton(tr("Add"));
	_okButton->setEnabled(false);
	_list = new QListWidget();
	buildComponentList();
	connect(_list,SIGNAL(itemSelectionChanged()),this,SLOT(componentSelected()));
	layout->addWidget(_list);

	QWidget * buttons = new QWidget();
	layout->addWidget(buttons);


	QHBoxLayout * buttonsLayout = new QHBoxLayout();
	buttons->setLayout(buttonsLayout);
	buttons->setStyleSheet("background-color: transparent");
	buttonsLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum));
	buttonsLayout->addWidget(_okButton);
	QPushButton * cancelButton = new QPushButton(tr("Cancel"));
	buttonsLayout->addWidget(cancelButton);

	connect(_okButton,SIGNAL(clicked()),this,SLOT(selectPress()));
	connect(cancelButton,SIGNAL(clicked()),this,SLOT(cancelPress()));
}

void ComponentBrowser::buildComponentList() {
	Registry::get()->eachFactory([&](ComponentUIFactory * factory) {
		_list->addItem(QString::fromStdString(factory->name()));
	});
}

void ComponentBrowser::selectPress() {
	if (_doneCallback && _list->currentItem()) {
		int itemIndex = _list->currentRow();
		ComponentUIFactory * factory = Registry::get()->factoryAtIndex(itemIndex);
		_doneCallback(factory);
	}
	close();
}

void ComponentBrowser::cancelPress() {
	close();
}

void ComponentBrowser::componentSelected() {
	if (_list->currentItem()) {
		_okButton->setEnabled(true);
	}
	else {
		_okButton->setEnabled(false);
	}
}

}
}
