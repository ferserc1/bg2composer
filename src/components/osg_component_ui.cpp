
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/components/osg_component_ui.hpp>

#include <vwgl/scene/camera.hpp>
#include <vwgl/cmd/scene_commands.hpp>
#include <vwgl/osg_plugin.hpp>

namespace bg2e {
namespace components {

using namespace vwgl;

ComponentUIFactory osgRegister("Osg",
	[](Registry * registry) {
		registry->registerWidget(typeid(vwgl::osg::OSGComponent).hash_code(),new OsgUI());
	},
	[]() {
		return new vwgl::osg::OSGComponent();
	});


OsgUI::OsgUI()
	:ComponentField("Open Scene Graph")
{
    _ignoreOnExport = new ui::BooleanField(tr("Ignore on export"));
    addField(_ignoreOnExport);
    _ignoreOnExport->onValueChanged([&](ui::BooleanField *) {
        vwgl::manipulation::SelectionHandler & sel = editor::AppDelegate::get()->inputMediator().selectionHandler();
        sel.eachNode([&](vwgl::scene::Node * node) {
            vwgl::osg::OSGComponent * c = node->getComponent<vwgl::osg::OSGComponent>();
            if (c) {
                c->setIgnoreOnExport(_ignoreOnExport->value());
            }
        });
    });
}

void OsgUI::selectionChanged(vwgl::manipulation::SelectionHandler & sel) {
    sel.eachNode([&](vwgl::scene::Node * node) {
        vwgl::osg::OSGComponent * c = node->getComponent<vwgl::osg::OSGComponent>();
        if (c) {
            _ignoreOnExport->setValue(c->getIgnoreOnExport());
        }
    });
}

void OsgUI::onRemoveComponent(vwgl::manipulation::SelectionHandler & sel) {
    cmd::RemoveComponentCommand<vwgl::osg::OSGComponent> *cmd = new cmd::RemoveComponentCommand<vwgl::osg::OSGComponent>(context());
	sel.eachNode([&](vwgl::scene::Node * node) {
        vwgl::osg::OSGComponent * c = node->getComponent<vwgl::osg::OSGComponent>();
		if (c) {
			cmd->addTargetNode(node);
		}
	});
	editor::AppDelegate::get()->commandManager().execute(cmd);
}

}
}
