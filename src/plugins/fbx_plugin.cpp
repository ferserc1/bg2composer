
#include <bg2e/plugins/fbx_plugin.hpp>

namespace bg2e {
namespace plugins {

FBXReader::FBXReader()
	:_drawable(nullptr)
	,_scaleFactor(0.01)
{

}

FBXReader::~FBXReader() {

}

bool FBXReader::acceptFileType(const std::string & path) {
	return vwgl::System::get()->getExtension(path) == "fbx";
}

vwgl::scene::Drawable * FBXReader::loadModel(const std::string &path) {
	vwgl::ptr<vwgl::scene::Drawable> drawable = new vwgl::scene::Drawable();
	_drawable = drawable.getPtr();
	FbxManager * sdkManager = FbxManager::Create();

	FbxIOSettings * ios = FbxIOSettings::Create(sdkManager, IOSROOT);
	sdkManager->SetIOSettings(ios);

	FbxImporter * importer = FbxImporter::Create(sdkManager, "");

	if (!importer->Initialize(path.c_str(), -1, sdkManager->GetIOSettings())) {
		std::cerr << "Initialization failed" << std::endl;
		std::cerr << importer->GetStatus().GetErrorString() << std::endl;
		drawable = nullptr;
		return nullptr;
	}

	FbxScene * scene = FbxScene::Create(sdkManager, "importScene");
	_fbxScene = scene;

	importer->Import(scene);

	importer->Destroy();

	FbxNode * root = scene->GetRootNode();
	if (root) {
		for (int i = 0; i < root->GetChildCount(); ++i) {
			importNode(root->GetChild(i));
		}
	}

	sdkManager->Destroy();

	_drawable = nullptr;
	return drawable.release();
}

void FBXReader::importNode(FbxNode * node) {
	const char * nodeName = node->GetName();
	FbxDouble3 translation = node->LclTranslation.Get();
	FbxDouble3 rotation = node->LclRotation.Get();
	FbxDouble3 scaling = node->LclScaling.Get();


	int materials = node->GetMaterialCount();
	
/*	for (int im = 0; im < materials; ++im) {
		FbxSurfaceMaterial * mat = node->GetMaterial(im);
		std::cout << mat->GetName() << std::endl;
	}
	*/

	FbxMesh * mesh = node->GetMesh();
	if (mesh) {
		importMesh(node, mesh);
	}

	for (int i = 0; i < node->GetChildCount(); ++i) {
		importNode(node->GetChild(i));
	}
}


void FBXReader::importMesh(FbxNode * node, FbxMesh * mesh) {
	FbxAnimEvaluator * sceneEval = _fbxScene->GetAnimationEvaluator();
	FbxAMatrix & trx = sceneEval->GetNodeGlobalTransform(node);

	double * val = reinterpret_cast<double*>(trx.Buffer());

	vwgl::Matrix4 matrix(	static_cast<float>(val[0]), static_cast<float>(val[1]), static_cast<float>(val[2]), static_cast<float>(val[3]),
							static_cast<float>(val[4]), static_cast<float>(val[5]), static_cast<float>(val[6]), static_cast<float>(val[7]),
							static_cast<float>(val[8]), static_cast<float>(val[9]), static_cast<float>(val[10]), static_cast<float>(val[11]),
							static_cast<float>(val[12]) * _scaleFactor, static_cast<float>(val[13]) * _scaleFactor, static_cast<float>(val[14]) * _scaleFactor, static_cast<float>(val[15]));
	matrix.scale(vwgl::Vector3(_scaleFactor, _scaleFactor, _scaleFactor));

	std::cout << matrix.toString() << std::endl;

	int polygonCount = mesh->GetPolygonCount();
	int vertexCounter = 0;
	int ctrlPointCount = mesh->GetControlPointsCount();

	// http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/how-to-work-with-fbx-sdk-r3582
	std::vector<vwgl::Vector3> vertexList;
	for (int i = 0; i < ctrlPointCount; ++i) {
		float x = mesh->GetControlPointAt(i).mData[0];
		float y = mesh->GetControlPointAt(i).mData[1];
		float z = mesh->GetControlPointAt(i).mData[2];

		vertexList.push_back(vwgl::Vector3(x, y, z));
	}

	unsigned int index = 0;
	int texChannels = mesh->GetElementUVCount();
	FbxStringList lUVSetNameList;
	mesh->GetUVSetNames(lUVSetNameList);
	FbxLayerElementArrayTemplate<int> * indices;
	mesh->GetMaterialIndices(&indices);
	int matCount = indices->GetCount();

	std::map<int, vwgl::ptr<vwgl::PolyList> > plistMap;


	vwgl::PolyList * currentPlist = nullptr;
	for (int i = 0; i < polygonCount; ++i) {
		int size = mesh->GetPolygonSize(i);
		int firstPoint = 0;
		int firstPointVC;
		int prevCtrlPoint = 0;
		int prevPointVC;
		int matIndex = indices->GetAt(i);
		if (plistMap.find(matIndex) == plistMap.end()) {
			plistMap[matIndex] = new vwgl::PolyList();
		}
		currentPlist = plistMap[matIndex].getPtr();
		index = currentPlist->getIndexCount();
		for (int j = 0; j < size; ++j) {
			vwgl::Vector3 normal;
			vwgl::Vector2 uv;
			FbxVector4 vn;
			FbxVector2 fbxUv;
			bool um;
			int ctrlPointIndex = mesh->GetPolygonVertex(i, j);
			int texUvIndex = mesh->GetTextureUVIndex(i, j);
			

			vwgl::Vector3 vector = vertexList[ctrlPointIndex];
			if (j == 0) {
				firstPoint = static_cast<unsigned int>(ctrlPointIndex);
				firstPointVC = vertexCounter;
			}
			if (j < 3) {
				//getNormal(mesh, ctrlPointIndex, vertexCounter, normal);
				mesh->GetPolygonVertexNormal(i, j, vn);
				normal.set(vn.mData[0], vn.mData[1], vn.mData[2]);
				currentPlist->addVertex(vector);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, j, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(fbxUv.mData[0], fbxUv.mData[1]);
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
				}
				currentPlist->addIndex(index++);
				// Primer triangulo
			}
			else {
				// resto
				//getNormal(mesh, prevCtrlPoint, prevPointVC, normal);
				mesh->GetPolygonVertexNormal(i, j - 1, vn);
				normal.set(vn.mData[0], vn.mData[1], vn.mData[2]);
				currentPlist->addVertex(vertexList[prevCtrlPoint]);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, j-1, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(fbxUv.mData[0], fbxUv.mData[1]);
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
				}
				currentPlist->addIndex(index++);

				//getNormal(mesh, ctrlPointIndex, vertexCounter, normal);
				mesh->GetPolygonVertexNormal(i, j, vn);
				normal.set(vn.mData[0], vn.mData[1], vn.mData[2]);
				currentPlist->addVertex(vertexList[ctrlPointIndex]);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, j, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(fbxUv.mData[0], fbxUv.mData[1]);
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
				}
				currentPlist->addIndex(index++);

				//getNormal(mesh, firstPoint, firstPointVC, normal);
				mesh->GetPolygonVertexNormal(i, 0, vn);
				normal.set(vn.mData[0], vn.mData[1], vn.mData[2]);
				currentPlist->addVertex(vertexList[firstPoint]);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, 0, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(fbxUv.mData[0], fbxUv.mData[1]);
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
				}
				currentPlist->addIndex(index++);
			}
			prevCtrlPoint = static_cast<unsigned int>(ctrlPointIndex);
			prevPointVC = vertexCounter;
			++vertexCounter;
		}
	}
	
	for (auto pl : plistMap) {
		vwgl::PolyList * polyList = pl.second.getPtr();
		polyList->buildPolyList();
		polyList->applyTransform(matrix);
		_drawable->addPolyList(polyList);
	}

//	polyList->buildPolyList();
//	polyList->applyTransform(matrix);

//	_drawable->addPolyList(polyList.getPtr());
}

void FBXReader::getNormal(FbxMesh * mesh, int ctrlPoint, int vertexCounter, vwgl::Vector3 & normal) {
	if (mesh->GetElementNormalCount() < 1) {
		normal.set(1.0, 0.0, 0.0);
	}
	else {
		FbxGeometryElementNormal * vertexNormal = mesh->GetElementNormal(0);
		switch (vertexNormal->GetMappingMode()) {
		case FbxGeometryElement::eByControlPoint:
			switch (vertexNormal->GetReferenceMode()) {
			case FbxGeometryElement::eDirect:
				normal.set(	static_cast<float>(vertexNormal->GetDirectArray().GetAt(ctrlPoint).mData[0]),
							static_cast<float>(vertexNormal->GetDirectArray().GetAt(ctrlPoint).mData[1]),
							static_cast<float>(vertexNormal->GetDirectArray().GetAt(ctrlPoint).mData[2]));
				break;
			case FbxGeometryElement::eIndexToDirect:
				{
					int index = vertexNormal->GetIndexArray().GetAt(ctrlPoint);
					normal.set(	static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[0]),
								static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[1]),
								static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[2]));
				}
				break;

			default:
				normal.set(1.0f, 0.0f, 0.0f);
			}
			break;
		case FbxGeometryElement::eByPolygonVertex:
			switch (vertexNormal->GetReferenceMode()) {
			case FbxGeometryElement::eDirect:
				normal.set(	static_cast<float>(vertexNormal->GetDirectArray().GetAt(vertexCounter).mData[0]),
							static_cast<float>(vertexNormal->GetDirectArray().GetAt(vertexCounter).mData[1]),
							static_cast<float>(vertexNormal->GetDirectArray().GetAt(vertexCounter).mData[2]));
				break;
			case FbxGeometryElement::eIndexToDirect:
			{
				int index = vertexNormal->GetIndexArray().GetAt(vertexCounter);
				normal.set(	static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[0]),
							static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[0]),
							static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[0]));
			}
				break;
			}
			break;
		}
	}
}

void FBXReader::getTexCoord(FbxMesh * mesh, int ctrlPoint, int uvIndex, int uvLayer, vwgl::Vector2 & texCoord) {
	if (uvLayer >= mesh->GetElementUVCount()) {
		texCoord = vwgl::Vector2(0.0f);
	}
	else {
		FbxGeometryElementUV * vertexUV = mesh->GetElementUV(uvLayer);

		switch (vertexUV->GetMappingMode()) {
		case FbxGeometryElement::eByControlPoint:
			switch (vertexUV->GetReferenceMode()) {
			case FbxGeometryElement::eDirect:
				texCoord.set(	static_cast<float>(vertexUV->GetDirectArray().GetAt(ctrlPoint).mData[0]),
								static_cast<float>(vertexUV->GetDirectArray().GetAt(ctrlPoint).mData[0]));
				break;
			case FbxGeometryElement::eIndexToDirect:
				{
					int index = vertexUV->GetIndexArray().GetAt(ctrlPoint);
					texCoord.set(	static_cast<float>(vertexUV->GetDirectArray().GetAt(index).mData[0]),
									static_cast<float>(vertexUV->GetDirectArray().GetAt(index).mData[0]));
				}
				break;
			default:
				texCoord.set(0.0f);
			}
			break;
		case FbxGeometryElement::eByPolygonVertex:
			switch (vertexUV->GetReferenceMode()) {
			case FbxGeometryElement::eDirect:
			case FbxGeometryElement::eIndexToDirect:
				texCoord.set(	static_cast<float>(vertexUV->GetDirectArray().GetAt(uvIndex).mData[0]),
								static_cast<float>(vertexUV->GetDirectArray().GetAt(uvIndex).mData[0]));
				break;
			default:
				texCoord.set(0.0f);
			}
			break;
		}
	}
}


}
}
