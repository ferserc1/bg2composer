#-------------------------------------------------
#
# Project created by QtCreator 2015-02-16T20:53:03
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Composer
TEMPLATE = app

VWGL_ROOT = $$PWD/../vwgl/
VWGL_BIN = $$VWGL_ROOT/build/vwgl/bin/
VWGL_SHADERS_DIR = $$VWGL_ROOT/cplusplus/shaders
VWGL_RESOURCES = $$VWGL_ROOT/cplusplus/resources
#VWGL_SHARED_RESOURCES = $$PWD/../resources

INCLUDEPATH += $$VWGL_ROOT/cplusplus/include
INCLUDEPATH += $$PWD/../fbx/include

BG2E_ROOT = $$PWD
BG2E_SRC = $$BG2E_ROOT/src
BG2E_INCLUDE = $$BG2E_ROOT/include

BG2LIB_ROOT = $$PWD/../bg2engine/
BG2LIB_INC = $$BG2LIB_ROOT/include
INCLUDEPATH += $$BG2LIB_INC

INCLUDEPATH += $$BG2E_INCLUDE

SOURCES +=  $$BG2E_SRC/closure_action.cpp \
		    $$BG2E_SRC/menu_factory.cpp \
		    $$BG2E_SRC/render_view.cpp \
            $$BG2E_SRC/save_thread.cpp \
		    $$BG2E_SRC/scene.cpp \
		    $$BG2E_SRC/settings.cpp \
		    $$BG2E_SRC/settings_window.cpp \
		    $$BG2E_SRC/version.cpp \
		    $$BG2E_SRC/tool_window_manager.cpp

HEADERS +=  $$BG2E_INCLUDE/bg2e/app_delegate.hpp \
		    $$BG2E_INCLUDE/bg2e/closure_action.hpp \
		    $$BG2E_INCLUDE/bg2e/main_window_builder.hpp \
		    $$BG2E_INCLUDE/bg2e/menu_factory.hpp \
		    $$BG2E_INCLUDE/bg2e/render_view.hpp \
		    $$BG2E_INCLUDE/bg2e/save_thread.hpp \
		    $$BG2E_INCLUDE/bg2e/scene.hpp \
		    $$BG2E_INCLUDE/bg2e/settings.hpp \
		    $$BG2E_INCLUDE/bg2e/settings_window.hpp \
		    $$BG2E_INCLUDE/bg2e/toolbar_factory.hpp \
		    $$BG2E_INCLUDE/bg2e/version.hpp \
		    $$BG2E_INCLUDE/bg2e/window_controller.hpp \
		    $$BG2E_INCLUDE/bg2e/tool_window_manager.hpp

SOURCES +=  $$BG2E_SRC/editor/app_delegate.cpp \
		    $$BG2E_SRC/editor/animation_toolbar.cpp \
		    $$BG2E_SRC/editor/axis.cpp \
		    $$BG2E_SRC/editor/component_settings_window.cpp \
		    $$BG2E_SRC/editor/creation_toolbar.cpp \
		    $$BG2E_SRC/editor/draw_plist_selection.cpp \
		    $$BG2E_SRC/editor/edit_options.cpp \
		    $$BG2E_SRC/editor/editor_toolbar_factory.cpp \
		    $$BG2E_SRC/editor/file_options.cpp \
		    $$BG2E_SRC/editor/gizmo_view_options.cpp \
		    $$BG2E_SRC/editor/grid.cpp \
		    $$BG2E_SRC/editor/help_options.cpp \
		    $$BG2E_SRC/editor/main.cpp\
		    $$BG2E_SRC/editor/main_window.cpp \
		    $$BG2E_SRC/editor/main_window_builder.cpp \
		    $$BG2E_SRC/editor/manipulator_gizmo_toolbar.cpp \
		    $$BG2E_SRC/editor/material_library.cpp \
		    $$BG2E_SRC/editor/material_settings_window.cpp \
		    $$BG2E_SRC/editor/multi_import_window.cpp \
			$$BG2E_SRC/editor/panels_toolbar.cpp \
		    $$BG2E_SRC/editor/poly_list_settings_window.cpp \
		    $$BG2E_SRC/editor/scene_tree_window.cpp \
			$$BG2E_SRC/editor/screenshot_window.cpp \
            $$BG2E_SRC/editor/selection_options.cpp \
            $$BG2E_SRC/editor/selection_window.cpp \
		    $$BG2E_SRC/editor/status_bar.cpp \		    
			$$BG2E_SRC/editor/ui_settings.cpp \
			$$BG2E_SRC/editor/window_controller.cpp

HEADERS  += $$BG2E_INCLUDE/bg2e/editor/app_delegate.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/animation_toolbar.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/axis.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/component_settings_window.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/creation_toolbar.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/draw_plist_selection.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/editor_toolbar_factory.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/edit_options.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/file_options.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/gizmo_view_options.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/grid.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/help_options.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/main_window.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/main_window_builder.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/manipulator_gizmo_toolbar.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/material_library.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/material_settings_window.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/multi_import_window.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/panels_toolbar.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/poly_list_settings_window.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/scene_tree_window.hpp \
			$$BG2E_INCLUDE/bg2e/editor/screenshot_window.hpp \
            $$BG2E_INCLUDE/bg2e/editor/selection_options.hpp \
            $$BG2E_INCLUDE/bg2e/editor/selection_window.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/status_bar.hpp \
			$$BG2E_INCLUDE/bg2e/editor/ui_settings.hpp \
		    $$BG2E_INCLUDE/bg2e/editor/window_controller.hpp


SOURCES +=  $$BG2E_SRC/ui/about_panel.cpp \
		    $$BG2E_SRC/ui/boolean_field.cpp \
		    $$BG2E_SRC/ui/button_field.cpp \
		    $$BG2E_SRC/ui/color_field.cpp \
		    $$BG2E_SRC/ui/combo_box_field.cpp \
		    $$BG2E_SRC/ui/edit_field.cpp \
		    $$BG2E_SRC/ui/file_field.cpp \
		    $$BG2E_SRC/ui/font_field.cpp \
		    $$BG2E_SRC/ui/menu_field.cpp \
		    $$BG2E_SRC/ui/numeric_text_widget.cpp \
		    $$BG2E_SRC/ui/scene_tree_item.cpp \
		    $$BG2E_SRC/ui/scene_tree_model.cpp \
		    $$BG2E_SRC/ui/slider_field.cpp \
		    $$BG2E_SRC/ui/text_field.cpp \
		    $$BG2E_SRC/ui/tool_window.cpp \
		    $$BG2E_SRC/ui/tree_view.cpp \
		    $$BG2E_SRC/ui/vector_field.cpp
		
HEADERS +=  $$BG2E_INCLUDE/bg2e/ui/about_panel.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/button_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/boolean_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/color_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/combo_box_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/edit_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/file_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/font_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/menu_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/numeric_text_widget.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/scene_tree_item.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/scene_tree_model.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/slider_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/text_field.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/tool_window.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/tree_view.hpp \
		    $$BG2E_INCLUDE/bg2e/ui/vector_field.hpp


# Components
SOURCES +=  $$BG2E_SRC/components/camera_ui.cpp \
		    $$BG2E_SRC/components/chain_ui.cpp \
		    $$BG2E_SRC/components/collider_ui.cpp \
		    $$BG2E_SRC/components/component_browser.cpp \
		    $$BG2E_SRC/components/component_field.cpp \
		    $$BG2E_SRC/components/drawable_ui.cpp \
		    $$BG2E_SRC/components/dynamics_ui.cpp \
		    $$BG2E_SRC/components/joint_ui.cpp \
		    $$BG2E_SRC/components/library_inspector.cpp \
		    $$BG2E_SRC/components/light_ui.cpp \
			$$BG2E_SRC/components/osg_component_ui.cpp \
		    $$BG2E_SRC/components/registry.cpp \
		    $$BG2E_SRC/components/rigid_body_ui.cpp \
		    $$BG2E_SRC/components/text_ui.cpp \
		    $$BG2E_SRC/components/transform_ui.cpp


HEADERS +=  $$BG2E_INCLUDE/bg2e/components/camera_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/chain_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/collider_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/component_browser.hpp \
		    $$BG2E_INCLUDE/bg2e/components/component_field.hpp \
		    $$BG2E_INCLUDE/bg2e/components/drawable_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/dynamics_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/joint_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/library_inspector.hpp \
		    $$BG2E_INCLUDE/bg2e/components/light_ui.hpp \
			$$BG2E_INCLUDE/bg2e/components/osg_component_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/registry.hpp \
		    $$BG2E_INCLUDE/bg2e/components/rigid_body_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/text_ui.hpp \
		    $$BG2E_INCLUDE/bg2e/components/transform_ui.hpp

# Plugins
SOURCES +=  $$BG2E_SRC/plugins/fbx_plugin.cpp

HEADERS +=  $$BG2E_INCLUDE/bg2e/plugins/fbx_plugin.hpp


win32:CONFIG(release, debug|release) {
    RC_FILE = resources/redpill.rc

    LIBS += -L$$VWGL_BIN -lvwgl
    DEFINES += NOMINMAX

    FBX_BIN = $$PWD/../fbx/lib/vs2015/x64/release
    LIBS += -L$$FBX_BIN -llibfbxsdk

    LIBS += -L$$BG2LIB_ROOT/build/bin -lbg2e

    QMAKE_POST_LINK += $$quote(cmd /c if not exist $$shell_path($$OUT_PWD/release) mkdir $$shell_path($$OUT_PWD/release)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$VWGL_BIN/vwgl.dll) $$shell_path($$OUT_PWD/release)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c if not exist $$shell_path($$OUT_PWD/release/shaders) mkdir $$shell_path($$OUT_PWD/release/shaders)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c if not exist $$shell_path($$OUT_PWD/release/resources) mkdir $$shell_path($$OUT_PWD/release/resources)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy $$shell_path($$VWGL_SHADERS_DIR/*) $$shell_path($$OUT_PWD/release/shaders)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$VWGL_RESOURCES/*) $$shell_path($$OUT_PWD/release/resources)$$escape_expand(\n\t))

    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$FBX_BIN/libfbxsdk.dll) $$shell_path($$OUT_PWD/release)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$BG2LIB_ROOT/build/bin/bg2e.dll) $$shell_path($$OUT_PWD/release)$$escape_expand(\n\t))
    #QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$VWGL_SHARED_RESOURCES/*) $$shell_path($$OUT_PWD/release/resources)$$escape_expand(\n\t))
}
else:win32:CONFIG(debug, debug|release) {
    RC_FILE = resources/redpill.rc

    LIBS += -L$$VWGL_BIN -lvwgld
    DEFINES += NOMINMAX

    FBX_BIN = $$PWD/../fbx/lib/vs2015/x64/debug
    LIBS += -L$$FBX_BIN -llibfbxsdk

    LIBS += -L$$BG2LIB_ROOT/build/bin -lbg2ed

    QMAKE_POST_LINK += $$quote(cmd /c if not exist $$shell_path($$OUT_PWD/debug) mkdir $$shell_path($$OUT_PWD/debug)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$VWGL_BIN/vwgld.dll) $$shell_path($$OUT_PWD/debug)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c if not exist $$shell_path($$OUT_PWD/debug/shaders) mkdir $$shell_path($$OUT_PWD/debug/shaders)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c if not exist $$shell_path($$OUT_PWD/debug/resources) mkdir $$shell_path($$OUT_PWD/debug/resources)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy $$shell_path($$VWGL_SHADERS_DIR/*) $$shell_path($$OUT_PWD/debug/shaders)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$VWGL_RESOURCES/*) $$shell_path($$OUT_PWD/debug/resources)$$escape_expand(\n\t))

    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$FBX_BIN/libfbxsdk.dll) $$shell_path($$OUT_PWD/debug)$$escape_expand(\n\t))
    QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$BG2LIB_ROOT/build/bin/bg2ed.dll) $$shell_path($$OUT_PWD/release)$$escape_expand(\n\t))

    #QMAKE_POST_LINK += $$quote(cmd /c copy /y $$shell_path($$VWGL_SHARED_RESOURCES/*) $$shell_path($$OUT_PWD/release/resources)$$escape_expand(\n\t))
}
else:macx {
    TARGET = "Composer"
    CONFIG += c++11
    ICON = resources/AppIcon.icns

    LIBS += -L$$PWD/../vwgl/cplusplus/build/osx/ -lvwgl

    FBX_BIN = $$PWD/../fbx/lib/osx/
    LIBS += -L$$FBX_BIN -lfbxsdk

    LIBS += -L$$BG2LIB_ROOT/bin/osx -lbg2e

    QT_TOOLS = $$[QT_INSTALL_PREFIX]/bin

    APP_LIBFILES.files = $$VWGL_ROOT/cplusplus/build/osx/libvwgl.dylib \
			$${FBX_BIN}libfbxsdk.dylib \
			$${BG2LIB_ROOT}/bin/osx/libbg2e.dylib

    APP_LIBFILES.path = Contents/MacOS
    QMAKE_BUNDLE_DATA += APP_LIBFILES

    APP_SHADERS.files = $$VWGL_ROOT/cplusplus/shaders
    APP_SHADERS.path = Contents/Resources
    QMAKE_BUNDLE_DATA += APP_SHADERS

    APP_RESOURCES.files = $$VWGL_ROOT/cplusplus/resources
    APP_RESOURCES.path = Contents/Resources
    QMAKE_BUNDLE_DATA += APP_RESOURCES

    QMAKE_POST_LINK += $$quote($$QT_TOOLS/macdeployqt \"$$OUT_PWD/$${TARGET}.app\"$$escape_expand(\n\t))
}

RESOURCES += \
    resources.qrc
