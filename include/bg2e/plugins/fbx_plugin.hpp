#ifndef _BG2E_PLUGINS_FBX_PLUGIN_HPP_
#define _BG2E_PLUGINS_FBX_PLUGIN_HPP_

#include <vwgl/readerplugin.hpp>
#include <fbxsdk.h>

namespace bg2e {
namespace plugins {

class FBXReader : public vwgl::ReadModelPlugin {
public:
	FBXReader();

	virtual bool acceptFileType(const std::string & path);
	virtual vwgl::scene::Drawable * loadModel(const std::string &path);

protected:
	virtual ~FBXReader();

	void importNode(FbxNode * node);
	void importMesh(FbxNode * node, FbxMesh * mesh);
	void getNormal(FbxMesh * mesh, int ctrlPoint, int vertexCounter, vwgl::Vector3 & normal);
	void getTexCoord(FbxMesh * mesh, int ctrlPoint, int uvIndex, int uvLayer, vwgl::Vector2 & texCoord);

	vwgl::scene::Drawable * _drawable;
	FbxScene * _fbxScene;
	float _scaleFactor;
};

}
}

#endif
