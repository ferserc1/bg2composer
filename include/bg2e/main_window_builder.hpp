#ifndef _BG2E_MAIN_WINDOW_BUILDER_HPP_
#define _BG2E_MAIN_WINDOW_BUILDER_HPP_

#include <QMainWindow>

namespace bg2e {

class MainWindowBuilder {
public:
	virtual void build(QMainWindow * wnd) = 0;
};

}

#endif
