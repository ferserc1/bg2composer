#ifndef _BG2E_EDITOR_MATERIAL_SETTINGS_WINDOW_HPP_
#define _BG2E_EDITOR_MATERIAL_SETTINGS_WINDOW_HPP_

#include <bg2e/settings.hpp>
#include <bg2e/ui/tool_window.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/vector_field.hpp>

#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/app/command_manager.hpp>
#include <vwgl/cmd/material_commands.hpp>

namespace bg2e {
namespace editor {

class MaterialSettingsWindow : public ui::ToolWindow,
	public vwgl::manipulation::ISelectionObserver,
	public vwgl::app::ICommandManagerObserver,
	public ISettingsChangedObserver
{
public:
	MaterialSettingsWindow();

	void selectionChanged(vwgl::manipulation::SelectionHandler &);
	void didExecuteCommand(vwgl::app::Command *);
	void didUndo(vwgl::app::Command *);
	void didRedo(vwgl::app::Command *);
	void onSettingsChanged(Settings *);

protected:
	void build();

	ui::BooleanField * _cullFace;

	ui::ColorField * _diffuseColor;
	ui::FloatSliderField * _alphaCutoff;
	ui::ColorField * _specularColor;
	ui::FloatSliderField * _shininess;

	ui::FileField * _shininessMask;
	ui::ComboBoxField * _shininessChannel;
	ui::BooleanField * _shininessInvert;

	ui::FloatSliderField * _lightEmission;
	ui::FileField * _lightEmissionMask;
	ui::ComboBoxField * _lightEmissionChannel;
	ui::BooleanField * _lightEmissionInvert;

	ui::FileField * _texture;
	ui::VectorField * _textureOffset;
	ui::VectorField * _textureScale;

	ui::FileField * _lightMap;

	ui::FileField * _normalMap;
	ui::VectorField * _normalMapOffset;
	ui::VectorField * _normalMapScale;

	ui::FloatSliderField * _reflectionAmount;
	ui::FileField * _reflectionMask;
	ui::ComboBoxField * _reflectionChannel;
	ui::BooleanField * _reflectionInvert;

	ui::BooleanField * _castShadows;
	ui::BooleanField * _receiveShadows;
	//ui::BooleanField * _receiveProjections;

	QLabel * _tessellationWarning;
	ui::FileField * _heightMap;
	ui::FloatSliderField * _heightFactor;
	ui::ComboBoxField * _heightMapUVSet;
	ui::VectorField * _tessellationDistances;
	ui::VectorField * _tessellationLevels;

	bool _blockUpdate;

	void applyModifier();
	void applyCullFace();
	void updateDisplacementNormals();;
};

}
}

#endif
