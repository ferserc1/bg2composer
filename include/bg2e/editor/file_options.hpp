#ifndef _BG2E_EDITOR_FILE_OPTIONS_HPP_
#define _BG2E_EDITOR_FILE_OPTIONS_HPP_

#include <bg2e/closure_action.hpp>

#include <bg2e/save_thread.hpp>

#include <bg2e/editor/multi_import_window.hpp>
#include <bg2e/editor/screenshot_window.hpp>

#include <vwgl/scene/drawable.hpp>

namespace bg2e {
namespace editor {

class FileOptions {
public:
	FileOptions();

	void init();

	void newScene();
	void openScene();
	void saveScene();
	void saveSceneAs();
	void importObject();
	void importMultiple();
	void exportObject();
	void closeScene();
	void exportSceneAs();
	void saveScreenshot();

protected:
	ClosureAction * _openScene;
	ClosureAction * _saveScene;
	ClosureAction * _saveSceneAs;
	ClosureAction * _newScene;
	ClosureAction * _closeScene;
	ClosureAction * _importObject;
	ClosureAction * _exportObject;
	ClosureAction * _importMultipleObjects;
	ClosureAction * _exportScene;
	ClosureAction * _saveScreenshot;
	ClosureAction * _quit;

	MultiImportWindow * _importWindow;
	ScreenshotWindow * _screenshotWindow;

	bool doExportObject(const std::string & path, vwgl::scene::Drawable * drw);
};

}
}
#endif
