
#ifndef _BG2E_EDITOR_GRID_HPP_
#define _BG2E_EDITOR_GRID_HPP_

#include <vwgl/scene/drawable.hpp>

namespace bg2e {
namespace editor {

class Grid : public vwgl::scene::Drawable {
public:
	Grid();

	inline bool isVisible() const { return _visible; }
	inline void setVisible(bool v) { _visible = v; }
	inline void show() { _visible = true; }
	inline void hide() { _visible = false; }

	inline int getSize() const { return _size; }
	inline float getGridSize() const { return _gridSize; }
	inline void setSize(int size) { _size = size; _updateGrid = true; }
	inline void setGridSize(float size) { _gridSize = size; _updateGrid = true; }

	virtual void update();
	virtual void draw();

	// Prevent save the componet to the scene file
	virtual bool serialize(vwgl::JsonSerializer &, bool) { return true; }
	virtual bool saveResourcesToPath(const std::string &) { return true; }
	virtual void deserialize(vwgl::JsonDeserializer &, const std::string &) {}

protected:
	virtual ~Grid();

	bool _visible;
	bool _updateGrid;
	int _size;
	float _gridSize;

};

}
}

#endif
