
#ifndef _BG2E_EDITOR_AXIS_HPP_
#define _BG2E_EDITOR_AXIS_HPP_

#include <vwgl/scene/drawable.hpp>

namespace bg2e {
namespace editor {

class Axis : public vwgl::scene::Drawable {
public:
	Axis();

	virtual void update();
	virtual void draw();

	// Prevent save the componet to the scene file
	virtual bool serialize(vwgl::JsonSerializer &, bool) { return true; }
	virtual bool saveResourcesToPath(const std::string &) { return true; }
	virtual void deserialize(vwgl::JsonDeserializer &, const std::string &) {}

	inline bool isVisible() const { return _visible; }
	inline void setVisible(bool v) { _visible = v; }
	inline void show() { _visible = true; }
	inline void hide() { _visible = false; }

protected:
	virtual ~Axis();

	bool _visible;
};

}
}

#endif
