
#ifndef _BG2E_EDITOR_CREATION_TOOLBAR_HPP_
#define _BG2E_EDITOR_CREATION_TOOLBAR_HPP_

#include <QToolBar>

#include <vwgl/scene/node.hpp>
#include <vwgl/app/command.hpp>

namespace bg2e {
namespace editor {


class CreationToolBar : public QToolBar {
	Q_OBJECT
public:
	CreationToolBar(const QString & title) :QToolBar(title,nullptr) { build(); }

	void build();

protected:
	vwgl::scene::Node * currentNode() const;
	inline vwgl::plain_ptr context() const;

	void createNode(vwgl::scene::Node * node);
};

}
}

#endif
