#ifndef _BG2E_EDITOR_GIZMO_VIEW_OPTIONS_HPP_
#define _BG2E_EDITOR_GIZMO_VIEW_OPTIONS_HPP_

#include <vwgl/manipulation/gizmo_manager.hpp>

#include <bg2e/closure_action.hpp>

namespace bg2e {
namespace editor {

class GizmoViewOptions {
public:
	void init();

	void showGizmo(vwgl::manipulation::GizmoManager::GizmoIcon icon);
	void hideGizmo(vwgl::manipulation::GizmoManager::GizmoIcon icon);

	void showAllGizmos();
	void hideAllGizmos();

protected:
	void updateGizmoVisibility();

	ClosureAction * _directionalLight;
	ClosureAction * _spotLight;
	ClosureAction * _pointLight;
	ClosureAction * _camera;
	ClosureAction * _transform;
	ClosureAction * _drawable;
	ClosureAction * _joint;

	ClosureAction * _collider;

	ClosureAction * _stats;
};

}
}

#endif // GIZMO_VIEW_OPTIONS_HPP

