
#ifndef _BG2E_EDITOR_UI_SETTINGS_HPP_
#define _BG2E_EDITOR_UI_SETTINGS_HPP_

#include <bg2e/settings.hpp>


namespace bg2e {
namespace editor {

class UISettings : public ISettingsExtension {
public:
	virtual void onLoadSettings(vwgl::Dictionary *);
	virtual void onSaveSettings(vwgl::Dictionary *);

	inline const QString & dialogPath() const { return _dialogPath; }
	inline void setDialogPath(const QString & dialogPath) { _dialogPath = dialogPath; }

protected:
	QString _dialogPath;
};
}
}

#endif
