#ifndef _BG2E_EDITOR_SELECTION_OPTIONS_HPP_
#define _BG2E_EDITOR_SELECTION_OPTIONS_HPP_

#include <bg2e/closure_action.hpp>

#include <vwgl/app/command_manager.hpp>
#include <vwgl/manipulation/selection_handler.hpp>
#include <bg2e/editor/selection_window.hpp>

namespace bg2e {
namespace editor {

class SelectionOptions : public vwgl::app::ICommandManagerObserver,
					public vwgl::manipulation::ISelectionObserver {
public:
	SelectionOptions();

	void init();

	// ICommandManagerObserver
	virtual void didExecuteCommand(vwgl::app::Command *) { checkEnabled(); }
	virtual void didUndo(vwgl::app::Command *) { checkEnabled(); }
	virtual void didRedo(vwgl::app::Command *) { checkEnabled(); }

	// ISelectionObserver
	virtual void selectionChanged(vwgl::manipulation::SelectionHandler &) {
		checkEnabled();
	}

protected:

	void checkEnabled();

	void selectDrawable(vwgl::scene::Drawable * drw);

	void centerSelection();

	SelectionWindow * _selectionWindow;
};

}
}
#endif


