
#ifndef _BG2E_EDITOR_MANIPULATOR_GIZMO_TOOLBAR_HPP_
#define _BG2E_EDITOR_MANIPULATOR_GIZMO_TOOLBAR_HPP_

#include <QToolBar>

namespace bg2e {
namespace editor {

class ManipulatorGizmoToolBar : public QToolBar {
	Q_OBJECT
public:
	ManipulatorGizmoToolBar(const QString & title) :QToolBar(title,nullptr) { build(); }

	void build();


};

}
}

#endif
