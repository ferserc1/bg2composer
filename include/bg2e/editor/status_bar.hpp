
#ifndef BG2E_EDITOR_STATUS_BAR_HPP
#define BG2E_EDITOR_STATUS_BAR_HPP

#include <QStatusBar>

#include <QCheckBox>
#include <QWidget>
#include <QLabel>
#include <QPushButton>


#include <vwgl/scene/node.hpp>
#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/manipulation/gizmo_manager.hpp>
#include <vwgl/texture.hpp>

namespace bg2e {
namespace editor {

class StatusBar : public QStatusBar,
				  public vwgl::manipulation::ISelectionObserver,
				  public vwgl::manipulation::IGizmoObserver
{
	Q_OBJECT
public:
	StatusBar();


	void updateNow();

	inline void setGridSize(float size) {
		_gridSizeText->setText(tr("Grid size") +
							   ": " +
							   QString::number(size) +
							   " m");
	}

	inline void setInlineText(const QString & text) { _inlineText->setText(text); }
	inline void setWarningText(const QString & text) { _warningText->setText(text); }

	virtual void selectionChanged(vwgl::manipulation::SelectionHandler &sender);

	void gizmoMoved(vwgl::manipulation::GizmoManager &);

protected:
	QCheckBox * _gridVisible;
	QCheckBox * _axisVisible;
	QLabel * _inlineText;
	QLabel * _selectionText;
	QLabel * _gridSizeText;
	QLabel * _warningText;
	QPushButton * _infoButton;

	QString _polygonsText;
	QString _texturesText;
	QString _polygonsWarning;
	QString _texturesWarning;

	QString _warningLog;

	QDialog * _warningWindow;

	void updateText();
	void updateWarning();
	bool checkPowerOfTwo(vwgl::Texture * tex);

public slots:
	void gridToggle(bool);
	void axisToggle(bool);
	void showMoreInfo();
	void closeMessage();
};

}
}

#endif
