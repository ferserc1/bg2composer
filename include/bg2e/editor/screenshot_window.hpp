
#ifndef _BG2E_EDITOR_SCREENSHOT_WINDOW_HPP_
#define _BG2E_EDITOR_SCREENSHOT_WINDOW_HPP_

#include <QDialog>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>

namespace bg2e {
namespace editor {

class ScreenshotWindow : public QDialog {
	Q_OBJECT
public:
	ScreenshotWindow();

protected:

	ui::VectorField * _size;

	QPushButton * _saveButton;
	QPushButton * _cancelButton;

public slots:
	void save();
	void cancel();
};

}
}

#endif
