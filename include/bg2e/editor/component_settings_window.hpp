#ifndef _BG2E_EDITOR_COMPONENT_SETTINGS_WINDOW_HPP_
#define _BG2E_EDITOR_COMPONENT_SETTINGS_WINDOW_HPP_

#include <bg2e/ui/tool_window.hpp>
#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/components/component_browser.hpp>

#include <vwgl/manipulation/selection_handler.hpp>

#include <vwgl/scene/node.hpp>

namespace bg2e {
namespace editor {

class ComponentSettingsWindow : public ui::ToolWindow, vwgl::manipulation::ISelectionObserver {
	Q_OBJECT
public:
	ComponentSettingsWindow();

	void selectionChanged(vwgl::manipulation::SelectionHandler &sender);

protected:
	void build();

	void showComponentSettings(vwgl::scene::Node * node);

	ui::TextField * _nodeName;
	ui::BooleanField * _enabled;

	components::ComponentBrowser * _browser;

public slots:
	void addButtonPressed();
};

}
}

#endif
