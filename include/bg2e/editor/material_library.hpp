#ifndef _BG2E_EDITOR_MATERIAL_LIBRARY_HPP_
#define _BG2E_EDITOR_MATERIAL_LIBRARY_HPP_

#include <vwgl/library/library.hpp>

#include <bg2e/components/library_inspector.hpp>
#include <bg2e/scene.hpp>

namespace bg2e {
namespace editor {

class MaterialLibrary : public ISceneObserver {
public:
	MaterialLibrary();
	
	// This function must be called after loading application settings
	void init();

	// Opens a library, and stores the new path it in application settings
	bool openLibrary(const std::string & path);

	inline vwgl::library::Group * getCurrentGroup() const { return _libraryInspector->getCurrentNode(); }
	inline components::LibraryInspectorComponent * getLibraryInspector() { return _libraryInspector.getPtr(); }

	vwgl::library::Material * find(const std::string & searchString);

	// ISceneObserver
	virtual void sceneDidLoad(vwgl::scene::Node *);

	void applyMaterial(vwgl::library::Material * material);

	inline vwgl::library::Group * library() { return _library.getPtr(); }

protected:
	std::string _libraryPath;

	vwgl::ptr<vwgl::library::Group> _library;
	vwgl::ptr<components::LibraryInspectorComponent> _libraryInspector;
};

}
}

#endif
