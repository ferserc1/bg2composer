#ifndef _BG2E_EDITOR_MAIN_WINDOW_BUILDER_HPP_
#define _BG2E_EDITOR_MAIN_WINDOW_BUILDER_HPP_

#include <bg2e/main_window_builder.hpp>
#include <bg2e/editor/material_settings_window.hpp>
#include <bg2e/editor/poly_list_settings_window.hpp>
#include <bg2e/editor/component_settings_window.hpp>
#include <bg2e/editor/scene_tree_window.hpp>

namespace bg2e {
namespace editor {

class MainWindowBuilder : public bg2e::MainWindowBuilder {
public:
	virtual void build(QMainWindow * wnd);

	inline ComponentSettingsWindow * componentSettings() { return _componentSettings; }
	inline MaterialSettingsWindow * materialSettings() { return _materialSettings; }
	inline PolyListSettingsWindow * polyListSettings() { return _polyListSettings; }
	inline SceneTreeWindow * sceneTreeWindow() { return _sceneTreeWindow; }

protected:
	ComponentSettingsWindow * _componentSettings;
	MaterialSettingsWindow * _materialSettings;
	PolyListSettingsWindow * _polyListSettings;
	SceneTreeWindow * _sceneTreeWindow;
};

}
}

#endif
