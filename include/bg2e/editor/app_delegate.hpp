
#ifndef _BG2E_EDITOR_APP_DELEGATE_HPP_
#define _BG2E_EDITOR_APP_DELEGATE_HPP_

#include <QGLContext>
#include <QMainWindow>

#include <bg2e/app_delegate.hpp>
#include <bg2e/editor/axis.hpp>
#include <bg2e/render_view.hpp>
#include <bg2e/scene.hpp>
#include <bg2e/render_view.hpp>
#include <bg2e/menu_factory.hpp>
#include <bg2e/tool_window_manager.hpp>
#include <bg2e/settings.hpp>
#include <bg2e/editor/gizmo_view_options.hpp>
#include <bg2e/editor/edit_options.hpp>
#include <bg2e/editor/file_options.hpp>
#include <bg2e/editor/grid.hpp>
#include <bg2e/editor/selection_options.hpp>
#include <bg2e/editor/status_bar.hpp>
#include <bg2e/editor/material_library.hpp>
#include <bg2e/editor/draw_plist_selection.hpp>
#include <bg2e/editor/ui_settings.hpp>

#include <bg2e/scene/dynamics.hpp>

#ifdef SIM3D
#include <bg2e/editor/sim3d_help_options.hpp>
#else
#include <bg2e/editor/help_options.hpp>
#endif

#include <vwgl/app/input_mediator.hpp>
#include <vwgl/scene/renderer.hpp>
#include <vwgl/manipulation/stats.hpp>

namespace bg2e {
namespace editor {

using namespace vwgl;

class MainWindow;
class AppDelegate : public QObject,
					public ISceneObserver,
					public bg2e::AppDelegateBase,
					public bg2e::ISettingsChangedObserver {
	Q_OBJECT
public:
	enum RenderQuality {
		kQualityLow,
		kQualityHigh
	};

	static AppDelegate * get() { return &s_singleton; }

	void configureMainWindow(MainWindow * wnd);
	void saveWindowSettings();
	void restoreWindowSettings();

	void initGL(QGLContext * context);

	void destroyGL();

	// Application mediator functions:
	inline Scene & scene() { return _scene; }
	inline Settings & settings() { return _settings; }
	inline UISettings & uiSettings() { return _uiSettings; }
	inline vwgl::scene::Renderer & renderer() { return *_renderer; }
	inline vwgl::scene::Renderer & highQualityRenderer() { return *_highQualityRenderer.getPtr(); }
	inline vwgl::scene::Renderer & lowQualityRenderer() { return *_lowQualityRenderer.getPtr(); }
	inline MenuFactory & menuFactory() { return *_menuFactory; }
	inline ToolWindowManager & toolWindowManager() { return *_toolWindowManager; }
	inline app::InputMediator & inputMediator() { return _inputMediator; }
	inline app::CommandManager & commandManager() { return _inputMediator.commandManager(); }
	inline GizmoViewOptions & gizmoViewOptions() { return _gizmoViewOptions; }
	inline EditOptions & editOptions() { return _editOptions; }
	inline SelectionOptions & selectionOptions() { return _selectionOptions; }
	inline StatusBar & statusBar() { return *_statusBar; }
	inline QMainWindow * mainWindow() { return _mainWindow; }
	inline MaterialLibrary & materialLibrary() { return _materialLibrary; }
	inline DrawPlistSelection & drawPlistSelection() { return _drawPlistSelection; }
	inline bg2e::scene::DynamicsVisitor & dynamics() { return _dynamics; }

	inline Grid * grid() { return _grid.getPtr(); }
	inline Axis * axis() { return _axis.getPtr(); }
	inline vwgl::manipulation::Stats * stats() { return _stats.getPtr(); }

	inline void updateRenderView() {
		if (_renderView) {
			_mainContext->makeCurrent();
			_renderView->updateRenderView();
		}
	}

	inline void updateRenderView(int numFrames) {
		if (_renderView) {
			_mainContext->makeCurrent();
			_renderView->updateRenderView(numFrames);
		}
	}

	void setRenderPath(RenderQuality q);
	inline RenderQuality getRenderPath() const {
		return (_renderer==_highQualityRenderer.getPtr()) ? kQualityHigh:kQualityLow;
	}

	// Scene observer
	bool sceneWillUnload(vwgl::scene::Node *);
	void sceneDidLoad(vwgl::scene::Node * sceneRoot);
	void sceneDidUnload();

	// Settings observer
	void onSettingsChanged(Settings *) {
		_mainContext->makeCurrent();
		vwgl::Size2Di size(vwgl::State::get()->getViewport().width(),
						   vwgl::State::get()->getViewport().height());
		_renderView->resizeGL(size.width(), size.height());
		updateRenderView();
	}

protected:
	AppDelegate();
	~AppDelegate();

	static AppDelegate s_singleton;

	Scene _scene;
	Settings _settings;
	UISettings _uiSettings;
	MenuFactory * _menuFactory;
	ToolWindowManager * _toolWindowManager;
	app::InputMediator _inputMediator;
	vwgl::scene::Renderer * _renderer;
	ptr<vwgl::scene::Renderer> _highQualityRenderer;
	ptr<vwgl::scene::Renderer> _lowQualityRenderer;
	GizmoViewOptions _gizmoViewOptions;
	MaterialLibrary _materialLibrary;
	DrawPlistSelection _drawPlistSelection;
	bg2e::scene::DynamicsVisitor _dynamics;

	FileOptions _fileOptions;
	EditOptions _editOptions;
	SelectionOptions _selectionOptions;
	HelpOptions _helpOptions;

	QMainWindow * _mainWindow;
	StatusBar * _statusBar;

	ClosureAction * _hqRendererAction;

	vwgl::ptr<Grid> _grid;
	vwgl::ptr<Axis> _axis;
	vwgl::ptr<vwgl::manipulation::Stats> _stats;

	void initRenderer();
	RenderView * createRenderView();

public slots:
	void prepareToQuit();
};

}
}

#endif
