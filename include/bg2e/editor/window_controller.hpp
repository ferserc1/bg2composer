#ifndef _BG2E_EDITOR_WINDOW_CONTROLLER_HPP_
#define _BG2E_EDITOR_WINDOW_CONTROLLER_HPP_

#include <vwgl/scene/renderer.hpp>

#include <bg2e/window_controller.hpp>

namespace bg2e {
namespace editor {
	
class WindowController : public bg2e::WindowController {
public:
	WindowController();
	
	virtual void initGL();
	virtual void reshape(int w, int h);
	virtual void draw();
	virtual void frame(float delta);
	virtual void destroy();
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt);
	virtual void keyDown(const vwgl::app::KeyboardEvent & evt);
	virtual void mouseDown(const vwgl::app::MouseEvent & evt);
	virtual void mouseDrag(const vwgl::app::MouseEvent & evt);
	virtual void mouseMove(const vwgl::app::MouseEvent & evt);
	virtual void mouseUp(const vwgl::app::MouseEvent & evt);
	virtual void mouseWheel(const vwgl::app::MouseEvent & evt);
		
protected:
	virtual ~WindowController();


};

}
}

#endif
