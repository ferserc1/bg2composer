#ifndef _BG2E_EDITOR_HELP_OPTIONS_HPP_
#define _BG2E_EDITOR_HELP_OPTIONS_HPP_

#include <bg2e/closure_action.hpp>

#include <vwgl/app/command_manager.hpp>
#include <vwgl/manipulation/selection_handler.hpp>

namespace bg2e {
namespace editor {

class HelpOptions {
public:
	HelpOptions();

	void init();

protected:
	ClosureAction * _about;
};

}
}

#endif
