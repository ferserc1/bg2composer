
#ifndef _BG2E_EDITOR_PANELS_TOOLBAR_HPP_
#define _BG2E_EDITOR_PANELS_TOOLBAR_HPP_

#include <QToolBar>

namespace bg2e {
namespace editor {

class PanelsToolBar : public QToolBar {
	Q_OBJECT
public:
	PanelsToolBar(const QString & title) :QToolBar(title,nullptr) { build(); }

	void build();


};

}
}

#endif
