#ifndef _BG2E_EDITOR_DRAW_PLIST_SELECTION_HPP_
#define _BG2E_EDITOR_DRAW_PLIST_SELECTION_HPP_

#include <vwgl/scene/node.hpp>
#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/material.hpp>
#include <vwgl/polylist.hpp>

#include <vector>

namespace bg2e {
namespace editor {

class DrawPlistSelectionMaterial : public vwgl::Material {
public:
	DrawPlistSelectionMaterial();

	virtual void initShader();

	inline void setColor(const vwgl::Color & c) { _markColor = c; }

protected:
	virtual ~DrawPlistSelectionMaterial();

	virtual void setupUniforms();

	vwgl::Color _markColor;

	static std::string s_vshader;
	static std::string s_fshader;
	static std::string s_vshader_gl3;
	static std::string s_fshader_gl3;
};

class DrawPlistSelection;
class DrawPlistSelectionVisitor : public vwgl::scene::NodeVisitor {
public:
	DrawPlistSelectionVisitor(DrawPlistSelection * mgr);

	virtual void visit(vwgl::scene::Node*);
	virtual void didVisit(vwgl::scene::Node*);

protected:
	DrawPlistSelectionMaterial * material();

	DrawPlistSelection * _manager;
	vwgl::ptr<DrawPlistSelectionMaterial> _mat;
};

class DrawPlistSelection {
public:
	DrawPlistSelection();
	~DrawPlistSelection();

	void draw();

	inline void setSceneRoot(vwgl::scene::Node * n) { _sceneRoot = n; }

	inline void clearSelection() { _selectedPlist.clear(); }
	inline void addSelection(vwgl::PolyList * plist) { _selectedPlist.push_back(plist); }
	inline bool isSelected(vwgl::PolyList * plist) {
		return std::find(_selectedPlist.begin(), _selectedPlist.end(), plist)!=_selectedPlist.end();
	}
	inline size_t selectionCount() const { return _selectedPlist.size(); }

protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	DrawPlistSelectionVisitor * _visitor;

	std::vector<vwgl::PolyList * > _selectedPlist;
	
};

}
}

#endif
