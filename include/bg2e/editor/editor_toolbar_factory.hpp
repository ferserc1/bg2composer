#ifndef _BG2E_EDITOR_EDITOR_TOOLBAR_FACTORY_HPP_
#define _BG2E_EDITOR_EDITOR_TOOLBAR_FACTORY_HPP_

#include <bg2e/toolbar_factory.hpp>

namespace bg2e {
namespace editor {

class EditorToolBarFactory : public ToolBarFactory {
public:
	void build(QMainWindow * window);
};

}
}

#endif
