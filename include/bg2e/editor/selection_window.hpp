
#ifndef _BG2E_EDITOR_SELECTION_WINDOW_HPP_
#define _BG2E_EDITOR_SELECTION_WINDOW_HPP_

#include <QDialog>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/boolean_field.hpp>

#include <vwgl/scene/node.hpp>
#include <vwgl/scene/drawable.hpp>

namespace bg2e {
namespace editor {


class SelectionWindow : public QDialog {
	Q_OBJECT
public:
	SelectionWindow();

protected:
	void selectPattern();
	void addToSelection(vwgl::scene::Node *, vwgl::scene::Drawable*);

	ui::TextField * _patternField;
	ui::BooleanField * _addToSelection;


};

}
}


#endif
