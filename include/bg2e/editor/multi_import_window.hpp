
#ifndef _BG2E_EDITOR_MULTI_IMPORT_WINDOW_HPP_
#define _BG2E_EDITOR_MULTI_IMPORT_WINDOW_HPP_

#include <QDialog>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>

namespace bg2e {
namespace editor {

class MultiImportWindow : public QDialog {
	Q_OBJECT
public:
	MultiImportWindow();

protected:
	ui::FileField * _folder;
	ui::TextField * _prefix;
	ui::TextField * _postfix;

	QPushButton * _importButton;
	QPushButton * _cancelButton;

public slots:
	void import();
	void cancel();
};

}
}

#endif
