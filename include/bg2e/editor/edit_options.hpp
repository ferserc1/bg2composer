#ifndef _BG2E_EDITOR_EDIT_OPTIONS_HPP_
#define _BG2E_EDITOR_EDIT_OPTIONS_HPP_

#include <bg2e/closure_action.hpp>

#include <vwgl/app/command_manager.hpp>
#include <vwgl/manipulation/selection_handler.hpp>

namespace bg2e {
namespace editor {

class EditOptions : public vwgl::app::ICommandManagerObserver,
					public vwgl::manipulation::ISelectionObserver {
public:
	EditOptions();

	void init();

	// ICommandManagerObserver
	virtual void didExecuteCommand(vwgl::app::Command *) { checkEnabled(); }
	virtual void didUndo(vwgl::app::Command *) { checkEnabled(); }
	virtual void didRedo(vwgl::app::Command *) { checkEnabled(); }

	// ISelectionObserver
	virtual void selectionChanged(vwgl::manipulation::SelectionHandler &) {
		checkEnabled();
	}

protected:
	ClosureAction * _undo;
	ClosureAction * _redo;
	ClosureAction * _delete;

	void checkEnabled();
};

}
}
#endif


