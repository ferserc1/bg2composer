#ifndef _BG2E_EDITOR_POLY_LIST_SETTINGS_WINDOW_HPP_
#define _BG2E_EDITOR_POLY_LIST_SETTINGS_WINDOW_HPP_

#include <QListWidget>

#include <bg2e/ui/tool_window.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/vector_field.hpp>

#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/app/command_manager.hpp>
#include <vwgl/cmd/polylist_commands.hpp>
#include <vwgl/scene/node.hpp>

namespace bg2e {
namespace editor {

class PolyListSettingsWindow : public ui::ToolWindow,
	public vwgl::manipulation::ISelectionObserver,
	public vwgl::app::ICommandManagerObserver
{
	Q_OBJECT

public:
	PolyListSettingsWindow();

	void selectionChanged(vwgl::manipulation::SelectionHandler &);
	void didExecuteCommand(vwgl::app::Command *);
	void didUndo(vwgl::app::Command *);
	void didRedo(vwgl::app::Command *);

protected:
	void build();

	QListWidget * _list;
	static int s_plistIndex;

	ui::TextField * _name;
	ui::TextField * _groupName;

	ui::BooleanField * _visible;

	ui::ComboBoxField * _swapUVfrom;
	ui::ComboBoxField * _swapUVTo;

	ui::MenuField * _actions;

	ui::TextField * _findName;
	ui::TextField * _replaceName;

	bool _preventUpdate;

	void deletePlist(vwgl::manipulation::SelectionHandler &);
	void detachPlist(vwgl::manipulation::SelectionHandler &);
	void duplicatePlist(vwgl::manipulation::SelectionHandler &);

	void blockFieldSignals(bool block);

public slots:
	void plistSelectionChanged();
	void executeCommand(vwgl::cmd::PolyListCommand * cmd);

	void eachSelectedPList(std::function<void(vwgl::PolyList * plist)> cb);
};

}
}

#endif
