#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <bg2e/editor/app_delegate.hpp>

#include <bg2e/render_view.hpp>

#include <bg2e/editor/editor_toolbar_factory.hpp>

namespace bg2e {
namespace editor {

class MainWindow : public QMainWindow
{
	friend class AppDelegate;

	Q_OBJECT

	void closeEvent(QCloseEvent *);

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

protected:
	RenderView * _renderView;
	EditorToolBarFactory _toolbarFactory;
};

}
}

#endif // MAIN_WINDOW_H
