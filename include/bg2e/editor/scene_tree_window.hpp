#ifndef _BG2E_EDITOR_SCENE_TREE_WINDOW_HPP_
#define _BG2E_EDITOR_SCENE_TREE_WINDOW_HPP_

#include <bg2e/scene.hpp>

#include <bg2e/ui/tool_window.hpp>
#include <bg2e/ui/tree_view.hpp>

#include <bg2e/ui/scene_tree_item.hpp>
#include <bg2e/ui/scene_tree_model.hpp>

#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/app/command_manager.hpp>
#include <vwgl/cmd/material_commands.hpp>
#include <vwgl/scene/node.hpp>

namespace bg2e {
namespace editor {


class SceneTreeWindow : public ui::ToolWindow,
	public vwgl::manipulation::ISelectionObserver,
	public vwgl::app::ICommandManagerObserver,
	public ISceneObserver
{
	Q_OBJECT
public:
	SceneTreeWindow();

	void selectionChanged(vwgl::manipulation::SelectionHandler &);
	void didExecuteCommand(vwgl::app::Command *);
	void didUndo(vwgl::app::Command *);
	void didRedo(vwgl::app::Command *);

	// ISceneObserver
	virtual void sceneDidLoad(vwgl::scene::Node *);
	virtual void sceneDidUnload();

public slots:
	void expandNode(const QModelIndex & index);
	void collapseNode(const QModelIndex & index);
	void nodeClicked(const QModelIndex & index);
	void contextClick(const QPoint & point);

	void moveNode(const QModelIndex & source, const QModelIndex & destination);

protected:
	void build();

	void reload();

	void restoreExpandedState();


	ui::TreeView * _treeView;
	vwgl::ptr<vwgl::scene::Node> _dragSource;
	ui::SceneTreeModel * _model;

	ui::SceneTreeItem * treeItem(const QModelIndex & index);

	void restoreChildren(vwgl::scene::Node * root);

	void handleSelection(const QModelIndex & index, bool additive);
};

}
}

#endif
