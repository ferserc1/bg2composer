
#ifndef _BG2E_EDITOR_ANIMATION_TOOLBAR_HPP_
#define _BG2E_EDITOR_ANIMATION_TOOLBAR_HPP_

#include <QToolBar>

#include <vwgl/scene/node.hpp>
#include <vwgl/app/command.hpp>

namespace bg2e {
namespace editor {


class AnimationToolBar : public QToolBar {
	Q_OBJECT
public:
	AnimationToolBar(const QString & title) :QToolBar(title,nullptr) { build(); }

	void build();

protected:
};

}
}

#endif
