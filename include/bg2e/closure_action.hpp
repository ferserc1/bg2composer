
#ifndef _BG2E_CLOSURE_ACTION_HPP_
#define _BG2E_CLOSURE_ACTION_HPP_

#include <QAction>
#include <QKeySequence>

#include <functional>

namespace bg2e {

class ClosureAction : public QAction {
	Q_OBJECT
public:
	ClosureAction(std::function<void()> closure);
	ClosureAction(const QString & text, std::function<void()> closure);
	ClosureAction(const QIcon & icon, std::function<void()> closure);
	ClosureAction(const QIcon & icon, const QString & text, std::function<void()> closure);
	ClosureAction(const QString & text, const QKeySequence & seq, std::function<void()> closure);
	ClosureAction(const QIcon & icon, const QKeySequence & seq, std::function<void()> closure);
	ClosureAction(const QIcon & icon, const QString & text, const QKeySequence & seq, std::function<void()> closure);

public slots:
	void actionTriggered();

protected:
	std::function<void ()> _closure;
};


}

#endif
