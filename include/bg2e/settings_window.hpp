
#ifndef _BG2E_EDITOR_SETTINGS_WINDOW_HPP_
#define _BG2E_EDITOR_SETTINGS_WINDOW_HPP_

#include <QDialog>

#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>

namespace bg2e {

class Settings;
class SettingsWindow : public QDialog {
	Q_OBJECT
public:
	SettingsWindow(Settings * settings);

protected:
	void buildSSAOSettings();
	void updateSSAO();

	void updateUISettings();
	void buildShadowSettings();
	void updateShadowSettings();
	void buildOtherSettings();
	void updateOtherSettings();

	void updateGL();

	Settings * _settings;

    ui::ComboBoxField * _api;

	ui::BooleanField * _ssaoEnabled;
	ui::SliderField * _ssaoKernelSize;
	ui::FloatSliderField * _ssaoSampleRadius;
	ui::ColorField *  _ssaoColor;
	ui::SliderField * _ssaoBlurIterations;
	ui::FloatSliderField * _ssaoMaxDistance;

	ui::ComboBoxField * _reflectionQuality;

	ui::ComboBoxField * _shadowQuality;
	ui::ComboBoxField * _shadowType;
	ui::SliderField * _shadowBlurIterations;

	ui::ColorField * _canvasColor;

	ui::BooleanField * _antialiasing;

	ui::BooleanField * _tessellation;
};

}

#endif
