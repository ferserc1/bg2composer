#ifndef _BG2E_COMPONENTS_DRAWABLE_UI_HPP_
#define _BG2E_COMPONENTS_DRAWABLE_UI_HPP_

#include <bg2e/components/component_field.hpp>

#include <vwgl/scene/drawable.hpp>
#include <vwgl/cmd/drawable_commands.hpp>

namespace bg2e {
namespace components {

class DrawableUI : public ComponentField {
	Q_OBJECT
public:
	DrawableUI();

	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);
	void didUndo(vwgl::app::Command * cmd);
	void didRedo(vwgl::app::Command * cmd);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:

	ui::TextField * _name;

	void switchUVs(vwgl::manipulation::SelectionHandler & selection);
	void flipNormals(vwgl::manipulation::SelectionHandler & selection);
	void flipFaces(vwgl::manipulation::SelectionHandler & selection);

	void freezeTransform(vwgl::manipulation::SelectionHandler & selection);
	void moveToCenter(vwgl::manipulation::SelectionHandler & selection);
	void putOnFloor(vwgl::manipulation::SelectionHandler & selection);

	void exportPrefab(vwgl::manipulation::SelectionHandler & selection);

	void importObject(vwgl::manipulation::SelectionHandler & selection);

	void executeDrawableCommand(vwgl::manipulation::SelectionHandler & selection, vwgl::cmd::DrawableCommand * cmd);

	void mergeSelection(vwgl::manipulation::SelectionHandler & selection);

public slots:
	void nameChanged(ui::TextField *);
};
	
}	
}

#endif
