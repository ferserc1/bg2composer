#ifndef _BG2E_COMPONENTS_OSG_UI_HPP_
#define _BG2E_COMPONENTS_OSG_UI_HPP_

#include <bg2e/components/component_field.hpp>

namespace bg2e {
namespace components {

class OsgUI : public ComponentField {
	Q_OBJECT
public:
	OsgUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:
    ui::BooleanField * _ignoreOnExport;
};
	
}	
}

#endif
