#ifndef _BG2E_COMPONENTS_REGISTRY_HPP_
#define _BG2E_COMPONENTS_REGISTRY_HPP_

#include <vwgl/scene/component.hpp>

#include <QWidget>

#include <unordered_map>
#include <functional>

namespace bg2e {
namespace components {

class ComponentUIFactory;
class Registry {
	friend class ComponentUIFactory;
public:
	static Registry * get() {
		if (s_registry==nullptr) {
			s_registry = new Registry();
		}
		return s_registry;
	}

	QWidget * getWidgetForComponent(vwgl::scene::Component * comp) {
		return _widgetMap[typeid(*comp).hash_code()];
	}

	void registerWidget(size_t componentTypeId, QWidget * widget) {
		_widgetMap[componentTypeId] = widget;
	}

	void registerFactories();

	inline std::vector<ComponentUIFactory*> componentUIFactoryVector() { return _componentUIFactoryVector; }

	inline void eachFactory(std::function<void(ComponentUIFactory*)> cb) {
		std::vector<ComponentUIFactory*>::iterator it;
		for (it=_componentUIFactoryVector.begin(); it!=_componentUIFactoryVector.end(); ++it) {
			cb(*it);
		}
	}

	inline ComponentUIFactory * factoryAtIndex(int i) {
		if (_componentUIFactoryVector.size()>static_cast<size_t>(i) && i>=0) {
			return _componentUIFactoryVector[i];
		}
		return nullptr;
	}

protected:
	Registry();
	~Registry();

	static Registry * s_registry;

	void registerFactory(ComponentUIFactory*);

	std::unordered_map<size_t,QWidget*> _widgetMap;
	std::vector<ComponentUIFactory*> _componentUIFactoryVector;
};

class ComponentUIFactory {
	friend class Registry;
public:
	ComponentUIFactory(const std::string & name,
					   std::function<void(Registry*)> factory,
					   std::function<vwgl::scene::Component*()> componentFactory,
					   std::function<void(vwgl::scene::Node*,vwgl::scene::Component*)> configureCallback = nullptr)
		:_name(name), _factory(factory), _componentFactory(componentFactory), _configureCallback(configureCallback) {
		Registry::get()->registerFactory(this);
	}

	inline const std::string & name() const { return _name; }
	inline vwgl::scene::Component * create() { return _componentFactory(); }
	inline void configure(vwgl::scene::Node * node, vwgl::scene::Component * comp) {
		if (_configureCallback) {
			_configureCallback(node,comp);
		}
	}

	void registerWidget() {
		_factory(Registry::get());
	}

protected:
	std::string _name;
	std::function<void(Registry*)> _factory;
	std::function<vwgl::scene::Component*()> _componentFactory;
	std::function<void(vwgl::scene::Node*,vwgl::scene::Component*)> _configureCallback;
};

}
}

#endif
