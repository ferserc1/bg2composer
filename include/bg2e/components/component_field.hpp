#ifndef _BG2E_COMPONENTS_COMPONENT_FIELD_HPP_
#define _BG2E_COMPONENTS_COMPONENT_FIELD_HPP_


#include <bg2e/components/registry.hpp>

#include <bg2e/ui/edit_field.hpp>

#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/app/command_manager.hpp>

#include <vwgl/scene/node.hpp>

#include <bg2e/ui/boolean_field.hpp>
#include <bg2e/ui/button_field.hpp>
#include <bg2e/ui/color_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/menu_field.hpp>
#include <bg2e/ui/slider_field.hpp>
#include <bg2e/ui/text_field.hpp>
#include <bg2e/ui/vector_field.hpp>
#include <bg2e/ui/font_field.hpp>

namespace bg2e {
namespace components {

class ComponentField : public QWidget,
		public vwgl::manipulation::ISelectionObserver,
		public vwgl::app::ICommandManagerObserver
{
	Q_OBJECT
public:
	ComponentField(const QString & title);

	void addField(ui::EditField * field);

	void selectionChanged(vwgl::manipulation::SelectionHandler &) {}

	virtual void onRemoveComponent(vwgl::manipulation::SelectionHandler &) = 0;

protected:

	vwgl::plain_ptr context();

public slots:
	void removeButtonPressed();
};
	
}	
}

#endif
