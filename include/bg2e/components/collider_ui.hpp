#ifndef _BG2E_COMPONENTS_COLLIDER_UI_HPP_
#define _BG2E_COMPONENTS_COLLIDER_UI_HPP_

#include <bg2e/components/component_field.hpp>

namespace bg2e {
namespace components {

class ColliderUI : public ComponentField {
	Q_OBJECT
public:
	ColliderUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:
	ui::ComboBoxField * _type;
	ui::VectorField * _boxSize;
	ui::TextField * _sphereRadius;

	void changeColliderType();
};
	
}	
}

#endif
