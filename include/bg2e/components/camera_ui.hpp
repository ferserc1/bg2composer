#ifndef _BG2E_COMPONENTS_CAMERA_UI_HPP_
#define _BG2E_COMPONENTS_CAMERA_UI_HPP_

#include <bg2e/components/component_field.hpp>

namespace bg2e {
namespace components {

class CameraUI : public ComponentField {
	Q_OBJECT
public:
	CameraUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:
};
	
}	
}

#endif
