#ifndef _BG2E_COMPONENTS_RIGID_BODY_UI_HPP_
#define _BG2E_COMPONENTS_RIGID_BODY_UI_HPP_

#include <bg2e/components/component_field.hpp>

namespace bg2e {
namespace components {

class RigidBodyUI : public ComponentField {
	Q_OBJECT
public:
	RigidBodyUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:
	ui::TextField * _mass;
};
	
}	
}

#endif
