#ifndef _BG2E_COMPONENTS_JOINT_UI_HPP_
#define _BG2E_COMPONENTS_JOINT_UI_HPP_

#include <bg2e/components/component_field.hpp>

namespace bg2e {
namespace components {

class JointUI : public ComponentField {
	Q_OBJECT
public:
	JointUI(const QString & title);

	virtual void updateData() = 0;

protected:

	void buildUI();

	ui::VectorField * _offset;
	ui::VectorField * _rotation;
};
	
class InputJointUI : public JointUI {
	Q_OBJECT
public:
	InputJointUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);
	
	virtual void updateData();

protected:
};

class OutputJointUI : public JointUI {
	Q_OBJECT
public:
	OutputJointUI();

	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);
	
	virtual void updateData();

protected:
	ui::ButtonField * _attachButton;
	vwgl::ptr<vwgl::scene::Node> _attachNode;
};

class AttachInputJointCommand : public vwgl::app::Command {
public:
	AttachInputJointCommand(vwgl::scene::Node * outNode, vwgl::scene::Node * inNode) :_outNode(outNode), _inNode(inNode) {}

	virtual void doCommand();
	virtual void undoCommand();

protected:
	~AttachInputJointCommand() {}

	vwgl::ptr<vwgl::scene::Node> _outNode;
	vwgl::ptr<vwgl::scene::Node> _inNode;
	vwgl::ptr<vwgl::scene::Node> _outParent;
	vwgl::ptr<vwgl::scene::Node> _inParent;
	vwgl::ptr<vwgl::scene::Node> _chain;
	vwgl::ptr<vwgl::TransformStrategy> _outNodeTransform;
};
	
}	
}

#endif
