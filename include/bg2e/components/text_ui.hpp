#ifndef _BG2E_COMPONENTS_CAMERA_UI_HPP_
#define _BG2E_COMPONENTS_CAMERA_UI_HPP_

#include <bg2e/components/component_field.hpp>

#include <vwgl/cmd/text_commands.hpp>

namespace bg2e {
namespace components {

class TextUI : public ComponentField {
	Q_OBJECT
public:
	TextUI();

	void buildUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:
	vwgl::plain_ptr context();

	ui::TextField * _text;
	ui::FontField * _font;
	ui::ColorField * _color;
	ui::ColorField * _specular;
	ui::FloatSliderField * _shininess;
	ui::FloatSliderField * _lightEmission;
	ui::BooleanField * _castShadows;
	ui::BooleanField * _receiveShadows;

	void applyProperties();
	void execute(vwgl::cmd::TextCommand * cmd);

	void blockFields(bool b);
};
	
}	
}

#endif
