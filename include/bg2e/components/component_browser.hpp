#ifndef _BG2E_COMPONENT_BROWSER_HPP_
#define _BG2E_COMPONENT_BROWSER_HPP_

#include <QDialog>
#include <QListWidget>
#include <QPushButton>

#include <bg2e/components/component_field.hpp>

#include <functional>

namespace bg2e {
namespace components {

class ComponentBrowser : public QDialog {
	Q_OBJECT
public:
	ComponentBrowser(QWidget * parent = nullptr);
	
	void setDoneCallback(std::function<void(ComponentUIFactory *)> cb) { _doneCallback = cb; }
	
protected:
	std::function<void(ComponentUIFactory * factory)> _doneCallback;
	QListWidget * _list;
	QPushButton * _okButton;
	
	void buildComponentList();

public slots:
	void selectPress();
	void cancelPress();
	void componentSelected();
};

}
}

#endif
