#ifndef _BG2E_COMPONENTS_DYNAMICS_UI_HPP_
#define _BG2E_COMPONENTS_DYNAMICS_UI_HPP_

#include <bg2e/components/component_field.hpp>

namespace bg2e {
namespace components {

class DynamicsUI : public ComponentField {
	Q_OBJECT
public:
	DynamicsUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:

	ui::VectorField * _gravity;
};
	
}	
}

#endif
