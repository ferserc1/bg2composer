#ifndef _BG2E_COMPONENTS_LIGHT_UI_HPP_
#define _BG2E_COMPONENTS_LIGHT_UI_HPP_

#include <bg2e/components/component_field.hpp>

#include <vwgl/scene/light.hpp>

namespace bg2e {
namespace components {

class LightUI : public ComponentField {
	Q_OBJECT
public:
	LightUI();

	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:
	ui::ComboBoxField * _type;
	ui::ColorField * _ambient;
	ui::ColorField * _diffuse;
	ui::ColorField * _specular;
	ui::FloatSliderField * _constantAtt;
	ui::FloatSliderField * _linearAtt;
	ui::FloatSliderField * _quadraticAtt;

	ui::FloatSliderField * _spotExponent;
	ui::FloatSliderField * _spotCutoff;

	ui::BooleanField * _castShadows;
	ui::FloatSliderField * _shadowStrenght;
	ui::FloatSliderField * _shadowBias;

	ui::FloatSliderField * _cutoffDistance;

	bool _preventUpdate;
	void applyChanges();
	void applyChanges(vwgl::scene::Light *);

};
	
}	
}

#endif
