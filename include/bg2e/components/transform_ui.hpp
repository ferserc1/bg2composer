#ifndef _BG2E_COMPONENTS_TRANSFORM_UI_HPP_
#define _BG2E_COMPONENTS_TRANSFORM_UI_HPP_

#include <bg2e/components/component_field.hpp>

#include <vwgl/transform_strategy.hpp>
#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/app/command.hpp>
#include <vwgl/manipulation/gizmo_manager.hpp>

namespace bg2e {
namespace components {

class TransformUI : public ComponentField,
		public vwgl::manipulation::IGizmoObserver
{
	Q_OBJECT
public:
	TransformUI();
	
	void selectionChanged(vwgl::manipulation::SelectionHandler & sender);
	void didExecuteCommand(vwgl::app::Command * cmd);
	void didUndo(vwgl::app::Command * cmd);
	void didRedo(vwgl::app::Command * cmd);
	void gizmoMoved(vwgl::manipulation::GizmoManager &);

	void onRemoveComponent(vwgl::manipulation::SelectionHandler &);

protected:
	ui::MenuField * _menu;

	ui::VectorField * _translation;
	ui::VectorField * _rotation;
	ui::VectorField * _scale;

	void setSelection(vwgl::TRSTransformStrategy *);
	void setSelection(const vwgl::Matrix4 &);


	void setTranslate(const vwgl::Vector3 & vector);
	void setRotate(const vwgl::Vector3 & vector);
	void setScale(const vwgl::Vector3 & vector);

	vwgl::TRSTransformStrategy * strategy();
	vwgl::scene::Transform * transform();

	void executeCommand(vwgl::app::Command * cmd);
};
	
}	
}

#endif
