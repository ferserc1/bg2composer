#ifndef _BG2E_COMPONENTS_LIBRARY_INSPECTOR_HPP_
#define _BG2E_COMPONENTS_LIBRARY_INSPECTOR_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/library/group.hpp>
#include <vwgl/gui/layout.hpp>
#include <vwgl/texture_manager.hpp>

#include <functional>

namespace bg2e {
namespace components {
	
class LibraryInspectorComponent : public vwgl::scene::SceneComponent {
public:
	typedef std::function<void(LibraryInspectorComponent*)> LibraryInspectorClosure;
	typedef std::function<void(LibraryInspectorComponent*, vwgl::library::Node*)> LibraryInspectorSelectClosure;

	LibraryInspectorComponent();

	virtual void drawUI(vwgl::flat::Renderer & renderer);

	inline void show() { _visible = true; }
	inline void hide() { _visible = false; }
	inline bool isVisible() const { return _visible; }

	inline void setLibrary(vwgl::library::Group * l) { _library = l; _currentNode = l; }
	inline vwgl::library::Group * getLibrary() { return _library.getPtr(); }
	inline vwgl::library::Group * getCurrentNode() { return _currentNode.getPtr(); }

	inline void setCurrentNode(vwgl::library::Group * node) {
		if (!node) return;
		_currentNode = node;
		if (_onEnterGroup) _onEnterGroup(this);
		vwgl::TextureManager::get()->clearUnused();
	}
	inline void goToRoot() { setCurrentNode(_library.getPtr()); }


	inline void onOpenLibrary(LibraryInspectorClosure c) { _onOpenLibrary = c; }
	inline void onEnterGroup(LibraryInspectorClosure c) { _onEnterGroup = c; }
	inline void onSelectItem(LibraryInspectorSelectClosure c) { _onSelectItem = c; }

protected:
	virtual ~LibraryInspectorComponent();

	bool _visible;
	int _scrollTop;
	bool _closeOnSelect;

	vwgl::ptr<vwgl::library::Group> _library;
	vwgl::ptr<vwgl::library::Group> _currentNode;

	LibraryInspectorClosure _onOpenLibrary;
	LibraryInspectorClosure _onEnterGroup;
	LibraryInspectorSelectClosure _onSelectItem;

	void drawSubview(vwgl::flat::Renderer & renderer, vwgl::gui::Layout & lo);
	void selectItem(vwgl::library::Node * node);
};

}
}

#endif
