
#ifndef _BG2E_EDITOR_SETTINGS_HPP_
#define _BG2E_EDITOR_SETTINGS_HPP_

#include <bg2e/settings_window.hpp>
#include <bg2e/closure_action.hpp>

#include <vwgl/ssao.hpp>
#include <vwgl/ss_raytrace.hpp>
#include <vwgl/shadow_render_pass.hpp>
#include <vwgl/deferred_gbuffer_material.hpp>
#include <vwgl/scene/renderer.hpp>
#include <vwgl/dictionary.hpp>
#include <vwgl/graphics.hpp>

#include <functional>
#include <vector>

namespace bg2e {

class Settings;
class ISettingsChangedObserver {
public:
	virtual void onSettingsChanged(Settings *) = 0;
};

class ISettingsExtension {
public:
	// onLoadSettings(): The dictionary may be null if there is no settings file
	virtual void onLoadSettings(vwgl::Dictionary *) = 0;

	virtual void onSaveSettings(vwgl::Dictionary *) = 0;
};

class Settings : public QObject {
	Q_OBJECT
public:
	typedef std::vector<ISettingsChangedObserver*> ObserverVector;

	Settings();

	inline void registerObserver(ISettingsChangedObserver * observer) { _observers.push_back(observer); }
	inline void registerExtension(ISettingsExtension * ext) { _settingsExtensions.push_back(ext); }

	void init(vwgl::scene::Renderer * highQuality, vwgl::scene::Renderer * lowQuality);

	inline vwgl::scene::Renderer & highQualityRenderer() { return *_highQualityRender; }
	inline vwgl::scene::Renderer & lowQualityRenderer() { return *_lowQualityRenderer; }

	SettingsWindow * settingsWindow();

	vwgl::SSAOMaterial * ssao();
	vwgl::SSRayTraceMaterial * ssrt();
	vwgl::ShadowRenderPassMaterial * shadows();
	vwgl::DeferredGBufferMaterial * gbuffers();

	const vwgl::Size2Di & getShadowQuality() { return _shadowQuality; }
	void setShadowQuality(const vwgl::Size2Di & size);
	void setCanvasColor(const vwgl::Color & col);
	const vwgl::Color & getCanvasColor() const { return _canvasColor; }

	inline bool isAntialiasingEnabled() const { return _highQualityRender->getRenderSettings().isAntialiasingEnabled(); }
	void setAntialiasingEnabled(bool e);
	inline const std::string & getMaterialLibraryPath() const { return _materialLibraryPath; }
	void setMaterialLibraryPath(const std::string & path);

	inline bool isTessellationEnabled() const { return _tessellation; }
	void setTessellationEnabled(bool e);

    inline int getReflectionsQuality() { return ssrt() ? static_cast<int>(ssrt()->getQuality()):vwgl::SSRayTraceMaterial::kQualityLow; }
	void setReflectionsQuality(int q);

	void saveSettings();
	void loadSettings();

    inline void setApi(vwgl::Graphics::Api api) { _api = api; }
    vwgl::Graphics::Api getApi();

	inline void notifySettingsChanged() {
		eachObserver([&](ISettingsChangedObserver * observer) { observer->onSettingsChanged(this); });
	}

protected:
	SettingsWindow * _settingsWindow;
	vwgl::Size2Di _shadowQuality;
	vwgl::Color _canvasColor;
	ObserverVector _observers;
	vwgl::scene::Renderer * _highQualityRender;
	vwgl::scene::Renderer * _lowQualityRenderer;
	std::string _materialLibraryPath;
	bool _tessellation;
    vwgl::Graphics::Api _api;

	inline void eachObserver(std::function<void(ISettingsChangedObserver*)> cb) {
		for (ObserverVector::iterator it=_observers.begin(); it!=_observers.end(); ++it) {
			cb(*it);
		}
	}

	std::string getSettingsPath();
	std::vector<ISettingsExtension*> _settingsExtensions;

protected slots:
	void settingsWindowClosed(int);
};

}

#endif
