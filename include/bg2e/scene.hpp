
#ifndef _BG2E_SCENE_HPP_
#define _BG2E_SCENE_HPP_

#include <bg2e/app_delegate.hpp>
#include <bg2e/save_thread.hpp>

#include <vwgl/scene/node.hpp>
#include <vwgl/scene/renderer.hpp>

#include <vector>
#include <functional>

namespace bg2e {

class ISceneObserver {
public:
	virtual void sceneWillLoad() {}
	virtual void sceneDidLoad(vwgl::scene::Node *) {}
	// Return false to cancel scene unload
	virtual bool sceneWillUnload(vwgl::scene::Node *) { return true; }
	virtual void sceneDidUnload() {}
};

typedef std::vector<ISceneObserver*> SceneObserverVector;

class Scene : public vwgl::scene::NodeVisitor {
public:
	void registerObserver(ISceneObserver * o);
	void unregisterObserver(ISceneObserver * o);

	Scene();

	inline void setAppDelegate(AppDelegateBase * appDel) { _appDelegate = appDel; }

	void loadReaders();
	void loadWriters();
	void initGL();

	inline void setSceneChanged() { _sceneChanged = true; }

	void createDefault();
	bool open(const std::string & scenePath);
	bool save();
	bool saveAs(const std::string & scenePath);
	bool exportAs(const std::string & scenePath);

	bool close();

	inline bool isOpened() const { return _sceneRoot.valid(); }
	inline bool canSave() const { return _currentScenePath!=""; }

	inline vwgl::scene::Node * getSceneRoot() { return _sceneRoot.getPtr(); }

	/* NodeVisitor */
	void visit(vwgl::scene::Node * node);

	void prepareSceneNode(vwgl::scene::Node * node);

protected:
	std::string _currentScenePath;

	void prepareMainCamera(vwgl::scene::Node * camera);

	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	bool _sceneChanged;

	SceneObserverVector _observers;

	AppDelegateBase * _appDelegate;

	SaveThread _saveThread;

	inline void eachObserver(std::function<bool (ISceneObserver*)> callback) {
		SceneObserverVector::iterator it;
		for (it=_observers.begin(); it!=_observers.end(); ++it) {
			if (!callback(*it)) {
				break;
			}
		}
	}

	void sendResizeEvent();
};

}

#endif
