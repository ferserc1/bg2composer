
#ifndef _BG2E_VERSION_HPP_
#define _BG2E_VERSION_HPP_

#include <string>

namespace bg2e {

class Version {
public:

	static int major() { return s_major; }
	static int minor() { return s_minor; }
	static int build() { return s_build; }
	static const std::string & getRevision();

	static const std::string & getVersionString();


protected:
	Version();
	~Version();

	static int s_major;
	static int s_minor;
	static int s_build;
	static std::string s_revision;
	static std::string s_versionString;
};

}

#endif
