#ifndef _BG2E_EDITOR_RENDER_VIEW_
#define _BG2E_EDITOR_RENDER_VIEW_

#include <functional>

#include <QtOpengl>
#include <QTimer>
#include <QTimerEvent>

#include <bg2e/window_controller.hpp>

#include <vwgl/vector.hpp>

namespace bg2e {

class RenderView : public QGLWidget {
	Q_OBJECT
public:
	typedef std::function<void(vwgl::plain_ptr)> InitGLFunction;

	explicit RenderView(bg2e::WindowController * windowController, QGLFormat fmt, QWidget * parent = 0);
	virtual ~RenderView();

	inline void sendResize() { makeCurrent(); resizeGL(_lastSize.width(), _lastSize.height()); }

	void updateRenderView(int numFrames = 4) {
		if (_paused) {
			_updateRenderFrames = numFrames;
			context()->makeCurrent();
			updateGL();
		}
	}

	void initializeGL();
	void paintGL();
	void resizeGL(int,int);
	void mousePressEvent(QMouseEvent * event);
	void mouseReleaseEvent(QMouseEvent * event);
	void mouseMoveEvent(QMouseEvent * event);
	void wheelEvent(QWheelEvent * event);

	void keyPressEvent(QKeyEvent * e);
	void keyReleaseEvent(QKeyEvent * e);

	// Controls the animation loop
	inline void play() { _paused = false; _singleStep = false; }
	inline void pause() { _paused = true; _singleStep = false; }
	inline void step() { _paused = false; _singleStep = true; }

	void frame();

	inline void setInitGLFunction(InitGLFunction func) { _initGLFunction = func; }

protected:
	bool _paused;
	bool _singleStep;
	bool _eventRefresh;

	bool _leftButton;
	bool _rightButton;
	bool _middleButton;

	vwgl::Size2Di _lastSize;
	InitGLFunction _initGLFunction;

	int _updateRenderFrames;
private:
	QTimer * _timer;
	uint _lastTime;
	vwgl::ptr<bg2e::WindowController> _windowController;

private slots:
	void animate();

	void setButtonMask(vwgl::app::MouseEvent & event);
};

}

#endif
