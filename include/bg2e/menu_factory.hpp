#ifndef _BG2E_MENU_FACTORY_HPP_
#define _BG2E_MENU_FACTORY_HPP_

#include <QMainWindow>
#include <QMenu>
#include <QAction>

#include <map>

namespace bg2e {

class MenuFactory {
public:
	typedef std::map<QString, QMenu*> MenuMap;

	MenuFactory(QMainWindow * wnd);

	void initMenu(const QString & name);
	void addAction(const QString & menuPath, QAction * action);
	inline void addSeparator(const QString & menuPath) {
		QString fullPath = menuPath + "/:separator";
		addAction(fullPath,nullptr);
	}

protected:
	QMainWindow * _mainWindow;

	MenuMap _menuMap;
};

}

#endif
