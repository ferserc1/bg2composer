
#ifndef _BG2E_TOOL_WINDOW_MANAGER_HPP_
#define _BG2E_TOOL_WINDOW_MANAGER_HPP_

#include <QMainWindow>

#include <bg2e/ui/tool_window.hpp>
#include <map>

namespace bg2e {

class ToolWindowManager {
public:
	ToolWindowManager(QMainWindow * mainWindow);

protected:
	QMainWindow _mainWindow;
};

}

#endif
