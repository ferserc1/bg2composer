#ifndef _BG2E_APP_DELEGATE_HPP_
#define _BG2E_APP_DELEGATE_HPP_

#include <QGLContext>

#include <bg2e/render_view.hpp>


namespace bg2e {

class AppDelegateBase {
public:
	inline QGLContext * getMainGLContext() { return _mainContext; }
	inline RenderView * renderView() { return _renderView; }

protected:
	QGLContext * _mainContext;
	RenderView * _renderView;

	AppDelegateBase() :_mainContext(nullptr), _renderView(nullptr) {}

};

}

#endif
