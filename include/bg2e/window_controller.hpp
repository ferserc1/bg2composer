#ifndef _BG2E_WINDOW_CONTROLLER_HPP_
#define _BG2E_WINDOW_CONTROLLER_HPP_

#include <vwgl/app/window_controller.hpp>
#include <vwgl/app/keyboard_event.hpp>
#include <vwgl/app/mouse_event.hpp>

namespace bg2e {
	
class WindowController : public vwgl::app::WindowController {
public:
	WindowController() {}
	
	virtual void initGL() {}
	virtual void reshape(int, int) {}
	virtual void draw() {}
	virtual void frame(float) {}
	virtual void destroy() {}
	virtual void keyUp(const vwgl::app::KeyboardEvent &) {}
	virtual void keyDown(const vwgl::app::KeyboardEvent &) {}
	virtual void mouseDown(const vwgl::app::MouseEvent &) {}
	virtual void mouseDrag(const vwgl::app::MouseEvent &) {}
	virtual void mouseMove(const vwgl::app::MouseEvent &) {}
	virtual void mouseUp(const vwgl::app::MouseEvent &) {}
	virtual void mouseWheel(const vwgl::app::MouseEvent &) {}
		
protected:
	virtual ~WindowController() {}
};

}

#endif
