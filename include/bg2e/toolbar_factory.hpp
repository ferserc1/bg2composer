
#ifndef _BG2E_TOOLBAR_FACTORY_HPP_
#define _BG2E_TOOLBAR_FACTORY_HPP_

#include <QToolBar>
#include <QMainWindow>

#include <map>
#include <functional>

namespace bg2e {

class ToolBarFactory {
public:
	virtual void build(QMainWindow * window) = 0;

};

}

#endif
