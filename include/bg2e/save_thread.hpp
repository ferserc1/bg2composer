
#ifndef _BG2E_SAVE_THREAD_HPP_
#define _BG2E_SAVE_THREAD_HPP_

#include <QThread>
#include <QObject>
#include <QDialog>
#include <QEvent>

#include <functional>

namespace bg2e {
	
class PostErrorWrapper : public QObject {
public:
	PostErrorWrapper(const QString & message) :_message(message) {}

	virtual bool event(QEvent * event);

protected:
	QString _message;
};

class ClosureWrapper : public QObject {
	Q_OBJECT

public:
	ClosureWrapper() :_callback(nullptr) {}

	inline void setCallback(std::function<void()> cb) { _callback = cb; }

	virtual bool event(QEvent * event) {
		if (event->type()==QEvent::User && _callback) {
			_callback();
			return true;
		}
		return false;
	}

protected:
	std::function<void()> _callback;
};

class SaveThread : public QThread {
    Q_OBJECT
public:
	SaveThread() :_threadCallback(nullptr), _progressDialog(nullptr) {}

	void showProgress();
	void hideProgress();

	inline void setThreadCallback(std::function<void()> cb) { _threadCallback = cb; }
	void setDoneCallback(std::function<void()> cb);
	void setErrorCallback(std::function<void()> cb);

	void postErrorMessage(const QString & message);

	inline void setSaveStatus(bool b) { _saveStatus = b; }

protected:
    virtual void run();

	std::function<void()> _threadCallback;
	ClosureWrapper _done;
	ClosureWrapper _error;
	QDialog * _progressDialog;
	bool _saveStatus;
};

}

#endif
