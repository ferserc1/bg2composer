#ifndef _BG2E_UI_BOOLEAN_FIELD_HPP_
#define _BG2E_UI_BOOLEAN_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <QCheckBox>

#include <functional>

namespace bg2e {
namespace ui {

class BooleanField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (BooleanField*)> BooleanFieldClosure;

	BooleanField();
	BooleanField(const QString & label);

	inline void setValue(bool v) { _checkBox->blockSignals(true); _checkBox->setChecked(v); _checkBox->blockSignals(false); }
	inline bool value() const { return _checkBox->checkState()==Qt::Checked; }

	inline void setCheckLabel(const QString & s) { _checkBox->setText(s); }
	inline QString checkLabel() const { return _checkBox->text(); }

	inline void onValueChanged(BooleanFieldClosure closure) { _onValueChanged = closure; }

protected:
	void build();

	QCheckBox * _checkBox;

	BooleanFieldClosure _onValueChanged;

protected slots:
	void checkStateChanged(int);

signals:
	void valueChanged(BooleanField*);
};

}
}

#endif

