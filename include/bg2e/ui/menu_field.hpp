#ifndef _BG2E_UI_MENU_FIELD_HPP_
#define _BG2E_UI_MENU_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <QPushButton>
#include <QMenu>

namespace bg2e {
namespace ui {

class MenuField : public EditField {
	Q_OBJECT
public:
	MenuField(const QString & title);

	inline void setTitle(const QString & title) { _button->setText(title); }
	inline QString title() const { return _button->text(); }

	void addAction(QAction *action);
	void addSeparator();

protected:
	void build();

	QPushButton * _button;
	QMenu * _menu;
};

}
}

#endif
