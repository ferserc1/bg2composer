#ifndef _BG2E_UI_SLIDER_FIELD_HPP_
#define _BG2E_UI_SLIDER_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <QSlider>
#include <QLabel>

#include <functional>

namespace bg2e {
namespace ui {

class SliderField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (SliderField*)> SliderFieldClosure;

	SliderField();
	SliderField(const QString & label);

	inline void setRange(int min, int max) { _slider->setRange(min,max); }
	inline void setValue(int value) {
		_slider->blockSignals(true);
		_slider->setValue(value);
		updateLabel();
		_slider->blockSignals(false);
	}
	inline int value() { return _slider->value(); }

	inline void onSliderChanged(SliderFieldClosure closure) { _onSliderChanged = closure; }
	inline void onSliderEnd(SliderFieldClosure closure) {}

protected:
	void build();
	void updateLabel();

	QSlider * _slider;
	QLabel * _label;

	SliderFieldClosure _onSliderChanged;
	SliderFieldClosure _onSliderEnd;

protected slots:
	void sliderMoved(int);
	void sliderChanged();

signals:
	void sliderValueChanged(SliderField *);
};

class FloatSliderField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (FloatSliderField *)> FloatSliderFieldClosure;

	FloatSliderField();
	FloatSliderField(const QString & label);

	inline void setRange(float min, float max) { _slider->setRange(static_cast<int>(min * s_valueScale),
																   static_cast<int>(max * s_valueScale)); }
	inline void setValue(float value) {
		_slider->blockSignals(true);
		_slider->setValue(static_cast<int>(value * s_valueScale));
		updateLabel();
		_slider->blockSignals(false);
	}
	inline float value() { return static_cast<float>(_slider->value()) / s_valueScale; }

	inline void onSliderChanged(FloatSliderFieldClosure closure) { _onSliderChanged = closure; }
	inline void onSliderEnd(FloatSliderFieldClosure closure) { _onSliderEnd = closure; }

protected:
	void build();
	void updateLabel();

	QSlider * _slider;
	QLabel * _label;

	FloatSliderFieldClosure _onSliderChanged;
	FloatSliderFieldClosure _onSliderEnd;

	static float s_valueScale;

protected slots:
	void sliderMoved(int);
	void sliderChanged();

signals:
	void sliderValueChanged(FloatSliderField *);
};

}
}

#endif

