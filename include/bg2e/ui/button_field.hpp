#ifndef _BG2E_UI_BUTTON_FIELD_HPP_
#define _BG2E_UI_BUTTON_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <QPushButton>

#include <functional>

namespace bg2e {
namespace ui {

class ButtonField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void ()> ButtonPressClosure;

	ButtonField(const QString & title);

	inline void setTitle(const QString & title) { _button->setText(title); }
	inline QString title() const { return _button->text(); }

	inline void onButtonClicked(ButtonPressClosure closure) { _onButtonPress = closure; }

protected:
	QPushButton * _button;

	void build();

	ButtonPressClosure _onButtonPress;

public slots:
	void btnClicked();

signals:
	void clicked();
};

}
}

#endif
