
#ifndef _BG2E_UI_FONT_FIELD_HPP_
#define _BG2E_UI_FONT_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>
#include <bg2e/ui/combo_box_field.hpp>
#include <bg2e/ui/file_field.hpp>
#include <bg2e/ui/text_field.hpp>

#include <QLineEdit>
#include <QPushButton>
#include <QDialog>
#include <QListWidget>

#include <vwgl/text_font.hpp>

#include <functional>

namespace bg2e {
namespace ui {

class CreateFontWidget : public QDialog {
	Q_OBJECT
public:
	typedef std::function<void(vwgl::TextFont*)> CreateFontClosure;

	CreateFontWidget(QWidget * parent = nullptr);

	inline void onCreateFont(CreateFontClosure cb) {
		_createFontClosure = cb;
	}

	inline void onCancel(CreateFontClosure cb) {
		_cancelClosure = cb;
	}



protected:
	QPushButton * _acceptButton;
	QPushButton * _cancelButton;
	ui::ComboBoxField * _fontType;
	ui::FileField * _customFont;
	ui::TextField * _size;
	ui::TextField * _resolution;

	CreateFontClosure _createFontClosure;
	CreateFontClosure _cancelClosure;

public slots:
	void acceptClicked();
	void cancelClicked();
};

class FontList : public QDialog {
	Q_OBJECT
public:
	typedef std::function<void(const std::string & fontName, vwgl::TextFont*)> FontSelectClosure;

	static FontList * get();

	vwgl::TextFont * getDefaultFont();

	inline void onFontSelected(FontSelectClosure cb) {
		_onFontSelected = cb;
		if (_onFontSelected) {
			_acceptButton->show();
		}
		else {
			_acceptButton->hide();
		}
	}

	virtual void show();

	void reloadFonts();

protected:
	FontList();
	virtual ~FontList();

	static FontList * s_singleton;

	QListWidget * _list;
	QPushButton * _deleteUnused;
	QPushButton * _addButton;
	QPushButton * _acceptButton;
	QPushButton * _closeButton;

	FontSelectClosure _onFontSelected;
	CreateFontWidget * _createFont;

	vwgl::TextFont * selectedFont();

public slots:
	void deleteUnused();
	void addClicked();
	void closeClicked();
	void selectClicked();
	void fontSelectionChanged(int);
};

class FontField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (FontField*)> FontChangedClosure;

	FontField();
	FontField(const QString & label);

	inline void onFontChanged(FontChangedClosure closure) { _onFontChanged = closure; }

	void setFont(vwgl::TextFont * font);
	void setFontName(const std::string & name);
	inline vwgl::TextFont * getFont() { return _font.getPtr(); }
	inline std::string getFontName() { return _fontName->text().toStdString(); }

protected:
	void build();

	QLabel * _fontName;
	QPushButton * _btn;

protected slots:
	void pickButtonClick();

signals:
	void fontChanged(FontField *);

private:
	FontChangedClosure _onFontChanged;
	vwgl::ptr<vwgl::TextFont> _font;
};

}
}

#endif
