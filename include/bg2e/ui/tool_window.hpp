#ifndef _BG2E_UI_TOOL_WINDOW_HPP_
#define _BG2E_UI_TOOL_WINDOW_HPP_

#include <QDockWidget>
#include <QFrame>
#include <QLayout>
#include <QScrollArea>
#include <QVBoxLayout>

#include <bg2e/ui/edit_field.hpp>

namespace bg2e {
namespace ui {

class ToolWindow : public QDockWidget {
	Q_OBJECT
public:
	ToolWindow(const QString & title);

	void addField(EditField * field);

	void addWidget(QWidget * widget);

	void addTitle(const QString & text);
	void addTitle(QLabel * label);

	void clear();

	void addSeparator();

	void endWidget();

	inline void hidePanel() { _centralWidget->hide(); }
	inline void showPanel() { _centralWidget->show(); }
	inline void disablePanel() { _centralWidget->setEnabled(false); }
	inline void enablePanel() { _centralWidget->setEnabled(true); }

protected:
	QWidget * _centralWidget;
	QScrollArea * _scroll;
	QVBoxLayout * _centralWidgetLayout;
};

}
}

#endif
