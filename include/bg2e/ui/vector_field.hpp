
#ifndef _BG2E_UI_VECTOR_FIELD_HPP_
#define _BG2E_UI_VECTOR_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <vwgl/vector.hpp>

#include <functional>

#include <QLineEdit>
#include <QLabel>
#include <QWheelEvent>
#include <QKeyEvent>

#include <bg2e/ui/numeric_text_widget.hpp>

namespace bg2e {
namespace ui {

class VectorLabels : public EditField {
public:
	VectorLabels();
	VectorLabels(const QString & label);


	inline void setLabels(const QString & x) { _x->setText(x); setComponents(1); }
	inline void setLabels(const QString & x, const QString & y) { _x->setText(x); _y->setText(y);  setComponents(2); }
	inline void setLabels(const QString & x, const QString & y, const QString & z) { _x->setText(x); _y->setText(y); _z->setText(z);  setComponents(3); }
	inline void setLabels(const QString & x, const QString & y, const QString & z, const QString & w) { _x->setText(x); _y->setText(y); _z->setText(z); _w->setText(w);  setComponents(4); }

	void setComponents(int c);

protected:
	QLabel * _x;
	QLabel * _y;
	QLabel * _z;
	QLabel * _w;

	int _components;

	void build();
};

class VectorField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (VectorField*)> VectorChangedClosure;
	enum Type {
		kVector2,
		kVector3,
		kVector4
	};

	VectorField();
	VectorField(const QString & label);

	inline void setComponentLabel(int component, const QString & label) { if (component>=0 && component<4) _labels[component]->setText(label); }
	inline QString componentLabel(int component) const { return (component>=0 && component<4) ? _labels[component]->text():""; }

	inline void setVector(const vwgl::Vector2 & v) { _vector2 = v; _type = kVector2; configureField(); }
	inline void setVector(const vwgl::Vector3 & v) { _vector3 = v; _type = kVector3; configureField(); }
	inline void setVector(const vwgl::Vector4 & v) { _vector4 = v; _type = kVector4; configureField(); }

	inline const vwgl::Vector2 & getVector2() const { return _vector2; }
	inline const vwgl::Vector3 & getVector3() const { return _vector3; }
	inline const vwgl::Vector4 & getVector4() const { return _vector4; }

	void setKeyIncrement(float i);
	void setWheelIncrement(float i);


	inline void onVectorChanged(VectorChangedClosure closure) { _onVectorChanged = closure; }


protected:
	void configureField();
	void build();
	void disableSignals();
	void enableSignals();
	void sendSignals();

	vwgl::Vector2 _vector2;
	vwgl::Vector3 _vector3;
	vwgl::Vector4 _vector4;

	NumericTextWidget * _fields[4];
	QLabel * _labels[4];

private:
	Type _type;
	VectorChangedClosure _onVectorChanged;
	bool _preventUpdate;

public slots:
	void xChanged(const QString &);
	void yChanged(const QString &);
	void zChanged(const QString &);
	void wChanged(const QString &);

signals:
	void vectorChanged(VectorField *);
};

}
}

#endif
