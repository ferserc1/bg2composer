
#ifndef _BG2E_UI_SCENE_TREE_MODEL_HPP_
#define _BG2E_UI_SCENE_TREE_MODEL_HPP_

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#include <vwgl/scene/node.hpp>

namespace bg2e {
namespace ui {

class SceneTreeModel : public QAbstractItemModel {
	Q_OBJECT
public:
	explicit SceneTreeModel(vwgl::scene::Node * root, QObject * parent=nullptr);
	~SceneTreeModel();

	void refresh();

	QVariant data(const QModelIndex & index, int role) const Q_DECL_OVERRIDE;
	Qt::ItemFlags flags(const QModelIndex & index) const Q_DECL_OVERRIDE;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
	QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QModelIndex parent(const QModelIndex & index) const Q_DECL_OVERRIDE;
	int rowCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int columnCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
	Qt::DropActions supportedDragActions() const Q_DECL_OVERRIDE;

private:
	vwgl::scene::Node * _sceneRoot;

	static int getNodeRow(vwgl::scene::Node * item);

	QList<const QModelIndex *> _expandedNodes;

signals:
	void dropTarget(vwgl::scene::Node * target);
};

}
}

#endif
