#ifndef _BG2E_UI_COMBO_BOX_FIELD_HPP_
#define _BG2E_UI_COMBO_BOX_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <QComboBox>

#include <functional>

namespace bg2e {
namespace ui {

class ComboBoxField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (ComboBoxField*)> ComboBoxFieldClosure;

	ComboBoxField();
	ComboBoxField(const QString & label);

	void addItem(const QString & item);
	void setCurrentIndex(int index) {
		_comboBox->blockSignals(true);
		_comboBox->setCurrentIndex(index);
		_comboBox->blockSignals(false);
	}
	void clear() {
		_comboBox->blockSignals(true);
		_comboBox->clear();
		_comboBox->blockSignals(false);
	}

	inline int currentIndex() const { return _comboBox->currentIndex(); }
	inline QString currentText() const { return _comboBox->currentText(); }

	inline void onComboBoxChanged(ComboBoxFieldClosure closure) { _onComboBoxChanged = closure; }

protected:
	void build();

	QComboBox * _comboBox;

protected slots:
	void indexChanged(int);

signals:
	void comboBoxChanged(ComboBoxField*);

private:
	ComboBoxFieldClosure _onComboBoxChanged;
};

}
}

#endif

