
#ifndef _BG2E_UI_FILE_FIELD_HPP_
#define _BG2E_UI_FILE_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <QLineEdit>
#include <QPushButton>

#include <functional>

namespace bg2e {
namespace ui {

class FileField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (FileField*)> FileChangedClosure;

	static const QString & getDefaultPath() { return s_defaultPath; }
	static void setDefaultPath(const QString & defaultPath) { s_defaultPath = defaultPath; }

    static QString openFileDialog(const QString & title, const QString & filter);
    static QString saveFileDialog(const QString & title, const QString & filter);
    static QString openDirectoryDialog(const QString & title);

    static std::string stdStringPath(const QString & path);

	FileField();
	FileField(const QString & label);
	
	inline void setDialogMessage(const QString & msg) { _dialogMessage = msg; }
	inline void setFilter(const QString & filter) { _filter = filter; }
	inline const QString & dialogMessage() const { return _dialogMessage; }
	inline const QString & filter() const { return _filter; }
	inline void setFolderPickerMode(bool mode) { _pickFolderMode = mode; }
	inline bool isFolderPicker() const { return _pickFolderMode; }

	inline void setButtonText(const QString & text) { _btn->setText(text); }
	inline QString buttonText() const { return _btn->text(); }

	inline void setPath(const QString & path) {
		_lineEdit->blockSignals(true);
		_lineEdit->setText(path);
		_lineEdit->blockSignals(false);
	}
	inline QString path() const { return _lineEdit->text(); }
    inline std::string getPath() const { return ui::FileField::stdStringPath(_lineEdit->text()); }

	inline void onFileChanged(FileChangedClosure closure) { _onFileChanged = closure; }

protected:
	void build();

	QLineEdit * _lineEdit;
	QPushButton * _btn;
	QString _dialogMessage;
	QString _filter;
	bool _pickFolderMode;

	static QString s_defaultPath;
protected slots:
	void pickButtonClick();
	void textChanged(const QString &);

signals:
	void fileChanged(FileField *);

private:
	FileChangedClosure _onFileChanged;
};

}
}

#endif
