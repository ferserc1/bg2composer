#ifndef _BG2E_UI_NUMERIC_TEXT_WIDGET_HPP_
#define _BG2E_UI_NUMERIC_TEXT_WIDGET_HPP_

#include <QLineEdit>
#include <QDoubleValidator>
#include <QIntValidator>

namespace bg2e {
namespace ui {

class NumericTextWidget : public QLineEdit {
	Q_OBJECT
public:
	enum Type {
		kInteger,
		kDouble
	};

	NumericTextWidget();

	inline void setValue(int val) { _lastValueOk = QString::number(val); setText(_lastValueOk); }
	inline void setValue(float val) { _lastValueOk = QString::number(val); setText(_lastValueOk); }

	inline void setType(Type t) {
		_type = t;
	}

	inline void setKeyIncrement(int i) { _keyIncrement = static_cast<float>(i); }
	inline void setWheelIncrement(int i) { _wheelIncrement = static_cast<float>(i); }
	inline void setKeyIncrement(float i) { _keyIncrement = i; }
	inline void setWheelIncrement(float i) { _wheelIncrement = i; }
	inline float keyIncrement() const { return _keyIncrement; }
	inline float wheelIncrement() const { return _wheelIncrement; }

	inline void setMinValue(int min) { _minFlt = static_cast<float>(min); }
	inline void setMaxValue(int max) { _maxFlt = static_cast<float>(max); }
	inline void setMinValue(float min) { _minFlt = min; }
	inline void setMaxValue(float max) { _maxFlt = max; }
	inline void setDecimals(int d) { _decimals = d; }

	inline float getMinValue() { return _minFlt; }
	inline float getMaxValue() { return _maxFlt; }
	inline int getDecimals() { return _decimals; }

	void keyPressEvent(QKeyEvent * evt);
	void changeEvent(QEvent * evt);

protected:
	Type _type;
	float _keyIncrement;
	float _wheelIncrement;
	QString _lastValueOk;

	float _minFlt;
	float _maxFlt;
	int _decimals;

	bool check();
};

}
}


#endif
