
#ifndef _BG2E_UI_COLOR_FIELD_HPP_
#define _BG2E_UI_COLOR_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>
#include <bg2e/ui/numeric_text_widget.hpp>

#include <QLineEdit>
#include <QPushButton>
#include <QColorDialog>

#include <functional>

#include <vwgl/vector.hpp>

namespace bg2e {
namespace ui {

class ColorField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (ColorField*)> ColorChangedClosure;

	ColorField();
	ColorField(const QString & label);

	inline void setColor(const QColor & color) { setColor(color.red(), color.green(), color.blue(), color.alpha()); }
	inline void setColor(const vwgl::Color & color) { setColor(color.r(), color.g(), color.b(), color.a()); }
	inline void setColor(float r, float g, float b, float a) {
		setColor(static_cast<int>(r * 255.0f),
				 static_cast<int>(g * 255.0f),
				 static_cast<int>(b * 255.0f),
				 static_cast<int>(a * 255.0f));
	}
	inline void setColor(int r, int g, int b, int a) {
		_rValue = r; _gValue = g; _bValue = b; _aValue = a;
		disableSignals();
		updateTextFieldValues();
		updateButtonBackground();
		enableSignals();
	}

	inline void onColorChanged(ColorChangedClosure closure) { _onColorChanged = closure; }

	inline vwgl::Color getEngineColor() const { return vwgl::Color(fRed(), fGreen(), fBlue(), fAlpha()); }
	inline QColor getUIColor() const { return QColor(_rValue, _gValue, _bValue, _aValue); }
	inline int red() const { return _rValue; }
	inline int green() const { return _gValue; }
	inline int blue() const { return _bValue; }
	inline int alpha() const { return _aValue; }
	inline float fRed() const { return static_cast<float>(_rValue) / 255.0f; }
	inline float fGreen() const { return static_cast<float>(_gValue) / 255.0f; }
	inline float fBlue() const { return static_cast<float>(_bValue) / 255.0f; }
	inline float fAlpha() const { return static_cast<float>(_aValue) / 255.0f; }


protected:
	void build();

	int _rValue;
	int _gValue;
	int _bValue;
	int _aValue;

	NumericTextWidget * _rField;
	NumericTextWidget * _gField;
	NumericTextWidget * _bField;
	NumericTextWidget * _aField;

	QPushButton * _selectorButton;

	QColorDialog * _colorDialog;

	ColorChangedClosure _onColorChanged;

	NumericTextWidget * createField(const QString & label);

	void updateButtonBackground();
	void updateTextFieldValues();
	int parseColorValue(const QString & val, int prevValue, QLineEdit * field);
	void disableSignals();
	void enableSignals();

	void callColorChanged();

protected slots:
	void rChanged(const QString &);
	void gChanged(const QString &);
	void bChanged(const QString &);
	void aChanged(const QString &);
	void buttonPress();
	void currentColorChanged(const QColor & color);
	void colorDialogClosed(int);

signals:
	void colorChanged(ColorField *);

};

}
}

#endif
