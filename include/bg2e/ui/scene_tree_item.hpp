
#ifndef _BG2E_UI_SCENE_TREE_ITEM_HPP_
#define _BG2E_UI_SCENE_TREE_ITEM_HPP_

#include <QModelIndex>

#include <vwgl/scene/component.hpp>

namespace bg2e {
namespace ui {

class SceneTreeItem : public vwgl::scene::Component {
public:
	SceneTreeItem() :_isExpanded(false) {}

	inline void setExpanded(bool expanded) { _isExpanded = expanded; }
	inline bool isExpanded() const { return _isExpanded; }
	inline void setModelIndex(const QModelIndex & index) { _modelIndex = index; }
	inline const QModelIndex & modelIndex() const { return _modelIndex; }

protected:
	virtual ~SceneTreeItem() {}

	QModelIndex _modelIndex;
	bool _isExpanded;
};

}
}

#endif
