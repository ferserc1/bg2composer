
#ifndef _BG2E_UI_TREE_VIEW_HPP_
#define _BG2E_UI_TREE_VIEW_HPP_

#include <QWidget>

#include <QLabel>
#include <QPushButton>
#include <QTreeView>

#include <bg2e/ui/scene_tree_item.hpp>


namespace bg2e {
namespace ui {


class TreeView : public QTreeView {
	Q_OBJECT
public:
	TreeView();

	void mousePressEvent(QMouseEvent * event);
	void mouseReleaseEvent(QMouseEvent * event);
	void dropEvent(QDropEvent * event);

protected:
	QModelIndex _sourceIndex;
	QModelIndex _destinationIndex;

signals:
	void moveNode(const QModelIndex & source, const QModelIndex & destination);

};

}
}

#endif
