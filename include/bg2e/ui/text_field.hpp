#ifndef _BG2E_UI_TEXT_FIELD_HPP_
#define _BG2E_UI_TEXT_FIELD_HPP_

#include <bg2e/ui/edit_field.hpp>

#include <QLineEdit>
#include <QValidator>

#include <functional>

namespace bg2e {
namespace ui {

class TextField : public EditField {
	Q_OBJECT
public:
	typedef std::function<void (TextField*)> TextChangedClosure;

	TextField();
	TextField(const QString & label);

	inline void setValidator(QValidator * validator) { _lineEdit->setValidator(validator); }
	inline const QValidator * validator() const { return _lineEdit->validator(); }

	inline void setText(const QString & text) {
		_lineEdit->blockSignals(true);
		_lineEdit->setText(text);
		_lineEdit->blockSignals(false);
	}
	inline QString text() const { return _lineEdit->text(); }
	inline std::string getText() const { return _lineEdit->text().toStdString(); }

	inline void onTextChanged(TextChangedClosure closure) { _onTextChanged = closure; }
	inline void onEditFinished(TextChangedClosure closure) { _onEditFinished = closure; }

	virtual void blockSignals(bool b) { _lineEdit->blockSignals(b); }

protected:
	QLineEdit * _lineEdit;
	
	void build();

public slots:
	void editTextChanged(const QString &);
	void editTextFinished();

signals:
	void textChanged(ui::TextField *);
	void editFinished(ui::TextField *);

private:
	TextChangedClosure _onTextChanged;
	TextChangedClosure _onEditFinished;
};

}
}

#endif
