#ifndef _BG2E_UI_EDIT_FIELD_HPP_
#define _BG2E_UI_EDIT_FIELD_HPP_

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>

namespace bg2e {
namespace ui {

class EditField : public QWidget {
	Q_OBJECT
public:
	EditField();
	
	inline QString label() const { return _label->text(); }
	inline void setLabel(const QString &text) { _label->setText(text); }

	inline QHBoxLayout * hboxLayout() { return dynamic_cast<QHBoxLayout*>(layout()); }

protected:
	QLabel * _label;
};

}
}

#endif
