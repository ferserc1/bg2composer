
#ifndef _BG2E_UI_ABOUT_PANEL_HPP_
#define _BG2E_UI_ABOUT_PANEL_HPP_

#include <QDialog>

namespace bg2e {
namespace ui {


class AboutPanel : public QDialog {
	Q_OBJECT;
public:
	AboutPanel(const QString & title, const QString & header, const QString & body, const QString & version, const QString & icon);

	void mousePressEvent(QMouseEvent *);

public slots:
	void logoClicked();
};
}
}

#endif
