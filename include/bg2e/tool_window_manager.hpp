
#ifndef _BG2E_TOOL_WINDOW_MANAGER_HPP_
#define _BG2E_TOOL_WINDOW_MANAGER_HPP_

#include <QMainWindow>
#include <QDockWidget>

#include <bg2e/ui/tool_window.hpp>

#include <map>
#include <functional>

namespace bg2e {

class ToolWindowManager {
public:
	enum DockSide {
		kDockLeft = Qt::LeftDockWidgetArea,
		kDockRight = Qt::RightDockWidgetArea,
		kDockBottom = Qt::BottomDockWidgetArea
	};

	typedef std::map<QString,ui::ToolWindow*> ToolWindowMap;

	ToolWindowManager(QMainWindow * mainWindow);

	void addToolWindow(const QString & name, DockSide side, ui::ToolWindow * wnd);
	inline ui::ToolWindow * toolWindow(const QString & title) { return _toolWindows[title]; }
	void eachToolWindow(std::function<void (const QString &,ui::ToolWindow*)> closure) {
		ToolWindowMap::iterator it;
		for (it=_toolWindows.begin(); it!=_toolWindows.end(); ++it) {
			closure(it->first, it->second);
		}
	}

	void tabify(ui::ToolWindow * first, ui::ToolWindow * second);

protected:
	QMainWindow * _mainWindow;

	ToolWindowMap _toolWindows;
};

}

#endif
