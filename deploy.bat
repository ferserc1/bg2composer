echo off
:: TODO: Modify this variables according with your system settings
SET qt=C:\Qt\Qt5.6.0\5.6\msvc2015_64
SET qtbin=%qt%\bin
SET qtplug=%qt%\plugins
SET vcredist=C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\redist\x64\Microsoft.VC140.CRT
SET qtout=build-editor-release\release
:: END TODO

SET bg2dir=%cd%
SET outdir=%bg2dir%\..\bg2-composer-deploy
SET build=%bg2dir%\..\%qtout%
SET fbx=%bg2dir%\..\fbx\lib\vs2015\x64\release\libfbxsdk.dll
SET vwgl=%bg2dir%\..\vwgl\build\vwgl\bin
SET bg2e=%bg2dir%\..\bg2engine\build\bin

:: Create output directory if not exists
if not exist "%outdir%" mkdir "%outdir%"
if not exist "%outdir%\plugins" mkdir "%outdir%\plugins"

echo Executable location: %build%

echo on

:: Copy executable Files
copy "%build%\Composer.exe" "%outdir%"

:: Copy dlls
:: fbx sdk
copy "%fbx%" "%outdir%"

:: vwgl
copy "%vwgl%\vwgl.dll" "%outdir%"

:: bg2e
copy "%bg2e%\bg2e.dll" "%outdir%"

:: qt
copy "%qtbin%\icudt53.dll" "%outdir%"
copy "%qtbin%\icuin53.dll" "%outdir%"
copy "%qtbin%\icuuc53.dll" "%outdir%"
copy "%qtbin%\Qt5Core.dll" "%outdir%"
copy "%qtbin%\Qt5Gui.dll" "%outdir%"
copy "%qtbin%\Qt5OpenGL.dll" "%outdir%"
copy "%qtbin%\Qt5Svg.dll" "%outdir%"
copy "%qtbin%\Qt5Widgets.dll" "%outdir%"

:: Visual C++ redistibutable
copy "%vcredist%\concrt140.dll" "%outdir%"
copy "%vcredist%\msvcp140.dll" "%outdir%"
copy "%vcredist%\vccorlib140.dll" "%outdir%"
copy "%vcredist%\vcruntime140.dll" "%outdir%"

:: Qt plugins
if not exist "%outdir%\plugins\imageformats" mkdir "%outdir%\plugins\imageformats"
if not exist "%outdir%\plugins\platforms" mkdir "%outdir%\plugins\platforms"
if not exist "%outdir%\plugins\printsupport" mkdir "%outdir%\plugins\printsupport"
copy "%qtplug%\imageformats\qgif.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qicns.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qico.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qjp2.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qjpeg.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qtga.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qsvg.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qtiff.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\imageformats\qwbmp.dll" "%outdir%\plugins\imageformats"
copy "%qtplug%\platforms\qwindows.dll" "%outdir%\plugins\platforms"
copy "%qtplug%\printsupport\windowsprintersupport.dll" "%outdir%\plugins\printsupport"

:: vwgl shaders and resources
if not exist "%outdir%\shaders" mkdir "%outdir%\shaders"
if not exist "%outdir%\resources" mkdir "%outdir%\resources"
xcopy "%vwgl%\shaders" "%outdir%\shaders" /s /e
xcopy "%vwgl%\resources" "%outdir%\resources" /s /e

::dir "%vsredist%"

explorer "%outdir%"
