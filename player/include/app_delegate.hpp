#ifndef _BG2E_PLAYER_APP_DELEGATE_HPP_
#define _BG2E_PLAYER_APP_DELEGATE_HPP_

#include "window_controller.hpp"
#include "settings.hpp"

#include <vwgl/app/window.hpp>

namespace bg2e {
namespace player {


class AppDelegate {
public:
	static AppDelegate & get() { return s_singleton; }

	void initParams(const std::vector<std::string> & argv);
	int run();

	Settings & settings() { return _settings; }

protected:
	AppDelegate();
	~AppDelegate();

	static AppDelegate s_singleton;

	vwgl::ptr<WindowController> _windowCtrl;
	vwgl::ptr<vwgl::app::Window> _window;
	Settings _settings;
	
	bool loadAppSettings();
	void loadGraphicsSettings();

	vwgl::app::Window * createWindow();
};

}
}

#endif
