#ifndef _BG2E_PLAYER_WINDOW_CONTROLLER_HPP_
#define _BG2E_PLAYER_WINDOW_CONTROLLER_HPP_

#include <vwgl/app/default_window_controller.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/scene_component.hpp>

namespace bg2e {
namespace player {

class WindowController;
class OpenDialog : public vwgl::scene::SceneComponent {
public:
	OpenDialog(WindowController * ctrl) :_winCtrl(ctrl) {}
	
	void drawUI(vwgl::flat::Renderer & renderer);
	
protected:
	virtual ~OpenDialog() {}
	
	WindowController * _winCtrl;
};
	
class WindowController : public vwgl::app::DefaultWindowController {
public:
    WindowController(vwgl::app::DefaultWindowController::Quality q = vwgl::app::DefaultWindowController::kQualityHigh);
	
	void openScene(const std::string & scene);
	
protected:
	virtual vwgl::scene::Node * createScene();
	virtual void initScene(vwgl::scene::Node * sceneRoot);
	virtual void configureRenderer(vwgl::scene::Renderer * renderer);
};

}
}

#endif
