#ifndef _BG2E_PLAYER_SETTINGS_HPP_
#define _BG2E_PLAYER_SETTINGS_HPP_

#include <vwgl/graphics.hpp>
#include <vwgl/app/default_window_controller.hpp>
#include <vwgl/ss_raytrace.hpp>

#include <string>

namespace bg2e {
namespace player {

class Settings {
public:
    Settings(const std::string & settingsFile = "app.json");

	inline std::vector<std::string> & getSettingsFilePaths() { return _settingsFilePaths; }
	inline const std::vector<std::string> & getSettingsFilePaths() const { return _settingsFilePaths; }
	inline void addSettingsFilePath(const std::string & p) { _settingsFilePaths.push_back(p); }

	bool loadAppSettings();
	bool loadGraphicsSettings();
	bool saveGraphicsSettings();

	inline const std::string & getAppName() const { return _appName; }
	inline vwgl::Graphics::Api getRenderApi() const { return _api; }
	inline vwgl::app::DefaultWindowController::Quality getRenderQuality() const { return _renderQuality; }
	inline vwgl::SSRayTraceMaterial::Quality getRayTracerQuality() const { return _ssrtQuality; }
	inline const vwgl::Size2Di & getWindowSize() const { return _windowSize; }
	inline bool isFullScreen() const { return _fullscreen; }
	inline const std::string & getSceneFilePath() const { return _scenePath; }
	inline const std::string & getPluginsPath() const { return _pluginsPath; }
	inline int getRemotePlayerPort() const { return _remotePlayerPort; }

protected:
	std::string _appSettingsFile;

	std::vector<std::string> _settingsFilePaths;

	std::string _appName;
	vwgl::Graphics::Api _api;
	vwgl::app::DefaultWindowController::Quality _renderQuality;
	vwgl::SSRayTraceMaterial::Quality _ssrtQuality;
	vwgl::Size2Di _windowSize;
	bool _fullscreen;
	std::string _scenePath;
	std::string _pluginsPath;
	int _remotePlayerPort;

	void loadDefaultSettingsPath();

	void replacePathVariables(std::string & path);
};

}
}

#endif
