
#include "app_delegate.hpp"

#include <vwgl/graphics.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/dictionary_loader.hpp>
#include <vwgl/app/main_loop.hpp>
#include <vwgl/app/window.hpp>
#include <vwgl/app/message_box.hpp>

namespace bg2e {
namespace player {

using namespace vwgl;

AppDelegate AppDelegate::s_singleton;

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate() {

}

void AppDelegate::initParams(const std::vector<std::string> & argv) {

}

int AppDelegate::run() {
	if (loadAppSettings()) {
		loadGraphicsSettings();

		_window = createWindow();

	}

	if (_window.valid()) {
		app::MainLoop::get()->setWindow(_window.getPtr());
		return app::MainLoop::get()->run();
	}

	return -1;
}

bool AppDelegate::loadAppSettings() {
	if (_settings.loadAppSettings()) {
		vwgl::Graphics::get()->useApi(_settings.getRenderApi());
		return true;
	}
	else {
		app::MessageBox::show(nullptr, "Error loading app config", "Fatal error: No such application configuration file.");
		return false;
	}
}

void AppDelegate::loadGraphicsSettings() {
	_settings.loadGraphicsSettings();
}

vwgl::app::Window * AppDelegate::createWindow() {
	_windowCtrl = new WindowController(settings().getRenderQuality());

	ptr<app::Window> window = vwgl::app::Window::newWindowInstance();
	if (window.valid()) {
		window->setWindowController(_windowCtrl.getPtr());
		window->setSize(settings().getWindowSize().width(), settings().getWindowSize().height());
		window->setPosition(100, 100);
		window->setTitle(settings().getAppName());
		window->setFullScreen(settings().isFullScreen());
		window->create();
	}
	else {
		std::cerr << "Could not create Window class: sorry, it seems that your platform is not supported. " << std::endl;
	}
	return window.release();
}

}
}