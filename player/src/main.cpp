#include "app_delegate.hpp"

int main(int argc, char ** argv) {
	std::vector<std::string> params;
	for (auto i = 0; i < argc; ++i) {
		params.push_back(argv[i]);
	}

	bg2e::player::AppDelegate::get().initParams(params);
	return bg2e::player::AppDelegate::get().run();
}
