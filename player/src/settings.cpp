
#include "settings.hpp"

#include "app_delegate.hpp"

#include <vwgl/system.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/dictionary_loader.hpp>
#include <vwgl/dictionary_writter.hpp>
#include <vwgl/ss_raytrace.hpp>

#include <regex>

namespace bg2e {
namespace player {

using namespace vwgl;

void getValue(ptr<Dictionary> & dic, std::string & val, const std::string & defaultValue = "")  {
	if (dic.valid() && dic->isString()) {
		val = dic->getString();
	}
	else {
		val = defaultValue;
	}
}

void getValue(ptr<Dictionary> & dic, int & val, int defaultValue = 0) {
	if (dic.valid() && dic->isNumber()) {
		val = static_cast<int>(dic->getNumber());
	}
	else {
		val = defaultValue;
	}
}

void getValue(ptr<Dictionary> & dic, bool & val, bool defaultValue = false) {
	if (dic.valid() && dic->isBoolean()) {
		val = dic->getBoolean();
	}
	else {
		val = defaultValue;
	}
}

void getValue(ptr<Dictionary> & dic, float & val, float defaultValue = 0.0f) {
	if (dic.valid() && dic->isNumber()) {
		val = static_cast<float>(dic->getNumber());
	}
	else {
		val = defaultValue;
	}
}

Settings::Settings(const std::string & settingsFile)
	: _appSettingsFile(settingsFile)
	, _appName("bg2e Player")
	, _api(Graphics::kApiOpenGLAdvanced)
	, _renderQuality(app::DefaultWindowController::kQualityHigh)
	, _ssrtQuality(SSRayTraceMaterial::kQualityHigh)
	, _windowSize(1280, 720)
	, _fullscreen(false)
	, _scenePath()
	, _pluginsPath()
	, _remotePlayerPort(2701)
{
	Loader::get()->registerReader(new DictionaryLoader());
	Loader::get()->registerWritter(new DictionaryWritter());
}

bool Settings::loadAppSettings() {
	ptr<Dictionary> settingsDictionary;

	if (_settingsFilePaths.size() == 0) {
		loadDefaultSettingsPath();
	}

	for (std::string path : getSettingsFilePaths()) {
		std::string settingsPath = System::get()->addPathComponent(path, _appSettingsFile);
		replacePathVariables(settingsPath);
		settingsDictionary = Loader::get()->loadDictionary(settingsPath);
		if (settingsDictionary.valid()) {
			break;
		}
	}

	if (settingsDictionary.valid() && settingsDictionary->isObject()) {
		getValue(settingsDictionary->getObject()["appName"], _appName, _appName);
		std::string cfgString;
		std::string platform;
		if (System::get()->isMac()) {
			platform = "osx";
		}
		else if (System::get()->isWindows()) {
			platform = "win";
		}

		Dictionary * apiDict = settingsDictionary->getObject()["api"].getPtr();
		Dictionary * renderQualityDict = settingsDictionary->getObject()["renderQuality"].getPtr();
		Dictionary * windowSettingsDict = settingsDictionary->getObject()["windowSettings"].getPtr();
		Dictionary * scenePath = settingsDictionary->getObject()["scenePath"].getPtr();
		Dictionary * pluginsPath = settingsDictionary->getObject()["pluginsPath"].getPtr();
		Dictionary * remotePlayer = settingsDictionary->getObject()["remotePlayer"].getPtr();
		Dictionary * ssrtDict = settingsDictionary->getObject()["raytracerQuality"].getPtr();

		if (apiDict && apiDict->isObject()) {
			getValue(apiDict->getObject()[platform], cfgString, "kApiOpenGLAdvanced");

			if (cfgString == "kApiOpenGL") {
				_api = Graphics::kApiOpenGL;
			}
			else if (cfgString == "kApiOpenGLBasic") {
				_api = Graphics::kApiOpenGLBasic;
			}
			else if (cfgString == "kApiOpenGLAdvanced") {
				_api = Graphics::kApiOpenGLAdvanced;
			}
			else if (cfgString == "kApiDirectX") {
				_api = Graphics::kApiDirectX;
			}
		}
		
		if (renderQualityDict && renderQualityDict->isString()) {
			std::string quality = renderQualityDict->getString();
			if (quality=="kQualityLow") {
				_renderQuality = app::DefaultWindowController::kQualityLow;
			}
			else if (quality=="kQualityMedium") {
				_renderQuality = app::DefaultWindowController::kQualityMedium;
			}
			else if (quality=="kQualityHigh") {
				_renderQuality = app::DefaultWindowController::kQualityHigh;
			}
			else if (quality=="kQualityExtreme") {
				_renderQuality = app::DefaultWindowController::kQualityExtreme;
			}
		}
		
		if (ssrtDict && ssrtDict->isString()) {
			std::string quality = ssrtDict->getString();
			if (quality=="kQualityLow") {
				_ssrtQuality = SSRayTraceMaterial::kQualityLow;
			}
			else if (quality=="kQualityMedium") {
				_ssrtQuality = SSRayTraceMaterial::kQualityMedium;
			}
			else if (quality=="kQualityHigh") {
				_ssrtQuality = SSRayTraceMaterial::kQualityHigh;
			}
			else if (quality=="kQualityExtreme") {
				_ssrtQuality = SSRayTraceMaterial::kQualityExtreme;
			}
		}

		if (windowSettingsDict && windowSettingsDict->isObject()) {
			int w, h;
			getValue(windowSettingsDict->getObject()["width"], w, 800);
			getValue(windowSettingsDict->getObject()["height"], h, 600);
			getValue(windowSettingsDict->getObject()["fullscreen"], _fullscreen, _fullscreen);
			_windowSize.set(w, h);
		}

 		if (scenePath && scenePath->isObject()) {
			getValue(scenePath->getObject()[platform], _scenePath, "");
			replacePathVariables(_scenePath);
		}

		if (pluginsPath && pluginsPath->isObject()) {
			getValue(pluginsPath->getObject()[platform], _pluginsPath, "");
			replacePathVariables(_pluginsPath);
		}

		if (remotePlayer && remotePlayer->isObject()) {
			getValue(remotePlayer->getObject()["port"], _remotePlayerPort, 2701);
		}

		return true;
	}

	return false;
}

bool Settings::loadGraphicsSettings() {
	return false;
}

bool Settings::saveGraphicsSettings() {
	return false;
}

void Settings::loadDefaultSettingsPath() {
	std::string executablePath = System::get()->getExecutablePath();
	addSettingsFilePath(executablePath);

	if (System::get()->isMac()) {
		executablePath = System::get()->addPathComponent(executablePath, "../../..");
		addSettingsFilePath(executablePath);
	}
}

void Settings::replacePathVariables(std::string & path) {
	// $EXE: Executable path:	Windows: the .exe path
	//							OS X: APPLICATION.app/Contents/MacOS
	std::string exePath = System::get()->getExecutablePath();

	// $APP: Application path:	Windows: the same as executable path
	//							OS X: the .app bundle path
	std::string appPath = exePath;
	if (System::get()->isMac()) {
		appPath = System::get()->addPathComponent(appPath, "../../..");
	}

	// $RES: Resources path:	Windows: a folder named "resources", at the same location as the .exe
	//							OS X: APPICATION:app/Contents/Resources
	std::string resPath = System::get()->getResourcesPath();

	path = std::regex_replace(path, std::regex("\\$EXE"), exePath);
	path = std::regex_replace(path, std::regex("\\$APP"), appPath);
	path = std::regex_replace(path, std::regex("\\$RES"), resPath);

	path = std::regex_replace(path, std::regex("\\/\\/"), "/");
}


}
}
