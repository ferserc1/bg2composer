
#include "window_controller.hpp"

#include "app_delegate.hpp"

#include <vwgl/scene/primitive_factory.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/scene/light.hpp>
#include <vwgl/manipulation/transform_controller.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/app/file_dialog.hpp>
#include <vwgl/app/main_loop.hpp>
#include <vwgl/gui/view.hpp>
#include <vwgl/gui/controls.hpp>

namespace bg2e {
namespace player {

using namespace vwgl;


void OpenDialog::drawUI(vwgl::flat::Renderer & renderer) {
	Viewport vp = State::get()->getViewport();
	int viewWidth = 340;
	int viewHeight = 150;
	int viewLeft = vp.width() / 2 - viewWidth / 2;
	int viewTop = vp.height() / 2 - viewHeight / 2;
	gui::View view(renderer, gui::Layout(viewLeft, viewTop, viewWidth, viewHeight));
	gui::Layout layout(10, 10, -10, 25);
	
	gui::Controls::label("No scene found", layout);
	gui::Controls::button("Open scene", layout.getNextDown(), [&]() {
		ptr<app::FileDialog> openFileDialog = app::FileDialog::openFileDialog();
		openFileDialog->addFilter("vitscn");
		openFileDialog->addFilter("vitscnj");
		if (openFileDialog->show()) {
			_winCtrl->openScene(openFileDialog->getResultPath());
		}
	});
	
	int port = AppDelegate::get().settings().getRemotePlayerPort();
	gui::Controls::label("Remote player listening on port " + std::to_string(port), layout.getNextDown());
	
	gui::Controls::button("Exit", layout.getNextDown(), [&]() {
		app::MainLoop::get()->quit(-1);
	});
}
	
WindowController::WindowController(vwgl::app::DefaultWindowController::Quality q)
    :app::DefaultWindowController(q)
{}
	
void WindowController::openScene(const std::string & scene) {
	ptr<scene::Node> root = Loader::get()->loadScene(scene);
	if (root.valid()) {
		setSceneRoot(root.getPtr());
	}
}
	
vwgl::scene::Node * WindowController::createScene() {
	std::string scenePath = AppDelegate::get().settings().getSceneFilePath();
	ptr<scene::Node> root = Loader::get()->loadScene(scenePath);
	
	if (!root.valid()) {
		root = new vwgl::scene::Node();
		root->addComponent(new OpenDialog(this));
	}
	
	return root.release();
}

void WindowController::initScene(vwgl::scene::Node * sceneRoot) {
	scene::Camera * cam = scene::Camera::getMainCamera();
	if (cam) {
		cam->node()->addComponent(new manipulation::TargetInputController());
	}
}
	
void WindowController::configureRenderer(vwgl::scene::Renderer * renderer) {
	DefaultWindowController::configureRenderer(renderer);
	if (renderer->getFilter<SSRayTraceMaterial>()) {
		renderer->getFilter<SSRayTraceMaterial>()->setQuality(AppDelegate::get().settings().getRayTracerQuality());
	}
}


}
}